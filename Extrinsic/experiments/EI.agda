module Extrinsic.experiments.EI where

open import Extrinsic.Extrinsic

open import lib
open import TT.Decl.Syntax
open import TT.Decl.Congr syntaxDecl
open import TT.Core.Syntax
open import TT.Core.Congr syntaxCore

EIC : {Γ : Con'}(Γw : ⊢C Γ) → Con
EIT : {Γ : Con'}(Γw : ⊢C Γ){A : Ty'}(Aw : Γ ⊢T A) → Ty (EIC Γw)
EIs : {Γ : Con'}(Γw : ⊢C Γ){Δ : Con'}(Δw : ⊢C Δ){σ : Tms'}(σw : Γ ⊢s σ ∶ Δ) → Tms (EIC Γw) (EIC Δw)
EIt : {Γ : Con'}(Γw : ⊢C Γ){A : Ty'}(Aw : Γ ⊢T A){t : Tm'}(tw : Γ ⊢t t ∶ A) → Tm (EIC Γw) (EIT Γw Aw)

EIC' : {Γ₀ Γ₁ : Con'}{Γw₀ : ⊢C Γ₀}{Γw₁ : ⊢C Γ₁}(Γc : ⊢C Γ₀ ~ Γ₁) → EIC Γw₀ ≡ EIC Γw₁

EIT' : {Γ₀ Γ₁ : Con'}{Γw₀ : ⊢C Γ₀}{Γw₁ : ⊢C Γ₁}(Γc : ⊢C Γ₀ ~ Γ₁)
       {A₀ A₁ : Ty'}{Aw₀ : Γ₀ ⊢T A₀}{Aw₁ : Γ₁ ⊢T A₁} (Ac : Γ₀ ⊢T A₀ ~ A₁)
     → EIT Γw₀ Aw₀ ≡[ Ty= (EIC' Γc) ]≡ EIT Γw₁ Aw₁

EIC •w = •
EIC (,w Γw Aw) = EIC Γw , EIT Γw Aw

EIT Γw ([]Tw Γw₁ Δw Aw σw) = EIT Δw Aw [ EIs Γw Δw σw ]T
EIT Γw (coeT {Γ₀}{Γ₁} Γ~ Aw) = coe (Ty= (EIC' {Γ₀}{Γ₁}{~C₀ Γ~}{Γw} Γ~))
                                   (EIT (~C₀ Γ~) Aw) 

EIs Γw Δw (idw Γw₁) = coe (TmsΓ-= (ap EIC (⊢C-prop Γw Δw))) (id {EIC Γw})
EIs Γw Δw (∘w Γw₁ Σw Δw₁ σw νw) = EIs Σw Δw σw ∘ EIs Γw Σw νw
EIs Γw •w (εw Γw₁) = ε
EIs Γw ΔAw (,sw Γw₁ Δw σw Aw tw) = coe (TmsΓ-= (ap EIC (⊢C-prop (,w Δw Aw) ΔAw)))
                                       (EIs Γw Δw σw ,s EIt Γw ([]Tw Γw Δw Aw σw) tw)
EIs Γw Δw (π₁w Γw₁ Δw₁ Aw σw) = π₁ (EIs Γw (,w Δw Aw) σw)
EIs Γw Δw (coes {Γ₀}{Γ₁} Γ~ {Δ₀}{Δ₁} Δ~ σw)
  = coe (Tms= (EIC' {Γ₀}{Γ₁}{~C₀ Γ~}{Γw} Γ~) (EIC' {Δ₀}{Δ₁}{~C₀ Δ~}{Δw} Δ~))
        (EIs (~C₀ Γ~) (~C₀ Δ~) σw)

EIt Γw A[σ]w ([]tw Γw₁ Δw Aw tw σw) = coe (TmΓ= (ap (EIT Γw) (⊢T-prop ([]Tw Γw Δw Aw σw) A[σ]w)))
                                          (EIt Δw Aw tw [ EIs Γw Δw σw ]t)
EIt Γw A[π₁σ]w (π₂w Γw₁ Δw Aw σw) = coe (TmΓ= (ap (EIT Γw) (⊢T-prop ([]Tw Γw Δw Aw (π₁w Γw Δw Aw σw)) A[π₁σ]w)))
                                        (π₂ (EIs Γw (,w Δw Aw) σw))
EIt Γw Aw (coet {Γ₀}{Γ₁} Γ~ {A₀}{A₁} A~ tw)
  = coe (Tm= (EIC' {Γ₀}{Γ₁}{~C₀ Γ~}{Γw} Γ~) (EIT' {Γ₀}{Γ₁}{~C₀ Γ~}{Γw} Γ~ {A₀}{A₁}{~T₀ A~}{Aw} A~))
        (EIt (~C₀ Γ~) (~T₀ A~) tw)

EIC' (Γc ◾C Γc₁) = {!EIC' Γc ◾ EIC' Γc₁!}
EIC' (Γc ⁻¹C) = {!EIC' Γc!}
EIC' •c = {!!}
EIC' (,c Γc A~) = {!!}
EIT' Γc (Ac ◾T Ac₁) = {!!}
EIT' Γc (Ac ⁻¹T) = {!!}
EIT' Γc (coeT~ Γ~ Ac) = {!!}
EIT' Γc ([]Tc Γc₁ Δc Ac σc) = {!!}
EIT' Γc ([id]Tw Γw Aw) = {!!}
EIT' Γc ([][]Tw Γw Δw Σw Aw σw δw) = {!!}
