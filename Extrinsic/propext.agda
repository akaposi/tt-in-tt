module Extrinsic.propext where

open import lib

ishProp : ∀{ℓ} → Set ℓ → Set ℓ
ishProp A = (a b : A) → a ≡ b

postulate
  propext : ∀{ℓ}{A B : Set ℓ}(pA : ishProp A)(pB : ishProp B) → (A → B) × (B → A) → A ≡ B

p⊤ : ishProp ⊤
p⊤ tt tt = refl

pLift⊤ : ∀{ℓ} → ishProp (Lift {_}{ℓ} ⊤)
pLift⊤ (lift tt)(lift tt) = refl

pΣ : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'} → ishProp A → ((x : A) → ishProp (B x)) → ishProp (Σ A B)
pΣ {B = B} pA pB (a ,Σ b)(a' ,Σ b') = ,Σ= (pA a a') (pB a' (coe (ap B (pA a a')) b) b')

p≡ : ∀{ℓ}{A : Set ℓ}{a a' : A} → ishProp (a ≡ a')
p≡ refl refl = refl
