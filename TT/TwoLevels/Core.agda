{-# OPTIONS --without-K --no-eta #-}

module TT.TwoLevels.Core where

open import Agda.Primitive
open import lib
open import TT.TwoLevels.Decl

-- Core substitution calculus

record Core (d : Decl) : Set₂ where
  open Decl d

  field
    •      : Con  -- \bub
    _,₀_   : (Γ : Con) → Ty₀ Γ → Con
    _,₁_   : (Γ : Con) → Ty₁ Γ → Con
    
    _[_]T₀ : ∀{Γ Δ} → Ty₀ Δ → Tms Γ Δ → Ty₀ Γ
    _[_]T₁ : ∀{Γ Δ} → Ty₁ Δ → Tms Γ Δ → Ty₁ Γ
    
    id     : ∀{Γ} → Tms Γ Γ
    _∘_    : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
    ε      : ∀{Γ} → Tms Γ •
    _,s₀_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty₀ Δ} → Tm₀ Γ (A [ σ ]T₀) → Tms Γ (Δ ,₀ A)
    _,s₁_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty₁ Δ} → Tm₁ Γ (A [ σ ]T₁) → Tms Γ (Δ ,₁ A)
    π₁₀    : ∀{Γ Δ}{A : Ty₀ Δ} → Tms Γ (Δ ,₀ A) → Tms Γ Δ
    π₁₁    : ∀{Γ Δ}{A : Ty₁ Δ} → Tms Γ (Δ ,₁ A) → Tms Γ Δ
    
    _[_]t₀ : ∀{Γ Δ}{A : Ty₀ Δ} → Tm₀ Δ A → (σ : Tms Γ Δ) → Tm₀ Γ (A [ σ ]T₀) 
    _[_]t₁ : ∀{Γ Δ}{A : Ty₁ Δ} → Tm₁ Δ A → (σ : Tms Γ Δ) → Tm₁ Γ (A [ σ ]T₁) 
    π₂₀    : ∀{Γ Δ}{A : Ty₀ Δ}(σ : Tms Γ (Δ ,₀ A)) → Tm₀ Γ (A [ π₁₀ σ ]T₀)
    π₂₁    : ∀{Γ Δ}{A : Ty₁ Δ}(σ : Tms Γ (Δ ,₁ A)) → Tm₁ Γ (A [ π₁₁ σ ]T₁)
    
  infixl 5 _,₀_ _,₁_
  infixl 7 _[_]T₀ _[_]T₁
  infixl 5 _,s₀_ _,s₁_
  infix  6 _∘_
  infixl 8 _[_]t₀ _[_]t₁

  field
    [id]T₀ : ∀{Γ}{A : Ty₀ Γ} → A [ id ]T₀ ≡ A
    [id]T₁ : ∀{Γ}{A : Ty₁ Γ} → A [ id ]T₁ ≡ A
    [][]T₀ : ∀{Γ Δ Σ}{A : Ty₀ Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
            → (A [ δ ]T₀ [ σ ]T₀) ≡ (A [ δ ∘ σ ]T₀)
    [][]T₁ : ∀{Γ Δ Σ}{A : Ty₁ Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
            → (A [ δ ]T₁ [ σ ]T₁) ≡ (A [ δ ∘ σ ]T₁)
            
    idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ 
    idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ 
    ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
          → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
    ,∘₀   : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty₀ Δ}{a : Tm₀ Γ (A [ δ ]T₀)}
          → ((δ ,s₀ a) ∘ σ) ≡ ((δ ∘ σ) ,s₀ coe (ap (Tm₀ Σ) [][]T₀) (a [ σ ]t₀))
    ,∘₁   : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty₁ Δ}{a : Tm₁ Γ (A [ δ ]T₁)}
          → ((δ ,s₁ a) ∘ σ) ≡ ((δ ∘ σ) ,s₁ coe (ap (Tm₁ Σ) [][]T₁) (a [ σ ]t₁))

    π₁β₀  : ∀{Γ Δ}{A : Ty₀ Δ}{δ : Tms Γ Δ}{a : Tm₀ Γ (A [ δ ]T₀)}
          → (π₁₀ (δ ,s₀ a)) ≡ δ
    π₁β₁  : ∀{Γ Δ}{A : Ty₁ Δ}{δ : Tms Γ Δ}{a : Tm₁ Γ (A [ δ ]T₁)}
          → (π₁₁ (δ ,s₁ a)) ≡ δ
    πη₀   : ∀{Γ Δ}{A : Ty₀ Δ}{δ : Tms Γ (Δ ,₀ A)} → (π₁₀ δ ,s₀ π₂₀ δ) ≡ δ
    πη₁   : ∀{Γ Δ}{A : Ty₁ Δ}{δ : Tms Γ (Δ ,₁ A)} → (π₁₁ δ ,s₁ π₂₁ δ) ≡ δ
    εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
    
    π₂β₀  : ∀{Γ Δ}{A : Ty₀ Δ}{δ : Tms Γ Δ}{a : Tm₀ Γ (A [ δ ]T₀)}
          → (π₂₀ (δ ,s₀ a)) ≡[ ap (Tm₀ _) (ap (λ z → A [ z ]T₀) π₁β₀) ]≡ a
    π₂β₁  : ∀{Γ Δ}{A : Ty₁ Δ}{δ : Tms Γ Δ}{a : Tm₁ Γ (A [ δ ]T₁)}
          → (π₂₁ (δ ,s₁ a)) ≡[ ap (Tm₁ _) (ap (λ z → A [ z ]T₁) π₁β₁) ]≡ a

  -- abbreviations

  wk₀ : ∀{Γ}{A : Ty₀ Γ} → Tms (Γ ,₀ A) Γ
  wk₀ = π₁₀ id
  wk₁ : ∀{Γ}{A : Ty₁ Γ} → Tms (Γ ,₁ A) Γ
  wk₁ = π₁₁ id

  vz₀ : ∀{Γ}{A : Ty₀ Γ} → Tm₀ (Γ ,₀ A) (A [ wk₀ ]T₀)
  vz₀ = π₂₀ id
  vz₁ : ∀ {Γ}{A : Ty₁ Γ} → Tm₁ (Γ ,₁ A) (A [ wk₁ ]T₁)
  vz₁ = π₂₁ id

  vs₀ : ∀{Γ}{A B : Ty₀ Γ} → Tm₀ Γ A → Tm₀ (Γ ,₀ B) (A [ wk₀ ]T₀)
  vs₀ x = x [ wk₀ ]t₀
  vs₁ : ∀{Γ}{A B : Ty₁ Γ} → Tm₁ Γ A → Tm₁ (Γ ,₁ B) (A [ wk₁ ]T₁)
  vs₁ x = x [ wk₁ ]t₁

  <_>₀ : ∀{Γ}{A : Ty₀ Γ} → Tm₀ Γ A → Tms Γ (Γ ,₀ A)
  < t >₀ = id ,s₀ coe (ap (Tm₀ _) ([id]T₀ ⁻¹)) t
  <_>₁ : ∀{Γ}{A : Ty₁ Γ} → Tm₁ Γ A → Tms Γ (Γ ,₁ A)
  < t >₁ = id ,s₁ coe (ap (Tm₁ _) ([id]T₁ ⁻¹)) t

  infix 4 <_>₀ <_>₁

  _^₀_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty₀ Δ) → Tms (Γ ,₀ A [ σ ]T₀) (Δ ,₀ A)
  _^₀_ = λ { σ A → (σ ∘ π₁₀ id) ,s₀ coe (ap (Tm₀ _) [][]T₀) (π₂₀ id) }
  _^₁_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty₁ Δ) → Tms (Γ ,₁ A [ σ ]T₁) (Δ ,₁ A)
  _^₁_ = λ { σ A → (σ ∘ π₁₁ id) ,s₁ coe (ap (Tm₁ _) [][]T₁) (π₂₁ id) }

  infixl 5 _^₀_ _^₁_

  [_,_]₀ : ∀{A : ∀{Γ} → Ty₀ Γ}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]T₀ ≡ A)
           (t : ∀{Γ} → Tm₀ Γ A → Tm₀ Γ A)
           {Γ : Con} → Tms (Γ ,₀ A) (Γ ,₀ A)
  [_,_]₀ = λ A[] t → wk₀ ,s₀ coe (ap (Tm₀ _) (A[] ⁻¹)) (t (coe (ap (Tm₀ _) A[]) vz₀))
  [_,_]₁ : ∀{A : ∀{Γ} → Ty₁ Γ}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]T₁ ≡ A)
           (t : ∀{Γ} → Tm₁ Γ A → Tm₁ Γ A)
           {Γ : Con} → Tms (Γ ,₁ A) (Γ ,₁ A)
  [_,_]₁ = λ A[] t → wk₁ ,s₁ coe (ap (Tm₁ _) (A[] ⁻¹)) (t (coe (ap (Tm₁ _) A[]) vz₁))
