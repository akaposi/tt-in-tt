{-# OPTIONS --without-K --no-eta #-}

module TT.TwoLevels.Decl where

-- Declaration of a model

record Decl : Set₃ where
  field
    Con : Set₂
    Ty₀ : Con → Set₁
    Ty₁ : Con → Set₂
    Tms : Con → Con → Set₁
    Tm₀ : (Γ : Con) → Ty₀ Γ → Set₁
    Tm₁ : (Γ : Con) → Ty₁ Γ → Set₁
