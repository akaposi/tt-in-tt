{-# OPTIONS --without-K --no-eta #-}

module TT.FuncImpred where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
open import TT.Base
import TT.Decl.Congr.NoJM
import TT.Core.Congr.NoJM
import TT.Core.Laws.NoJM
import TT.Base.Congr.NoJM

-- a universe with Pi

record FuncU {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c) : Set (i ⊔ j) where
  open Decl d
  open Core c
  open Base b
  open TT.Decl.Congr.NoJM d
  open TT.Core.Congr.NoJM c
  open TT.Base.Congr.NoJM b
  open TT.Core.Laws.NoJM c

  field
    Π   : ∀{Γ}(A : Ty Γ)(b : Tm (Γ , A) U) → Tm Γ U
    
    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{b : Tm (Δ , A) U}
        → ((Π A b) [ σ ]t)
        ≡[ TmΓ= U[] ]≡
          (Π (A [ σ ]T) (coe (TmΓ= U[]) (b [ σ ^ A ]t)))

    lam : ∀{Γ}{A : Ty Γ}{b : Tm (Γ , A) U} → Tm (Γ , A) (El b) → Tm Γ (El (Π A b))
    app : ∀{Γ}{A : Ty Γ}{b : Tm (Γ , A) U} → Tm Γ (El (Π A b)) → Tm (Γ , A) (El b)

    lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{b : Tm (Δ , A) U}{t : Tm (Δ , A) (El b)}                       
          → (lam t) [ δ ]t
          ≡[ TmΓ= (El[]'' Π[]) ]≡
            lam (coe (TmΓ= (El[]' refl)) (t [ δ ^ A ]t))

    Πβ    : ∀{Γ}{A : Ty Γ}{b : Tm (Γ , A) U}{t : Tm (Γ , A) (El b)} → app (lam t) ≡ t
    Πη    : ∀{Γ}{A : Ty Γ}{b : Tm (Γ , A) U}{t : Tm Γ (El (Π A b))} → lam (app t) ≡ t

  -- abbreviations

  _$_ : ∀ {Γ}{A : Ty Γ}{b : Tm (Γ , A) U}(t : Tm Γ (El (Π A b)))(u : Tm Γ A) → Tm Γ (El b [ < u > ]T)
  t $ u = (app t) [ < u > ]t

  infixl 5 _$_
