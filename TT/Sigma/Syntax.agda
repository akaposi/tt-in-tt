module TT.Sigma.Syntax where

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Sigma

postulate
  syntaxSigma : Sigma syntaxCore
  
open Sigma syntaxSigma public
