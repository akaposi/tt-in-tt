open import TT.Decl
open import TT.Core
open import TT.Sigma

module TT.Sigma.Congr.JM {i}{j}{d : Decl {i}{j}}{c : Core d}(s : Sigma c) where

open import lib
open import JM

open Decl d
open Core c
open Sigma s
open import TT.Decl.Congr.NoJM d
open import TT.Core.Congr.NoJM c

,Σ'=
  : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
    {B₀ : Ty (Γ₀ , A₀)}{B₁ : Ty (Γ₁ , A₁)}(B₂ : B₀ ≃ B₁)
    {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≃ t₁)
    {u₀ : Tm Γ₀ (B₀ [ < t₀ > ]T)}{u₁ : Tm Γ₁ (B₁ [ < t₁ > ]T)}(u₂ : u₀ ≃ u₁)
  → (_,Σ'_ {B = B₀} t₀ u₀) ≃ (_,Σ'_ {B = B₁} t₁ u₁)
,Σ'= refl (refl ,≃ refl)(refl ,≃ refl)(refl ,≃ refl)(refl ,≃ refl) = refl ,≃ refl
