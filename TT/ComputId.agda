module TT.ComputId where

open import Agda.Primitive
open import lib
open import JM

open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.Props
import TT.Decl.Congr
import TT.Core.Congr hiding (coe[]T)
import TT.Core.Laws

module _ {i j}{d : Decl}(c : Core d)(b : Base {i}{j} c)(p : Props b) where
  open Decl d
  open Core c
  open Base b
  open TT.Decl.Congr d
  open TT.Core.Congr c
  open TT.Core.Laws c
  open Props p -- the universe of propositions is U, El

  record ComputId : Set (i ⊔ j) where
    field
      _~C : (Γ : Con){Ω : Con}(ρ₀ ρ₁ : Tms Ω Γ) → Tm Ω U
      RC : (Γ : Con){Ω : Con}(ρ : Tms Ω Γ) → Tm Ω (El ((Γ ~C) ρ ρ))
      SC : (Γ : Con){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁))) → Tm Ω (El ((Γ ~C) ρ₁ ρ₀))
      TC : (Γ : Con){Ω : Con}{ρ₀ ρ₁ ρ₂ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(ρ₁₂ : Tm Ω (El ((Γ ~C) ρ₁ ρ₂))) →
        Tm Ω (El ((Γ ~C) ρ₀ ρ₂))

      _~T : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
        (t₀ : Tm Ω (A [ ρ₀ ]T))(t₁ : Tm Ω (A [ ρ₁ ]T)) → Tm Ω U
      RT : {Γ : Con}(A : Ty Γ){Ω : Con}(ρ : Tms Ω Γ)(t : Tm Ω (A [ ρ ]T)) → Tm Ω (El ((A ~T) (RC Γ ρ) t t))
      ST : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
        {t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}(t₀₁ : Tm Ω (El ((A ~T) ρ₀₁ t₀ t₁))) →
        Tm Ω (El ((A ~T) (SC Γ ρ₀₁) t₁ t₀))
      TT : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ ρ₂ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(ρ₁₂ : Tm Ω (El ((Γ ~C) ρ₁ ρ₂)))
        {t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}{t₂ : Tm Ω (A [ ρ₂ ]T)}
        (t₀₁ : Tm Ω (El ((A ~T) ρ₀₁ t₀ t₁)))(t₁₂ : Tm Ω (El ((A ~T) ρ₁₂ t₁ t₂))) →
        Tm Ω (El ((A ~T) (TC Γ ρ₀₁ ρ₁₂) t₀ t₂))
      coeT : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(t₀ : Tm Ω (A [ ρ₀ ]T)) →
        Tm Ω (A [ ρ₁ ]T)
      cohT : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(t₀ : Tm Ω (A [ ρ₀ ]T)) →
        Tm Ω (El ((A ~T) ρ₀₁ t₀ (coeT A ρ₀₁ t₀)))

      _~s : {Γ Δ : Con}(δ : Tms Γ Δ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁))) →
        Tm Ω (El ((Δ ~C) (δ ∘ ρ₀) (δ ∘ ρ₁)))

      _~t : {Γ : Con}{A : Ty Γ}(t : Tm Γ A){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁))) →
        Tm Ω (El ((A ~T) ρ₀₁ (t [ ρ₀ ]t) (t [ ρ₁ ]t)))

      _~[]C : (Γ : Con){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{Ψ : Con}{ν : Tms Ψ Ω} →
        ((Γ ~C) ρ₀ ρ₁) [ ν ]t ≡[ TmΓ= U[] ]≡ (Γ ~C) (ρ₀ ∘ ν) (ρ₁ ∘ ν)
      _~[]T : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁))}
        {t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}{Ψ : Con}{ν : Tms Ψ Ω} →
        ((A ~T) ρ₀₁ t₀ t₁) [ ν ]t ≡[ TmΓ= U[] ]≡
        (A ~T) {ρ₀ = ρ₀ ∘ ν} {ρ₁ = ρ₁ ∘ ν} (coe (TmΓ= (El[] ◾ ap El (Γ ~[]C))) (ρ₀₁ [ ν ]t)) (coe (TmΓ= [][]T) (t₀ [ ν ]t)) (coe (TmΓ= [][]T) (t₁ [ ν ]t))
      coe[]T : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(t₀ : Tm Ω (A [ ρ₀ ]T)){Ψ : Con}{ν : Tms Ψ Ω} →
        (coeT A ρ₀₁ t₀) [ ν ]t ≡[ TmΓ= [][]T ]≡
        coeT A {ρ₀ = ρ₀ ∘ ν} {ρ₁ = ρ₁ ∘ ν} (coe (TmΓ= (El[] ◾ ap El (Γ ~[]C))) (ρ₀₁ [ ν ]t)) (coe (TmΓ= [][]T) (t₀ [ ν ]t))

      •~ : {Ω : Con} → (• ~C) ε ε ≡ ⊤' {Ω}
      ,~ : {Γ : Con}{A : Ty Γ}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)} →
        ((Γ , A) ~C) (ρ₀ ,s t₀) (ρ₁ ,s t₁) ≡
        Σ' ((Γ ~C) ρ₀ ρ₁) ((A ~T) (coe (TmΓ= (El[] ◾ ap El (Γ ~[]C))) vz) (coe (TmΓ= [][]T) (t₀ [ wk ]t)) (coe (TmΓ= [][]T) (t₁ [ wk ]t)))
        
      []T~ : {Δ : Con}{A : Ty Δ}{Γ : Con}{δ : Tms Γ Δ}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
        (t₀ : Tm Ω (A [ δ ]T [ ρ₀ ]T))(t₁ : Tm Ω (A [ δ ]T [ ρ₁ ]T)) →
        ((A [ δ ]T) ~T) ρ₀₁ t₀ t₁ ≡ (A ~T) ((δ ~s) ρ₀₁) (coe (TmΓ= [][]T) t₀) (coe (TmΓ= [][]T) t₁)
      coeT[]T : {Δ : Con}{A : Ty Δ}{Γ : Con}{δ : Tms Γ Δ}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
        (t₀ : Tm Ω (A [ δ ]T [ ρ₀ ]T)) →
        coeT (A [ δ ]T) ρ₀₁ t₀ ≡[ TmΓ= [][]T ]≡ coeT A ((δ ~s) ρ₀₁) (coe (TmΓ= [][]T) t₀)
      
      U~ : {Γ : Con}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(a₀ : Tm Ω (U [ ρ₀ ]T))(a₁ : Tm Ω (U [ ρ₁ ]T)) →
        (U ~T) ρ₀₁ a₀ a₁ ≡ (El (coe (TmΓ= U[]) a₀) ⇒ coe (TmΓ= U[]) a₁) ×' (El (coe (TmΓ= U[]) a₁) ⇒ coe (TmΓ= U[]) a₀)
      coeTU : {Γ : Con}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))(a₀ : Tm Ω (U [ ρ₀ ]T)) →
        coeT U ρ₀₁ a₀ ≡ coe (TmΓ= (U[] ◾ U[] ⁻¹)) a₀

      El~ : {Γ : Con}(a : Tm Γ U){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
        (t₀ : Tm Ω (El a [ ρ₀ ]T))(t₁ : Tm Ω (El a [ ρ₁ ]T)) →
        ((El a) ~T) ρ₀₁ t₀ t₁ ≡ ⊤'
      {-
      coeEl : {Γ : Con}{a : Tm Γ U}{Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (El ((Γ ~C) ρ₀ ρ₁)))
        (t₀ : Tm Ω (El a [ ρ₀ ]T)) →
        coeT (El a) ρ₀₁ t₀ ≡
        coe (TmΓ= (El[] ◾ ap El {!!} ◾ El[] ⁻¹))
            (proj₁' (coe (TmΓ= (ap El (U~ ρ₀₁ (a [ ρ₀ ]t) (a [ ρ₁ ]t)))) ((a ~t) ρ₀₁)) $ coe (TmΓ= El[]) t₀)
      -}

      -- TODO: add Func and Sigma and its equalities
