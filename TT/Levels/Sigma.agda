{-# OPTIONS --no-eta #-}

module TT.Levels.Sigma where

open import lib

open import TT.Levels.Decl
open import TT.Levels.Core

record Sigma {d : Decl}(c : Core d) : Set where
  open Decl d
  open Core c

  field
    Σ' : ∀{Γ n}(A : Ty Γ n)(B : Ty (Γ , A) n) → Ty Γ n
    
    Σ[]    : ∀{Γ Δ}{σ : Tms Γ Δ}{n}{A : Ty Δ n}{B : Ty (Δ , A) n}
           → ((Σ' A B) [ σ ]T) ≡ (Σ' (A [ σ ]T) (B [ σ ^ A ]T))

    _,Σ'_  : ∀{Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n}(a : Tm Γ A) → Tm Γ (B [ < a > ]T) → Tm Γ (Σ' A B)
    proj₁' : ∀{Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n}(w : Tm Γ (Σ' A B)) → Tm Γ A
    proj₂' : ∀{Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n}(w : Tm Γ (Σ' A B)) → Tm Γ (B [ < proj₁' w > ]T)
    Σβ₁    : ∀{Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n}{a : Tm Γ A}{b : Tm Γ (B [ < a > ]T)}
           → proj₁' (a ,Σ' b) ≡ a
    Σβ₂    : ∀{Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n}{a : Tm Γ A}{b : Tm Γ (B [ < a > ]T)}
           → proj₂' (a ,Σ' b) ≡[ ap (Tm _) (ap (λ z → B [ < z > ]T) Σβ₁) ]≡ b
    ,Σ[]   : ∀{Γ n}{A : Ty Γ n}{B : Ty (Γ , A) n}{a : Tm Γ A}{b : Tm Γ (B [ < a > ]T)}
             {Θ}{σ : Tms Θ Γ}
           → (a ,Σ' b) [ σ ]t
           ≡[ ap (Tm _) Σ[] ]≡
             (   (a [ σ ]t)
             ,Σ' coe (ap (Tm _) ([][]T ◾ ap (_[_]T B) {!!} ◾ [][]T ⁻¹)) (b [ σ ]t))
