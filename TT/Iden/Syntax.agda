module TT.Iden.Syntax where

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Iden

postulate
  syntaxIden : Iden syntaxCore
  
open Iden syntaxIden public
