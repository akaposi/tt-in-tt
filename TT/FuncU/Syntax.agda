module TT.FuncU.Syntax where

open import TT.Base.Syntax
open import TT.FuncU

postulate
  syntaxFuncU : FuncU syntaxBase
  
open FuncU syntaxFuncU public
