module TT.Boolean.Syntax where

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Boolean

postulate
  syntaxBoolean : Boolean syntaxCore
  
open Boolean syntaxBoolean public
