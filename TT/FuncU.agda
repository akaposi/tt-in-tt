{-# OPTIONS --without-K --no-eta #-}

module TT.FuncU where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
open import TT.Base
import TT.Decl.Congr.NoJM
import TT.Core.Congr.NoJM
import TT.Core.Laws.NoJM
import TT.Base.Congr.NoJM

-- a universe with Pi

record FuncU {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c) : Set (i ⊔ j) where
  open Decl d
  open Core c
  open Base b
  open TT.Decl.Congr.NoJM d
  open TT.Core.Congr.NoJM c
  open TT.Base.Congr.NoJM b
  open TT.Core.Laws.NoJM c

  field
    Π   : ∀{Γ}(A : Tm Γ U)(B : Tm (Γ , El A) U) → Tm Γ U
    
    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Tm Δ U}{B : Tm (Δ , El A) U}
        → ((Π A B) [ σ ]t)
        ≡[ TmΓ= U[] ]≡
          (Π (coe (TmΓ= U[])
                  (A [ σ ]t))
             (coe (Tm= (Γ,C= El[]) (U[]' (Γ,C= El[])))
                  (B [ σ ^ El A ]t)))
        
    lam : ∀{Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U} → Tm (Γ , El A) (El B) → Tm Γ (El (Π A B))
    app : ∀{Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U} → Tm Γ (El (Π A B)) → Tm (Γ , El A) (El B)

    lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Tm Δ U}{B : Tm (Δ , El A) U}{t : Tm (Δ , El A) (El B)}                       
          → (lam t) [ δ ]t
          ≡[ TmΓ= (El[]'' Π[]) ]≡
            lam (coe (Tm= (Γ,C= El[]) (El[]' (Γ,C= El[])))
                     (t [ δ ^ El A ]t))
            
    Πβ    : ∀{Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U}{t : Tm (Γ , El A) (El B)} → app (lam t) ≡ t
    Πη    : ∀{Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U}{t : Tm Γ (El (Π A B))}    → lam (app t) ≡ t

  -- abbreviations

  _$_ : ∀ {Γ}{A : Tm Γ U}{B : Tm (Γ , El A) U}(t : Tm Γ (El (Π A B)))(u : Tm Γ (El A)) → Tm Γ (El B [ < u > ]T)
  t $ u = (app t) [ < u > ]t

  infixl 5 _$_
