module TT.TC.Syntax where

open import TT.Decl.Syntax public
open import TT.Core.Syntax public
open import TT.Func.Syntax public
open import TT.TC

postulate
  syntaxTC : TC syntaxFunc
  
open TC syntaxTC public
