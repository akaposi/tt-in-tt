{-# OPTIONS --rewriting #-}

module TT.ConElim where

open import lib
open import JM

open import TT.Syntax

open import TT.Decl
open import TT.Core
import TT.Core.Rec

----------------------------------------------------------------------
-- A special recursor for contexts
----------------------------------------------------------------------

RecCon' : ∀{ℓ}(Conᴹ : Set ℓ)(•ᴹ : Conᴹ)(,Cᴹ : Conᴹ → Conᴹ)
        → Con → Conᴹ
RecCon' {ℓ} Conᴹ •ᴹ ,ᴹ = RecCon

  where

    d : Decl {ℓ}{ℓ}
    d = record { Con = Conᴹ ; Ty = λ _ → Lift ⊤ ; Tms = λ _ _ → Lift ⊤ ; Tm = λ _ _ → Lift ⊤ }

    c : Core d
    c = record
      { c1 = record
      { • = •ᴹ
      ; _,_ = λ Γᴹ _ → ,ᴹ Γᴹ
      ; _[_]T = λ _ _ → lift tt
      ; id = lift tt
      ; _∘_ = λ _ _ → lift tt
      ; ε = lift tt
      ; _,s_ = λ _ _ → lift tt
      ; π₁ = λ _ → lift tt
      ; _[_]t = λ _ _ → lift tt
      ; π₂ = λ _ → lift tt
      } ; c2 = record
      { [id]T = refl
      ; [][]T = refl
      ; idl = refl
      ; idr = refl
      ; ass = refl
      ; ,∘ = refl
      ; π₁β = refl
      ; πη = refl
      ; εη = refl
      ; π₂β = refl
      } }

    open TT.Core.Rec c
    
----------------------------------------------------------------------
-- A special eliminator for contexts
----------------------------------------------------------------------

open import TT.Decl.DepModel
open import TT.Core.DepModel
open import TT.Base.DepModel
open import TT.Func.DepModel

import TT.Elim

ElimCon' : ∀{ℓ}
           (Conᴹ : Con → Set ℓ)
           (•ᴹ : Conᴹ •)
           (,ᴹ : ∀{Γ A} → Conᴹ Γ → Conᴹ (Γ , A))
         → (Γ : Con) → Conᴹ Γ
ElimCon' {ℓ} Conᴹ •ᴹ ,ᴹ = ElimCon

  where

    d : Declᴹ {ℓ}{ℓ}
    d = record { Conᴹ = Conᴹ ; Tyᴹ = λ _ _ → Lift ⊤ ; Tmsᴹ = λ _ _ _ → Lift ⊤ ; Tmᴹ = λ _ _ _ → Lift ⊤ }

    c : Coreᴹ d
    c = record
          { c1ᴹ = record
          { •ᴹ = •ᴹ
          ; _,Cᴹ_ = λ Γᴹ _ → ,ᴹ Γᴹ
          ; _[_]Tᴹ = λ _ _ → lift tt
          ; εᴹ = lift tt
          ; _,sᴹ_ = λ _ _ → lift tt
          ; idᴹ = lift tt
          ; _∘ᴹ_ = λ _ _ → lift tt
          ; π₁ᴹ = λ _ → lift tt
          ; _[_]tᴹ = λ _ _ → lift tt
          ; π₂ᴹ = λ _ → lift tt
          } ; c2ᴹ = record
          { [id]Tᴹ = refl
          ; [][]Tᴹ = refl
          ; idlᴹ = refl
          ; idrᴹ = refl
          ; assᴹ = refl
          ; π₁βᴹ = refl
          ; πηᴹ = refl
          ; εηᴹ = refl
          ; ,∘ᴹ = refl
          ; π₂βᴹ = refl
          } }

    b : Baseᴹ c
    b = record { Uᴹ = lift tt ; Elᴹ = λ _ → lift tt ; U[]ᴹ = refl ; El[]ᴹ = refl }

    f : Funcᴹ c
    f = record
          { Πᴹ = λ _ _ → lift tt
          ; appᴹ = λ _ → lift tt
          ; lamᴹ = λ _ → lift tt
          ; Π[]ᴹ = refl
          ; lam[]ᴹ = refl
          ; Πβᴹ = refl
          ; Πηᴹ = refl
          }

    open TT.Elim b f

----------------------------------------------------------------------
-- Disjointness and injectivity of constructors for contexts
----------------------------------------------------------------------

open import TT.Decl.Syntax
open import TT.Decl.Congr syntaxDecl
open import TT.Core.Syntax

disj•, : ∀{Γ A} → • ≡ (Γ , A) → ⊥
disj•, {Γ}{A} p = coe (ap (RecCon' Set ⊥ (λ _ → ⊤)) p ⁻¹) tt

inj, : {Γ₀ Γ₁ : Con}{A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
      → Γ₀ , A₀ ≡ Γ₁ , A₁
      → Σ (Γ₀ ≡ Γ₁) λ Γ₂ → A₀ ≡[ Ty= Γ₂ ]≡ A₁
inj, {Γ₀}{Γ₁}{A₀}{A₁} q
  = transport
      {A = Con}
      (λ Γ → ElimCon' (λ _ → Set)
                      ⊥
                      (λ {Γ'}{A'} _ → Σ (Γ₀ ≡ Γ') λ Γ₂ → A₀ ≡[ Ty= Γ₂ ]≡ A')
                      Γ)
      {Γ₀ , A₀}
      {Γ₁ , A₁}
      q
      (refl ,Σ refl)

inj,₀ : {Γ₀ Γ₁ : Con}{A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
      → Γ₀ , A₀ ≡ Γ₁ , A₁
      → Γ₀ ≡ Γ₁
inj,₀ p = proj₁ (inj, p)

inj,₁ : {Γ₀ Γ₁ : Con}{A₀ : Ty Γ₀}{A₁ : Ty Γ₁}
        (p : Γ₀ , A₀ ≡ Γ₁ , A₁)
      → A₀ ≡[ Ty= (inj,₀ p) ]≡ A₁
inj,₁ p = proj₂ (inj, p)
