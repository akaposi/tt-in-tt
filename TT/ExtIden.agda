module TT.ExtIden where

open import Agda.Primitive
open import lib
open import JM

open import TT.Decl
open import TT.Core
import TT.Decl.Congr.NoJM
import TT.Core.Congr.NoJM
import TT.Core.Laws.NoJM

-- Extensional identity type

module _ {i j}{d : Decl}(c : Core {i}{j} d) where
  open Decl d
  open TT.Decl.Congr.NoJM d
  open TT.Core.Congr.NoJM c
  open TT.Core.Laws.NoJM c
  open Core c

  record ExtIden : Set (i ⊔ j) where
    field
      Id : {Γ : Con}{A : Ty Γ} → Tm Γ A → Tm Γ A → Ty Γ

      Id[] : ∀{Γ Θ A}{u v : Tm Γ A}{σ : Tms Θ Γ}
           → Id u v [ σ ]T ≡ Id (u [ σ ]t) (v [ σ ]t)

      ref : ∀{Γ A}(u : Tm Γ A) → Tm Γ (Id u u)

      ref[] : ∀{Γ Θ A}{u : Tm Γ A}{σ : Tms Θ Γ}
            → ref u [ σ ]t ≡[ TmΓ= Id[] ]≡ ref (u [ σ ]t)

      reflect : ∀{Γ A}{u v : Tm Γ A} → Tm Γ (Id u v) → u ≡ v

    transp : ∀{Γ A}(P : Ty (Γ , A)){u v : Tm Γ A}(w : Tm Γ (Id u v)) →
      Tm Γ (P [ < u > ]T) → Tm Γ (P [ < v > ]T)
    transp {Γ} P w = transport (λ z → Tm Γ (P [ < z > ]T)) (reflect w)

    transpβ : ∀{Γ A}{P : Ty (Γ , A)}{u : Tm Γ A}{t : Tm Γ (P [ < u > ]T)} → transp P (ref u) t ≡ t
    transpβ = loopcoe _

    transpβ' : ∀{Γ A}{P : Ty (Γ , A)}{u v : Tm Γ A}{w : Tm Γ (Id u v)}{t : Tm Γ (P [ < u > ]T)} →
       transp P w t ≡ transport (λ z → Tm Γ (P [ < z > ]T)) (reflect w) t
    transpβ' = refl

    cong : ∀{Γ A B}(f : Tm (Γ , A) (B [ wk ]T)){u v : Tm Γ A}(e : Tm Γ (Id u v))
       → Tm Γ (Id (transport (Tm Γ) ([][]T ◾ ap (B [_]T) (π₁id∘ ◾ π₁β) ◾ [id]T) (f [ < u > ]t))
                  (transport (Tm Γ) ([][]T ◾ ap (B [_]T) (π₁id∘ ◾ π₁β) ◾ [id]T) (f [ < v > ]t)))
    cong {Γ}{A}{B} f {u}{v} e = transport (λ v → Tm Γ (Id (transport (Tm Γ) ([][]T ◾ ap (_[_]T B) (π₁id∘ ◾ π₁β) ◾ [id]T) (f [ < u > ]t)) (transport (Tm Γ) ([][]T ◾ ap (_[_]T B) (π₁id∘ ◾ π₁β) ◾ [id]T) (f [ < v > ]t))))
                                          (reflect e)
                                          (ref _)

    congd : ∀{Γ A B}(f : Tm (Γ , A) B){u v : Tm Γ A}(e : Tm Γ (Id u v)) →
      Tm Γ (Id {Γ}{B [ < v > ]T}
               (transp B e (f [ < u > ]t))
               (f [ < v > ]t))
    congd {Γ}{A}{B} f {u}{v} e = J (λ {v} reflecte → Tm Γ (Id (coe (ap (λ z → Tm Γ (B [ id ,s coe (Tm= refl ([id]T ⁻¹)) z ]T)) reflecte) (f [ id ,s coe (Tm= refl ([id]T ⁻¹)) u ]t)) (f [ id ,s coe (Tm= refl ([id]T ⁻¹)) v ]t)))
                                   (ref _)
                                   (reflect e)

    reflectref : ∀{Γ A}{u : Tm Γ A} → reflect (ref u) ≡ refl
    reflectref = UIP _ _

    reflect⁻¹ : ∀{Γ A}{u v : Tm Γ A} → u ≡ v → Tm Γ (Id u v)
    reflect⁻¹ refl = ref _

    reflect⁻¹= : ∀{Γ A}{u v : Tm Γ A}(e : u ≡ v) →
      reflect⁻¹ e ≡ transport (λ z → Tm Γ (Id u z)) e (ref u)
    reflect⁻¹= refl = refl

    reflectβ : ∀{Γ A}{u v : Tm Γ A}{p : u ≡ v} → reflect (reflect⁻¹ p) ≡ p
    reflectβ {p = refl} = UIP _ _
