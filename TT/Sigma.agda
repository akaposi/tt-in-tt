module TT.Sigma where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
import TT.Decl.Congr.NoJM
import TT.Core.Congr.NoJM
import TT.Core.Laws.JM

record Sigma {i j}{d : Decl}(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr.NoJM d
  open Core c
  open TT.Core.Congr.NoJM c
  open TT.Core.Laws.JM c

  field
    Σ' : ∀{Γ}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
    
    Σ[]    : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
           → ((Σ' A B) [ σ ]T) ≡ (Σ' (A [ σ ]T) (B [ σ ^ A ]T))

    _,Σ'_  : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}(u : Tm Γ A)(v : Tm Γ (B [ < u > ]T)) → Tm Γ (Σ' A B)
    proj₁' : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}(w : Tm Γ (Σ' A B)) → Tm Γ A
    proj₂' : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}(w : Tm Γ (Σ' A B)) → Tm Γ (B [ < proj₁' w > ]T)
    Σβ₁    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{u : Tm Γ A}{v : Tm Γ (B [ < u > ]T)}
           → proj₁' (u ,Σ' v) ≡ u
    Σβ₂    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{u : Tm Γ A}{v : Tm Γ (B [ < u > ]T)}
           → proj₂' (u ,Σ' v) ≡[ TmΓ= (A[]T= (<>= Σβ₁)) ]≡ v
    Ση     : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{w : Tm Γ (Σ' A B)}
           → (proj₁' w ,Σ' proj₂' w) ≡ w
    ,Σ[]   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{u : Tm Γ A}{v : Tm Γ (B [ < u > ]T)}
             {Θ}{σ : Tms Θ Γ}
           → (u ,Σ' v) [ σ ]t
           ≡[ TmΓ= Σ[] ]≡
             (   (u [ σ ]t)
             ,Σ' coe (TmΓ= ([][]T ◾ A[]T= <>∘ ◾ [][]T ⁻¹)) (v [ σ ]t))

  _×'_ : {Γ : Con} → Ty Γ → Ty Γ → Ty Γ
  A ×' B = Σ' A (B [ wk ]T)
  infixl 4 _×'_
