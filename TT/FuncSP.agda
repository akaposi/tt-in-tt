{-# OPTIONS --without-K --no-eta #-}

module TT.FuncSP where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
open import TT.Base
import TT.Decl.Congr.NoJM
import TT.Core.Congr.NoJM
import TT.Core.Laws.NoJM
import TT.Base.Congr.NoJM

-- function space for strictly positive definitions

record FuncSP {i j}{d : Decl}{c : Core {i}{j} d}(b : Base c) : Set (i ⊔ j) where
  open Decl d
  open Core c
  open Base b
  open TT.Decl.Congr.NoJM d
  open TT.Core.Congr.NoJM c
  open TT.Base.Congr.NoJM b
  open TT.Core.Laws.NoJM c

  field
    Π   : ∀{Γ}(a : Tm Γ U)(B : Ty (Γ , El a)) → Ty Γ
    
    Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}
        → ((Π a B) [ σ ]T)
        ≡ (Π (coe (TmΓ= U[])
                  (a [ σ ]t))
             (coe (Ty= (Γ,C= El[]))
                  (B [ σ ^ El a ]T)))

    lam : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm (Γ , El a) B → Tm Γ (Π a B)
    app : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)} → Tm Γ (Π a B) → Tm (Γ , El a) B

    lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{a : Tm Δ U}{B : Ty (Δ , El a)}{t : Tm (Δ , El a) B}                       
          → (lam t) [ δ ]t
          ≡[ TmΓ= Π[] ]≡
            lam (coe (Tm= (Γ,C= El[]) refl)
                     (t [ δ ^ El a ]t))

    Πβ    : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm (Γ , El a) B} → app (lam t) ≡ t
    Πη    : ∀{Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}{t : Tm Γ (Π a B)}    → lam (app t) ≡ t

  -- abbreviations

  _$_ : ∀ {Γ}{a : Tm Γ U}{B : Ty (Γ , El a)}(t : Tm Γ (Π a B))(u : Tm Γ (El a)) → Tm Γ (B [ < u > ]T)
  t $ u = (app t) [ < u > ]t

  infixl 5 _$_
