module TT.DepModel where

open import TT.Decl.DepModel public
open import TT.Core.DepModel public
open import TT.Base.DepModel public
open import TT.Func.DepModel public
