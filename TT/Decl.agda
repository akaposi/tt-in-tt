{-# OPTIONS --without-K #-}

module TT.Decl where

open import Agda.Primitive

-- Declaration of a model

record Decl {ℓ ℓ'} : Set (lsuc (ℓ ⊔ ℓ')) where
  constructor decl
  field
    Con : Set ℓ
    Ty  : Con → Set ℓ
    Tms : Con → Con → Set ℓ'
    Tm  : (Γ : Con) → Ty Γ → Set ℓ'
