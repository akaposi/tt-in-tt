open import TT.Decl
open import TT.Core

module TT.Core.Congr {i}{j}{d : Decl {i}{j}}(c : Core d) where

open import TT.Core.Congr.NoJM c public
open import TT.Core.Congr.JM c public
