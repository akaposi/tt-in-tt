open import TT.Decl
open import TT.Core

module TT.Core.Laws {i}{j}{d : Decl {i}{j}}(c : Core d) where

open import TT.Core.Laws.NoJM c public
open import TT.Core.Laws.JM c public
