{-# OPTIONS --without-K --no-eta #-}

module TT.Core.DepModel where

open import Agda.Primitive

open import lib

open import TT.Core.Syntax
open import TT.Core.Congr.NoJM syntaxCore

open import TT.Decl.Congr.NoJM syntaxDecl

open import TT.Decl.DepModel

module _ {i j}(dᴹ : Declᴹ {i}{j}) where

  record Core1ᴹ : Set (i ⊔ j) where
    open Declᴹ dᴹ

    field
      •ᴹ     : Conᴹ •
      _,Cᴹ_  : ∀{Γ}(Γᴹ : Conᴹ Γ){A : Ty Γ} → Tyᴹ Γᴹ A → Conᴹ (Γ , A)
      
      _[_]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A)
               {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ) → Tyᴹ Γᴹ (A [ δ ]T)
               
      εᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ •ᴹ ε
      _,sᴹ_  : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
               {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Γ (A [ δ ]T)}(tᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) t)
             → Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) (δ ,s t)
      idᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ} → Tmsᴹ Γᴹ Γᴹ id
      _∘ᴹ_   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{σ : Tms Δ Σ}(σᴹ : Tmsᴹ Δᴹ Σᴹ σ)
               {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ) → Tmsᴹ Γᴹ Σᴹ (σ ∘ δ)
      π₁ᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
               {δ : Tms Γ (Δ , A)}(δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ) → Tmsᴹ Γᴹ Δᴹ (π₁ δ)

      _[_]tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}{t : Tm Δ A} → Tmᴹ Δᴹ Aᴹ t
             → {δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ) → Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) (t [ δ ]t)
      π₂ᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
               {δ : Tms Γ (Δ , A)}(δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ)→ Tmᴹ Γᴹ (Aᴹ [ π₁ᴹ δᴹ ]Tᴹ) (π₂ δ)

    infixl 4 _,Cᴹ_
    infixl 7 _[_]Tᴹ
    infixl 4 _,sᴹ_
    infix  6 _∘ᴹ_
    infixl 8 _[_]tᴹ

  module _ (c1ᴹ : Core1ᴹ) where
    open Declᴹ dᴹ
    open Core1ᴹ c1ᴹ

    []Tᴹ=′
      : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
        {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
        {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
        {Δᴹ₀ : Conᴹ Δ₀}{Δᴹ₁ : Conᴹ Δ₁}(Δᴹ₂ : Δᴹ₀ ≡[ Conᴹ= Δ₂ ]≡ Δᴹ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
        {Aᴹ₀ : Tyᴹ Δᴹ₀ A₀}{Aᴹ₁ : Tyᴹ Δᴹ₁ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ Tyᴹ= Δ₂ Δᴹ₂ A₂ ]≡ Aᴹ₁)
        {δ₀ : Tms Γ₀ Δ₀}{δ₁ : Tms Γ₁ Δ₁}(δ₂ : δ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ δ₁)
        {δ₀ᴹ : Tmsᴹ Γᴹ₀ Δᴹ₀ δ₀}{δ₁ᴹ : Tmsᴹ Γᴹ₁ Δᴹ₁ δ₁}(δ₂ᴹ : δ₀ᴹ ≡[ Tmsᴹ= Γ₂ Γᴹ₂ Δ₂ Δᴹ₂ δ₂ ]≡ δ₁ᴹ)
      → Aᴹ₀ [ δ₀ᴹ ]Tᴹ ≡[ Tyᴹ= Γ₂ Γᴹ₂ ([]T= Γ₂ Δ₂ A₂ δ₂) ]≡ Aᴹ₁ [ δ₁ᴹ ]Tᴹ
    []Tᴹ=′ refl refl refl refl refl refl refl refl = refl

    record Core2ᴹ : Set (i ⊔ j) where
      field
        [id]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
               → Aᴹ [ idᴹ ]Tᴹ ≡[ TyΓᴹ= [id]T ]≡ Aᴹ
        [][]Tᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{A : Ty Σ}{Aᴹ : Tyᴹ Σᴹ A}
                 {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}{δ : Tms Δ Σ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
               → (Aᴹ [ δᴹ ]Tᴹ) [ σᴹ ]Tᴹ ≡[ TyΓᴹ= [][]T ]≡ Aᴹ [ δᴹ ∘ᴹ σᴹ ]Tᴹ

        idlᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
               → (idᴹ ∘ᴹ δᴹ) ≡[ TmsΓΔᴹ= idl ]≡ δᴹ
        idrᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
               → (δᴹ ∘ᴹ idᴹ) ≡[ TmsΓΔᴹ= idr ]≡ δᴹ
        assᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}{Ω}{Ωᴹ : Conᴹ Ω}
                 {σ : Tms Σ Ω}{σᴹ : Tmsᴹ Σᴹ Ωᴹ σ}{δ : Tms Γ Σ}{δᴹ : Tmsᴹ Γᴹ Σᴹ δ}
                 {ν : Tms Δ Γ}{νᴹ : Tmsᴹ Δᴹ Γᴹ ν}
               → ((σᴹ ∘ᴹ δᴹ) ∘ᴹ νᴹ) ≡[ TmsΓΔᴹ= ass ]≡ (σᴹ ∘ᴹ (δᴹ ∘ᴹ νᴹ))
        π₁βᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
                 {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
                 {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
               → (π₁ᴹ (δᴹ ,sᴹ aᴹ)) ≡[ TmsΓΔᴹ= π₁β ]≡ δᴹ
        πηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
                 {δ : Tms Γ (Δ , A)}{δᴹ : Tmsᴹ Γᴹ (Δᴹ ,Cᴹ Aᴹ) δ}
               → (π₁ᴹ δᴹ ,sᴹ π₂ᴹ δᴹ) ≡[ TmsΓΔᴹ= πη ]≡ δᴹ
        εηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{σ : Tms Γ •}{σᴹ : Tmsᴹ Γᴹ •ᴹ σ}
               → σᴹ ≡[ TmsΓΔᴹ= εη ]≡ εᴹ
        ,∘ᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{Σ}{Σᴹ : Conᴹ Σ}
                 {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
                 {σ : Tms Σ Γ}{σᴹ : Tmsᴹ Σᴹ Γᴹ σ}
                 {A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
                 {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
               → ((δᴹ ,sᴹ aᴹ) ∘ᴹ σᴹ) ≡[ TmsΓΔᴹ= ,∘ ]≡ ((δᴹ ∘ᴹ σᴹ) ,sᴹ coe (TmΓᴹ= [][]T [][]Tᴹ refl) (aᴹ [ σᴹ ]tᴹ))
               
        π₂βᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
                 {δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
                 {a : Tm Γ (A [ δ ]T)}{aᴹ : Tmᴹ Γᴹ (Aᴹ [ δᴹ ]Tᴹ) a}
               → π₂ᴹ (δᴹ ,sᴹ aᴹ)
               ≡[ TmΓᴹ= ([]T= refl refl refl π₁β) ([]Tᴹ=′ refl refl refl refl refl refl π₁β π₁βᴹ) π₂β ]≡
                 aᴹ

      _^ᴹ_ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
             {A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A) → Tmsᴹ (Γᴹ ,Cᴹ Aᴹ [ δᴹ ]Tᴹ) (Δᴹ ,Cᴹ Aᴹ) (δ ^ A)
      _^ᴹ_ = λ δᴹ Aᴹ → (δᴹ ∘ᴹ π₁ᴹ idᴹ) ,sᴹ coe (TmΓᴹ= [][]T [][]Tᴹ refl) (π₂ᴹ idᴹ)

      infixl 5 _^ᴹ_

      ,Cᴹ= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
             {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
             {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
             {Aᴹ₀ : Tyᴹ Γᴹ₀ A₀}{Aᴹ₁ : Tyᴹ Γᴹ₁ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ Tyᴹ= Γ₂ Γᴹ₂ A₂ ]≡ Aᴹ₁)
           → (Γᴹ₀ ,Cᴹ Aᴹ₀) ≡[ Conᴹ= (,C= Γ₂ A₂) ]≡ (Γᴹ₁ ,Cᴹ Aᴹ₁)
      ,Cᴹ= refl refl refl refl = refl

      Γ,Cᴹ= : {Γ : Con}
              {Γᴹ : Conᴹ Γ}
              {A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
              {Aᴹ₀ : Tyᴹ Γᴹ A₀}{Aᴹ₁ : Tyᴹ Γᴹ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ TyΓᴹ= A₂ ]≡ Aᴹ₁)
            → (Γᴹ ,Cᴹ Aᴹ₀) ≡[ Conᴹ= (,C= refl A₂) ]≡ (Γᴹ ,Cᴹ Aᴹ₁)
      Γ,Cᴹ= A₂ Aᴹ₂ = ,Cᴹ= refl refl A₂ Aᴹ₂

  record Coreᴹ : Set (i ⊔ j) where
    field
      c1ᴹ : Core1ᴹ
      c2ᴹ : Core2ᴹ c1ᴹ

    open Declᴹ dᴹ
    open Core1ᴹ c1ᴹ public
    open Core2ᴹ c2ᴹ public

    []Tᴹ=
      : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
        {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
        {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
        {Δᴹ₀ : Conᴹ Δ₀}{Δᴹ₁ : Conᴹ Δ₁}(Δᴹ₂ : Δᴹ₀ ≡[ Conᴹ= Δ₂ ]≡ Δᴹ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
        {Aᴹ₀ : Tyᴹ Δᴹ₀ A₀}{Aᴹ₁ : Tyᴹ Δᴹ₁ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ Tyᴹ= Δ₂ Δᴹ₂ A₂ ]≡ Aᴹ₁)
        {δ₀ : Tms Γ₀ Δ₀}{δ₁ : Tms Γ₁ Δ₁}(δ₂ : δ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ δ₁)
        {δ₀ᴹ : Tmsᴹ Γᴹ₀ Δᴹ₀ δ₀}{δ₁ᴹ : Tmsᴹ Γᴹ₁ Δᴹ₁ δ₁}(δ₂ᴹ : δ₀ᴹ ≡[ Tmsᴹ= Γ₂ Γᴹ₂ Δ₂ Δᴹ₂ δ₂ ]≡ δ₁ᴹ)
      → Aᴹ₀ [ δ₀ᴹ ]Tᴹ ≡[ Tyᴹ= Γ₂ Γᴹ₂ ([]T= Γ₂ Δ₂ A₂ δ₂) ]≡ Aᴹ₁ [ δ₁ᴹ ]Tᴹ
    []Tᴹ= = []Tᴹ=′ c1ᴹ 

    ,sᴹ=
      : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
        {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
        {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
        {Δᴹ₀ : Conᴹ Δ₀}{Δᴹ₁ : Conᴹ Δ₁}(Δᴹ₂ : Δᴹ₀ ≡[ Conᴹ= Δ₂ ]≡ Δᴹ₁)
        {δ₀ : Tms Γ₀ Δ₀}{δ₁ : Tms Γ₁ Δ₁}(δ₂ : δ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ δ₁)
        {δᴹ₀ : Tmsᴹ Γᴹ₀ Δᴹ₀ δ₀}{δᴹ₁ : Tmsᴹ Γᴹ₁ Δᴹ₁ δ₁}(δᴹ₂ : δᴹ₀ ≡[ Tmsᴹ= Γ₂ Γᴹ₂ Δ₂ Δᴹ₂ δ₂ ]≡ δᴹ₁)
        {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
        {Aᴹ₀ : Tyᴹ Δᴹ₀ A₀}{Aᴹ₁ : Tyᴹ Δᴹ₁ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ Tyᴹ= Δ₂ Δᴹ₂ A₂ ]≡ Aᴹ₁)
        {t₀ : Tm Γ₀ (A₀ [ δ₀ ]T)}{t₁ : Tm Γ₁ (A₁ [ δ₁ ]T)}(t₂ : t₀ ≡[ Tm= Γ₂ ([]T= Γ₂ Δ₂ A₂ δ₂) ]≡ t₁)
        {tᴹ₀ : Tmᴹ Γᴹ₀ (Aᴹ₀ [ δᴹ₀ ]Tᴹ) t₀}{tᴹ₁ : Tmᴹ Γᴹ₁ (Aᴹ₁ [ δᴹ₁ ]Tᴹ) t₁}(tᴹ₂ : tᴹ₀ ≡[ Tmᴹ= Γ₂ Γᴹ₂ ([]T= Γ₂ Δ₂ A₂ δ₂) ([]Tᴹ= Γ₂ Γᴹ₂ Δ₂ Δᴹ₂ A₂ Aᴹ₂ δ₂ δᴹ₂) t₂ ]≡ tᴹ₁)
      → (δᴹ₀ ,sᴹ tᴹ₀) ≡[ Tmsᴹ= Γ₂ Γᴹ₂ (,C= Δ₂ A₂) (,Cᴹ= Δ₂ Δᴹ₂ A₂ Aᴹ₂) (,s= Γ₂ Δ₂ δ₂ A₂ t₂) ]≡ (δᴹ₁ ,sᴹ tᴹ₁)
    ,sᴹ= refl refl refl refl refl refl refl refl refl refl = refl
