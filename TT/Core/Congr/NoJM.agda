{-# OPTIONS --without-K --no-eta #-}

open import TT.Decl
open import TT.Core

module TT.Core.Congr.NoJM {i}{j}{d : Decl {i}{j}}(c : Core d) where

open import lib

open Decl d
open Core c
open import TT.Decl.Congr.NoJM d

-- Con

,C= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
    → _≡_ {A = Con} (Γ₀ , A₀) (Γ₁ , A₁)
,C= refl refl = refl

Γ,C= : {Γ : Con}{A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
    → _≡_ {A = Con} (Γ , A₀) (Γ , A₁)
Γ,C= A₂ = ,C= refl A₂

-- Ty

[]T= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
       {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ σ₁)
     → A₀ [ σ₀ ]T ≡[ Ty= Γ₂ ]≡ A₁ [ σ₁ ]T
[]T= = []T=′ d (Core.c1 c)

[]T=' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){Δ : Con}
        {A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁)
        {σ₀ : Tms Γ₀ Δ}{σ₁ : Tms Γ₁ Δ}(σ₂ : σ₀ ≡[ Tms-Γ= Γ₂ ]≡ σ₁)
      → A₀ [ σ₀ ]T ≡[ Ty= Γ₂ ]≡ A₁ [ σ₁ ]T
[]T=' Γ₂ A₂ σ₂ = []T= Γ₂ refl A₂ σ₂

A[]T= : {Γ Δ : Con}{A : Ty Δ}{σ₀ σ₁ : Tms Γ Δ}(σ₂ : σ₀ ≡ σ₁)
      → A [ σ₀ ]T ≡ A [ σ₁ ]T
A[]T= σ₂ = []T= refl refl refl σ₂

[σ]T= : {Γ Δ : Con}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){σ : Tms Γ Δ}
      → A₀ [ σ ]T ≡ A₁ [ σ ]T
[σ]T= A₂ = []T= refl refl A₂ refl

coe[]T : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁){A : Ty Γ₀}{Θ : Con}{σ : Tms Θ Γ₁}
        → coe (Ty= Γ₂) A [ σ ]T ≡ A [ coe (TmsΓ-= (Γ₂ ⁻¹)) σ ]T
coe[]T refl = refl

A[◾]T= : {Γ Δ : Con}{A : Ty Δ}{σ₀ σ₁ σ₁' : Tms Γ Δ}(σ₂ : σ₀ ≡ σ₁)(σ₂' : σ₁ ≡ σ₁')
      → A[]T= {Γ}{Δ}{A}{σ₀}{σ₁'}(σ₂ ◾ σ₂') ≡ (A[]T= σ₂ ◾ A[]T= σ₂')
A[◾]T= refl refl = refl

A[⁻¹]T= : {Γ Δ : Con}{A : Ty Δ}{σ₀ σ₁ : Tms Γ Δ}(σ₂ : σ₀ ≡ σ₁)
      → A[]T= {Γ}{Δ}{A}{σ₁}{σ₀}(σ₂ ⁻¹) ≡ A[]T= σ₂ ⁻¹
A[⁻¹]T= refl = refl

-- Tms

ε= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → ε {Γ₀} ≡[ Tms= Γ₂ refl ]≡ ε {Γ₁}
ε= refl = refl

,s= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
      {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ σ₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
      {t₀ : Tm Γ₀ (A₀ [ σ₀ ]T)}{t₁ : Tm Γ₁ (A₁ [ σ₁ ]T)}(t₂ : t₀ ≡[ Tm= Γ₂ ([]T= Γ₂ Δ₂ A₂ σ₂) ]≡ t₁)
    → (σ₀ ,s t₀) ≡[ Tms= Γ₂ (,C= Δ₂ A₂) ]≡ (σ₁ ,s t₁)
,s= refl refl refl refl refl = refl

,st= : ∀{Γ}{Δ}
       {ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
       {A : Ty Δ}{t : Tm Γ (A [ ρ₀ ]T)}
     → _≡_ {A = Tms Γ (Δ , A)} (ρ₀ ,s t) (ρ₁ ,s coe (TmΓ= (ap (_[_]T A) ρ₂)) t)
,st= refl = refl

∘= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
     {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
     {Θ₀ Θ₁ : Con}(Θ₂ : Θ₀ ≡ Θ₁)
     {ρ₀ : Tms Γ₀ Δ₀}{ρ₁ : Tms Γ₁ Δ₁}(ρ₂ : ρ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ ρ₁)
     {σ₀ : Tms Θ₀ Γ₀}{σ₁ : Tms Θ₁ Γ₁}(σ₂ : σ₀ ≡[ Tms= Θ₂ Γ₂ ]≡ σ₁)
   → (ρ₀ ∘ σ₀) ≡[ Tms= Θ₂ Δ₂ ]≡ (ρ₁ ∘ σ₁)
∘= refl refl refl refl refl = refl

∘=' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ Θ : Con}
      {ρ₀ : Tms Γ₀ Δ}{ρ₁ : Tms Γ₁ Δ}(ρ₂ : ρ₀ ≡[ Tms-Γ= Γ₂ ]≡ ρ₁)
      {σ₀ : Tms Θ Γ₀}{σ₁ : Tms Θ Γ₁}(σ₂ : σ₀ ≡[ Tms= refl Γ₂ ]≡ σ₁)
    → (ρ₀ ∘ σ₀) ≡ (ρ₁ ∘ σ₁)
∘=' Γ₂ ρ₂ σ₂ = ∘= Γ₂ refl refl ρ₂ σ₂

σ∘= : {Γ Δ Θ : Con}
      {ρ : Tms Γ Δ}
      {σ₀ σ₁ : Tms Θ Γ}(σ₂ : σ₀ ≡ σ₁)
    → (ρ ∘ σ₀) ≡ (ρ ∘ σ₁)
σ∘= σ₂ = ∘= refl refl refl refl σ₂

∘σ= : {Γ Δ Θ : Con}
      {ρ₀ ρ₁ : Tms Γ Δ}(ρ₂ : ρ₀ ≡ ρ₁)
      {σ : Tms Θ Γ}
    → (ρ₀ ∘ σ) ≡ (ρ₁ ∘ σ)
∘σ= σ₂ = ∘= refl refl refl σ₂ refl

coe∘ : ∀{Θ Δ Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){σ : Tms Γ₀ Δ}{δ : Tms Θ Γ₁}
     → coe (Tms-Γ= Γ₂) σ ∘ δ
     ≡ σ ∘ coe (TmsΓ-= (Γ₂ ⁻¹)) δ
coe∘ refl = refl

π₁= : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
      {δ₀ : Tms Γ₀ (Δ₀ , A₀)}{δ₁ : Tms Γ₁ (Δ₁ , A₁)}(δ₂ : δ₀ ≡[ Tms= Γ₂ (,C= Δ₂ A₂) ]≡ δ₁)
    → π₁ δ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ π₁ δ₁
π₁= refl refl refl refl = refl

π₁=' : ∀{Γ Δ}{A : Ty Δ}{δ₀ δ₁ : Tms Γ (Δ , A)}(δ₂ : δ₀ ≡ δ₁) → π₁ δ₀ ≡ π₁ δ₁
π₁=' δ₂ = π₁= refl refl refl δ₂

coe<> : ∀{Γ Δ}{A : Ty Δ}{B : Ty Γ}{σ : Tms Γ Δ}{t : Tm Δ A}(p : A [ σ ]T ≡ B)
      → coe (Tms= refl (,C= refl p)) < t [ σ ]t > ≡ < coe (TmΓ= p) (t [ σ ]t) >
coe<> refl = refl

<>=''
  : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
    {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
    {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≡[ Tm= Γ₂ A₂ ]≡ t₁)
  → < t₀ > ≡[ Tms= Γ₂ (,C= Γ₂ A₂) ]≡ < t₁ >
<>='' refl refl refl = refl

<>= : ∀{Γ}{A}{t₀ t₁ : Tm Γ A}(t₂ : t₀ ≡ t₁) → < t₀ > ≡ < t₁ >
<>= t₂ = <>='' refl refl t₂

<>='
  : ∀{Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
    {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≡[ TmΓ= A₂ ]≡ t₁)
  → < t₀ > ≡[ Tms= refl (,C= refl A₂) ]≡ < t₁ >
<>=' A₂ t₂ = <>='' refl A₂ t₂

-- Tm

[]t='
  : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
    {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
    {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
    {t₀ : Tm Δ₀ A₀}{t₁ : Tm Δ₁ A₁}(t₂ : t₀ ≡[ Tm= Δ₂ A₂ ]≡ t₁)
    {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : σ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ σ₁)
  → t₀ [ σ₀ ]t ≡[ Tm= Γ₂ ([]T= Γ₂ Δ₂ A₂ σ₂) ]≡ t₁ [ σ₁ ]t
[]t=' refl refl refl refl refl = refl

[]t= : ∀{Γ}{Δ}{A : Ty Δ}{t₀ t₁ : Tm Δ A}(t₂ : t₀ ≡ t₁)
        {ρ : Tms Γ Δ}
       → t₀ [ ρ ]t ≡ t₁ [ ρ ]t
[]t= t₂ = []t=' refl refl refl t₂ refl

coe[]t : ∀{Γ Δ}{ρ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){u : Tm Δ A₀}
       → coe (TmΓ= A₂) u [ ρ ]t ≡ coe (TmΓ= (ap (λ z → z [ ρ ]T) A₂)) (u [ ρ ]t)
coe[]t refl = refl

coe[]t''
  : ∀{Γ Δ}{ρ : Tms Γ Δ}{A₀ A₁ : Ty Δ}(A₂ : A₀ ≡ A₁){u : Tm Δ A₀}
  → coe (TmΓ= A₂) u [ ρ ]t ≡ coe (TmΓ= ([σ]T= A₂)) (u [ ρ ]t)
coe[]t'' refl = refl

π₂= : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
       {ρ₀ : Tms Γ₀ (Δ₀ , A₀)}{ρ₁ : Tms Γ₁ (Δ₁ , A₁)}(ρ₂ : ρ₀ ≡[ Tms= Γ₂ (,C= Δ₂ A₂)]≡ ρ₁)
     → π₂ ρ₀ ≡[ Tm= Γ₂ ([]T= Γ₂ Δ₂ A₂ (π₁= Γ₂ Δ₂ A₂ ρ₂)) ]≡ π₂ ρ₁
π₂= refl refl refl refl = refl

π₂=' : ∀{Γ Δ}{A : Ty Δ}{δ₀ δ₁ : Tms Γ (Δ , A)}(δ₂ : δ₀ ≡ δ₁) → π₂ δ₀ ≡[ TmΓ= (A[]T= (π₁=' δ₂)) ]≡ π₂ δ₁
π₂=' δ₂ = π₂= refl refl refl δ₂

-- 

◾TmΓ : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}
       {A₁' : Ty Γ}(A₂' : A₀ ≡ A₁'){t₁' : Tm Γ A₁'}(q : t₀ ≡[ TmΓ= A₂' ]≡ t₁')
       (r : t₁' ≡[ TmΓ= (A₂' ⁻¹ ◾ A₂) ]≡ t₁)
     → t₀ ≡[ TmΓ= A₂ ]≡ t₁
◾TmΓ refl refl refl refl = refl

◾TmΓ⁻¹ : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}
         {A₀' : Ty Γ}(A₂' : A₁ ≡ A₀'){t₀' : Tm Γ A₀'}(q : t₁ ≡[ TmΓ= A₂' ]≡ t₀')
         (r : t₀ ≡[ TmΓ= (A₂ ◾ A₂') ]≡ t₀')
       → t₀ ≡[ TmΓ= A₂ ]≡ t₁
◾TmΓ⁻¹ refl refl refl refl = refl

◾TmΓ⁻¹' : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}
          {A₀' : Ty Γ}(A₂' : A₀' ≡ A₁){t₀' : Tm Γ A₀'}(q : t₀' ≡[ TmΓ= A₂' ]≡ t₁)
          (r : t₀ ≡[ TmΓ= (A₂ ◾ A₂' ⁻¹) ]≡ t₀')
        → t₀ ≡[ TmΓ= A₂ ]≡ t₁
◾TmΓ⁻¹' refl refl refl refl = refl

coe2rTmΓ : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}
         → t₀ ≡[ TmΓ= A₂ ]≡ t₁ → t₀ ≡ coe (TmΓ= (A₂ ⁻¹)) t₁
coe2rTmΓ refl p = p

coe2rTmΓ' : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₁ ≡ A₀){t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}
          → t₀ ≡ coe (TmΓ= A₂) t₁ → coe (TmΓ= (A₂ ⁻¹)) t₀ ≡ t₁
coe2rTmΓ' refl p = p

coecoeTmΓ
  : {Γ : Con}{A B C : Ty Γ}(p : A ≡ B)(q : B ≡ C){t : Tm Γ A}
  → coe (TmΓ= q) (coe (TmΓ= p) t) ≡ coe (TmΓ= (p ◾ q)) t
coecoeTmΓ refl refl = refl

coe2rTmΓ''
  : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₁ ≡ A₀)
    {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≡[ TmΓ= (A₂ ⁻¹) ]≡ t₁)
  → t₀ ≡ coe (TmΓ= A₂) t₁
coe2rTmΓ'' refl refl = refl

⁻¹TmΓ : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
        {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≡[ TmΓ= A₂ ]≡ t₁)
      → t₁ ≡[ TmΓ= (A₂ ⁻¹) ]≡ t₀
⁻¹TmΓ refl refl = refl
