{-# OPTIONS --without-K --no-eta #-}

module TT.Core.Syntax where

open import TT.Decl.Syntax public
open import TT.Core

postulate
  syntaxCore : Core syntaxDecl
  
open Core syntaxCore public
