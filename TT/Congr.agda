{-# OPTIONS --no-eta #-}

module TT.Congr where

open import lib
open import TT.Syntax

open import TT.Decl.Congr syntaxDecl public
open import TT.Core.Congr syntaxCore public
open import TT.Base.Congr syntaxBase public
open import TT.Func.Congr syntaxFunc public
