{-# OPTIONS --without-K #-}

module TT.Empty where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
import TT.Decl.Congr.NoJM

record Empty {i j}{d : Decl}(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open TT.Decl.Congr.NoJM d
  open Core c

  field
    Void   : ∀{Γ} → Ty Γ
    Void[] : ∀{Γ Θ}{σ : Tms Γ Θ} → Void [ σ ]T ≡ Void
    abort   : ∀{Γ}{C : Ty Γ} → Tm Γ Void → Tm Γ C
    abort[] : ∀{Γ Θ}{σ : Tms Γ Θ}{C : Ty Θ}{t : Tm Θ Void}
            → abort t [ σ ]t ≡[ TmΓ= Void[] ]≡ abort (coe (TmΓ= Void[]) (t [ σ ]t))
