module TT.Laws where

open import TT.Syntax

open import TT.Core.Laws syntaxCore public
open import TT.Func.Laws syntaxFunc public
