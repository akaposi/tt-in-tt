{-# OPTIONS --without-K --rewriting #-}

open import TT.Decl
open import TT.Core
open import TT.Brunerie

module TT.Brunerie.Rec {i j k}{d : Decl}{c : Core {i}{j} d}(b : Brunerie {k = k} c) where

open import lib

open import TT.Core.Syntax as S
open import TT.Brunerie.Syntax

private module D = Decl d
private module C = Core c
private module B = Brunerie b
private module SB = Brunerie syntaxBrunerie

-- Decl

postulate
  RecCon : S.Con → D.Con
  RecTy  : ∀{Γ}(A : S.Ty Γ) → D.Ty (RecCon Γ)
  RecTms : ∀{Γ Δ}(δ : S.Tms Γ Δ) → D.Tms (RecCon Γ) (RecCon Δ)
  RecTm  : ∀{Γ}{A : S.Ty Γ}(t : S.Tm Γ A) → D.Tm (RecCon Γ) (RecTy A)

-- Core

postulate
  β• : RecCon S.• ≡ C.•
  β, : ∀{Γ A} → (RecCon (Γ S., A)) ≡ ((RecCon Γ) C., (RecTy A))

{-# REWRITE β• #-}
{-# REWRITE β, #-}

postulate
  β[]T   : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ Δ}
         → (RecTy (A S.[ σ ]T)) ≡ (RecTy A C.[ RecTms σ ]T)

{-# REWRITE β[]T #-}

postulate
  βid : ∀{Γ} → (RecTms (S.id {Γ})) ≡ C.id
  β∘  : ∀{Γ Δ Σ}{σ : S.Tms Δ Σ}{ν : S.Tms Γ Δ}
      → (RecTms (σ S.∘ ν)) ≡ (RecTms σ C.∘ RecTms ν)
  βε  : ∀{Γ} → (RecTms (S.ε {Γ})) ≡ C.ε
  β,s : ∀{Γ Δ}{σ : S.Tms Γ Δ}{A : S.Ty Δ}{t : S.Tm Γ (A S.[ σ ]T)}
      → (RecTms (σ S.,s t)) ≡ (RecTms σ C.,s RecTm t)
  βπ₁ : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ (Δ S., A)}
      → (RecTms (S.π₁ σ)) ≡ (C.π₁ (RecTms σ))

{-# REWRITE βid #-}
{-# REWRITE β∘  #-}
{-# REWRITE βε  #-}
{-# REWRITE β,s #-}
{-# REWRITE βπ₁ #-}

postulate
  β[]t : ∀{Γ Δ}{A : S.Ty Δ}{t : S.Tm Δ A}{σ : S.Tms Γ Δ}
       → (RecTm (t S.[ σ ]t)) ≡ (RecTm t C.[ RecTms σ ]t)
  βπ₂  : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ (Δ S., A)}
       → (RecTm (S.π₂ σ)) ≡ (C.π₂ (RecTms σ))

{-# REWRITE β[]t #-}
{-# REWRITE βπ₂  #-}

-- BRUNERIE TYPE THEORY
--
-- for sorts (that go into a "Set i"):
--   functions are needed
-- for point constructors (i.e. those which create elements ≠ path constructors (which create equalities):
--   equalities are needed
-- for path constructors:
--   nothing is needed (only because the meta-theory has K)

postulate
  RecContr  : ∀{Γ} → SB.isContr Γ → B.isContr (RecCon Γ)


postulate
  β⋆   : RecTy SB.⋆ ≡ B.⋆

{-# REWRITE β⋆ #-}


postulate
  β≃   : ∀{Γ} {A} → (a : S.Tm Γ A) → (b : S.Tm Γ A)
       → (RecTy (a SB.≃ b)) ≡ ((RecTm a) B.≃ (RecTm b))

{-# REWRITE β≃ #-}


postulate
  βbase-contr : RecContr SB.base-contr ≡ B.base-contr

{-# REWRITE βbase-contr #-}


postulate
  βrec-contr : ∀ {Γ} {A} → {t : S.Tm Γ A} → (p : SB.isContr Γ)
             → RecContr {(Γ , A , (t S.[ S.wk ]t) SB.≃ S.vz )} (SB.rec-contr {Γ} {A} {t} p) ≡ B.rec-contr {RecCon Γ} {RecTy A} {RecTm t} (RecContr {Γ} p)

{-# REWRITE βrec-contr #-}


postulate
  βcoh : ∀{Δ Γ} → {A : S.Ty Δ} {δ : S.Tms Γ Δ} → (p : SB.isContr Δ)
       → (RecTm (SB.coherence {Δ} {Γ} {A} {δ} p)) ≡ (B.coherence (RecContr p))

{-# REWRITE βcoh #-}
