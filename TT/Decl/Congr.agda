open import TT.Decl

module TT.Decl.Congr {i}{j}(d : Decl {i}{j}) where

open import TT.Decl.Congr.JM d public
open import TT.Decl.Congr.NoJM d public
