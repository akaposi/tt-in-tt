{-# OPTIONS --without-K #-}

open import TT.Decl

module TT.Decl.Rec {i j}(d : Decl {i}{j}) where

open import TT.Decl.Syntax as S

private module D = Decl d

postulate
  RecCon : S.Con → D.Con
  RecTy  : ∀{Γ}(A : S.Ty Γ) → D.Ty (RecCon Γ)
  RecTms : ∀{Γ Δ}(δ : S.Tms Γ Δ) → D.Tms (RecCon Γ) (RecCon Δ)
  RecTm  : ∀{Γ}{A : S.Ty Γ}(t : S.Tm Γ A) → D.Tm (RecCon Γ) (RecTy A)
