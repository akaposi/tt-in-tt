{-# OPTIONS --without-K --no-eta #-}

module TT.Decl.DModel where

open import Agda.Primitive

open import lib

open import TT.Decl
import TT.Decl.Congr.NoJM

record Declᴹ {i j i' j'}(d : Decl {i'}{j'}) : Set (lsuc (i ⊔ j) ⊔ i' ⊔ j') where
  open Decl d
  open TT.Decl.Congr.NoJM d

  field
    Conᴹ : Con → Set i
    Tyᴹ  : ∀{Γ} → Conᴹ Γ → Ty Γ → Set i
    Tmsᴹ : ∀{Γ Δ} → Conᴹ Γ → Conᴹ Δ → Tms Γ Δ → Set j
    Tmᴹ  : ∀{Γ}(Γᴹ : Conᴹ Γ) → {A : Ty Γ} → Tyᴹ Γᴹ A → Tm Γ A → Set j

  -- congruence rules

  Conᴹ= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
        → Conᴹ Γ₀ ≡ Conᴹ Γ₁
  Conᴹ= refl = refl

  Conᴹ== : {Γ₀ Γ₁ : Con}(Γ₂₀ Γ₂₁ : Γ₀ ≡ Γ₁)(Γ₂₂ : Γ₂₀ ≡ Γ₂₁)
           {Γ₀ᴹ : Conᴹ Γ₀}{Γ₁ᴹ : Conᴹ Γ₁}
         → (Γ₀ᴹ ≡[ Conᴹ= Γ₂₀  ]≡ Γ₁ᴹ) ≡ (Γ₀ᴹ ≡[ Conᴹ= Γ₂₁  ]≡ Γ₁ᴹ)
  Conᴹ== p .p refl = refl

  Tyᴹ= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
        → Tyᴹ Γᴹ₀ A₀ ≡ Tyᴹ Γᴹ₁ A₁
  Tyᴹ= refl refl refl = refl

  TyΓᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
        → Tyᴹ Γᴹ A₀ ≡ Tyᴹ Γᴹ A₁
  TyΓᴹ= A₂ = Tyᴹ= refl refl A₂

  TyΓᴹ== : {Γ : Con}{Γᴹ : Conᴹ Γ}
           {A₀ A₁ : Ty Γ}(A₂₀ A₂₁ : A₀ ≡ A₁)(A₂₂ : A₂₀ ≡ A₂₁)
           {A₀ᴹ : Tyᴹ Γᴹ A₀}{A₁ᴹ : Tyᴹ Γᴹ A₁}
         → (A₀ᴹ ≡[ TyΓᴹ= A₂₀ ]≡ A₁ᴹ) ≡ (A₀ᴹ ≡[ TyΓᴹ= A₂₁ ]≡ A₁ᴹ)
  TyΓᴹ== p .p refl = refl

  Tmsᴹ=
    : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
      {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
      {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
      {Δᴹ₀ : Conᴹ Δ₀}{Δᴹ₁ : Conᴹ Δ₁}(Δᴹ₂ : Δᴹ₀ ≡[ Conᴹ= Δ₂ ]≡ Δᴹ₁)
      {δ₀ : Tms Γ₀ Δ₀}{δ₁ : Tms Γ₁ Δ₁}(δ₂ : δ₀ ≡[ Tms= Γ₂ Δ₂ ]≡ δ₁)
    → Tmsᴹ Γᴹ₀ Δᴹ₀ δ₀ ≡ Tmsᴹ Γᴹ₁ Δᴹ₁ δ₁
  Tmsᴹ= refl refl refl refl refl = refl

  TmsΓΔᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
            {δ₀ δ₁ : Tms Γ Δ}(δ₂ : δ₀ ≡ δ₁)
          → Tmsᴹ Γᴹ Δᴹ δ₀ ≡ Tmsᴹ Γᴹ Δᴹ δ₁
  TmsΓΔᴹ= δ₂ = Tmsᴹ= refl refl refl refl δ₂

  TmsΓΔᴹ== : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
             {δ₀ δ₁ : Tms Γ Δ}(δ₂₀ δ₂₁ : δ₀ ≡ δ₁)(δ₂₂ : δ₂₀ ≡ δ₂₁)
             {δ₀ᴹ : Tmsᴹ Γᴹ Δᴹ δ₀}{δ₁ᴹ : Tmsᴹ Γᴹ Δᴹ δ₁}
           → (δ₀ᴹ ≡[ TmsΓΔᴹ= δ₂₀ ]≡ δ₁ᴹ) ≡ (δ₀ᴹ ≡[ TmsΓΔᴹ= δ₂₁ ]≡ δ₁ᴹ)
  TmsΓΔᴹ== p .p refl = refl

  Tmᴹ= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
         {Γᴹ₀ : Conᴹ Γ₀}{Γᴹ₁ : Conᴹ Γ₁}(Γᴹ₂ : Γᴹ₀ ≡[ Conᴹ= Γ₂ ]≡ Γᴹ₁)
         {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≡[ Ty= Γ₂ ]≡ A₁)
         {Aᴹ₀ : Tyᴹ Γᴹ₀ A₀}{Aᴹ₁ : Tyᴹ Γᴹ₁ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ Tyᴹ= Γ₂ Γᴹ₂ A₂ ]≡ Aᴹ₁)
         {t₀ : Tm Γ₀ A₀}{t₁ : Tm Γ₁ A₁}(t₂ : t₀ ≡[ Tm= Γ₂ A₂ ]≡ t₁)
       → Tmᴹ Γᴹ₀ Aᴹ₀ t₀ ≡ Tmᴹ Γᴹ₁ Aᴹ₁ t₁
  Tmᴹ= refl refl refl refl refl = refl

  TmΓAᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}
           {A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
           {t₀ t₁ : Tm Γ A}(t₂ : t₀ ≡ t₁)
         → Tmᴹ Γᴹ Aᴹ t₀ ≡ Tmᴹ Γᴹ Aᴹ t₁
  TmΓAᴹ= t₂ = Tmᴹ= refl refl refl refl t₂

  TmΓᴹ= : {Γ : Con}{Γᴹ : Conᴹ Γ}
          {A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
          {Aᴹ₀ : Tyᴹ Γᴹ A₀}{Aᴹ₁ : Tyᴹ Γᴹ A₁}(Aᴹ₂ : Aᴹ₀ ≡[ TyΓᴹ= A₂ ]≡ Aᴹ₁)
          {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≡[ TmΓ= A₂ ]≡ t₁)
        → Tmᴹ Γᴹ Aᴹ₀ t₀ ≡ Tmᴹ Γᴹ Aᴹ₁ t₁
  TmΓᴹ= A₂ Aᴹ₂ t₂ = Tmᴹ= refl refl A₂ Aᴹ₂ t₂

  TmΓAᴹ== : {Γ : Con}{Γᴹ : Conᴹ Γ}
            {A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
            {t₀ t₁ : Tm Γ A}(t₂₀ t₂₁ : t₀ ≡ t₁)(t₂₂ : t₂₀ ≡ t₂₁)
            {t₀ᴹ : Tmᴹ Γᴹ Aᴹ t₀}{t₁ᴹ : Tmᴹ Γᴹ Aᴹ t₁}
          → (t₀ᴹ ≡[ TmΓAᴹ= t₂₀ ]≡ t₁ᴹ) ≡ (t₀ᴹ ≡[ TmΓAᴹ= t₂₁ ]≡ t₁ᴹ)
  TmΓAᴹ== p .p refl = refl
