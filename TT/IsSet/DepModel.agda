{-# OPTIONS --without-K --no-eta #-}

module TT.IsSet.DepModel where

open import Agda.Primitive

open import lib

open import TT.Decl.Syntax
open import TT.Decl.Congr.NoJM syntaxDecl

open import TT.IsSet.Syntax

open import TT.Decl.DepModel

record IsSetᴹ {i j}(d : Declᴹ {i}{j}) : Set (i ⊔ j) where
  open Declᴹ d

  field
    setCᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}
            {Δ : Con}{Δᴹ : Conᴹ Δ}
            {α : Γ ≡ Δ}{αᴹ : Γᴹ ≡[ Conᴹ= α ]≡ Δᴹ}
            {β : Γ ≡ Δ}{βᴹ : Γᴹ ≡[ Conᴹ= β ]≡ Δᴹ}
          → αᴹ ≡[ Conᴹ== α β (setC {α = α}{β}) ]≡ βᴹ
    setTᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}
            {A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
            {B : Ty Γ}{Bᴹ : Tyᴹ Γᴹ B}
            {α : A ≡ B}{αᴹ : Aᴹ ≡[ TyΓᴹ= α ]≡ Bᴹ}
            {β : A ≡ B}{βᴹ : Aᴹ ≡[ TyΓᴹ= β ]≡ Bᴹ}
          → αᴹ ≡[ TyΓᴹ== α β (setT {α = α}{β}) ]≡ βᴹ
    setsᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}
            {Δ : Con}{Δᴹ : Conᴹ Δ}
            {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}
            {ν : Tms Γ Δ}{νᴹ : Tmsᴹ Γᴹ Δᴹ ν}
            {α : σ ≡ ν}{αᴹ : σᴹ ≡[ TmsΓΔᴹ= α ]≡ νᴹ}
            {β : σ ≡ ν}{βᴹ : σᴹ ≡[ TmsΓΔᴹ= β ]≡ νᴹ}
          → αᴹ ≡[ TmsΓΔᴹ== α β (sets {α = α}{β}) ]≡ βᴹ
    settᴹ : {Γ : Con}{Γᴹ : Conᴹ Γ}
            {A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
            {t : Tm Γ A}{tᴹ : Tmᴹ Γᴹ Aᴹ t}
            {u : Tm Γ A}{uᴹ : Tmᴹ Γᴹ Aᴹ u}
            {α : t ≡ u}{αᴹ : tᴹ ≡[ TmΓAᴹ= α ]≡ uᴹ}
            {β : t ≡ u}{βᴹ : tᴹ ≡[ TmΓAᴹ= β ]≡ uᴹ}
          → αᴹ ≡[ TmΓAᴹ== α β (sett {α = α}{β}) ]≡ βᴹ
