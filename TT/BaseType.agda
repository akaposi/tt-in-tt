{-# OPTIONS --no-eta #-}

module TT.BaseType where

open import Agda.Primitive

open import TT.Decl public
open import TT.Core public

-- Identity type, but with a base type

record BaseType {i j}(d : Decl)(c : Core {i}{j} d) : Set (i ⊔ j) where
  open Decl d
  open Core c

  field
    ⋆ : Ty •

