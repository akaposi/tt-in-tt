{-# OPTIONS --without-K --no-eta #-}

module TT.Brunerie where

open import Agda.Primitive
open import lib

open import TT.Decl
open import TT.Core
import TT.Decl.Congr.NoJM

-- Brunerie ω-groupoid type theory



record Brunerie {i j k}{d : Decl}(c : Core {i}{j} d) : Set (i ⊔ j ⊔ lsuc k) where
  open Decl d
  open TT.Decl.Congr.NoJM d
  open Core c

  field
    ⋆ : Ty •

    _≃_ : ∀{Γ} {A} → (a : Tm Γ A) → (b : Tm Γ A) → Ty Γ

    ≃[] : ∀{Γ Θ A}{u v : Tm Γ A}{σ : Tms Θ Γ} → u ≃ v [ σ ]T ≡ (u [ σ ]t) ≃ (v [ σ ]t)

    isContr     : Con → Set k

    base-contr  : isContr (• , ⋆)

    rec-contr   : ∀ {Γ} {A} → {t : Tm Γ A} → isContr Γ → isContr (Γ , A , (t [ wk ]t) ≃ vz )

    coherence   : ∀{Δ Γ} → {A : Ty Δ} {δ : Tms Γ Δ} → isContr Δ → Tm Γ (A [ δ ]T)

    coherence[] : ∀{Δ Γ Θ} → {A : Ty Δ} {δ : Tms Γ Δ} {p : isContr Δ} {σ : Tms Θ Γ}
                           → (coherence {A = A} {δ = δ} p) [ σ ]t ≡[ TmΓ= [][]T ]≡  coherence {δ = δ ∘ σ} p

