module TT.FuncSP.DepModel where

open import Agda.Primitive

open import lib

open import TT.Decl.Syntax
open import TT.Core.Syntax
open import TT.Base.Syntax
open import TT.FuncSP.Syntax

open import TT.Decl.Congr.NoJM syntaxDecl
open import TT.Core.Congr.NoJM syntaxCore
open import TT.Base.Congr.NoJM syntaxBase

open import TT.Decl.DepModel
open import TT.Core.DepModel
open import TT.Base.DepModel

record FuncSPᴹ {i j}{d : Declᴹ {i}{j}}{c : Coreᴹ d}(b : Baseᴹ c) : Set (i ⊔ j) where
  open Declᴹ d
  open Coreᴹ c
  open Baseᴹ b

  field
    Πᴹ     : ∀{Γ}{Γᴹ : Conᴹ Γ}{a : Tm Γ U}(aᴹ : Tmᴹ Γᴹ Uᴹ a)
             {B : Ty (Γ , El a)}(Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Elᴹ aᴹ) B)
           → Tyᴹ Γᴹ (Π a B)

    appᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{a : Tm Γ U}{aᴹ : Tmᴹ Γᴹ Uᴹ a}
             {B : Ty (Γ , El a)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Elᴹ aᴹ) B}
             {t : Tm Γ (Π a B)} → Tmᴹ Γᴹ (Πᴹ aᴹ Bᴹ) t → Tmᴹ (Γᴹ ,Cᴹ Elᴹ aᴹ) Bᴹ (app t)

    lamᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{a : Tm Γ U}{aᴹ : Tmᴹ Γᴹ Uᴹ a}
             {B : Ty (Γ , El a)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Elᴹ aᴹ) B}
             {t : Tm (Γ , El a) B} → Tmᴹ (Γᴹ ,Cᴹ Elᴹ aᴹ) Bᴹ t → Tmᴹ Γᴹ (Πᴹ aᴹ Bᴹ) (lam t)

    Π[]ᴹ   : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {a : Tm Δ U}{aᴹ : Tmᴹ Δᴹ Uᴹ a}{B : Ty (Δ , El a)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Elᴹ aᴹ) B}
           → Πᴹ aᴹ Bᴹ [ δᴹ ]Tᴹ
           ≡[ TyΓᴹ= Π[] ]≡
             Πᴹ (coe (TmΓᴹ= U[] U[]ᴹ refl)
                     (aᴹ [ δᴹ ]tᴹ))
                (coe (Tyᴹ= (,C= refl El[]) (Γ,Cᴹ= El[] El[]ᴹ) refl)
                     (Bᴹ [ δᴹ ^ᴹ Elᴹ aᴹ ]Tᴹ))

    lam[]ᴹ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}{δᴹ : Tmsᴹ Γᴹ Δᴹ δ}
             {a : Tm Δ U}{aᴹ : Tmᴹ Δᴹ Uᴹ a}{B : Ty (Δ , El a)}{Bᴹ : Tyᴹ (Δᴹ ,Cᴹ Elᴹ aᴹ) B}
             {t : Tm (Δ , El a) B}{tᴹ : Tmᴹ (Δᴹ ,Cᴹ Elᴹ aᴹ) Bᴹ t}
           → (lamᴹ tᴹ) [ δᴹ ]tᴹ
           ≡[ TmΓᴹ= Π[] Π[]ᴹ lam[] ]≡
             lamᴹ (coe (Tmᴹ= (Γ,C= El[]) (Γ,Cᴹ= El[] El[]ᴹ) refl refl refl)
                       (tᴹ [ δᴹ ^ᴹ Elᴹ aᴹ ]tᴹ))

    Πβᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{a : Tm Γ U}{aᴹ : Tmᴹ Γᴹ Uᴹ a}
             {B : Ty (Γ , El a)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Elᴹ aᴹ) B}
             {t : Tm (Γ , El a) B}{tᴹ : Tmᴹ (Γᴹ ,Cᴹ Elᴹ aᴹ) Bᴹ t}
           → appᴹ (lamᴹ tᴹ) ≡[ TmΓAᴹ= Πβ ]≡ tᴹ

    Πηᴹ    : ∀{Γ}{Γᴹ : Conᴹ Γ}{a : Tm Γ U}{aᴹ : Tmᴹ Γᴹ Uᴹ a}
             {B : Ty (Γ , El a)}{Bᴹ : Tyᴹ (Γᴹ ,Cᴹ Elᴹ aᴹ) B}
             {t : Tm Γ (Π a B)}{tᴹ : Tmᴹ Γᴹ (Πᴹ aᴹ Bᴹ) t}
           → lamᴹ (appᴹ tᴹ) ≡[ TmΓAᴹ= Πη ]≡ tᴹ
