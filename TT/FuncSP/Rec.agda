{-# OPTIONS --without-K --rewriting #-}

open import TT.Decl
open import TT.Core
open import TT.Base
open import TT.FuncSP

module TT.FuncSP.Rec {i j}{d : Decl}{c : Core {i}{j} d}{b : Base c}(f : FuncSP b) where

open import lib

open import TT.Core.Syntax as S
open import TT.Base.Syntax as SB
open import TT.FuncSP.Syntax as SF

private module D = Decl d
private module C = Core c
private module B = Base b
private module F = FuncSP f

-- Decl

postulate
  RecCon : S.Con → D.Con
  RecTy  : ∀{Γ}(A : S.Ty Γ) → D.Ty (RecCon Γ)
  RecTms : ∀{Γ Δ}(δ : S.Tms Γ Δ) → D.Tms (RecCon Γ) (RecCon Δ)
  RecTm  : ∀{Γ}{A : S.Ty Γ}(t : S.Tm Γ A) → D.Tm (RecCon Γ) (RecTy A)

-- Core

postulate
  β• : RecCon S.• ≡ Core.• c
  β, : ∀{Γ A} → (RecCon (Γ S., A)) ≡ ((RecCon Γ) C., (RecTy A))

{-# REWRITE β• #-}
{-# REWRITE β, #-}

postulate
  β[]T   : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ Δ}
         → (RecTy (A S.[ σ ]T)) ≡ (RecTy A C.[ RecTms σ ]T)

{-# REWRITE β[]T #-}

postulate
  βid : ∀{Γ} → (RecTms (S.id {Γ})) ≡ C.id
  β∘  : ∀{Γ Δ Σ}{σ : S.Tms Δ Σ}{ν : S.Tms Γ Δ}
      → (RecTms (σ S.∘ ν)) ≡ (RecTms σ C.∘ RecTms ν)
  βε  : ∀{Γ} → (RecTms (S.ε {Γ})) ≡ C.ε
  β,s : ∀{Γ Δ}{σ : S.Tms Γ Δ}{A : S.Ty Δ}{t : S.Tm Γ (A S.[ σ ]T)}
      → (RecTms (σ S.,s t)) ≡ (RecTms σ C.,s RecTm t)
  βπ₁ : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ (Δ S., A)}
      → (RecTms (S.π₁ σ)) ≡ (C.π₁ (RecTms σ))

{-# REWRITE βid #-}
{-# REWRITE β∘  #-}
{-# REWRITE βε  #-}
{-# REWRITE β,s #-}
{-# REWRITE βπ₁ #-}

postulate
  β[]t : ∀{Γ Δ}{A : S.Ty Δ}{t : S.Tm Δ A}{σ : S.Tms Γ Δ}
       → (RecTm (t S.[ σ ]t)) ≡ (RecTm t C.[ RecTms σ ]t)
  βπ₂  : ∀{Γ Δ}{A : S.Ty Δ}{σ : S.Tms Γ (Δ S., A)}
       → (RecTm (S.π₂ σ)) ≡ (C.π₂ (RecTms σ))

{-# REWRITE β[]t #-}
{-# REWRITE βπ₂  #-}

-- Base

postulate
  βU : ∀{Γ} → (RecTy (SB.U {Γ})) ≡ B.U
  
{-# REWRITE βU #-}

postulate
  βEl : ∀{Γ}{Â : S.Tm Γ SB.U}
      → (RecTy (SB.El Â)) ≡ (B.El (RecTm Â))
      
{-# REWRITE βEl #-}
  
-- FuncSP

postulate
  βΠ : ∀{Γ}{a : S.Tm Γ SB.U}{B : S.Ty (Γ S., SB.El a)}
     → (RecTy (SF.Π a B)) ≡ (F.Π (RecTm a) (RecTy B))

{-# REWRITE βΠ #-}

postulate
  βlam : ∀{Γ}{a : S.Tm Γ SB.U}{B : S.Ty (Γ S., SB.El a)}{t : S.Tm (Γ S., SB.El a) B}
       → (RecTm (SF.lam t)) ≡ (F.lam (RecTm t))
  βapp : ∀{Γ}{a : S.Tm Γ SB.U}{B : S.Ty (Γ S., SB.El a)}{t : S.Tm Γ (SF.Π a B)}
       → (RecTm (SF.app t)) ≡ (F.app (RecTm t))

{-# REWRITE βlam #-}
{-# REWRITE βapp #-}
