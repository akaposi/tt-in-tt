module TT.experiments.IR3 where

open import lib

data Con : Set
data Ty : Con → Set
data Tm : (Γ : Con) → Ty Γ → Set

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con)(A : Ty Γ) → Con

data Ty where
  wkT   : ∀{Γ B} → Ty Γ → Ty (Γ , B)
  U     : ∀{Γ} → Ty Γ
  El    : ∀{Γ}(a : Tm Γ U) → Ty Γ

data Tm where
  zero  : ∀{Γ A} → Tm (Γ , A) (wkT A)
  wkt   : ∀{Γ A B} → Tm Γ A → Tm (Γ , B) (wkT A)

postulate
  wkU : ∀{Γ A} → wkT {Γ}{A} U ≡ U




data Tms : Con → Con → Set
_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
_[_]t : ∀{Γ Δ}{A : Ty Δ}(t : Tm Δ A)(σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)

data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ}(t : Tm Γ (A [ σ ]T)) → Tms Γ (Δ , A)

wkT A [ σ ,s t ]T = A [ σ ]T
U [ σ ]T = U
El t [ σ ]T = El (t [ σ ]t)

zero [ σ ,s t ]t = t
wkt t [ σ ,s t₁ ]t = t [ σ ]t

infixl 5 _,_
infixl 7 _[_]T
infixl 5 _,s_
infixl 8 _[_]t

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ
π₁ (σ ,s t) = σ

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)
π₂ (σ ,s t) = t

wks : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Γ} → Tms (Γ , A) Δ

wkTs : ∀{Γ Δ B}(σ : Tms Γ Δ){A : Ty Δ} → wkT (A [ σ ]T) ≡ A [ wks σ {B} ]T

wks ε = ε
wks {Γ}{Δ , B}(σ ,s t){A} = wks σ ,s coe (ap (Tm _) (wkTs σ {B})) (wkt t)

wkTs σ {wkT A} = {!!}
wkTs σ {U} = wkU
wkTs σ {El a} = {!!}

id : ∀{Γ} → Tms Γ Γ
id {•} = ε
id {Γ , A} = wks (id {Γ})
           ,s coe (ap (Tm _) (ap wkT {!!} ◾ wkTs id {A}))
                  (Tm.zero {Γ}{A})

{-

id : ∀{Γ} → Tms Γ Γ
wks : {Γ Δ : Con}{B : Ty Γ} → Tms Γ Δ → Tms (Γ , B) Δ
wkt : {Γ : Con}{A : Ty Γ}{B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wks id ]T)

wks {Γ} {.•} {B} ε = ε
wks {Γ} {.(_ , _)} {B} (σ ,s t) = wks σ ,s {!wkt t!}
wks {Γ} {Δ} {B} (π₁ σ) = π₁ (wks σ)

wkt = {!!}

id {•} = ε
id {Γ , A} = wks (id {Γ}) ,s {!!}

{-

id {•} = ε
id {Γ , A} = π₁ (id {Γ , A}) ,s π₂ (id {Γ , A})

_∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
_∘_ = {!!}

infix  6 _∘_
-}
-}
