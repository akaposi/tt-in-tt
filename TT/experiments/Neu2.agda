{-# OPTIONS --without-K #-}

module TT.experiments.Neu2 where

open import lib

infixl 5 _▷_
infixl 7 _[_]T
infixl 8 _[_]V
infixl 8 _[_]N
infix  6 _∘_
infixl 5 _,_
infixl 5 _^_

data Con : Set
data Ty : Con → Set
data Var : (Γ : Con) → Ty Γ → Set
data Neu : (Γ : Con) → Ty Γ → Set

data Con where
  •   : Con
  _▷_ : (Γ : Con)(A : Ty Γ) → Con

data Ty where
  U  : {Γ : Con} → Ty Γ
  El : {Γ : Con}(a : Neu Γ U) → Ty Γ
  Π  : {Γ : Con}(a : Neu Γ U)(B : Ty (Γ ▷ El a)) → Ty Γ

data Neus : Con → Con → Set
_[_]T : {Δ : Con}(A : Ty Δ){Γ : Con}(σ : Neus Γ Δ) → Ty Γ
_[_]V : {Δ : Con}{A : Ty Δ}(x : Var Δ A){Γ : Con}(σ : Neus Γ Δ) → Neu Γ (A [ σ ]T)
_[_]N : {Δ : Con}{A : Ty Δ}(n : Neu Δ A){Γ : Con}(σ : Neus Γ Δ) → Neu Γ (A [ σ ]T)
wkNs  : {Γ Δ : Con}(σ : Neus Γ Δ){C : Ty Γ} → Neus (Γ ▷ C) Δ
id    : {Γ : Con} → Neus Γ Γ

data Var where
  vz : {Γ : Con}{A : Ty Γ} → Var (Γ ▷ A) (A [ wkNs id ]T)
  vs : {Γ : Con}{A B : Ty Γ} → Var Γ A → Var (Γ ▷ B) (A [ wkNs id ]T)

data Neu where
  var : {Γ : Con}{A : Ty Γ} → Var Γ A → Neu Γ A

data Neus where
  ε : {Γ : Con} → Neus Γ •
  _,_ : {Γ Δ : Con}(σ : Neus Γ Δ){A : Ty Δ}(n : Neu Γ (A [ σ ]T)) → Neus Γ (Δ ▷ A)

wkN   : {Γ : Con}{A : Ty Γ}(n : Neu Γ A){C : Ty Γ} → Neu (Γ ▷ C) (A [ wkNs id ]T)
_∘_   : {Γ Θ Δ : Con}(σ : Neus Θ Δ)(δ : Neus Γ Θ) → Neus Γ Δ
[∘]T  : {Γ Θ Δ : Con}{σ : Neus Θ Δ}{δ : Neus Γ Θ}{A : Ty Δ} → A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T
[∘]N  : {Γ Θ Δ : Con}{σ : Neus Θ Δ}{δ : Neus Γ Θ}{A : Ty Δ}{n : Neu Δ A} → transport (Neu Γ) [∘]T (n [ σ ∘ δ ]N) ≡ n [ σ ]N [ δ ]N
∘wk   : {Γ Δ : Con}{σ : Neus Γ Δ}{C : Ty Γ} → σ ∘ wkNs id {C} ≡ wkNs σ
εη    : {Γ : Con}(σ : Neus Γ •) → σ ≡ ε

εη ε = refl

ε ∘ δ = ε
(σ , n) ∘ δ = (σ ∘ δ) , transport (Neu _) ([∘]T ⁻¹) (n [ δ ]N)

,= : {Γ Δ : Con}{σ₀ σ₁ : Neus Γ Δ}(σ₂ : σ₀ ≡ σ₁){A : Ty Δ}
  {n₀ : Neu Γ (A [ σ₀ ]T)}{n₁ : Neu Γ (A [ σ₁ ]T)}(n₂ : transport (λ z → Neu Γ (A [ z ]T)) σ₂ n₀ ≡ n₁) →
  (σ₀ , n₀) ≡ (σ₁ , n₁)
,= refl refl = refl

wkNs ε = ε
wkNs (σ , n) = wkNs σ , transport (Neu _) ([∘]T ⁻¹ ◾ ap (_ [_]T) ∘wk) (wkN n)

∘wk {σ = ε} = εη (ε ∘ wkNs id) ◾ εη (wkNs ε) ⁻¹
∘wk {σ = σ , n} = ,= (∘wk {σ = σ}) {!!}

id {•} = ε
id {Γ ▷ A} = wkNs (id {Γ}) , var vz

wkN (var x) = var (vs x)

U [ σ ]T = U
El a [ σ ]T = El (a [ σ ]N)
Π a B [ σ ]T = Π (a [ σ ]N) (B [ wkNs σ , transport (λ z → Neu _ (El z)) ([∘]N ⁻¹ ◾ {!!}) (var vz) ]T)

[∘]T = {!!}

vz [ σ , n ]V = {!n!}
vs x [ σ , n ]V = {!x [ σ ]V!}

var x [ σ ]N = x [ σ ]V

[∘]N {σ = σ} {δ} {A} {var x} = {!!}

_^_ : {Γ Δ : Con}(σ : Neus Γ Δ)(A : Ty Δ) → Neus (Γ ▷ A [ σ ]T) (Δ ▷ A)
σ ^ A = {!!}
