{-# OPTIONS --without-K --rewriting #-}

open import TT.Decl
open import TT.experiments.Core1
open import TT.experiments.Core1.Syntax
open import TT.experiments.Core1.Rec

module TT.experiments.Core1.Defn where

id : ∀{Γ} → Tms Γ Γ
id {Γ} = {!RecCon !}


{-
infix  6 _∘_
id    : ∀{Γ} → Tms Γ Γ
_∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
          → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)
          
  idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ 
  idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ 
  ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
  ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
        → ((δ ,s a) ∘ σ) ≡ ((δ ∘ σ) ,s coe (TmΓ= [][]T) (a [ σ ]t))


-- abbreviations

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ , A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ , A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ , B) (A [ wk ]T) 
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ , A)
< t > = id ,s coe (TmΓ= ([id]T ⁻¹)) t

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ , A [ σ ]T) (Δ , A)
σ ^ A = (σ ∘ wk) ,s coe (TmΓ= [][]T) vz

infixl 5 _^_

[_,_] : {A : ∀{Γ} → Ty Γ}(A[] : ∀{Γ Θ}{σ : Tms Γ Θ} → A [ σ ]T ≡ A)(t : ∀{Γ} → Tm Γ A → Tm Γ A)
        {Γ : Con} → Tms (Γ , A) (Γ , A)
[ A[] , t ] = wk ,s coe (TmΓ= (A[] ⁻¹)) (t (coe (TmΓ= A[]) vz))

-}




