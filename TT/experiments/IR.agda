module TT.IR where

open import lib

data Con : Set
data Ty : Con → Set
data Tms : Con → Con → Set
data Tm : (Γ : Con) → Ty Γ → Set

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

data Con where
  •     : Con  -- \bub
  _,_   : (Γ : Con) → Ty Γ → Con

π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) →  Tms Γ Δ

data Ty where

data Tms where
  ε     : ∀{Γ} → Tms Γ •
  _,s_  : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)

data Tm where
  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ σ ]T)


infixl 5 _,_
infixl 7 _[_]T
-- infixl 5 _,s_
-- infix  6 _∘_
-- infixl 8 _[_]t

() [ σ ]T

π₁ (σ ,s t) = σ
