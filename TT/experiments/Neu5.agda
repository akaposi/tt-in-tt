{-# OPTIONS --rewriting --without-K #-}

module TT.experiments.Neu5 where

open import lib

infixl 5 _▷_
infixl 6 _++_
infix 7 [_←]_  
infix 7 [_←_]T_
infix 7 [_←_]N_

data Con : Set
data Ty : Con → Set

data Con where
  •   : Con
  _▷_ : (Γ : Con)(A : Ty Γ) → Con

data Neu : (Γ : Con) → Ty Γ → Set

data Ty where
  U  : {Γ : Con} → Ty Γ
  El : {Γ : Con}(a : Neu Γ U) → Ty Γ
  Π  : {Γ : Con}(a : Neu Γ U)(B : Ty (Γ ▷ El a)) → Ty Γ

data Tel : Con → Set
_++_ : (Γ : Con)(Ω : Tel Γ) → Con

data Tel where
  •   : {Γ : Con} → Tel Γ
  _▷_ : {Γ : Con}(Ω : Tel Γ)(A : Ty (Γ ++ Ω)) → Tel Γ

Γ ++ • = Γ
Γ ++ (Ω ▷ A) = (Γ ++ Ω) ▷ A

[_←]_   : {Γ : Con}(Ψ : Tel Γ)(Ω : Tel Γ) → Tel (Γ ++ Ψ)
[_←_]T_ : {Γ : Con}(Ψ : Tel Γ)(Ω : Tel Γ)(A : Ty (Γ ++ Ω)) → Ty (Γ ++ Ψ ++ [ Ψ ←] Ω)
[_←_]N_ : {Γ : Con}(Ψ : Tel Γ)(Ω : Tel Γ){A : Ty (Γ ++ Ω)}(n : Neu (Γ ++ Ω) A) → Neu (Γ ++ Ψ ++ [ Ψ ←] Ω) ([ Ψ ← Ω ]T A)

[ Ψ ←] • = •
[ Ψ ←] (Ω ▷ A) = [ Ψ ←] Ω ▷ ([ Ψ ← Ω ]T A)

[ Ψ ← Ω ]T U = U
[ Ψ ← Ω ]T El a = El ([ Ψ ← Ω ]N a)
[ Ψ ← Ω ]T Π a B = Π ([ Ψ ← Ω ]N a) ([ Ψ ← Ω ▷ El a ]T B)

data Neu where
  var : {Γ : Con}{A : Ty Γ}(Ω : Tel (Γ ▷ A)) → Neu ((Γ ▷ A) ++ Ω) ([ Ω ← • ]T [ • ▷ A ← • ]T A)
--  app : {Γ : Con}{a : Tm Γ U}{B : Ty (Γ ▷ El a)}(t : Neu Γ (Π a B))(u : Neu Γ (El a)) → Neu Γ ()

[ Ψ ← Ω ]N n = {!!}
