open import TT.Decl
open import TT.Core
open import TT.Base

module TT.Base.Congr {i}{j}{d : Decl {i}{j}}{c : Core d}(b : Base c) where

open import TT.Base.Congr.NoJM b public
open import TT.Base.Congr.JM b public
