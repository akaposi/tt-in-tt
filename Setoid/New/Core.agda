module Setoid.New.Core where

open import lib

open import Setoid.New.Decl

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t

• : Con
• = record
  { ∣_∣C   = ⊤
  ; _C_~_  = λ _ _ → ⊤
  ; symC   = λ _ → tt
  ; transC = λ _ _ → tt
  }

_▷_ : (Γ : Con) → Ty Γ → Con
Γ ▷ A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _C_~_  = λ { (γ ,Σ α)(γ' ,Σ α') → Σ (Γ C γ ~ γ') (A T_⊢ α ~ α') }
  ; symC   = λ { (p ,Σ r) → symC Γ p ,Σ symT A r }
  ; transC = λ { (p ,Σ r)(p' ,Σ r') → transC Γ p p' ,Σ transT A r r' }
  }

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
A [ σ ]T = record
  { ∣_∣T_   = λ γ → ∣ A ∣T (∣ σ ∣s γ)
  ; _T_⊢_~_ = λ p α α' → A T ~s σ p ⊢ α ~ α'
  ; symT    = λ r → irrT A (symT A r) 
  ; transT  = λ r r' → irrT A (transT A r r') 
  ; irrT    = irrT A
  ; coeT    = λ p → coeT A (~s σ p)
  ; cohT    = λ p → cohT A (~s σ p)
  }

id : ∀{Γ} → Tms Γ Γ
id = record
  { ∣_∣s = λ γ → γ
  ; ~s   = λ p → p
  }

_∘_ : ∀{Γ Θ Δ} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
σ ∘ ν = record
  { ∣_∣s = λ γ → ∣ σ ∣s (∣ ν ∣s γ)
  ; ~s   = λ p → ~s σ (~s ν p)
  }

ε : ∀{Γ} → Tms Γ •
ε = record
  { ∣_∣s = λ _ → tt
  ; ~s   = λ _ → tt
  }

_,_ : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
σ , t = record
  { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ
  ; ~s   = λ p → ~s σ p ,Σ ~t t p
  }

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▷ A) →  Tms Γ Δ
π₁ σ = record
  { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ)
  ; ~s   = λ p → proj₁ (~s σ p)
  }

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = record
  { ∣_∣t = λ γ → ∣ t ∣t (∣ σ ∣s γ)
  ; ~t   = λ p → ~t t (~s σ p)
  }

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ σ = record
  { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ)
  ; ~t   = λ p → proj₂ (~s σ p)
  }

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T {Γ}{A} = mkTy= refl refl {!!} {!!} refl refl refl

{-
[][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
        → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)
        
idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ 
idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ 
ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
      → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
      → ((δ ,s a) ∘ σ) ≡ ((δ ∘ σ) ,s coe (TmΓ= [][]T) (a [ σ ]t))
π₁β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → (π₁ (δ ,s a)) ≡ δ
πη    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
      → (π₁ δ ,s π₂ δ) ≡ δ
εη    : ∀{Γ}{σ : Tms Γ •}
      → σ ≡ ε

π₂β   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → π₂ (δ ,s a) ≡[ TmΓ= ([]T=′ refl refl refl π₁β) ]≡ a
-}
