{-# OPTIONS --without-K --prop #-}

module Setoid.SProp.lib where

-- Prop library

infixl 4 _◾p_
infix 5 _⁻¹p

open import Agda.Primitive

open import lib

record ⊤p : Prop where
  constructor ttp

data ⊥p : Prop where

abort⊥p : ∀{ℓ}{A : Set ℓ} → ⊥p → A
abort⊥p ()

abort⊥pp : ∀{ℓ}{A : Prop ℓ} → ⊥p → A
abort⊥pp ()

abortp : ∀{ℓ}{A : Prop ℓ} → ⊥ → A
abortp ()

record Σp {ℓ ℓ'} (A : Prop ℓ) (B : A → Prop ℓ') : Prop (ℓ ⊔ ℓ') where
  constructor _,p_
  field
    proj₁p : A
    proj₂p : B proj₁p
infixl 5 _,p_
open Σp public

_×p_ : ∀{ℓ ℓ'} → Prop ℓ → Prop ℓ' → Prop (ℓ ⊔ ℓ')
A ×p B = Σp A λ _ → B
infixl 4 _×p_

record Σsp {ℓ ℓ'} (A : Set ℓ) (B : A → Prop ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,sp_
  field
    proj₁sp : A
    proj₂sp : B proj₁sp
infixl 5 _,sp_
open Σsp public

record Liftp {ℓ}(A : Prop ℓ) : Set ℓ where
  constructor liftp
  field
    unliftp : A
open Liftp public

record LiftP {ℓ ℓ'}(A : Prop ℓ) : Prop (ℓ ⊔ ℓ') where
  constructor liftP
  field
    unliftP : A
open LiftP public

⊤p' : ∀{ℓ} → Prop ℓ
⊤p' = LiftP ⊤p

ttp' : ∀{ℓ} → ⊤p' {ℓ}
ttp' = liftP ttp

⊥p' : ∀{ℓ} → Prop ℓ
⊥p' = LiftP ⊥p

abort⊥p' : ∀{ℓ ℓ'}{A : Set ℓ} → ⊥p' {ℓ'} → A
abort⊥p' x = abort⊥p (unliftP x)

open import lib

trp : ∀{ℓ ℓ'}{A : Set ℓ}(P : A → Prop ℓ'){x y : A}(p : x ≡ y) → P x → P y
trp P refl u = u

Jp : ∀{ℓ ℓ'}{A : Set ℓ}{x : A}(P : {y : A} → x ≡ y → Prop ℓ') → P refl → {y : A} → (w : x ≡ y) → P w
Jp P pr refl = pr

liftp= : ∀{ℓ}{P : Prop ℓ}{a a' : P} → liftp a ≡ liftp a'
liftp= = refl

,sp= : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Prop ℓ'}{a a' : A}(w : a ≡ a'){b : B a}{b' : B a'} → (a ,sp b) ≡ (a' ,sp b')
,sp= refl = refl

data _≡p_ {ℓ}{A : Set ℓ} (x : A) : A → Prop ℓ where
  reflp : x ≡p x

infix 4 _≡p_

ap-p : ∀{ℓ ℓ'}{A : Set ℓ}{B : Set ℓ'}(f : A → B){a₀ a₁ : A}(a₂ : a₀ ≡p a₁)
    → f a₀ ≡p f a₁
ap-p f reflp = reflp

J-p : ∀{ℓ ℓ'}{A : Set ℓ}{x : A}(P : {y : A} → x ≡p y → Prop ℓ') → P reflp → {y : A} → (w : x ≡p y) → P w
J-p P pr reflp = pr

tr-p : ∀{ℓ ℓ'}{A : Set ℓ}(P : A → Prop ℓ'){x y : A}(p : x ≡p y) → P x → P y
tr-p P reflp u = u

_⁻¹p : ∀{ℓ}{A : Set ℓ}{a a' : A} → a ≡p a' → a' ≡p a
reflp ⁻¹p = reflp

_◾p_ : ∀{ℓ}{A : Set ℓ}{a a' a'' : A} → a ≡p a' → a' ≡p a'' → a ≡p a''
reflp ◾p reflp = reflp

data _⊢_≡[_]≡_ {ℓ}{ℓ'}{A : Set ℓ}(B : A → Set ℓ'){a : A}(b : B a) : {a' : A}(a= : a ≡p a')(b' : B a') → Prop (ℓ ⊔ ℓ') where
  reflp : B ⊢ b ≡[ reflp ]≡ b

_⁻¹pp : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{a a' : A}{a= : a ≡p a'}{b : B a}{b' : B a'} → B ⊢ b ≡[ a= ]≡ b' → B ⊢ b' ≡[ a= ⁻¹p ]≡ b
reflp ⁻¹pp = reflp

_◾pp_ : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{a a' a'' : A}{a= : a ≡p a'}{a=' : a' ≡p a''}{b : B a}{b' : B a'}{b'' : B a''} →
  B ⊢ b ≡[ a= ]≡ b' → B ⊢ b' ≡[ a=' ]≡ b'' → B ⊢ b ≡[ a= ◾p a=' ]≡ b''
reflp ◾pp reflp = reflp

{-
postulate
  transportp : ∀{ℓ ℓ'}{A : Set ℓ}(P : A → Set ℓ'){x y : A}(p : x ≡p y) → P x → P y
  transportpβ : ∀{ℓ ℓ'}{A : Set ℓ}(P : A → Set ℓ'){x : A}{u : P x} → transportp P reflp u ≡ u
{-# REWRITE transportpβ #-}
-}

postulate
  funextip : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{f g : {x : A} → B x} → 
    ((x : A) → f {x} ≡p g {x}) → _≡p_ {A = {x : A} → B x} f g
