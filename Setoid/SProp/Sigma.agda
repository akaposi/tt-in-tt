{-# OPTIONS --without-K --prop #-}

module Setoid.SProp.Sigma {ℓ} where

open import lib
open import Setoid.SProp.lib

open import Setoid.SProp.Decl {ℓ}
open import Setoid.SProp.Core {ℓ}

Σ' : {Γ : Con}(A : Ty Γ) → Ty (Γ ▷ A) → Ty Γ
Σ' {Γ} A B = record
  { ∣_∣T_ = λ γ → Σ (∣ A ∣T γ) λ α → ∣ B ∣T γ ,Σ α
  ; _T_⊢_~_ = λ { γ~ (α ,Σ β) (α' ,Σ β') → Σp (A T γ~ ⊢ α ~ α') λ α~ → B T γ~ ,p α~ ⊢ β ~ β' }
  ; refT = λ αβ → refT A (proj₁ αβ) ,p refT B (proj₂ αβ)
  ; symT = λ αβ~ → symT A (proj₁p αβ~) ,p symT B (proj₂p αβ~)
  ; transT = λ αβ~ αβ~' → transT A (proj₁p αβ~) (proj₁p αβ~') ,p transT B (proj₂p αβ~) (proj₂p αβ~')
  ; coeT = λ γ~ αβ → (coeT A γ~ (proj₁ αβ)) ,Σ coeT B (γ~ ,p cohT A γ~ (proj₁ αβ)) (proj₂ αβ)
  ; cohT = λ γ~ αβ → (cohT A γ~ (proj₁ αβ)) ,p cohT B (γ~ ,p cohT A γ~ (proj₁ αβ)) (proj₂ αβ)
  }

_,'_ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)}(u : Tm Γ A)(v : Tm Γ (B [ < u > ]T)) → Tm Γ (Σ' A B)
u ,' v = record {
  ∣_∣t = λ γ → ∣ u ∣t γ ,Σ ∣ v ∣t γ ;
  ~t = λ γ~ → ~t u γ~ ,p ~t v γ~ }

proj₁' : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)}(t : Tm Γ (Σ' A B)) → Tm Γ A
proj₁' t = record {
  ∣_∣t = λ γ → proj₁ (∣ t ∣t γ) ;
  ~t = λ γ~ → proj₁p (~t t γ~) }

proj₂' : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)}(t : Tm Γ (Σ' A B)) → Tm Γ (B [ < proj₁' {Γ}{A}{B} t > ]T)
proj₂' t = record {
  ∣_∣t = λ γ → proj₂ (∣ t ∣t γ) ;
  ~t = λ γ~ → proj₂p (~t t γ~) }
