{-# OPTIONS --without-K --prop #-}

module Setoid.SProp.Func {ℓ} where

open import lib
open import Setoid.SProp.lib

open import Setoid.SProp.Decl {ℓ}
open import Setoid.SProp.Core {ℓ}

∣Π∣ : {Γ : Con}(A : Ty Γ)(B : Ty (Γ ▷ A))(γ : ∣ Γ ∣C) → Set ℓ
∣Π∣ {Γ} A B γ = Σsp ((α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α)) λ ∣_∣Π →
  {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ,p α~ ⊢ ∣_∣Π α ~ ∣_∣Π α'

infix 4 ∣_∣Π

∣_∣Π : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Prop ℓ'} → Σsp A B → A
∣_∣Π = proj₁sp

~Π : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Prop ℓ'}(w : Σsp A B) → B (proj₁sp w)
~Π = proj₂sp

Π : {Γ : Con} (A : Ty Γ) → Ty (Γ ▷ A) → Ty Γ
Π {Γ} A B = record
  { ∣_∣T_ = ∣Π∣ A B
  ; _T_⊢_~_ = λ {γ}{γ'} γ~ f f' → {α : ∣ A ∣T γ}{α' : ∣ A ∣T γ'}(α~ : A T γ~ ⊢ α ~ α') →
                                  B T γ~ ,p α~ ⊢ ∣ f ∣Π α ~ ∣ f' ∣Π α'
  ; refT = ~Π
  ; symT = λ f~ α~ → symT B (f~ (symT A α~))
  ; transT = λ {_}{_}{_}{γ~} f~ f~' {α}{α''} α~ → transT B (f~ (cohT A γ~ α)) (f~' (transT A (symT A (cohT A γ~ α)) α~))
  ; coeT = λ {γ}{γ'} γ~ f → (λ α' → coeT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α'))) ,sp
      (λ {α}{α'} α~ → transT B
                              (symT B (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α)) (∣ f ∣Π (coeT A (symC Γ γ~) α)))) (transT B
                              (~Π f (transT A
                                       (symT A (cohT A (symC Γ γ~) α)) (transT A
                                       α~
                                       (cohT A (symC Γ γ~) α'))))
                              (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α')))))
  ; cohT = λ {γ}{γ'} γ~ f {α}{α'} α~ → transT B
      (~Π f (transT A α~ (cohT A (symC Γ γ~) α')))
      (cohT B (γ~ ,p symT A (cohT A (symC Γ γ~) α')) (∣ f ∣Π (coeT A (symC Γ γ~) α')))
  }

Π[] : {Γ Δ : Con} {σ : Tms Γ Δ} {A : Ty Δ} {B : Ty (Δ ▷ A)} →
  Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ _,_ (σ ∘ π₁ {A = A [ σ ]T} id) {A} (π₂ {A = A [ σ ]T} id) ]T)
Π[] = refl

lam : {Γ : Con} {A : Ty Γ} {B : Ty (Γ ▷ A)} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
lam {Γ}{A}{B} t = record
  { ∣_∣t = λ γ → (λ α → ∣ t ∣t (γ ,Σ α)) ,sp λ α~ → ~t t (refC Γ γ ,p α~)
  ; ~t = λ γ~ α~ → ~t t (γ~ ,p α~)
  }

app : {Γ : Con} {A : Ty Γ} {B : Ty (Γ ▷ A)} → Tm Γ (Π A B) → Tm (Γ ▷ A) B
app {Γ}{A}{B} t = record
  { ∣_∣t = λ { (γ ,Σ α) → ∣ ∣ t ∣t γ ∣Π α }
  ; ~t = λ { (γ~ ,p α~) → ~t t γ~ α~ }
  }

_$_ : ∀ {Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}(t : Tm Γ (Π A B))(u : Tm Γ A) → Tm Γ (B [ < u > ]T)
_$_ {A = A}{B} t u = (app {A = A}{B} t) [ < u > ]t

infixl 5 _$_

-- functional extensionality

open import Setoid.SProp.Iden {ℓ}

ext : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)}{f g : Tm Γ (Π A B)} →
  Tm (Γ ▷ A) (Id {A = B} (app {A = A} f) (app {A = A} g)) →
  Tm Γ (Id {A = Π A B} f g)
ext {Γ}{A}{B}{f}{g} t = record
  { ∣_∣t = λ γ → liftp λ {α}{α'} α~ → transT B (unliftp (∣ t ∣t (γ ,Σ α))) (~Π (∣ g ∣t γ) α~) }

open import TT.Func

f : Func c
f = record
  { Π = Π
  ; Π[] = λ {Γ}{Δ}{σ}{A}{B} → Π[] {Γ}{Δ}{σ}{A}{B}
  ; lam = λ {Γ}{A}{B} t → lam {Γ}{A}{B} t
  ; app = λ {Γ}{A}{B} t → app {Γ}{A}{B} t
  ; lam[] = refl
  ; Πβ = refl
  ; Πη = refl
  }
