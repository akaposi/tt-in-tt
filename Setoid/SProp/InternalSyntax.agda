{-# OPTIONS --prop --rewriting #-}

module Setoid.SProp.InternalSyntax where

open import Agda.Primitive

open import lib
open import Setoid.SProp.lib

import Setoid.SProp.Decl {lzero} as M
import Setoid.SProp.Core as M
import Setoid.SProp.Func as M
import Setoid.SProp.Props as M
import Setoid.SProp.ComputIden as M

import TT.Decl.Syntax as S
import TT.Core.Syntax as S
import TT.Func.Syntax as S
import TT.Base.Syntax as S
-- import TT.Props.Syntax as S
-- import TT.ComputId.Syntax as S

Con : M.Ty M.•
Con = record
  { ∣_∣T_ = λ _ → S.Con
  ; _T_⊢_~_ = λ _ → _≡p_
  ; refT = λ _ → reflp
  ; symT = λ α~ → α~ ⁻¹p
  ; transT = λ α~ α~' → α~ ◾p α~'
  ; coeT = λ _ Γ → Γ
  ; cohT = λ _ _ → reflp
  }

Ty : M.Ty (M.• M.▷ Con)
Ty = record
  { ∣_∣T_ = λ { (_ ,Σ Γ) → S.Ty Γ }
  ; _T_⊢_~_ = λ { (_ ,p Γ₀₁) A₀ A₁ → S.Ty ⊢ A₀ ≡[ Γ₀₁ ]≡ A₁ } 
  ; refT = λ A → reflp
  ; symT = _⁻¹pp
  ; transT = _◾pp_
  ; coeT = λ { (_ ,p Γ₀₁) A₀ → {!A₀!} } -- we can't coerce along ≡p
  ; cohT = {!!}
  }
