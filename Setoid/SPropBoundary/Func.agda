{-# OPTIONS --prop #-}

module Setoid.SPropBoundary.Func where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropBoundary.Decl
open import Setoid.SPropBoundary.Core

module nondependent {Γ : Con}(A B : Ty Γ true) where
  ∣Π∣ : ∣ Γ ∣C → Set
  ∣Π∣ γ = Σsp (∣ A ∣T γ → ∣ B ∣T γ) λ f →
              {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ⊢ f α ~ f α'

  Fun : Ty Γ true
  Fun = mkTy
    ∣Π∣
    (λ {γ}{γ'} γ~ f f' → {α : ∣ A ∣T γ}{α' : ∣ A ∣T γ'}(α~ : A T γ~ ⊢ α ~ α') → B T γ~ ⊢ proj₁sp f α ~ proj₁sp f' α')
    proj₂sp
    (λ f~ α~ → symT B (f~ (symT A α~)))
    (λ {_}{_}{_}{γ~} f~ f~' {α}{α'} α~ → transT B (f~ (cohT A γ~ α)) (f~' (transT A (symT A (cohT A γ~ α)) α~)))
    (λ γ~ f → (λ α' → coeT B γ~ (proj₁sp f (coeT A (symC Γ γ~) α'))) ,sp
              λ {α}{α'} α~ → transT3 B
                                     (symT B (cohT B γ~ (proj₁sp f (coeT* A γ~ α))))
                                     (proj₂sp f (transT A (cohT* A γ~ α) (transT A α~ (cohT A (symC Γ γ~) α'))))
                                     (cohT B γ~ (proj₁sp f (coeT* A γ~ α'))))
    (λ {γ}{γ'} γ~ f {α}{α'} α~ → transT B (proj₂sp f (transT A α~ (cohT A (symC Γ γ~) α'))) (cohT B γ~ (proj₁sp f (coeT A (symC Γ γ~) α'))))
    λ {γ}{γ'} γ~ p {α} → {!!}
{-


module _ {Γ : Con}(A : Ty Γ true)(B : Ty (Γ ▷ A) true) where
  ∣Π∣ : ∣ Γ ∣C → Set
  ∣Π∣ γ = Σsp ((α : ∣ A ∣T γ) → ∣ B ∣T (γ ,Σ α)) λ f →
              {α α' : ∣ A ∣T γ}(α~ : A T refC Γ γ ⊢ α ~ α') → B T refC Γ γ ,p α~ ⊢ f α ~ f α'

  Π : Ty Γ true
  Π = mkTy {Γ}{true}
    ∣Π∣
    (λ {γ}{γ'} γ~ f f' → {α : ∣ A ∣T γ}{α' : ∣ A ∣T γ'}(α~ : A T γ~ ⊢ α ~ α') → B T γ~ ,p α~ ⊢ proj₁sp f α ~ proj₁sp f' α')
    proj₂sp
    (λ f~ α~ → symT B (f~ (symT A α~)))
    (λ {_}{_}{_}{γ~} f~ f~' {α}{α'} α~ → transT B (f~ (cohT A γ~ α)) (f~' (transT A (symT A (cohT A γ~ α)) α~)))
    (λ γ~ f → (λ α' → coeT B (γ~ ,p cohT* A γ~ α') (proj₁sp f (coeT A (symC Γ γ~) α'))) ,sp
              λ {α}{α'} α~ → transT3 B
                                     (symT B (cohT B (γ~ ,p cohT* A γ~ α) (proj₁sp f (coeT* A γ~ α))))
                                     (proj₂sp f (transT A (cohT* A γ~ α) (transT A α~ (cohT A (symC Γ γ~) α'))))
                                     (cohT B (γ~ ,p cohT* A γ~ α') (proj₁sp f (coeT* A γ~ α'))))
    (λ {γ}{γ'} γ~ f {α}{α'} α~ → transT B (proj₂sp f (transT A α~ (cohT A (symC Γ γ~) α'))) (cohT B (γ~ ,p cohT* A γ~ α') (proj₁sp f (coeT A (symC Γ γ~) α'))))
    (J-p
       {A = {γα : ∣ Γ ▷ A ∣C} → ∣ B ∣T γα → ∣ B ∣T γα}
       {λ {γα} → coeT B (refC (Γ ▷ A) γα)}
       (λ {z} e →
         _≡p_
           {A = {γ : ∣ Γ ∣C} → ∣Π∣ γ → ∣Π∣ γ}
           (λ {γ} f → (λ α' → coeT B (refC Γ γ ,p cohT* A (refC Γ γ) α') (proj₁sp f (coeT* A (refC Γ γ) α'))) ,sp
                      (λ {α}{α'} α~ → transT3 B (symT B (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α) (proj₁sp f (coeT* A (refC Γ γ) α))))
                                                (proj₂sp f (transT A (cohT* A (refC Γ γ) α) (transT A α~ (cohT A (symC Γ (refC Γ γ)) α'))))
                                                (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α') (proj₁sp f (coeT* A (refC Γ γ) α')))))
           (λ {γ} f → (λ α' → z (proj₁sp f α')) ,sp
                      (λ {α}{α'} α~ → tr-p (λ z → B T refC Γ γ ,p α~ ⊢ z (proj₁sp f α) ~ z (proj₁sp f α'))
                                          e
                                          (transT3 B (symT B (cohT B (refC (Γ ▷ A) (γ ,Σ α)) (proj₁sp f α)))
                                                     (proj₂sp f α~)
                                                     (cohT B (refC (Γ ▷ A) (γ ,Σ α')) (proj₁sp f α'))))))
       (J-p {A = {γ : ∣ Γ ∣C} → ∣ A ∣T γ → ∣ A ∣T γ}
          {λ {γ} → coeT A (refC Γ γ)}
          (λ {z} e →
             _≡p_
               {A = {γ : ∣ Γ ∣C} → ∣Π∣ γ → ∣Π∣ γ}
               (λ {γ} f → (λ α' → coeT B (refC Γ γ ,p cohT* A (refC Γ γ) α') (proj₁sp f (coeT* A (refC Γ γ) α'))) ,sp
                      (λ {α}{α'} α~ → transT3 B (symT B (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α) (proj₁sp f (coeT* A (refC Γ γ) α))))
                                                (proj₂sp f (transT A (cohT* A (refC Γ γ) α) (transT A α~ (cohT A (symC Γ (refC Γ γ)) α'))))
                                                (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α') (proj₁sp f (coeT* A (refC Γ γ) α')))))
               (λ {γ} f → (λ α' → coeT B (refC Γ γ ,p tr-p (λ z → A T refC Γ γ ⊢ z α' ~ α') e (cohT* A (refC Γ γ) α')) (proj₁sp f (z α'))) ,sp
                          (λ {α}{α'} α~ →
                            J-p (λ {z} e' → B T refC Γ γ ,p α~ ⊢ coeT B (refC Γ γ ,p tr-p (λ z₁ → A T refC Γ γ ⊢ z₁ α  ~ α ) e' (cohT* A (refC Γ γ) α )) (proj₁sp f (z α )) ~
                                                                coeT B (refC Γ γ ,p tr-p (λ z₁ → A T refC Γ γ ⊢ z₁ α' ~ α') e' (cohT* A (refC Γ γ) α')) (proj₁sp f (z α')))
                                (transT3 B (symT B (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α) (proj₁sp f (coeT* A (refC Γ γ) α))))
                                                   (proj₂sp f (transT A (cohT* A (refC Γ γ) α) (transT A α~ (cohT A (symC Γ (refC Γ γ)) α'))))
                                                   (cohT B (refC Γ γ ,p cohT* A (refC Γ γ) α') (proj₁sp f (coeT* A (refC Γ γ) α'))))
                                e)))
          reflp
          (coeTRef A))
       (coeTRef B))

lam : {Γ : Con}{A : Ty Γ true}{B : Ty (Γ ▷ A) true} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
lam {Γ} t = record { ∣_∣t = λ γ → (λ α → ∣ t ∣t (γ ,Σ α)) ,sp λ α~ → ~t t (refC Γ γ ,p α~) ; ~t = λ γ~ α~ → ~t t (γ~ ,p α~) }

app : {Γ : Con}{A : Ty Γ true} {B : Ty (Γ ▷ A) true} → Tm Γ (Π A B) → Tm (Γ ▷ A) B
app {Γ} t = record { ∣_∣t = λ { (γ ,Σ α) → proj₁sp (∣ t ∣t γ) α } ; ~t = λ { (γ~ ,p α~) → ~t t γ~ α~ } }

Π[] : {Γ Δ : Con} {σ : Tms Γ Δ}{b : Bool}{A : Ty Δ true}{B : Ty (Δ ▷ A) true} →
  Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ^ A ]T)
Π[] = refl

lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ true}{B : Ty (Δ ▷ A) true}{t : Tm (Δ ▷ A) B} →
      (_[_]t {Γ}{Δ}{true}{Π A B}(lam {Δ}{A}{B} t) δ) ≡ (lam {Γ}{A [ δ ]T}{B [ δ ^ A ]T}(t [ δ ^ A ]t))
lam[] = refl

Πβ : {Γ : Con}{A : Ty Γ true}{B : Ty (Γ ▷ A) true}{t : Tm (Γ ▷ A) B} → app {Γ}{A}{B}(lam {Γ}{A}{B} t) ≡ t
Πβ = refl

Πη : {Γ : Con}{A : Ty Γ true}{B : Ty (Γ ▷ A) true}{t : Tm Γ (Π A B)} → lam {Γ}{A}{B}(app {Γ}{A}{B} t) ≡ t
Πη = refl

open import Setoid.SPropRefl.Iden

extt : {Γ : Con}{A : Ty Γ true}{B : Ty (Γ ▷ A) true}{f g : Tm Γ (Π A B)} → Tm (Γ ▷ A) (Id {A = B} (app {A = A} f) (app {A = A} g)) → Tm Γ (Id {A = Π A B} f g)
extt {Γ}{A}{B}{f}{g} t = record { ∣_∣t = λ γ → liftp λ {α}{α'} α~ → transT B (unliftp (∣ t ∣t (γ ,Σ α))) (proj₂sp (∣ g ∣t γ) α~) }
-}
