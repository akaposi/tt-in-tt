{-# OPTIONS --prop --rewriting #-}

module Setoid.SPropBoundary.Core where

open import lib
open import JM
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropBoundary.Decl

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t

-- definitions

• : Con
• = record
  { ∣_∣C   = Lift ⊤
  ; _C_~_  = λ _ _ → LiftP ⊤p
  }

_▷_ : (Γ : Con){b : Bool} → Ty Γ b → Con
_▷_ Γ {true} A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _C_~_  = λ { (γ ,Σ α)(γ' ,Σ α') → Σp (Γ C γ ~ γ') (A T_⊢ α ~ α') }
  ; refC   = λ { (γ ,Σ α) → refC Γ γ ,p refT A α }
  ; symC   = λ { (p ,p r) → symC Γ p ,p symT A r }
  ; transC = λ { (p ,p r)(p' ,p r') → transC Γ p p' ,p transT A r r' }
  }
_▷_ Γ {false} A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _C_~_  = λ { (γ ,Σ α)(γ' ,Σ α') → Σp (Γ C γ ~ γ') (A T_⊢ α ~ α') }
  ; refC   = λ { (γ ,Σ α) → refC Γ γ ,p refT A α }
  ; symC   = λ { (p ,p r) → symC Γ p ,p symT A r }
  ; transC = λ { (p ,p r)(p' ,p r') → transC Γ p p' ,p transT A r r' }
  }

_[_]T : ∀{Γ Δ b} → Ty Δ b → Tms Γ Δ → Ty Γ b
A [ σ ]T = record
  { ∣_∣T_   = λ γ → ∣ A ∣T (∣ σ ∣s γ)
  ; _T_⊢_~_ = λ p α α' → A T ~s σ p ⊢ α ~ α'
  ; refT    = refT A
  ; symT    = symT A
  ; transT  = transT A
  ; coeT    = λ p → coeT A (~s σ p)
  ; cohT    = λ p → cohT A (~s σ p)
  ; coeTRef = λ γ~ → coeTRef A (~s σ γ~)
  }

id : ∀{Γ} → Tms Γ Γ
id = record
  { ∣_∣s = λ γ → γ
  ; ~s   = λ p → p
  }

_∘_ : ∀{Γ Θ Δ} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
σ ∘ ν = record
  { ∣_∣s = λ γ → ∣ σ ∣s (∣ ν ∣s γ)
  ; ~s   = λ p → ~s σ (~s ν p)
  }

ε : ∀{Γ} → Tms Γ •
ε = record
  { ∣_∣s = λ _ → lift tt
  }

_,_ : ∀{Γ Δ}(σ : Tms Γ Δ){b}{A : Ty Δ b} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
_,_ σ {true}  t = record { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ ; ~s = λ p → ~s σ p ,p ~t t p }
_,_ σ {false} t = record { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ ; ~s = λ p → ~s σ p ,p ~t t p }

π₁ : ∀{Γ Δ b}{A : Ty Δ b} → Tms Γ (Δ ▷ A) →  Tms Γ Δ
π₁ {b = true}  σ = record { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ) ; ~s = λ p → proj₁p (~s σ p) }
π₁ {b = false} σ = record { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ) ; ~s = λ p → proj₁p (~s σ p) }

_[_]t : ∀{Γ Δ b}{A : Ty Δ b} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = record { ∣_∣t = λ γ → ∣ t ∣t (∣ σ ∣s γ) ; ~t = λ p → ~t t (~s σ p) }

π₂ : ∀{Γ Δ b}{A : Ty Δ b}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ {b = true}  σ = record { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ) ; ~t = λ p → proj₂p (~s σ p) }
π₂ {b = false} σ = record { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ) ; ~t = λ p → proj₂p (~s σ p) }

[id]T : ∀{Γ b}{A : Ty Γ b}  → A [ id ]T ≡ A
[][]T : ∀{Γ Δ Σ b}{A : Ty Σ b}{σ : Tms Γ Δ}{δ : Tms Δ Σ} → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)
idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ
idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ
ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ} → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
,∘t   : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ true} {a : Tm Γ (A [ δ ]T)} →
  (_,_ δ {A = A} a) ∘ σ ≡ _,_ (δ ∘ σ) {A = A} (a [ σ ]t)
,∘f   : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ false}{a : Tm Γ (A [ δ ]T)} →
  (_,_ δ {A = A} a) ∘ σ ≡ _,_ (δ ∘ σ) {A = A} (a [ σ ]t)
π₁βt  : ∀{Γ Δ}{A : Ty Δ true} {δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → (π₁ {A = A}(_,_ δ {A = A} a)) ≡ δ
π₁βf  : ∀{Γ Δ}{A : Ty Δ false}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → (π₁ {A = A}(_,_ δ {A = A} a)) ≡ δ
πηt   : ∀{Γ Δ}{A : Ty Δ true} {δ : Tms Γ (Δ ▷ A)} → (_,_ (π₁ {A = A} δ) {A = A} (π₂ {A = A} δ)) ≡ δ
πηf   : ∀{Γ Δ}{A : Ty Δ false}{δ : Tms Γ (Δ ▷ A)} → (_,_ (π₁ {A = A} δ) {A = A} (π₂ {A = A} δ)) ≡ δ
εη    : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
π₂βt  : ∀{Γ Δ}{A : Ty Δ true} {δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₂ {A = A}(_,_ δ {A = A} a) ≡ a
π₂βf  : ∀{Γ Δ}{A : Ty Δ false}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₂ {A = A}(_,_ δ {A = A} a) ≡ a

[id]T {A = A} = refl
[][]T {A = A} = refl

idl   = refl
idr   = refl
ass   = refl
,∘t {Γ}{Δ}{Σ}{δ}{σ}{A}{a} = refl
,∘f {Γ}{Δ}{Σ}{δ}{σ}{A}{a} = refl
π₁βt  = refl
π₁βf  = refl
πηt   = refl
πηf   = refl
εη    = refl
π₂βt  = refl
π₂βf  = refl

wk : ∀{Γ b}{A : Ty Γ b} → Tms (Γ ▷ A) Γ
wk {A = A} = π₁ {A = A} id

vz : ∀{Γ b}{A : Ty Γ b} → Tm (Γ ▷ A) (A [ wk {A = A} ]T)
vz {A = A} = π₂ {A = A} id

vs : ∀{Γ b c}{A : Ty Γ b}{B : Ty Γ c} → Tm Γ A → Tm (Γ ▷ B) (A [ wk {A = B} ]T) 
vs {B = B} x = x [ wk {A = B} ]t


<_> : ∀{Γ b}{A : Ty Γ b} → Tm Γ A → Tms Γ (Γ ▷ A)
<_> {Γ}{b}{A} t = _,_ id {A = A} t

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ){b : Bool}(A : Ty Δ b) → Tms (Γ ▷ A [ σ ]T) (Δ ▷ A)
_^_ {Γ}{Δ} σ {b} A = _,_ (σ ∘ wk {A = A [ σ ]T}) {A = A} (vz {A = A [ σ ]T})

infixl 5 _^_

