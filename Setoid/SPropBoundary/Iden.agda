{-# OPTIONS --prop #-}

module Setoid.SPropBoundary.Iden where

open import lib
open import JM
open import Setoid.SProp.lib

open import Setoid.SPropBoundary.Decl
open import Setoid.SPropBoundary.Core

Id : {Γ : Con}{b : Bool}{A : Ty Γ b} → Tm Γ A → Tm Γ A → Ty Γ b
Id {Γ}{b}{A} t u = record
  { ∣_∣T_ = λ γ → Liftp (A T refC Γ γ ⊢ ∣ t ∣t γ ~ ∣ u ∣t γ)
  ; _T_⊢_~_ = λ _ _ _ → LiftP ⊤p
  ; coeT = λ { {γ}{γ'} γ~ (liftp α~) → liftp (transT A (symT A (~t t γ~)) (transT A
                                                       α~
                                                       (~t u γ~))) }
  ; coeTRef = λ {γ}{γ'} γ~ p {α} → refl
  }

Id[] : ∀{Γ Θ b}{A : Ty Γ b}{u v : Tm Γ A}{σ : Tms Θ Γ}
     → Id u v [ σ ]T ≡ Id (u [ σ ]t) (v [ σ ]t)
Id[] {b = true}  = refl
Id[] {b = false} = refl

ref : ∀{Γ b}{A : Ty Γ b}(u : Tm Γ A) → Tm Γ (Id u u)
ref {Γ}{A} u = record { ∣_∣t = λ γ → liftp (~t u (refC Γ γ)) }

ref[]t : ∀{Γ Θ}{A : Ty Γ true} {u : Tm Γ A}{σ : Tms Θ Γ} → ref u [ σ ]t ≡ ref (u [ σ ]t)
ref[]f : ∀{Γ Θ}{A : Ty Γ false}{u : Tm Γ A}{σ : Tms Θ Γ} → ref u [ σ ]t ≡ ref (u [ σ ]t)
ref[]t = refl
ref[]f = refl

transp : ∀{Γ b}{A : Ty Γ b}(P : Ty (Γ ▷ A) b){t u : Tm Γ A}(e : Tm Γ (Id t u)) →
  Tm Γ (P [ < t > ]T) → Tm Γ (P [ < u > ]T)
transp {Γ}{true}{A} P {t}{u} e w = record
  { ∣_∣t = λ γ → coeT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ )
  ; ~t = λ {γ}{γ'} γ~ → transT P (symT P (cohT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ))) (transT P (~t w γ~) (cohT P (refC Γ γ' ,p unliftp (∣ e ∣t γ')) (∣ w ∣t γ')))
  }
transp {Γ}{false}{A} P {t}{u} e w = record
  { ∣_∣t = λ γ → coeT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ )
  ; ~t = λ {γ}{γ'} γ~ → transT P (symT P (cohT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ))) (transT P (~t w γ~) (cohT P (refC Γ γ' ,p unliftp (∣ e ∣t γ')) (∣ w ∣t γ')))
  }

transp[]t : ∀{Γ}{A : Ty Γ true}{P : Ty (Γ ▷ A) true}{t u : Tm Γ A}{e : Tm Γ (Id t u)}
  {w : Tm Γ (P [ < t > ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = P [ <_> {A = A} u ]T} (transp P {t}{u} e w) σ ≡
  transp (P [ σ ^ A ]T) {t [ σ ]t}{u [ σ ]t}(_[_]t e σ) (_[_]t {A = P [ < t > ]T} w σ)
transp[]f : ∀{Γ}{A : Ty Γ false}{P : Ty (Γ ▷ A) false}{t u : Tm Γ A}{e : Tm Γ (Id t u)}
  {w : Tm Γ (P [ < t > ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = P [ <_> {A = A} u ]T} (transp P {t}{u} e w) σ ≡
  transp (P [ σ ^ A ]T) {t [ σ ]t}{u [ σ ]t}(_[_]t e σ) (_[_]t {A = P [ < t > ]T} w σ)
transp[]t = refl
transp[]f = refl

-- strong β rules

transpβt : ∀{Γ}{A : Ty Γ true} {P : Ty (Γ ▷ A) true} {t : Tm Γ A}{w : Tm Γ (P [ < t > ]T)} → transp P {t}{t} (ref t) w ≡ w
transpβf : ∀{Γ}{A : Ty Γ false}{P : Ty (Γ ▷ A) false}{t : Tm Γ A}{w : Tm Γ (P [ < t > ]T)} → transp P {t}{t} (ref t) w ≡ w
transpβt {Γ}{A}{P}{t}{w} = mkTm= (funext λ γ → coeTRef P (refC Γ γ ,p  ~t t (refC Γ γ)) refl)
transpβf {Γ}{A}{P}{t}{w} = mkTm= (funext λ γ → coeTRef P (refC Γ γ ,p  ~t t (refC Γ γ)) refl)
