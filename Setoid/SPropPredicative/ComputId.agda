{-# OPTIONS --type-in-type --prop #-}

module Setoid.SPropPredicative.ComputId where

open import lib
open import Setoid.SProp.lib

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core
open import Setoid.SPropPredicative.Props0

_~C : (Γ : Con){Ω : Con} → Tms Ω Γ → Tms Ω Γ → Tm Ω ℙ₀
(Γ ~C) ρ₀ ρ₁ =
  record { ∣_∣t = λ γ → Γ C ∣ ρ₀ ∣s γ ~ ∣ ρ₁ ∣s γ
         ; ~t = λ p → liftP ((λ x → transC Γ (symC Γ (~s ρ₀ p)) (transC Γ x (~s ρ₁ p))) ,p λ x → transC Γ (~s ρ₀ p) (transC Γ x (symC Γ (~s ρ₁ p)))) }

RC : (Γ : Con){Ω : Con}(ρ : Tms Ω Γ) → Tm Ω (Elℙ₀ ((Γ ~C) ρ ρ))
RC Γ {Ω} ρ = record { ∣_∣t = λ γ → liftp (refC Γ (∣ ρ ∣s γ))
                    ; ~t = λ p → ttp }

SC : (Γ : Con){Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (Elℙ₀ ((Γ ~C) ρ₀ ρ₁))) → Tm Ω (Elℙ₀ ((Γ ~C) ρ₁ ρ₀))
SC Γ {Ω} ρ₀₁ = record { ∣_∣t = λ γ → liftp (symC Γ ( unliftp (∣ ρ₀₁ ∣t γ)))
                      ; ~t = λ p₁ → ttp }

TC : (Γ : Con){Ω : Con}{ρ₀ ρ₁ ρ₂ : Tms Ω Γ} → Tm Ω (Elℙ₀ ((Γ ~C) ρ₀ ρ₁)) → Tm Ω (Elℙ₀ ((Γ ~C) ρ₁ ρ₂)) → Tm Ω (Elℙ₀ ((Γ ~C) ρ₀ ρ₂))
TC Γ {Ω} p q = record { ∣_∣t = λ γ → liftp ((transC Γ ( unliftp (∣ p ∣t γ)) ( unliftp (∣ q ∣t γ))))
                      ; ~t = λ p₁ → ttp }

_~T : {Γ Ω : Con}{b : Bool}(A : Ty Γ b){ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (Elℙ₀ ((Γ ~C) ρ₀ ρ₁))) → Tm Ω (A [ ρ₀ ]T) → Tm Ω (A [ ρ₁ ]T) → Tm Ω ℙ₀
(A ~T) ρ₀₁ t₀ t₁ = record { ∣_∣t = λ γ → A T unliftp (∣ ρ₀₁ ∣t γ) ⊢ ∣ t₀ ∣t γ ~ ∣ t₁ ∣t γ
                          ; ~t = λ p₁ → liftP ((λ x → transT A (symT A (~t t₀ p₁)) (transT A x (~t t₁ p₁)))
                                           ,p (λ x → transT A (~t t₀ p₁) (transT A x (symT A (~t t₁ p₁))))) }

RT : {Γ Ω : Con}{b : Bool}(A : Ty Γ b){ρ : Tms Ω Γ}(t : Tm Ω (A [ ρ ]T)) → Tm Ω (Elℙ₀ ((A ~T) (RC Γ ρ) t t))
RT A t = record { ∣_∣t = λ γ → liftp (refT A (∣ t ∣t γ ))
                ; ~t = λ _ → ttp }

ST : {Γ Ω : Con}{b : Bool}(A : Ty Γ b){ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ ((Γ ~C) ρ₀ ρ₁))}{t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}
     (t₀₁ : Tm Ω (Elℙ₀ ((A ~T) ρ₀₁ t₀ t₁))) → Tm Ω (Elℙ₀ ((A ~T) (SC Γ {ρ₀ = ρ₀}{ρ₁} ρ₀₁) t₁ t₀))
ST A t₀₁ = record { ∣_∣t = λ γ → liftp (symT A (unliftp (∣ t₀₁ ∣t γ)))
                  ; ~t = λ _ → ttp }

TT : {Γ Ω : Con}{b : Bool}(A : Ty Γ b){ρ₀ ρ₁ ρ₂ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ ((Γ ~C) ρ₀ ρ₁))}{ρ₁₂ : Tm Ω (Elℙ₀ ((Γ ~C) ρ₁ ρ₂))}
     {t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}{t₂ : Tm Ω (A [ ρ₂ ]T)}
     (t₀₁ : Tm Ω (Elℙ₀ ((A ~T) ρ₀₁ t₀ t₁)))(t₁₂ : Tm Ω (Elℙ₀ ((A ~T) ρ₁₂ t₁ t₂))) → Tm Ω (Elℙ₀ ((A ~T) (TC Γ {ρ₀ = ρ₀}{ρ₁}{ρ₂} ρ₀₁ ρ₁₂) t₀ t₂))
TT A t₀₁ t₁₂ = record { ∣_∣t = λ γ → liftp (transT A (unliftp (∣ t₀₁ ∣t γ)) (unliftp (∣ t₁₂ ∣t γ)))
                      ; ~t = λ _ → ttp }

coeT' : {Γ Ω : Con}{b : Bool}(A : Ty Γ b){ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (Elℙ₀ ((Γ ~C) ρ₀ ρ₁)))(t₀ : Tm Ω (A [ ρ₀ ]T))
      → Tm Ω (A [ ρ₁ ]T)
coeT' A ρ₀₁ t = record { ∣_∣t = λ γ → coeT A (unliftp (∣ ρ₀₁ ∣t γ)) (∣ t ∣t γ)
                          ; ~t = λ p₁ → let X = symT A (cohT A (unliftp (∣ ρ₀₁ ∣t _)) (∣ t ∣t _) ) in
                                        let Y = cohT A (unliftp (∣ ρ₀₁ ∣t _)) (∣ t ∣t _) in
                                        transT A X (transT A (~t t p₁) Y) }
{-
cohA : {Γ Ω : Con}{b : Bool}(A : Ty Γ b){ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁)))(t₀ : Tm Ω (A [ ρ₀ ]T))
      → Tm Ω (Elℙ₀ (A~ A {ρ₀}{ρ₁} ρ₀₁ t₀ (coeA A ρ₀₁ t₀)))
cohA A p t = record { ∣_∣t = λ γ → liftp (cohT A (unliftp (∣ p ∣t γ)) (∣ t ∣t γ))
                    ; ~t = λ p₁ → ttp }

s~ : {Γ Ω Δ : Con}(σ : Tms Γ Δ){ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))) → Tm Ω (Elℙ₀ (Γ~ (σ ∘ ρ₀) (σ ∘ ρ₁)))
s~ σ ρ₀₁ = record { ∣_∣t = λ γ → liftp (~s σ (unliftp (∣ ρ₀₁ ∣t γ)))
                  ; ~t = λ p → ttp }

t~ : {Γ Ω : Con}{b : Bool}{A : Ty Γ b}(t : Tm Γ A){ρ₀ ρ₁ : Tms Ω Γ}(ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))) → Tm Ω (Elℙ₀ (A~ A ρ₀₁ (t [ ρ₀ ]t) (t [ ρ₁ ]t)))
t~ t ρ₀₁ = record { ∣_∣t = λ γ → liftp (~t t (unliftp (∣ ρ₀₁ ∣t γ)))
                  ; ~t = λ p → ttp }

Γ~[] : {Γ Ω Δ : Con}{σ : Tms Δ Ω}{ρ₀ ρ₁ : Tms Ω Γ} → (Γ~ ρ₀ ρ₁) [ σ ]t ≡[ TmΓ= (ℙ₀[] {σ = σ}) ]≡ Γ~ (ρ₀ ∘ σ) (ρ₁ ∘ σ)
Γ~[] = refl

A~[] : {Γ Ω Δ : Con}{σ : Tms Δ Ω}{b : Bool}{A : Ty Γ b}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))}{t₀ : Tm Ω (A [ ρ₀ ]T)}{t₁ : Tm Ω (A [ ρ₁ ]T)}
       → (A~ A ρ₀₁ t₀ t₁) [ σ ]t ≡[ TmΓ= (ℙ₀[] {σ = σ}) ]≡
             A~ A {ρ₀ ∘ σ} {ρ₁ ∘ σ} (ρ₀₁ [ σ ]t) (t₀ [ σ ]t) (t₁ [ σ ]t)
A~[] = refl

coe[] : {Γ Ω Δ : Con}{σ : Tms Δ Ω}{b : Bool}{A : Ty Γ b}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))}{t₀ : Tm Ω (A [ ρ₀ ]T)}
        → (coeA A {ρ₀}{ρ₁} ρ₀₁ t₀) [ σ ]t ≡ coeA A {ρ₀ ∘ σ} {ρ₁ ∘ σ} (ρ₀₁ [ σ ]t) (t₀ [ σ ]t)
coe[] = refl


open import Setoid.SPropPredicative.Sets0


≡Tmℙ : {Γ : Con}{a a' : Tm Γ ℙ₀}(p : ∣_∣t a ≡ ∣_∣t a') → a ≡ a'
≡Tmℙ refl = refl


-- A~unitunit : {Γ Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))} → A~ 𝕊₀ {ρ₀}{ρ₁} ρ₀₁ unit unit ≡ ⊤ℙ₀ {Ω}
-- A~unitunit = ≡Tmℙ {!!}

-- A~unitbot : {Γ Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))} → A~ 𝕊₀ {ρ₀}{ρ₁} ρ₀₁ unit void ≡ ⊥ℙ₀ {Ω}
-- A~unitbot = ≡Tmℙ {!!}

-- coe𝕊₀ : {Γ Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))}(t₀ : Tm Ω 𝕊₀) → coeA 𝕊₀ {ρ₀}{ρ₁} ρ₀₁ t₀ ≡ t₀
-- coe𝕊₀ t₀ = refl

-- coeEl𝕊₀unit : {Γ Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))}(t₀ : Tm Ω (El𝕊₀ unit)) → coeA (El𝕊₀ unit) {ρ₀}{ρ₁} ρ₀₁ t₀ ≡ t₀
-- coeEl𝕊₀unit t₀ = refl

-- coeEl𝕊₀bot : {Γ Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))}(t₀ : Tm Ω (El𝕊₀ void)) → coeA (El𝕊₀ void) {ρ₀}{ρ₁} ρ₀₁ t₀ ≡ t₀
-- coeEl𝕊₀bot t₀ = refl

-- coeEl𝕊₀Π : {Γ Ω : Con}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))}(a : Tm Γ 𝕊₀)(b : Tm (Γ , El𝕊₀ a) 𝕊₀)
--            (t₀ : Tm Ω (El𝕊₀ ((Π a b) [ ρ₀ ]t))) → coeA (El𝕊₀ (Π a b)) {ρ₀}{ρ₁} ρ₀₁ t₀ ≡ {!!}
-- coeEl𝕊₀Π t₀ = {!!}

-- A~ΠΠ : {Γ Ω : Con}{b : Bool}{A : Ty Γ b}{ρ₀ ρ₁ : Tms Ω Γ}{ρ₀₁ : Tm Ω (Elℙ₀ (Γ~ ρ₀ ρ₁))}{a a' : Tm Ω 𝕊₀}{b : Tm (Ω , El𝕊₀ a) 𝕊₀}{b' : Tm (Ω , El𝕊₀ a') 𝕊₀}
--        →  ∣_∣t (A~ 𝕊₀ {ρ₀}{ρ₁} ρ₀₁ (Π a b) (Π a' b'))
--              ≡ ∣_∣t  (let a~a' = A~ 𝕊₀ {ρ₀}{ρ₁} ρ₀₁ a a' in
--                       let Ela₀ = El𝕊₀ (a [ wk {A = Elℙ₀ a~a'} ]t) in
--                       let ii = id {Ω , Elℙ₀ a~a' , Ela₀} in
--                       let pii = π₁ {_}{_}{_}{Ela₀} ii in
--                       let ppii = π₁ {_}{_}{_}{Elℙ₀ a~a'} pii in
--                       let Ela₁ = El𝕊₀ (a' [ ppii ]t) in
--                       let ii' = id {Ω , Elℙ₀ a~a' , Ela₀ , Ela₁} in
--                       let pii' = π₁ {_}{_}{_}{Ela₁} ii' in
--                       let ppii' = π₁ {_}{_}{_}{Ela₀} pii' in
--                       let pppii' = π₁ {_}{_}{_}{Elℙ₀ a~a'} ppii' in
--                       let X = A~ {Γ , 𝕊₀} (El𝕊₀ (vz {Γ}{_}{𝕊₀})) {(_,s_ ρ₀ {_}{𝕊₀} a) ∘ pppii'}{(_,s_ ρ₁ {_}{𝕊₀} a') ∘ pppii'}
--                                  -- (let XX : Tm Γ (Elℙ₀ (Γ~ (ρ₀ ,s a) (ρ₁ ,s a')))
--                                  --      XX = {!!} in {!!})
--                                  ? (vs {B = Ela₁} (vz {A = Ela₀})) (vz {A = Ela₁}) in --(A~ (El𝕊₀ a) {pppii' }{pppii'} (RΓ pppii') {!!} (coe (TmΓ= {!!}) vz))
--                       let ii'' = id {Ω , Elℙ₀ a~a' , Ela₀ , Ela₁ , Elℙ₀ X} in
--                       let pii'' = π₁ {_}{_}{_}{Elℙ₀ X} ii'' in
--                       let ppii'' = π₁ {_}{_}{_}{Ela₁} pii'' in
--                       let pppii'' = π₁ {_}{_}{_}{Ela₀} ppii'' in
--                       let ppppii'' = π₁ {_}{_}{_}{Elℙ₀ a~a'} pppii'' in
--                       Σℙ₀ a~a' (Πℙ₀ (Ela₀) (Πℙ₀ (Ela₁) (Πℙ₀ (Elℙ₀ X)
--                           (A~ {Γ} 𝕊₀ {ρ₀ ∘ ppppii''}{ρ₁ ∘ ppppii''} (ρ₀₁ [ ppppii'' ]t) (b [ {!!} ]t) (b' [ {!!} ]t))))))

-- A~ΠΠ {ρ₀₁ = ρ₀₁} = {!!}


-- open import Setoid.SPropPredicative.Func renaming (Π to ΠΠ)


-- ap' : {Γ : Con}{A : Ty Γ true}{B : Ty (Γ , A) true}(f : Tm Γ (ΠΠ A B))
--      -- → Tm Γ (Elℙ₀ (A~ (ΠΠ A B) {id}{id} (RΓ id) f f))
--      → Tm Γ (Elℙ₀ (Πℙ₀ A (Πℙ₀ (A [ wk {A = A} ]T) (Πℙ₀ (Elℙ₀ (A~ A {π₁ {_}{_}{_}{A} (π₁ {_}{_}{_}{A [ wk {A = A} ]T} id)}{π₁ {_}{_}{_}{A} (π₁ {_}{_}{_}{A [ wk {A = A} ]T} id)}(RΓ (π₁ {_}{_}{_}{A} (π₁ {_}{_}{_}{A [ wk {A = A} ]T} id))) {!!} {!vz!})) {!!}))))
-- ap' {Γ}{A}{B}f = record { ∣_∣t = {!!} ; ~t = {!!} } --RA {A = ΠΠ A B} id f

postulate
  propext : ∀{i}{a a' : Prop i} → (a → a') ×p (a' → a) → a ≡ a'

reflectℙ : ∀{Γ}{b₀ b₁ : Tm Γ ℙ₀} → Tm Γ (Elℙ₀ (A~ ℙ₀ {id {Γ}}{id {Γ}} (RΓ id) b₀ b₁)) → ∣ b₀ ∣t ≡ ∣ b₁ ∣t
reflectℙ {_}{record { ∣_∣t = b₀ ; ~t = ~b₀ }}{record { ∣_∣t = b₁ ; ~t = ~b₁ }} p =
  funext λ γ → propext (proj₁p (unliftP (unliftp (∣ p ∣t γ))) ,p proj₂p (unliftP (unliftp (∣ p ∣t γ))))
-}
