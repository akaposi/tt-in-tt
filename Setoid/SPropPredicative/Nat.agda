{-# OPTIONS --without-K --prop #-}

module Setoid.SPropPredicative.Nat where

open import lib
open import Setoid.SProp.lib

open import TT.Natural

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core
open import Setoid.SPropPredicative.Props0

data ℕᴿ : ℕ → ℕ → Prop where
  zeroᴿ : ℕᴿ zero zero
  sucᴿ  : ∀ {n m} → ℕᴿ n m → ℕᴿ (suc n) (suc m)

refᴿ : (n : ℕ) → ℕᴿ n n
refᴿ zero = zeroᴿ
refᴿ (suc n) = sucᴿ (refᴿ n)

symᴿ : {n m : ℕ} → ℕᴿ n m → ℕᴿ m n
symᴿ zeroᴿ = zeroᴿ
symᴿ (sucᴿ p) = sucᴿ (symᴿ p)

transᴿ : {n m k : ℕ} → ℕᴿ n m → ℕᴿ m k → ℕᴿ n k
transᴿ zeroᴿ zeroᴿ = zeroᴿ
transᴿ (sucᴿ p) (sucᴿ q) = sucᴿ (transᴿ p q)

Nat : {Γ : Con} → Ty Γ true
Nat {Γ} = record
  { ∣_∣T_ = λ γ → ℕ
  ; _T_⊢_~_ = λ _ → ℕᴿ
  ; coeT = λ _ x → x
  ; refT = refᴿ
  ; symT = symᴿ
  ; transT = transᴿ
  ; cohT = λ _ → refᴿ
  }

Nat[] : ∀{Γ Θ}{σ : Tms Θ Γ} → Nat [ σ ]T ≡ Nat
Nat[] = refl

zeroᵗ : {Γ : Con} → Tm Γ Nat
∣ zeroᵗ ∣t _ = zero
~t zeroᵗ _ = zeroᴿ

sucᵗ : {Γ : Con} → Tm Γ Nat → Tm Γ Nat
∣ sucᵗ t ∣t γ = suc (∣ t ∣t γ)
~t (sucᵗ t) p = sucᴿ (~t t p)

ind' : ∀{l}(P : ℕ → Set l)(z : P zero)(s : {n : ℕ} → P n → P (suc n))(n : ℕ) → P n
ind' P z s zero = z
ind' P z s (suc n) = s (ind' P z s n)

indᴿ : ∀{l}(P₀ P₁ : ℕ → Set l)(Q : ∀ {n₀ n₁} → ℕᴿ n₀ n₁ → P₀ n₀ → P₁ n₁ → Prop l)(z₀ : P₀ zero)(z₁ : P₁ zero)(z' : Q zeroᴿ z₀ z₁)
       (s₀ : ∀ {n} → P₀ n → P₀ (suc n))(s₁ : ∀ {n} → P₁ n → P₁ (suc n))(s' : ∀ {n₀ n₁}{x₀ : P₀ n₀}{x₁ : P₁ n₁}{p} → Q p x₀ x₁ → Q (sucᴿ p) (s₀ x₀) (s₁ x₁))
       {n₀ n₁ : ℕ} → (p : ℕᴿ n₀ n₁) → Q p (ind' P₀ z₀ s₀ n₀) (ind' P₁ z₁ s₁ n₁)
indᴿ P₀ P₁ Q z₀ z₁ z' s₀ s₁ s' zeroᴿ = z'
indᴿ P₀ P₁ Q z₀ z₁ z' s₀ s₁ s' (sucᴿ p) = s' (indᴿ P₀ P₁ Q z₀ z₁ z' s₀ s₁ s' p)

sucv₁ : ∀ Γ{b}(P : Ty (Γ , Nat) b) → Tms (Γ , Nat , P) (Γ , Nat)
sucv₁ Γ P = (_,s_ {Γ , Nat}{Γ} (wk' Nat) {true}{Nat} (sucᵗ (vz {Γ}{true}{Nat}))) ∘  (wk' P)

indᵗ : ∀ {Γ}{b}(P : Ty (Γ , Nat) b)(z : Tm Γ (P [ < zeroᵗ > ]T))(s : Tm (Γ , Nat , P) (P [ sucv₁ Γ P ]T))(n : Tm Γ Nat) → Tm Γ (P [ < n > ]T)
indᵗ {Γ}{true} P z s n =
  record { ∣_∣t = λ γ → ind' (λ n → ∣ P ∣T γ ,Σ n) (∣ z ∣t γ) (λ IH → ∣ s ∣t (_ ,Σ IH)) (∣ n ∣t γ)
         ; ~t   = λ {γ}{γ'}p → indᴿ (λ n → ∣ P ∣T γ ,Σ n) (λ n → ∣ P ∣T γ' ,Σ n) (λ nₚ x₀ x₁ → P T (p ,p nₚ) ⊢ x₀ ~ x₁) (∣ z ∣t γ) (∣ z ∣t γ') (~t z p) (λ IH → ∣ s ∣t (_ ,Σ IH)) (λ IH → ∣ s ∣t (_ ,Σ IH)) (λ x → ~t s (_ ,p x)) (~t n p) }
indᵗ {Γ}{false} P z s n =
  record { ∣_∣t = λ γ → ind' (λ n → ∣ P ∣T γ ,Σ n) (∣ z ∣t γ) (λ IH → ∣ s ∣t (_ ,Σ IH)) (∣ n ∣t γ)
         ; ~t   = λ {γ}{γ'}p → indᴿ (λ n → ∣ P ∣T γ ,Σ n) (λ n → ∣ P ∣T γ' ,Σ n) (λ nₚ x₀ x₁ → P T (p ,p nₚ) ⊢ x₀ ~ x₁) (∣ z ∣t γ) (∣ z ∣t γ') (~t z p) (λ IH → ∣ s ∣t (_ ,Σ IH)) (λ IH → ∣ s ∣t (_ ,Σ IH)) (λ x → ~t s (_ ,p x)) (~t n p) }

zero[] : ∀{Γ Θ}{σ : Tms Γ Θ} → zeroᵗ [ σ ]t ≡ zeroᵗ
zero[] = refl

suc[]  : ∀{Γ Θ}{σ : Tms Γ Θ}{n : Tm Θ Nat} → sucᵗ n [ σ ]t ≡ sucᵗ (n [ σ ]t)
suc[] = refl


-- ind[] todo


zeroβ  : ∀{Γ}{b}{P : Ty (Γ , Nat) b}{pz : Tm Γ (P [ < zeroᵗ > ]T)}{ps : Tm (Γ , Nat , P) (P [ sucv₁ Γ P ]T)} → indᵗ P pz ps zeroᵗ ≡ pz
zeroβ {b = true} = refl
zeroβ {b = false} = refl

sucβt   : ∀{Γ}{P : Ty (Γ , Nat) true}{pz : Tm Γ (P [ < zeroᵗ > ]T)}
          {ps : Tm (Γ , Nat , P) (P [ sucv₁ Γ P ]T)}{n : Tm Γ Nat}
           → indᵗ P pz ps (sucᵗ n) ≡ ps [  _,s_  < n > {_}{P} (indᵗ P pz ps n) ]t
sucβt = refl

sucβf   : ∀{Γ}{P : Ty (Γ , Nat) false}{pz : Tm Γ (P [ < zeroᵗ > ]T)}
          {ps : Tm (Γ , Nat , P) (P [ sucv₁ Γ P ]T)}{n : Tm Γ Nat}
           → indᵗ P pz ps (sucᵗ n) ≡ ps [  _,s_  < n > {_}{P} (indᵗ P pz ps n) ]t
sucβf = refl
