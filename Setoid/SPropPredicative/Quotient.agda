module Setoid.SPropPredicative.Quotient where

open import lib
open import Agda.Primitive
open import Setoid.SProp.lib

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core
open import Setoid.SPropPredicative.Iden
open import Setoid.SPropPredicative.Props0

swap : {Γ : Con}(A B : Ty Γ true) → Tms (Γ , A , wkT A B) (Γ , B , wkT B A)
swap A B = _,s_ (_,s_ (π₁ {A = A} (π₁ {A = wkT A B} id)) {_}{B} (vz' (wkT A B))) {_}{wkT B A} (vs' (wkT A A) (wkT A B) (vz' A))

skip : {Γ : Con}(A B C : Ty Γ true) → Tms (Γ , A , wkT A B , wkT (wkT A B) (wkT A C)) (Γ , A , wkT A C)
skip A B C = wk' (wkT A B) ^ (wkT A C)

module Quotient {Γ : Con}(A : Ty Γ true)(R : Tm (Γ , A , wkT A A) ℙ₀)(r : Tm (Γ , A) (Elℙ₀ R [ < vz' A > ]T))
                (sy : Tm (_ , Elℙ₀ R) (Elℙ₀ (R [ swap A A ∘ wk' (Elℙ₀ R) ]t)))
                (tr : let A₀ = A in let Γ₀ = Γ , A₀ in
                      let A₁ = wkT A₀ A₀ in let Γ₁ = Γ₀ , A₁ in
                      let A₂ = wkT A₁ A₁ in let Γ₂ = Γ₁ , A₂ in
                      let R₀ = wkT A₂ (Elℙ₀ R) in let Γ₃ = Γ₂ , R₀ in
                      let R₁ = Elℙ₀ R [ (wk' A ^ A ^ A₁) ∘ wk' R₀ ]T in let Γ₄ = Γ₃ , R₁ in
                      Tm Γ₄ (Elℙ₀ (R [ (skip A A A ∘ wk' R₀) ∘ wk' R₁ ]t))) where

  R' = Elℙ₀ R

  Q : Ty Γ true
  Q = record
            { ∣_∣T_ = λ γ → ∣ A ∣T γ
            ; _T_⊢_~_ = λ p x₀ x₁ → ∣ R ∣t (_ ,Σ coeT A p x₀ ,Σ x₁)
            ; refT = λ α → let rr = ∣ r ∣t (_ ,Σ α) in unliftp (coeT R' (refC Γ _ ,p cohT A (refC Γ _) α ,p refT A α) rr)
            ; symT = λ {_}{_}{p} q → let q' = ∣ sy ∣t (_ ,Σ (liftp q)) in unliftp (coeT R' ((symC Γ p ,p cohT A _ _) ,p symT A (cohT A _ _)) q')
            ; transT = λ {_}{_}{_}{p₀}{p₁} q₀ q₁ → let q' = ∣ tr ∣t (_ ,Σ liftp q₀ ,Σ coeT R' (_ ,p symT A (cohT A p₁ _) ,p cohT A (symC Γ p₁) _) (liftp q₁))
                                                   in unliftp (coeT R' (( _ ,p transT A (symT A (cohT A p₀ _)) (cohT A _ _) ) ,p symT A (cohT A _ _)) q')
            ; coeT = coeT A
            ; cohT = λ p α → unliftp (∣ r ∣t (_ ,Σ coeT A p α))
            }

  π : (t : Tm Γ A) → Tm Γ Q
  π t = record { ∣_∣t = ∣ t ∣t
               ; ~t = λ {γ}{γ'} p → unliftp (coeT R' (_ ,p transT A (symT A (~t t p)) (cohT A p (∣ t ∣t γ)) ,p refT A _)
                                                     (∣ r ∣t (_ ,Σ ∣ t ∣t γ'))) }

  eq : (t u : Tm Γ A)(x : Tm Γ (R' [ _,s_ < t > {_}{A [ wk {A = A} ]T} u ]T))
       → Tm Γ (Id (π t) (π u))
  eq t u e = record { ∣_∣t = λ γ → coeT R' ((refC Γ _ ,p cohT A (refC Γ _) (∣ t ∣t γ)) ,p refT A _) (∣ e ∣t γ)
                    ; ~t = λ p → liftP ttp }


  -- We use πe instead of π (and eqe instead of eq) to avoid considering the quotient of R[σ] in this module. See πe-ok below.
  πe : {Θ : Con}(σ : Tms Θ Γ)(t : Tm Θ (A [ σ ]T)) → Tm Θ (Q [ σ ]T)
  πe σ t = record { ∣_∣t = λ γ → ∣ t ∣t γ
                  ; ~t = λ {γ}{γ'} p → unliftp (coeT R' (_ ,p transT A (symT A (~t t p)) (cohT A (~s σ p) (∣ t ∣t γ)) ,p refT A _)
                                                       (∣ r ∣t (_ ,Σ ∣ t ∣t γ'))) }

  eqe : {Θ : Con}(σ : Tms Θ Γ)(t u : Tm Θ (A [ σ ]T))(e : Tm Θ (R' [ _,s_ (_,s_ σ {_}{A} t) {_}{wkT A A} u ]T))
       → Tm Θ (Id (πe σ t) (πe σ u))
  eqe σ t u e = record { ∣_∣t = λ γ → coeT R' (refC Γ _ ,p cohT A (refC Γ _) (∣ t ∣t γ) ,p refT A _) (∣ e ∣t γ)
                       ; ~t = λ p → liftP ttp }

  elim : (P : Ty Γ true)(π' : Tm (Γ , A) (wkT A P))
          (eq' : let ΓΓ = Γ , A , wkT A A , R' in
                 let wk₁ = wk' A in
                 let wk₂ = wk₁ ∘ wk' (wkT A A) in
                 let wk₃ = wk₂ ∘ wk' R' in
                 let x₀ : Tm ΓΓ (A [ wk₃ ]T)
                     x₀ = vs' (A [ wk₂ ]T) R' (vs' (A [ wk₁ ]T) (wkT A A) (vz' A)) in
                 let πx₀ : Tm ΓΓ (Q [ wk₃ ]T)
                     πx₀ = πe wk₃ x₀ in
                 let x₁ : Tm ΓΓ (A [ wk₃ ]T)
                     x₁ = vs' (A [ wk₂ ]T) R' (vz' (wkT A A)) in
                 let πx₁ : Tm ΓΓ (Q [ wk₃ ]T)
                     πx₁ = πe wk₃ x₁ in
                 let P' = P [ wk₃ ∘ wk' (Q [ wk₃ ]T) ]T in
                 Tm ΓΓ (Id {A = P' [ < πx₁ > ]T}
                           (let t = π' [ wk' (wkT A A) ∘ wk' R' ]t in transp {ΓΓ}{_}{Q [ wk₃ ]T}P'{πx₀}{πx₁}(eqe wk₃ x₀ x₁ (vz' R')) t)
                           (π' [ (wk' A ^ A) ∘ wk' R' ]t)))
          (w : Tm Γ Q)
          → Tm Γ P
  elim P π' eq' w = record { ∣_∣t = λ γ → ∣ π' ∣t (γ ,Σ ∣ w ∣t γ)
                            ; ~t = λ {γ}{γ'} p → let W = ~t π' {γ ,Σ ∣ w ∣t γ} {γ' ,Σ coeT A p (∣ w ∣t γ)} (p ,p cohT A p (∣ w ∣t γ)) in
                                                 let Z = unliftp (∣ eq' ∣t (γ' ,Σ coeT A p (∣ w ∣t γ) ,Σ ∣ w ∣t γ' ,Σ liftp (~t w p))) in
                                                 transT P W (transT P (cohT P _ _) Z) }

  elimπ : ∀{P}{π'}{eq'}{t : Tm Γ A} → elim P π' eq' (πe id t) ≡ π' [ < t > ]t
  elimπ = refl


  elimD : (P : Ty (Γ , Q) true)(π' : Tm (Γ , A) (P [ _,s_ (wk' A) {_}{Q} (πe (wk' A) (vz' A))  ]T))
          (eq' : let ΓΓ = Γ , A , wkT A A , R' in
                 let wk₁ = wk' A in
                 let wk₂ = wk₁ ∘ wk' (wkT A A) in
                 let wk₃ = wk₂ ∘ wk' R' in
                 let x₀ : Tm ΓΓ (A [ wk₃ ]T)
                     x₀ = vs' (A [ wk₂ ]T) R' (vs' (A [ wk₁ ]T) (wkT A A) (vz' A)) in
                 let πx₀ : Tm ΓΓ (Q [ wk₃ ]T)
                     πx₀ = πe wk₃ x₀ in
                 let x₁ : Tm ΓΓ (A [ wk₃ ]T)
                     x₁ = vs' (A [ wk₂ ]T) R' (vz' (wkT A A)) in
                 let πx₁ : Tm ΓΓ (Q [ wk₃ ]T)
                     πx₁ = πe wk₃ x₁ in
                 let P' = P [ wk₃ ^ Q ]T in
                 Tm ΓΓ (Id {A = P' [ < πx₁ > ]T}
                           (let t = π' [ wk' (wkT A A) ∘ wk' R' ]t in transp {ΓΓ}{_}{Q [ wk₃ ]T}P'{πx₀}{πx₁}(eqe wk₃ x₀ x₁ (vz' R')) t)
                           (π' [ (wk' A ^ A) ∘ wk' R' ]t)))
          (w : Tm Γ Q)
          → Tm Γ (P [ < w > ]T)
  elimD P π' eq' w = record { ∣_∣t = λ γ → ∣ π' ∣t (γ ,Σ ∣ w ∣t γ)
                            ; ~t = λ {γ}{γ'} p → let W = ~t π' {γ ,Σ ∣ w ∣t γ} {γ' ,Σ coeT A p (∣ w ∣t γ)} (p ,p cohT A p (∣ w ∣t γ)) in
                                                 let Z = unliftp (∣ eq' ∣t (γ' ,Σ coeT A p (∣ w ∣t γ) ,Σ ∣ w ∣t γ' ,Σ liftp (~t w p))) in
                                                 transT P (transT P W (cohT P _ _)) Z }

  elimDπ : ∀{P}{π'}{eq'}{t : Tm Γ A} → elimD P π' eq' (πe id t) ≡ π' [ < t > ]t
  elimDπ = refl



module SubstLaws {Γ : Con}{A : Ty Γ true}{R : Tm (Γ , A , wkT A A) ℙ₀}{r : Tm (Γ , A) (Elℙ₀ R [ < vz' A > ]T)}
                 {sy : Tm (_ , Elℙ₀ R) (Elℙ₀ (R [ swap A A ∘ wk' (Elℙ₀ R) ]t))}
                 {tr : let A₀ = A in let Γ₀ = Γ , A₀ in
                       let A₁ = wkT A₀ A₀ in let Γ₁ = Γ₀ , A₁ in
                       let A₂ = wkT A₁ A₁ in let Γ₂ = Γ₁ , A₂ in
                       let R₀ = wkT A₂ (Elℙ₀ R) in let Γ₃ = Γ₂ , R₀ in
                       let R₁ = Elℙ₀ R [ (wk' A ^ A ^ A₁) ∘ wk' R₀ ]T in let Γ₄ = Γ₃ , R₁ in
                       Tm Γ₄ (Elℙ₀ (R [ (skip A A A ∘ wk' R₀) ∘ wk' R₁ ]t))} where
  module S = Quotient A R r sy tr
  open Quotient

  πe[] : ∀{Θ Θ'}{σ : Tms Θ' Θ}{δ : Tms Θ Γ}{t : Tm Θ (A [ δ ]T)} → (S.πe δ t) [ σ ]t ≡ S.πe (δ ∘ σ) (t [ σ ]t)
  πe[] = refl

  eqe[] : ∀{Θ Θ'}{σ : Tms Θ' Θ}{δ : Tms Θ Γ}{t u : Tm Θ (A [ δ ]T)}{e : Tm Θ (Elℙ₀ R [ _,s_ (_,s_ δ {_}{A} t) {_}{wkT A A} u ]T)}
       → (S.eqe δ t u e) [ σ ]t ≡ S.eqe (δ ∘ σ) (t [ σ ]t) (u [ σ ]t) (e [ σ ]t)
  eqe[] = refl

  module Γ' {Γ' : Con}{σ : Tms Γ' Γ} where

    trσ = let A₁ = wkT A A in let A₂ = wkT A₁ A₁ in
          let R₀ = wkT A₂ (Elℙ₀ R) in let R₁ = Elℙ₀ R [ (wk' A ^ A ^ A₁) ∘ wk' R₀ ]T in
          σ ^ A ^ A₁ ^ A₂ ^ R₀ ^ R₁

    module T = Quotient (A [ σ ]T) (R [ σ ^ A ^ (wkT A A) ]t) (r [ (σ ^ A) ]t) (sy [ σ ^ A ^ (wkT A A) ^ Elℙ₀ R ]t) (tr [ trσ ]t)

    Q[] : S.Q [ σ ]T  ≡ T.Q
    Q[] = refl

    elim[] : ∀{P : Ty Γ true}{π' : Tm (Γ , A) (wkT A P)}{eq'}{w : Tm Γ S.Q}
              → (S.elim P π' eq' w) [ σ ]t ≡ T.elim (P [ σ ]T) (π' [ σ ^ A ]t) (eq' [ σ ^ A ^ wkT A A ^ Elℙ₀ R ]t) (w [ σ ]t)
    elim[] = refl

    elimD[] : ∀{P : Ty (Γ , S.Q) true}{π'}{eq'}{w : Tm Γ S.Q}
              → (S.elimD P π' eq' w) [ σ ]t ≡ T.elimD (P [ σ ^ S.Q ]T) (π' [ σ ^ A ]t) (eq' [ σ ^ A ^ wkT A A ^ Elℙ₀ R ]t) (w [ σ ]t)
    elimD[] = refl


    πe-ok : {t : Tm Γ' (A [ σ ]T)} → S.πe σ t ≡ T.π t
    πe-ok = refl

    eqe-ok : {t u : Tm Γ' (A [ σ ]T)}{e : Tm Γ' (Elℙ₀ R [ _,s_ (_,s_ σ {_}{A} t) {_}{wkT A A} u ]T)} → S.eqe σ t u e ≡ T.eq t u e
    eqe-ok = refl
