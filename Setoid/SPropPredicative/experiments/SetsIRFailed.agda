{-# OPTIONS --enable-prop #-}

module Setoid.SPropPredicative.experiments.SetsIRFailed where

-- we try to remedy the problem in the previous attempt by using an
-- inductive-recursive universe of sets.

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core

-- an inductive-recursive universe of sets

-- note that we use UIP and funext below

data U : Set
EL : U → Set

data U where
  ⊤s : U
  ⊥s : U
  Σs : (a : U)(b : EL a → U) → U
  Πs : (a : U)(b : EL a → U) → U

EL ⊤s = ⊤
EL ⊥s = ⊥
EL (Σs a b) = Σ (EL a) λ γ → EL (b γ)
EL (Πs a b) = (γ : EL a) → EL (b γ)

Σs= : {a a' : U}(a= : a ≡ a'){b : EL a → U}{b' : EL a' → U}(b= : b ≡[ ap (λ z → EL z → U) a= ]≡ b') → Σs a b ≡ Σs a' b'
Σs= refl refl = refl

Πs= : {a a' : U}(a= : a ≡ a'){b : EL a → U}{b' : EL a' → U}(b= : b ≡[ ap (λ z → EL z → U) a= ]≡ b') → Πs a b ≡ Πs a' b'
Πs= refl refl = refl

_~_ : U → U → Prop
reflect~ : {a b : U} → a ~ b → a ≡ b
⊤s ~ ⊤s = ⊤p
⊤s ~ ⊥s = ⊥p
⊤s ~ Σs a b = ⊥p
⊤s ~ Πs a b = ⊥p
⊥s ~ ⊤s = ⊥p
⊥s ~ ⊥s = ⊤p
⊥s ~ Σs a b = ⊥p
⊥s ~ Πs a b = ⊥p
Σs a b ~ ⊤s = ⊥p
Σs a b ~ ⊥s = ⊥p
Σs a b ~ Σs a' b' = Σp (a ~ a') λ a~ → {α : EL a}{α' : EL a'}(α~ : α ≡[ ap EL (reflect~ a~) ]≡ α') → b α ~ b' α'
Σs a b ~ Πs a' b' = ⊥p
Πs a b ~ ⊤s = ⊥p
Πs a b ~ ⊥s = ⊥p
Πs a b ~ Σs a' b' = ⊥p
Πs a b ~ Πs a' b' = Σp (a ~ a') λ a~ → {α : EL a}{α' : EL a'}(α~ : α ≡[ ap EL (reflect~ a~) ]≡ α') → b α ~ b' α'

reflect~ {⊤s} {⊤s} p = refl
reflect~ {⊤s} {⊥s} ()
reflect~ {⊤s} {Σs a b} ()
reflect~ {⊤s} {Πs a b} ()
reflect~ {⊥s} {⊤s} ()
reflect~ {⊥s} {⊥s} p = refl
reflect~ {⊥s} {Σs a b} ()
reflect~ {⊥s} {Πs a b} ()
reflect~ {Σs a b} {⊤s} ()
reflect~ {Σs a b} {⊥s} ()
reflect~ {Σs a b} {Σs a' b'} p = Σs= (reflect~ (proj₁p p)) (funext λ α → reflect~ (trp (λ z → z ~ b' α) (tr$ EL b (reflect~ (proj₁p p))) (proj₂p p {transport EL ((reflect~ (proj₁p p)) ⁻¹) α}{α} (trtr' EL (reflect~ (proj₁p p))))))
reflect~ {Σs a b} {Πs a' b'} ()
reflect~ {Πs a b} {⊤s} ()
reflect~ {Πs a b} {⊥s} ()
reflect~ {Πs a b} {Σs a' b'} ()
reflect~ {Πs a b} {Πs a' b'} p = Πs= (reflect~ (proj₁p p)) (funext λ α → reflect~ (trp (λ z → z ~ b' α) (tr$ EL b (reflect~ (proj₁p p))) (proj₂p p {transport EL ((reflect~ (proj₁p p)) ⁻¹) α}{α} (trtr' EL (reflect~ (proj₁p p))))))

open import JM

ref~ : (a : U) → a ~ a
ref~ ⊤s = ttp
ref~ ⊥s = ttp
ref~ (Σs a b) = ref~ a ,p λ {α}{α'} α= → trp (λ z → b α ~ b z) (loopcoe (ap EL (reflect~ (ref~ a))) ⁻¹ ◾ α=) (ref~ (b α))
ref~ (Πs a b) = ref~ a ,p λ {α}{α'} α= → trp (λ z → b α ~ b z) (loopcoe (ap EL (reflect~ (ref~ a))) ⁻¹ ◾ α=) (ref~ (b α))

coe~ : {a a' : U}(a~ : a ~ a') → EL a → EL a'
coe~ a~ u = transport EL (reflect~ a~) u

-- a universe of sets

Sets : {Γ : Con} → Ty Γ true
Sets {Γ} = record
  { ∣_∣T_ = λ _ → U
  ; _T_⊢_~_ = λ _ → _~_
  ; refT = ref~
  ; symT = {!!}
  ; transT = {!!}
  ; coeT = λ _ a → a
  ; cohT = λ _ → ref~
  }

El : {Γ : Con} → Tm Γ Sets → Ty Γ true
El {Γ} a = record
  { ∣_∣T_ = λ γ → EL (∣ a ∣t γ)
  ; _T_⊢_~_ = {!!}
  ; refT = {!!}
  ; symT = {!!}
  ; transT = {!!}
  ; coeT = λ γ~ → coe~ (~t a γ~)
  ; cohT = {!!}
  }
