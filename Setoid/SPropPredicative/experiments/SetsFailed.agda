{-# OPTIONS --without-K --enable-prop --rewriting #-}

module Setoid.SPropPredicative.experiments.SetsFailed where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core

-- equality into Prop

infix 4 _~_
infixl 4 _◾~_
infix 5 _⁻¹~

data _~_ {ℓ}{A : Set ℓ}(a : A) : A → Prop ℓ where
  refl~ : a ~ a

postulate
  tr~ : ∀{ℓ ℓ'}{A : Set ℓ}(P : A → Set ℓ'){x y : A}(p : x ~ y) → P x → P y
  tr~β : ∀{ℓ ℓ'}{A : Set ℓ}{P : A → Set ℓ'}{x : A}{p : x ~ x}{u : P x} → tr~ P p u ≡ u

{-# REWRITE tr~β #-}

coe~ : ∀{ℓ}{A A' : Set ℓ}(A~ : A ~ A') → A → A'
coe~ A~ = tr~ (λ x → x) A~


_⁻¹~ : ∀{ℓ}{A : Set ℓ}{a b : A} → a ~ b → b ~ a
_⁻¹~ refl~ = refl~

_◾~_ : ∀{ℓ}{A : Set ℓ}{a b c : A} → a ~ b → b ~ c → a ~ c
_◾~_ refl~ a~ = a~

_~[_]_ : ∀{ℓ}{A : Set ℓ}(a : A){A' : Set ℓ} → A ~ A' → A' → Prop ℓ
a ~[ A~ ] a' = coe~ A~ a ~ a'

refl~~ : ∀{ℓ}{A : Set ℓ}{a : A} → a ~[ refl~ ] a
refl~~ {ℓ}{A}{a} = refl~

_⁻¹~~ : ∀{ℓ}{A A' : Set ℓ}{A~ : A ~ A'}{a : A}{a' : A'} → a ~[ A~ ] a' → a' ~[ A~ ⁻¹~ ] a
_⁻¹~~ {A~ = refl~} refl~ = refl~

_◾~~_ : ∀{ℓ}{A A' A'' : Set ℓ}{A~ : A ~ A'}{A~' : A' ~ A''}{a : A}{a' : A'}{a'' : A''} →
  a ~[ A~ ] a' → a' ~[ A~' ] a'' → a ~[ A~ ◾~ A~' ] a''
_◾~~_ {A~ = refl~} {refl~} refl~ refl~ = refl~

-- a universe of sets

Sets : {Γ : Con} → Ty Γ false
Sets {Γ} = record
  { ∣_∣T_ = λ γ → Set
  ; _T_⊢_~_ = λ _ → _~_
  ; refT = λ _ → refl~
  ; symT = _⁻¹~
  ; transT = _◾~_
  ; coeT = λ _ a → a
  ; cohT = λ _ _ → refl~
  }

El : {Γ : Con} → Tm Γ Sets → Ty Γ true
El {Γ} a = record
  { ∣_∣T_ = λ γ → ∣ a ∣t γ
  ; _T_⊢_~_ = λ γ~ α α' → α ~[ ~t a γ~ ] α'
  ; refT = λ {γ} α → refl~~
  ; symT = _⁻¹~~
  ; transT = λ {_}{_}{_}{γ~}{γ~'}{_}{_}{_} α~ α~' → _◾~~_ {A~ = ~t a γ~}{A~' = ~t a γ~'} α~ α~'
  ; coeT = λ γ~ → coe~ (~t a γ~)
  ; cohT = λ _ _ → refl~ 
  }

-- this doesn't work: in Sets, we only embed the representations of
-- the setoids and we only consider them equal if their
-- representations are equal: so we forget about their equivalence
-- relations, which is bad! Now we can't turn a setoid into an element
-- here, because they don't respect the equivelence relations

⌜_⌝Sets : {Γ : Con} → Ty Γ true → Tm Γ Sets
⌜_⌝Sets {Γ} A = record
  { ∣_∣t = λ γ → ∣ A ∣T γ
  ; ~t   = λ { {γ}{γ'} γ~ → {!!} }
  }

π : {Γ : Con}(a : Tm Γ Sets)(b : Tm (Γ ▷ El a) Sets) → Tm Γ Sets
π {Γ} a b = record
  { ∣_∣t = λ γ → (α : ∣ a ∣t γ) → ∣ b ∣t (γ ,Σ α)
  ; ~t = λ γ~ → {!!}
  }
  
-- if we want to do this properly, we need to redefine Sets to
-- something like this:

Sets1 : {Γ : Con} → Ty Γ false
Sets1 {Γ} = record
  { ∣_∣T_ = λ γ → Σ Set λ A → Σsp (A → A → Prop) λ _~A_ → (α : A) → α ~A α
  ; _T_⊢_~_ = λ _ → λ { (A ,Σ (~A ,sp rA)) (A' ,Σ (~A' ,sp rA')) → {!Σ (A → A') λ f → Σ (A' → A) λ g → ?!} } -- this cannot be a Prop because it includes a function from A → A'
  ; refT = λ _ → {!!}
  ; symT = {!!}
  ; transT = {!!}
  ; coeT = λ _ a → a
  ; cohT = λ _ _ → {!!}
  }


