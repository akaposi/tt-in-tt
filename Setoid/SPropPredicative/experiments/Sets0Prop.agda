module Setoid.SPropPredicative.experiments.Sets0Prop where

open import lib
open import Setoid.SProp.lib
open import Agda.Primitive

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core
open import Setoid.SPropPredicative.Props0

-- an inductive-recursive universe of sets

data U : Set₁
_~U_ : U → U → Prop₁
refU : (a : U) → a ~U a
symU : {a a' : U}(a~ : a ~U a') → a' ~U a
transU : {a a' a'' : U}(a~ : a ~U a')(a~' : a' ~U a'') → a ~U a''

El : U → Set₁
_⊢_~El_ : {a a' : U}(a~ : a ~U a') → El a → El a' → Prop₁
-- refEl defined separately below
symEl : {a a' : U}{a~ : a ~U a'}{x : El a}{x' : El a'}(x~ : a~ ⊢ x ~El x') → symU a~ ⊢ x' ~El x
transEl : {a a' a'' : U}{a~ : a ~U a'}{a~' : a' ~U a''}{x : El a}{x' : El a'}{x'' : El a''}
  (x~ : a~ ⊢ x ~El x')(x~' : a~' ⊢ x' ~El x'') → transU a~ a~' ⊢ x ~El x''
coeEl : {a a' : U}(a~ : a ~U a') → El a → El a'
cohEl : {a a' : U}(a~ : a ~U a')(x : El a) → a~ ⊢ x ~El coeEl a~ x

data U where
  ⊤s : U
  ⊥s : U
  Σs : (a : U)(b : El a → U)(refb : {x x' : El a}(x~ : refU a ⊢ x ~El x') → b x ~U b x') → U
  Πs : (a : U)(b : El a → U)(refb : {x x' : El a}(x~ : refU a ⊢ x ~El x') → b x ~U b x') → U
  prop : U
  ι : Prop → U

⊤s ~U ⊤s = ⊤p'
⊥s ~U ⊥s = ⊤p'
Σs a b refb ~U Σs a' b' refb' = Σp (a ~U a') λ a~ → {x : El a}{x' : El a'}(x~ : a~ ⊢ x ~El x') → b x ~U b' x'
Πs a b refb ~U Πs a' b' refb' = Σp (a ~U a') λ a~ → {x : El a}{x' : El a'}(x~ : a~ ⊢ x ~El x') → b x ~U b' x'
prop ~U prop = ⊤p'
ι a ~U ι b = LiftP ((a → b) ×p (b → a))
_ ~U _ = ⊥p'

refU ⊤s = ttp'
refU ⊥s = ttp'
refU (Σs a b refb) = refU a ,p λ x~ → refb x~
refU (Πs a b refb) = refU a ,p λ x~ → refb x~
refU prop = ttp'
refU (ι a) = liftP ((λ x → x) ,p (λ x → x))

symU {⊤s} {⊤s} a~ = ttp'
symU {⊤s} {⊥s} ()
symU {⊤s} {Σs a' b' refb'} ()
symU {⊤s} {Πs a' b' refb'} ()
symU {⊤s} {prop} ()
symU {⊥s} {⊤s} ()
symU {⊥s} {⊥s} a~ = ttp'
symU {⊥s} {Σs a' b' refb'} ()
symU {⊥s} {Πs a' b' refb'} ()
symU {⊥s} {prop} ()
symU {Σs a b refb} {⊤s} ()
symU {Σs a b refb} {⊥s} ()
symU {Σs a b refb} {Σs a' b' refb'}(a~ ,p b~) = symU a~ ,p λ x~ → symU (b~ (symEl x~))
symU {Σs a b refb} {Πs a' b' refb'} ()
symU {Σs a b refb} {prop} ()
symU {Πs a b refb} {⊤s} ()
symU {Πs a b refb} {⊥s} ()
symU {Πs a b refb} {Σs a' b' refb'} ()
symU {Πs a b refb} {Πs a' b' refb'}(a~ ,p b~) = symU a~ ,p λ x~ → symU (b~ (symEl x~))
symU {Πs a b refb} {prop} ()
symU {prop} {⊤s} ()
symU {prop} {⊥s} ()
symU {prop} {Σs a' b' refb'} ()
symU {prop} {Πs a' b' refb'} ()
symU {prop} {prop} a~ = ttp'
symU {ι a} {⊤s} ()
symU {ι a} {⊥s} ()
symU {ι a} {Σs b b₁ refb} ()
symU {ι a} {Πs b b₁ refb} ()
symU {ι a} {prop} ()
symU {ι a} {ι x₁} (liftP (f ,p g)) = liftP (g ,p f)

transU {⊤s} {⊤s} {⊤s} a~ a~' = ttp'
transU {⊤s} {⊤s} {⊥s} a~ ()
transU {⊤s} {⊤s} {Σs a'' b'' refb''} a~ ()
transU {⊤s} {⊤s} {Πs a'' b'' refb''} a~ ()
transU {⊤s} {⊤s} {prop} a~ ()
transU {⊤s} {⊥s} {_} ()
transU {⊤s} {Σs a' b refb'} {_} ()
transU {⊤s} {Πs a' b refb'} {_} ()
transU {⊤s} {prop} {_} ()
transU {⊥s} {⊤s} {_} ()
transU {⊥s} {⊥s} {⊤s} a~ ()
transU {⊥s} {⊥s} {⊥s} a~ a~' = ttp'
transU {⊥s} {⊥s} {Σs a'' b'' refb''} a~ ()
transU {⊥s} {⊥s} {Πs a'' b'' refb''} a~ ()
transU {⊥s} {⊥s} {prop} a~ ()
transU {⊥s} {Σs a' b' refb'} {_} ()
transU {⊥s} {Πs a' b' refb'} {_} ()
transU {⊥s} {prop} {_} ()
transU {Σs a b refb} {⊤s} {_} ()
transU {Σs a b refb} {⊥s} {_} ()
transU {Σs a b refb} {Σs a' b' refb'} {⊤s} a~ ()
transU {Σs a b refb} {Σs a' b' refb'} {⊥s} a~ ()
transU {Σs a b refb} {Σs a' b' refb'} {Σs a'' b'' refb''} (a~ ,p b~) (a~' ,p b~') =
  transU a~ a~' ,p λ {x}{x''} x~ → transU (b~ (cohEl a~ x)) (transU (refb' (transEl (symEl (cohEl a~ x)) (transEl x~ (cohEl (symU a~') x'')) )) (b~' (symEl (cohEl (symU a~') x''))))
transU {Σs a b refb} {Σs a' b' refb'} {Πs a'' b'' refb''} a~ ()
transU {Σs a b refb} {Σs a' b' refb'} {prop} a~ ()
transU {Σs a b refb} {Πs a' b' refb'} {_} ()
transU {Σs a b refb} {prop} {_} ()
transU {Πs a b refb} {⊤s} {_} ()
transU {Πs a b refb} {⊥s} {_} ()
transU {Πs a b refb} {Σs a' b' refb'} {_} ()
transU {Πs a b refb} {Πs a' b' refb'} {⊤s} a~ ()
transU {Πs a b refb} {Πs a' b' refb'} {⊥s} a~ ()
transU {Πs a b refb} {Πs a' b' refb'} {Σs a'' b'' refb''} a~ ()
transU {Πs a b refb} {Πs a' b' refb'} {prop} a~ ()
transU {Πs a b refb} {Πs a' b' refb'} {Πs a'' b'' refb''} (a~ ,p b~) (a~' ,p b~') =
  transU a~ a~' ,p λ {x}{x''} x~ → transU (b~ (cohEl a~ x)) (transU (refb' (transEl (symEl (cohEl a~ x)) (transEl x~ (cohEl (symU a~') x'')) )) (b~' (symEl (cohEl (symU a~') x''))))
transU {Πs a b refb} {prop} {_} ()
transU {prop} {⊤s} {_} ()
transU {prop} {⊥s} {_} ()
transU {prop} {Σs a' b refb'} {_} ()
transU {prop} {Πs a' b refb'} {_} ()
transU {prop} {prop} {⊤s} a~ ()
transU {prop} {prop} {⊥s} a~ ()
transU {prop} {prop} {Σs a'' b'' refb''} a~ ()
transU {prop} {prop} {Πs a'' b'' refb''} a~ ()
transU {prop} {prop} {prop} a~ a~' = ttp'
transU {ι a} {⊤s} {c} (liftP ())
transU {ι a} {⊥s} {c} (liftP ())
transU {ι a} {Σs b b₁ refb} {c} (liftP ())
transU {ι a} {Πs b b₁ refb} {c} (liftP ())
transU {ι a} {prop} {c} (liftP ())
transU {ι a} {ι b} {⊤s} x (liftP ())
transU {ι a} {ι b} {⊥s} x (liftP ())
transU {ι a} {ι b} {Σs c b₁ refb} x (liftP ())
transU {ι a} {ι b} {Πs c b₁ refb} x (liftP ())
transU {ι a} {ι b} {prop} x (liftP ())
transU {ι a} {ι b} {ι c} (liftP (f ,p g)) (liftP (f' ,p g')) = liftP ((λ x → f' (f x)) ,p (λ y → g (g' y)))

El ⊤s = Lift ⊤
El ⊥s = Lift ⊥
El (Σs a b refb) = Σ (El a) λ x → El (b x)
El (Πs a b refb) = Σsp ((x : El a) → El (b x)) λ f → {x x' : El a}(x~ : refU a ⊢ x ~El x') → refb x~ ⊢ f x ~El f x'
El prop = Prop
El (ι a) = Lift (Liftp a)

_⊢_~El_ {⊤s} {⊤s} a~ x x' = ⊤p'
_⊢_~El_ {⊤s} {⊥s} () x x'
_⊢_~El_ {⊤s} {Σs a' b' refb'} () x x'
_⊢_~El_ {⊤s} {Πs a' b' refb'} () x x'
_⊢_~El_ {⊤s} {prop} () x x'
_⊢_~El_ {⊤s} {ι a} () x x'
_⊢_~El_ {⊥s} {⊤s} () x x'
_⊢_~El_ {⊥s} {⊥s} a~ x x' = ⊤p'
_⊢_~El_ {⊥s} {Σs a' b' refb'} () x x'
_⊢_~El_ {⊥s} {Πs a' b' refb'} () x x'
_⊢_~El_ {⊥s} {prop} () x x'
_⊢_~El_ {⊥s} {ι a} () x x'
_⊢_~El_ {Σs a b refb} {⊤s} () x x'
_⊢_~El_ {Σs a b refb} {⊥s} () x x'
_⊢_~El_ {Σs a b refb} {Σs a' b' refb'} w~ (x ,Σ y) (x' ,Σ y') =
  Σp (proj₁p w~ ⊢ x ~El x') λ x~ → proj₂p w~ x~ ⊢ y ~El y'
_⊢_~El_ {Σs a b refb} {Πs a' b' refb'} () x x'
_⊢_~El_ {Σs a b refb} {prop} () x x'
_⊢_~El_ {Σs a b refb} {ι c} () x x'
_⊢_~El_ {Πs a b refb} {⊤s} () x x'
_⊢_~El_ {Πs a b refb} {⊥s} () x x'
_⊢_~El_ {Πs a b refb} {Σs a' b' refb'} () x x'
_⊢_~El_ {Πs a b refb} {Πs a' b' refb'} w~ (f ,sp _) (f' ,sp _) =
  {x : El a}{x' : El a'}(x~ : proj₁p w~ ⊢ x ~El x') → proj₂p w~ x~ ⊢ f x ~El f' x'
_⊢_~El_ {Πs a b refb} {prop} () x x'
_⊢_~El_ {Πs a b refb} {ι c} () x x'
_⊢_~El_ {prop} {⊤s} () x x'
_⊢_~El_ {prop} {⊥s} () x x'
_⊢_~El_ {prop} {Σs a' b' refb'} () x x'
_⊢_~El_ {prop} {Πs a' b' refb'} () x x'
_⊢_~El_ {prop} {prop} _ a b = LiftP ((a → b) ×p (b → a))
_⊢_~El_ {prop} {ι a} () x x'
_⊢_~El_ {ι a} {⊤s} ()
_⊢_~El_ {ι a} {⊥s} ()
_⊢_~El_ {ι a} {Σs b b₁ refb} ()
_⊢_~El_ {ι a} {Πs b b₁ refb} ()
_⊢_~El_ {ι a} {prop} ()
_⊢_~El_ {ι a} {ι b} _ x y = ⊤p'

symEl {⊤s} {⊤s} {a~} {x} {x'} x~ = ttp'
symEl {⊤s} {⊥s} {()}
symEl {⊤s} {Σs a' b' refb'} {()}
symEl {⊤s} {Πs a' b' refb'} {()}
symEl {⊥s} {⊤s} {()}
symEl {⊥s} {⊥s} {a~} {x} {x'} x~ = ttp'
symEl {⊥s} {Σs a' b' refb'} {()}
symEl {⊥s} {Πs a' b' refb'} {()}
symEl {Σs a b refb} {⊤s} {()}
symEl {Σs a b refb} {⊥s} {()}
symEl {Σs a b refb} {Σs a' b' refb'} {w~} {x ,Σ y} {x' ,Σ y'}(x~ ,p y~) = symEl x~ ,p symEl y~
symEl {Σs a b refb} {Πs a' b' refb'} {()}
symEl {Πs a b refb} {⊤s} {()}
symEl {Πs a b refb} {⊥s} {()}
symEl {Πs a b refb} {Σs a' b' refb'} {()}
symEl {Πs a b refb} {Πs a' b' refb'} {w~} {f ,sp _} {f' ,sp _} f~ {x}{x'} x~ = symEl (f~ (symEl x~))
symEl {⊤s} {prop} {liftP ()}
symEl {⊥s} {prop} {liftP ()}
symEl {Σs a b refb} {prop} {liftP ()}
symEl {Πs a b refb} {prop} {liftP ()}
symEl {prop} {⊤s} {liftP ()}
symEl {prop} {⊥s} {liftP ()}
symEl {prop} {Σs b b₁ refb} {liftP ()}
symEl {prop} {Πs b b₁ refb} {liftP ()}
symEl {prop} {prop} {ttp'} (liftP (f ,p g)) = liftP (g ,p f)
symEl {ι a} {⊤s} {liftP ()}
symEl {ι a} {⊥s} {liftP ()}
symEl {ι a} {Σs b b₁ refb} {liftP ()}
symEl {ι a} {Πs b b₁ refb} {liftP ()}
symEl {ι a} {prop} {liftP ()}
symEl {ι a} {ι b} x = x

transEl {⊤s} {⊤s} {⊤s} {_} {_} {x} {x'} {x''} x~ x~' = ttp'
transEl {⊤s} {⊤s} {⊥s} {_} {()}
transEl {⊤s} {⊤s} {Σs a'' b'' refb''} {_} {()}
transEl {⊤s} {⊤s} {Πs a'' b'' refb''} {_} {()}
transEl {⊤s} {⊥s} {_} {()}
transEl {⊤s} {Σs a' b' refb'} {_} {()}
transEl {⊤s} {Πs a' b' refb'} {_} {()}
transEl {⊥s} {⊤s} {_} {()}
transEl {⊥s} {⊥s} {⊤s} {_} {()}
transEl {⊥s} {⊥s} {⊥s} {_} {_} {x} {x'} {x''} x~ x~' = ttp'
transEl {⊥s} {⊥s} {Σs a'' b'' refb''} {_} {()}
transEl {⊥s} {⊥s} {Πs a'' b'' refb''} {_} {()}
transEl {⊥s} {Σs a' b' refb'} {_} {()}
transEl {⊥s} {Πs a' b' refb'} {_} {()}
transEl {Σs a b refb} {⊤s} {_} {()}
transEl {Σs a b refb} {⊥s} {_} {()}
transEl {Σs a b refb} {Σs a' b' refb'} {⊤s} {_} {()}
transEl {Σs a b refb} {Σs a' b' refb'} {⊥s} {_} {()}
transEl {Σs a b refb} {Σs a' b' refb'} {Σs a'' b'' refb''}{_}{_}{x ,Σ y}{x' ,Σ y'}{x'' ,Σ y''}(x~ ,p y~)(x~' ,p y~') =
  transEl x~ x~' ,p transEl y~ y~'
transEl {Σs a b refb} {Σs a' b' refb'} {Πs a'' b'' refb''} {_} {()}
transEl {Σs a b refb} {Πs a' b' refb'} {_} {()}
transEl {Πs a b refb} {⊤s} {_} {()}
transEl {Πs a b refb} {⊥s} {_} {()}
transEl {Πs a b refb} {Σs a' b' refb'} {_} {()}
transEl {Πs a b refb} {Πs a' b' refb'} {⊤s} {_} {()}
transEl {Πs a b refb} {Πs a' b' refb'} {⊥s} {_} {()}
transEl {Πs a b refb} {Πs a' b' refb'} {Σs a'' b'' refb''}{_}{()}
transEl {Πs a b refb} {Πs a' b' refb'} {Πs a'' b'' refb''}{a~ ,p b~}{a~' ,p b~'}{f ,sp reff}{f' ,sp reff'}{f'' ,sp reff''} f~ f~' {x}{x''} x~ = transEl
  (f~ (cohEl a~ x)) (transEl
  (reff' (transEl (symEl (cohEl a~ x)) (transEl x~ (cohEl (symU a~') x'')) ))
  (f~' (symEl (cohEl (symU a~') x''))))
transEl {⊤s} {prop} {b} {liftP ()}
transEl {⊥s} {prop} {b} {liftP ()}
transEl {Σs a b refb} {prop} {x} {liftP ()}
transEl {Πs a b refb} {prop} {x} {liftP ()}
transEl {prop} {⊤s} {b} {liftP ()}
transEl {prop} {⊥s} {b} {liftP ()}
transEl {prop} {Σs b b₁ refb} {x} {liftP ()}
transEl {prop} {Πs b b₁ refb} {x} {liftP ()}
transEl {prop} {prop} {⊤s} {b} {liftP ()}
transEl {prop} {prop} {⊥s} {b} {liftP ()}
transEl {prop} {prop} {Σs b b₁ refb} {b} {liftP ()}
transEl {prop} {prop} {Πs b b₁ refb} {b} {liftP ()}
transEl {prop} {prop} {prop} (liftP (f ,p g)) (liftP (f' ,p g')) = liftP ((λ x → f' (f x)) ,p (λ y → g (g' y)))
transEl {ι a} {⊤s} {c} {liftP ()}
transEl {ι a} {⊥s} {c} {liftP ()}
transEl {ι a} {Σs b b₁ refb} {c} {liftP ()}
transEl {ι a} {Πs b b₁ refb} {c} {liftP ()}
transEl {ι a} {prop} {c} {liftP ()}
transEl {ι a} {ι b} {⊤s} {_} {liftP ()}
transEl {ι a} {ι b} {⊥s} {_} {liftP ()}
transEl {ι a} {ι b} {Σs c b₁ refb} {_} {liftP ()}
transEl {ι a} {ι b} {Πs c b₁ refb} {_} {liftP ()}
transEl {ι a} {ι b} {prop} {_} {liftP ()}
transEl {ι a} {ι b} {ι c} x y = ttp'

coeEl {⊤s} {⊤s} a~ x = lift tt
coeEl {⊤s} {⊥s} () x
coeEl {⊤s} {Σs a' b' refb'} () x
coeEl {⊤s} {Πs a' b' refb'} () x
coeEl {⊥s} {⊤s} () x
coeEl {⊥s} {⊥s} a~ x = x
coeEl {⊥s} {Σs a' b refb} () x
coeEl {⊥s} {Πs a' b refb} () x
coeEl {Σs a b refb} {⊤s} () x
coeEl {Σs a b refb} {⊥s} () x
coeEl {Σs a b refb} {Σs a' b' refb'} w~ (x ,Σ y) with proj₁p w~ | proj₂p w~
... | a~ | b~ = coeEl a~ x ,Σ coeEl (b~ (cohEl a~ x)) y
coeEl {Σs a b refb} {Πs a' b' refb'} () x
coeEl {Πs a b refb} {⊤s} () x
coeEl {Πs a b refb} {⊥s} () x
coeEl {Πs a b refb} {Σs a' b' refb'} () x
coeEl {Πs a b refb} {Πs a' b' refb'} w~ (f ,sp reff) with proj₁p w~ | proj₂p w~
... | a~ | b~ = (λ x' → coeEl (b~ (symEl (cohEl (symU a~) x'))) (f (coeEl (symU a~) x'))) ,sp λ {x}{x'} x~ → transEl
  (symEl (cohEl (b~ (symEl (cohEl (symU a~) x))) (f (coeEl (symU a~) x)))) (transEl
  (reff (transEl (symEl (cohEl (symU a~) x)) (transEl x~ (cohEl (symU a~) x'))))
  (cohEl (b~ (symEl (cohEl (symU a~) x'))) (f (coeEl (symU a~) x'))))
coeEl {⊤s} {prop} x = abort⊥p' x
coeEl {⊥s} {prop} x = abort⊥p' x
coeEl {Σs a b refb} {prop} x = abort⊥p' x
coeEl {Πs a b refb} {prop} x = abort⊥p' x
coeEl {prop} {⊤s} x = abort⊥p' x
coeEl {prop} {⊥s} x = abort⊥p' x
coeEl {prop} {Σs b b₁ refb} x = abort⊥p' x
coeEl {prop} {Πs b b₁ refb} x = abort⊥p' x
coeEl {prop} {prop} x p = p
coeEl {ι a} {⊤s} x = abort⊥p' x
coeEl {ι a} {⊥s} x = abort⊥p' x
coeEl {ι a} {Σs b b₁ refb} x = abort⊥p' x
coeEl {ι a} {Πs b b₁ refb} x = abort⊥p' x
coeEl {ι a} {prop} x = abort⊥p' x
coeEl {ι a} {ι b} fg (lift (liftp x)) = lift (liftp (proj₁p (unliftP fg) x))

cohEl {⊤s} {⊤s} a~ x = ttp'
cohEl {⊤s} {⊥s} ()
cohEl {⊤s} {Σs a' b' refb'} ()
cohEl {⊤s} {Πs a' b' refb'} ()
cohEl {⊥s} {⊤s} ()
cohEl {⊥s} {⊥s} a~ x = ttp'
cohEl {⊥s} {Σs a' b' refb'} ()
cohEl {⊥s} {Πs a' b' refb'} ()
cohEl {Σs a b refb} {⊤s} ()
cohEl {Σs a b refb} {⊥s} ()
cohEl {Σs a b refb} {Σs a' b' refb'}(a~ ,p b~)(x ,Σ y) = cohEl a~ x ,p cohEl (b~ (cohEl a~ x)) y 
cohEl {Σs a b refb} {Πs a' b' refb'} ()
cohEl {Πs a b refb} {⊤s} ()
cohEl {Πs a b refb} {⊥s} ()
cohEl {Πs a b refb} {Σs a' b' refb'} ()
cohEl {Πs a b refb} {Πs a' b' refb'}(a~ ,p b~)(f ,sp reff){x}{x'} x~ = transEl
  (reff (transEl x~ (cohEl (symU a~) x')))
  (cohEl (b~ (symEl (cohEl (symU a~) x'))) (f (coeEl (symU a~) x')))
cohEl {⊤s} {prop} (liftP ())
cohEl {⊥s} {prop} (liftP ())
cohEl {Σs a b refb} {prop} (liftP ())
cohEl {Πs a b refb} {prop} (liftP ())
cohEl {prop} {⊤s} (liftP ())
cohEl {prop} {⊥s} (liftP ())
cohEl {prop} {Σs b b₁ refb} (liftP ())
cohEl {prop} {Πs b b₁ refb} (liftP ())
cohEl {prop} {prop} _ p = liftP ((λ x → x) ,p (λ x → x))
cohEl {ι a} {⊤s} (liftP ())
cohEl {ι a} {⊥s} (liftP ())
cohEl {ι a} {Σs b b₁ refb} (liftP ())
cohEl {ι a} {Πs b b₁ refb} (liftP ())
cohEl {ι a} {prop} (liftP ())
cohEl {ι a} {ι b} (liftP (f ,p g)) x = ttp'


refEl : {a : U}(x : El a) → refU a ⊢ x ~El x
refEl {⊤s} x = ttp'
refEl {⊥s} x = ttp'
refEl {Σs a b refb} (x ,Σ y) = refEl x ,p refEl y
refEl {Πs a b refb} (f ,sp reff) = reff
refEl {prop} x = liftP ((λ x → x) ,p (λ x → x))
refEl {ι a} x = ttp'




-- a universe of sets

𝕊₁ : {Γ : Con} → Ty Γ false -- should be Ty2
𝕊₁ {Γ} = record
  { ∣_∣T_ = λ _ → U
  ; _T_⊢_~_ = λ _ → _~U_
  ; refT = refU
  ; symT = symU
  ; transT = transU
  ; coeT = λ _ a → a
  ; cohT = λ _ → refU
  }

El𝕊₁ : {Γ : Con} → Tm Γ 𝕊₁ → Ty Γ false
El𝕊₁ {Γ} a = record
  { ∣_∣T_ = λ γ → El (∣ a ∣t γ)
  ; _T_⊢_~_ = λ γ~ → ~t a γ~ ⊢_~El_
  ; refT = refEl
  ; symT = symEl
  ; transT = transEl
  ; coeT = λ γ~ → coeEl (~t a γ~)
  ; cohT = λ γ~ → cohEl (~t a γ~)
  }

𝕊₁[] : ∀{Γ Δ}{σ : Tms Γ Δ} → (𝕊₁ [ σ ]T) ≡ 𝕊₁
𝕊₁[] = refl

El𝕊₁[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ 𝕊₁}
     → (El𝕊₁ a [ σ ]T) ≡ (El𝕊₁ (a [ σ ]t))
El𝕊₁[] = refl


-- closed under ⊤

unit : {Γ : Con} → Tm Γ 𝕊₁
unit {Γ} = record { ∣_∣t = λ _ → ⊤s ; ~t = λ _ → ttp' }

unit[] : ∀{Γ Θ}{σ : Tms Γ Θ} → unit [ σ ]t ≡ unit
unit[] = refl

* : ∀{Γ} → Tm Γ (El𝕊₁ unit)
* {Γ} = record { ∣_∣t = λ _ → lift tt ; ~t = λ _ → ttp' }

*[] : ∀{Γ Θ}{σ : Tms Γ Θ} → * [ σ ]t ≡ *
*[] = refl

-- closed under ⊥

void : {Γ : Con} → Tm Γ 𝕊₁
void {Γ} = record { ∣_∣t = λ _ → ⊥s ; ~t = λ _ → ttp' }

void[] : ∀{Γ Θ}{σ : Tms Γ Θ} → void [ σ ]t ≡ void
void[] = refl

abort : ∀{Γ b}{C : Ty Γ b} → Tm Γ (El𝕊₁ void) → Tm Γ C
abort {Γ}{C} t = record { ∣_∣t = λ γ → ⊥-elim (unlift (∣ t ∣t γ)) ; ~t = λ {γ} _ → abortp (unlift (∣ t ∣t γ)) }

abort[] : ∀{Γ Θ b}{σ : Tms Γ Θ}{C : Ty Θ b}{t : Tm Θ (El𝕊₁ void)} → _[_]t {Γ}{Θ}{b}{C} (abort t) σ ≡ abort (t [ σ ]t)
abort[] = refl

-- closed under Σ

ΣΣ : ∀{Γ}(a : Tm Γ 𝕊₁)(b : Tm (Γ , El𝕊₁ a) 𝕊₁) → Tm Γ 𝕊₁
ΣΣ {Γ} a b = record {
  ∣_∣t = λ γ → Σs (∣ a ∣t γ) (λ x → ∣ b ∣t (γ ,Σ x)) λ x~ → ~t b (refC Γ γ ,p x~) ;
  ~t = λ γ~ → ~t a γ~ ,p λ x~ → ~t b (γ~ ,p x~) }

Σ[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ 𝕊₁}{b : Tm (Δ , El𝕊₁ a) 𝕊₁} → (ΣΣ a b) [ σ ]t ≡ ΣΣ (a [ σ ]t) (b [ σ ^ El𝕊₁ a ]t)
Σ[] = refl

_,ΣΣ_ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}(u : Tm Γ (El𝕊₁ a))(v : Tm Γ (El𝕊₁ b [ < u > ]T)) → Tm Γ (El𝕊₁ (ΣΣ a b))
u ,ΣΣ v = record { ∣_∣t = λ γ → ∣ u ∣t γ ,Σ ∣ v ∣t γ ; ~t = λ γ~ → ~t u γ~ ,p ~t v γ~ }

proj₁Σ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}(w : Tm Γ (El𝕊₁ (ΣΣ a b))) → Tm Γ (El𝕊₁ a)
proj₁Σ w = record { ∣_∣t = λ γ → proj₁ (∣ w ∣t γ) ; ~t = λ γ~ → proj₁p (~t w γ~)  }

proj₂Σ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}(w : Tm Γ (El𝕊₁ (ΣΣ a b))) → Tm Γ (El𝕊₁ b [ < proj₁Σ {Γ}{a}{b} w > ]T)
proj₂Σ w = record { ∣_∣t = λ γ → proj₂ (∣ w ∣t γ) ; ~t = λ γ~ → proj₂p (~t w γ~)  }

,Σ[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ 𝕊₁}{b : Tm (Δ , El𝕊₁ a) 𝕊₁}{u : Tm Δ (El𝕊₁ a)}{v : Tm Δ (El𝕊₁ b [ < u > ]T)} →
  (_,ΣΣ_ {Δ}{a}{b} u v) [ σ ]t ≡ _,ΣΣ_ {Γ}{a [ σ ]t}{b [ σ ^ El𝕊₁ a ]t} (u [ σ ]t) (v [ σ ]t)
,Σ[] = refl

Σβ₁ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}{u : Tm Γ (El𝕊₁ a)}{v : Tm Γ (El𝕊₁ b [ < u > ]T)} →
  proj₁Σ {Γ}{a}{b}(_,ΣΣ_ {Γ}{a}{b} u v) ≡ u
Σβ₁ = refl

Σβ₂ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}{u : Tm Γ (El𝕊₁ a)}{v : Tm Γ (El𝕊₁ b [ < u > ]T)} →
  proj₂Σ {Γ}{a}{b}(_,ΣΣ_ {Γ}{a}{b} u v) ≡ v
Σβ₂ = refl

Ση : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}{w : Tm Γ (El𝕊₁ (ΣΣ a b))} →
  (_,ΣΣ_ {Γ}{a}{b} (proj₁Σ {Γ}{a}{b} w) (proj₂Σ {Γ}{a}{b} w)) ≡ w
Ση = refl

-- closed under Π

Π : ∀{Γ}(a : Tm Γ 𝕊₁)(b : Tm (Γ , El𝕊₁ a) 𝕊₁) → Tm Γ 𝕊₁
Π {Γ} a b = record {
  ∣_∣t = λ γ → Πs (∣ a ∣t γ) (λ x → ∣ b ∣t (γ ,Σ x)) λ x~ → ~t b (refC Γ γ ,p x~) ;
  ~t = λ γ~ → ~t a γ~ ,p λ x~ → ~t b (γ~ ,p x~) }

Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{a : Tm Δ 𝕊₁}{b : Tm (Δ , El𝕊₁ a) 𝕊₁} → (Π a b) [ σ ]t ≡ Π (a [ σ ]t) (b [ σ ^ El𝕊₁ a ]t)
Π[] = refl

lam : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁} → Tm (Γ , El𝕊₁ a) (El𝕊₁ b) → Tm Γ (El𝕊₁ (Π a b))
lam {Γ} t = record {
  ∣_∣t = λ γ → (λ x → ∣ t ∣t (γ ,Σ x)) ,sp (λ x~ → ~t t (refC Γ γ ,p x~)) ;
  ~t = λ γ~ x~ → ~t t (γ~ ,p x~) }

app : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁} → Tm Γ (El𝕊₁ (Π a b)) → Tm (Γ , El𝕊₁ a) (El𝕊₁ b)
app {Γ} t = record {
  ∣_∣t = λ { (γ ,Σ x) → proj₁sp (∣ t ∣t γ) x } ;
  ~t = λ { (γ~ ,p x~) → ~t t γ~ x~ } }

lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{a : Tm Δ 𝕊₁}{b : Tm (Δ , El𝕊₁ a) 𝕊₁}{t : Tm (Δ , El𝕊₁ a) (El𝕊₁ b)}                       
      → (lam t) [ δ ]t ≡ lam (t [ δ ^ El𝕊₁ a ]t)
lam[] = refl

Πβ : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}{t : Tm (Γ , El𝕊₁ a) (El𝕊₁ b)} → app {Γ}(lam t) ≡ t
Πβ = refl

Πη : ∀{Γ}{a : Tm Γ 𝕊₁}{b : Tm (Γ , El𝕊₁ a) 𝕊₁}{t : Tm Γ (El𝕊₁ (Π a b))} → lam (app {Γ}{a}{b} t) ≡ t
Πη = refl


-- containing a code for Prop

prop𝕊₁ : {Γ : Con} → Tm Γ 𝕊₁
prop𝕊₁ {Γ} = record { ∣_∣t = λ _ → prop ; ~t = λ _ → ttp' }

prop𝕊₁[] : ∀{Γ Θ}{σ : Tms Γ Θ} → prop𝕊₁ [ σ ]t ≡ prop𝕊₁
prop𝕊₁[] = refl

Elprop𝕊₁ : {Γ : Con} → El𝕊₁ (prop𝕊₁ {Γ}) ≡ ℙ₀
Elprop𝕊₁ = refl

-- containing Prop (explicit subtyping coercion)

ιp₀₁ : {Γ : Con} → Tm Γ ℙ₀ → Tm Γ 𝕊₁
ιp₀₁ {Γ} a = record { ∣_∣t = λ γ → ι (∣ a ∣t γ) ; ~t = λ p → ~t a p }

ιp₀₁[] : ∀{Γ Θ}{σ : Tms Γ Θ}(t : Tm Θ ℙ₀) → (ιp₀₁ t) [ σ ]t ≡ ιp₀₁ (t [ σ ]t)
ιp₀₁[] t = refl

TyLift : {Γ : Con} → Ty Γ true → Ty Γ false
TyLift A = record
             { ∣_∣T_ = λ γ → Lift (∣ A ∣T γ)
             ; _T_⊢_~_ = λ {p (lift x) (lift y) → LiftP (A T p ⊢ x ~ y) }
             ; refT = λ α → liftP (refT A _)
             ; symT = λ {(liftP p) → liftP (symT  A p)}
             ; transT = λ {(liftP p) (liftP q) → liftP (transT A p q)}
             ; coeT = λ {p (lift x) → lift (coeT A p x)}
             ; cohT = λ {p (lift α) → liftP (cohT A p α)}
             }

Elιp₀₁ : {Γ : Con}(a : Tm Γ ℙ₀) → El𝕊₁ (ιp₀₁ a)  ≡  TyLift (Elℙ₀ a)
Elιp₀₁ a = refl
