{-# OPTIONS --without-K --prop #-}

module Setoid.SPropPredicative.Iden where

open import lib
open import Setoid.SProp.lib

open import Setoid.SPropPredicative.Decl
open import Setoid.SPropPredicative.Core
open import Setoid.SPropPredicative.Props0

Id : {Γ : Con}{b : Bool}{A : Ty Γ b} → Tm Γ A → Tm Γ A → Ty Γ b
Id {Γ}{b}{A} t u = record
  { ∣_∣T_ = λ γ → Liftp (A T refC Γ γ ⊢ ∣ t ∣t γ ~ ∣ u ∣t γ)
  ; _T_⊢_~_ = λ _ _ _ → LiftP ⊤p
  ; coeT = λ { {γ}{γ'} γ~ (liftp α~) → liftp (transT A (symT A (~t t γ~)) (transT A
                                                       α~
                                                       (~t u γ~))) }
  }

Id[] : ∀{Γ Θ b}{A : Ty Γ b}{u v : Tm Γ A}{σ : Tms Θ Γ}
     → Id u v [ σ ]T ≡ Id (u [ σ ]t) (v [ σ ]t)
Id[]  = refl

ref : ∀{Γ b}{A : Ty Γ b}(u : Tm Γ A) → Tm Γ (Id u u)
ref {Γ}{A} u = record { ∣_∣t = λ γ → liftp (~t u (refC Γ γ)) }

ref[] : ∀{Γ Θ}{b}{A : Ty Γ b} {u : Tm Γ A}{σ : Tms Θ Γ} → ref u [ σ ]t ≡ ref (u [ σ ]t)
ref[] = refl

transp : ∀{Γ b}{A : Ty Γ b}(P : Ty (Γ , A) b){t u : Tm Γ A}(e : Tm Γ (Id t u)) →
  Tm Γ (P [ < t > ]T) → Tm Γ (P [ < u > ]T)
transp {Γ}{true}{A} P {t}{u} e w = record
  { ∣_∣t = λ γ → coeT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ )
  ; ~t = λ {γ}{γ'} γ~ → transT P (symT P (cohT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ))) (transT P (~t w γ~) (cohT P (refC Γ γ' ,p unliftp (∣ e ∣t γ')) (∣ w ∣t γ')))
  }
transp {Γ}{false}{A} P {t}{u} e w = record
  { ∣_∣t = λ γ → coeT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ )
  ; ~t = λ {γ}{γ'} γ~ → transT P (symT P (cohT P (refC Γ γ ,p unliftp (∣ e ∣t γ)) (∣ w ∣t γ))) (transT P (~t w γ~) (cohT P (refC Γ γ' ,p unliftp (∣ e ∣t γ')) (∣ w ∣t γ')))
  }

transp[]t : ∀{Γ}{A : Ty Γ true}{P : Ty (Γ , A) true}{t u : Tm Γ A}{e : Tm Γ (Id t u)}
  {w : Tm Γ (P [ < t > ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = P [ <_> {A = A} u ]T} (transp P {t}{u} e w) σ ≡
  transp (P [ σ ^ A ]T) {t [ σ ]t}{u [ σ ]t}(_[_]t e σ) (_[_]t {A = P [ < t > ]T} w σ)
transp[]f : ∀{Γ}{A : Ty Γ false}{P : Ty (Γ , A) false}{t u : Tm Γ A}{e : Tm Γ (Id t u)}
  {w : Tm Γ (P [ < t > ]T)}{Θ : Con}{σ : Tms Θ Γ} →
  _[_]t {A = P [ <_> {A = A} u ]T} (transp P {t}{u} e w) σ ≡
  transp (P [ σ ^ A ]T) {t [ σ ]t}{u [ σ ]t}(_[_]t e σ) (_[_]t {A = P [ < t > ]T} w σ)
transp[]t = refl
transp[]f = refl

-- weak β rule

transpβt : ∀{Γ}{A : Ty Γ true} {P : Ty (Γ , A) true} {t : Tm Γ A}{w : Tm Γ (P [ < t > ]T)} → Tm Γ (Id (transp P {t}{t} (ref t) w) w)
transpβf : ∀{Γ}{A : Ty Γ false}{P : Ty (Γ , A) false}{t : Tm Γ A}{w : Tm Γ (P [ < t > ]T)} → Tm Γ (Id (transp P {t}{t} (ref t) w) w)
transpβt {Γ}{A}{P}{t}{w} = record { ∣_∣t = λ γ → liftp (symT P (cohT P (refC Γ γ ,p ~t t (refC Γ γ)) (∣ w ∣t γ))) }
transpβf {Γ}{A}{P}{t}{w} = record { ∣_∣t = λ γ → liftp (symT P (cohT P (refC Γ γ ,p ~t t (refC Γ γ)) (∣ w ∣t γ))) }


-- dependent eliminator

line : {Γ : Con}{b : Bool}{A : Ty Γ b}(a u : Tm Γ A)(p : Tm Γ (Id a u)) → Tms Γ (Γ , A , Id (a [ wk' A ]t) vz)
line {Γ}{true}{A}  a u p = _,s_ (_,s_ id {_}{A} u) {_}{Id (a [ wk' A ]t) (vz' A)} p
line {Γ}{false}{A} a u p = _,s_ (_,s_ id {_}{A} u) {_}{Id (a [ wk' A ]t) (vz' A)} p


IdJ : {Γ : Con}{b b' : Bool}{A : Ty Γ b}(u : Tm Γ A)(P : Ty (Γ , A , Id (u [ wk' A ]t) (vz' A)) b')
      (t : Tm Γ (P [ line u u (ref u) ]T)) {v : Tm Γ A}(p : Tm Γ (Id u v))
    → Tm Γ (P [ line u v p ]T)
IdJ {Γ}{true}{b}  u P t {v} p = record { ∣_∣t = λ γ → coeT P ((refC Γ _ ,p unliftp (∣ p ∣t γ)) ,p ttp') (∣ t ∣t γ)
                            ; ~t = λ {γ}{γ'} γ~ → transT P (symT P (cohT P _ _)) (transT P (~t t γ~) (cohT P _ _)) }
IdJ {Γ}{false}{b} u P t {v} p = record { ∣_∣t = λ γ → coeT P ((refC Γ _ ,p unliftp (∣ p ∣t γ)) ,p ttp') (∣ t ∣t γ)
                            ; ~t = λ {γ}{γ'} γ~ → transT P (symT P (cohT P _ _)) (transT P (~t t γ~) (cohT P _ _)) }

IdJβ : {Γ : Con}{b b' : Bool}{A : Ty Γ b}{u : Tm Γ A}{P : Ty (Γ , A , Id (u [ wk' A ]t) (vz' A)) b'}
       {t : Tm Γ (P [ line u u (ref u) ]T)}
       → Tm Γ (Id (IdJ u P t {u} (ref u)) t)
IdJβ {Γ}{true}{u = u}{P}{t} = record { ∣_∣t = λ γ → liftp (symT P (cohT P _ (∣ t ∣t γ))) }
IdJβ {Γ}{false}{u = u}{P}{t} = record { ∣_∣t = λ γ → liftp (symT P (cohT P _ (∣ t ∣t γ))) }

-- propositional extensionality

ext : {Γ : Con}{a b : Tm Γ ℙ₀} →
  Tm (Γ , Elℙ₀ a) (Elℙ₀ b [ wk {A = Elℙ₀ a} ]T) →
  Tm (Γ , Elℙ₀ b) (Elℙ₀ a [ wk {A = Elℙ₀ b} ]T) →
  Tm Γ (Id a b)
ext f g = record { ∣_∣t = λ γ → liftp (liftP ((λ α → unliftp (∣ f ∣t (γ ,Σ liftp α))) ,p
                                               λ β → unliftp (∣ g ∣t (γ ,Σ liftp β)))) }
