{-# OPTIONS --rewriting #-}

module Setoid.New1.Core where

open import lib

open import Setoid.New1.Decl

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t

• : Con
• = record
  { ∣_∣C   = ⊤
  ; _C_~_  = λ _ _ → ⊤
  ; symC   = λ _ → tt
  ; transC = λ _ _ → tt
  }

_▷_ : (Γ : Con) → Ty Γ → Con
Γ ▷ A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _C_~_  = λ { (γ ,Σ α)(γ' ,Σ α') → Σ (Γ C γ ~ γ') (A T_⊢ α ~ α') }
  ; symC   = λ { (p ,Σ r) → symC Γ p ,Σ symT A r }
  ; transC = λ { (p ,Σ r)(p' ,Σ r') → transC Γ p p' ,Σ transT A r r' }
  }

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
A [ σ ]T = record
  { ∣_∣T_   = λ γ → ∣ A ∣T (∣ σ ∣s γ)
  ; _T_⊢_~_ = λ p α α' → A T ~s σ p ⊢ α ~ α'
  ; symT    = λ r → transport (A T_⊢ _ ~ _) (syms σ _) (symT A r)
  ; transT  = λ r r' → transport (A T_⊢ _ ~ _) (transs σ _ _) (transT A r r')
  ; coeT    = λ p → coeT A (~s σ p)
  ; cohT    = λ p → cohT A (~s σ p)
  }

id : ∀{Γ} → Tms Γ Γ
id = record
  { ∣_∣s   = λ γ → γ
  ; ~s     = λ p → p
  ; syms   = λ _ → refl
  ; transs = λ _ _ → refl
  }

_∘_ : ∀{Γ Θ Δ} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
σ ∘ ν = record
  { ∣_∣s   = λ γ → ∣ σ ∣s (∣ ν ∣s γ)
  ; ~s     = λ p → ~s σ (~s ν p)
  ; syms   = λ p → syms σ (~s ν p) ◾ ap (~s σ) (syms ν p)
  ; transs = λ p p' → transs σ (~s ν p) (~s ν p') ◾ ap (~s σ) (transs ν p p')
  }

ε : ∀{Γ} → Tms Γ •
ε = record
  { ∣_∣s   = λ _ → tt
  ; ~s     = λ _ → tt
  ; syms   = λ _ → refl
  ; transs = λ _ _ → refl
  }

_,_ : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
σ , t = record
  { ∣_∣s   = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ
  ; ~s     = λ p → ~s σ p ,Σ ~t t p
  ; syms   = λ p → ,Σ= (syms σ p) (symt t p)
  ; transs = λ p p' → ,Σ= (transs σ p p') (transt t p p')
  }

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▷ A) →  Tms Γ Δ
π₁ σ = record
  { ∣_∣s   = λ γ → proj₁ (∣ σ ∣s γ)
  ; ~s     = λ p → proj₁ (~s σ p)
  ; syms   = λ p → ,Σ=0 (syms σ p)
  ; transs = λ p p' → ,Σ=0 (transs σ p p')
  }

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
_[_]t {A = A} t σ = record
  { ∣_∣t   = λ γ → ∣ t ∣t (∣ σ ∣s γ)
  ; ~t     = λ p → ~t t (~s σ p)
  ; symt   = λ p → [ ~t t ] symt t (~s σ p) ◾ syms σ p
  ; transt = λ p p' → [ ~t t ] transt t (~s σ p) (~s σ p') ◾ transs σ p p'
  }

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ {A = A} σ = record
  { ∣_∣t   = λ γ → proj₂ (∣ σ ∣s γ)
  ; ~t     = λ p → proj₂ (~s σ p)
  ; symt   = λ p → ,Σ=1 (syms σ p)
  ; transt = λ p p' → ,Σ=1 (transs σ p p')
  }

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T = refl

[][]T :
  {Γ Θ Δ : Con}{A : Ty Δ}{σ : Tms Θ Δ}{δ : Tms Γ Θ} → 
  (A [ σ ]T [ δ ]T) ≡ (A [ σ ∘ δ ]T)
[][]T {Γ}{Θ}{Δ}{A}{σ}{δ}
  = mkTy=
      refl refl
      (funexti λ γ → funexti λ γ' → funexti λ p → funexti λ α → funexti λ α' → funext λ r →
         trtr (A T_⊢ α' ~ α)  (~s σ) (syms δ p)     (syms σ (~s δ p)))
      (funexti λ γ → funexti λ γ' → funexti λ γ'' → funexti λ p → funexti λ q → funexti λ α → funexti λ α' → funexti λ α'' → funext λ r → funext λ r' →
         trtr (A T_⊢ α ~ α'') (~s σ) (transs δ p q) (transs σ (~s δ p) (~s δ q)))
      refl refl

idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ 
idl
  = mkTms=
      refl refl
      (funexti λ γ → funexti λ γ' → funext λ p → ◾lid _ ◾ apid _)
      (funexti λ γ → funexti λ γ' → funexti λ γ'' → funext λ p → funext λ p' → ◾lid _ ◾ apid _)

idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ
idr
  = mkTms=
      refl refl
      (funexti λ γ → funexti λ γ' → funext λ p → ◾rid _)
      (funexti λ γ → funexti λ γ' → funexti λ γ'' → funext λ p → funext λ p' → ◾rid _)

ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
    → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
ass  {σ = σ}{δ}{ν}
  = mkTms=
      refl refl
      (funexti λ γ → funexti λ γ' → funext λ p →
         ◾ap◾ap (~s δ) (~s σ) (syms ν p)      (syms δ (~s ν p))             (syms σ (~s δ (~s ν p))))
      (funexti λ γ → funexti λ γ' → funexti λ γ'' → funext λ p → funext λ p' →
         ◾ap◾ap (~s δ) (~s σ) (transs ν p p') (transs δ (~s ν p) (~s ν p')) (transs σ (~s δ (~s ν p)) (~s δ (~s ν p'))))

,∘ : ∀{Γ Θ Δ}{σ : Tms Θ Δ}{δ : Tms Γ Θ}{A : Ty Δ}{t : Tm Θ (A [ σ ]T)}
   → ((_,_ σ {A} t) ∘ δ) ≡ (_,_ (σ ∘ δ) {A} (coe (ap (Tm Γ) ([][]T {A = A}{σ}{δ})) (t [ δ ]t)))
,∘ {σ = σ}{δ}{A}{t}
  = mkTms=
      {!!}
      {!!}
      {!!}
      {!!}

π₁β : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)} → (π₁ {A = A}(_,_ σ {A} t)) ≡ σ
π₁β {σ = σ}{t}
  = mkTms= refl refl
      (funexti λ γ → funexti λ γ' → funext λ p → ,Σ=β0 (syms σ p) (symt t p))
      (funexti λ γ → funexti λ γ' → funexti λ γ'' → funext λ p → funext λ p' → ,Σ=β0 (transs σ p p') (transt t p p'))

πη : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ ▷ A)} → _,_ (π₁ {A = A} σ) {A} (π₂ {A = A} σ) ≡ σ
πη {σ = σ}
  = mkTms= refl refl
      (funexti λ γ → funexti λ γ' → funext λ p → ,Σ=η (syms σ p))
      (funexti λ γ → funexti λ γ' → funexti λ γ'' → funext λ p → funext λ p' → ,Σ=η (transs σ p p'))

εη : ∀{Γ}{σ : Tms Γ •} → σ ≡ ε
εη = mkTms= refl refl
            (funexti λ γ → funexti λ γ' → funext λ p → set⊤ _ _)
            (funexti λ γ → funexti λ γ' → funexti λ γ'' → funext λ p → funext λ p' → set⊤ _ _)

∣tr∣t :
  {Γ Δ : Con}
  {σ₀ σ₁ : Tms Γ Δ}(σ₂ : σ₀ ≡ σ₁)
  {A : Ty Δ}{t : Tm Γ (A [ σ₀ ]T)} →
  ∣ transport (λ z → Tm Γ (A [ z ]T)) σ₂ t ∣t ≡
  λ γ → transport (λ z → ∣ A [ z ]T ∣T γ) σ₂ ( ∣ t ∣t γ)
∣tr∣t refl = refl -- instead of this, we need: ∣ tr P (mkTm= ∣∣₂ ...) t ∣t ≡ tr ... ∣∣₂ t

π₂β : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}{t : Tm Γ (A [ σ ]T)}
    → π₂ {A = A}(_,_ σ {A} t) ≡[ ap (λ z → Tm Γ (A [ z ]T)) (π₁β {A = A}{σ}{t}) ]≡ t
π₂β {Γ}{Δ}{A}{σ}{t} =
  mkTm=
    (∣tr∣t (π₁β {A = A}{σ}{t}) ◾ {!!})
    {!!}
    {!!}
    {!!}
