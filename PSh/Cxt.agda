{-# OPTIONS --no-eta #-}

open import lib
open import Cats

module PSh.Cxt (C : Cat) where

open Cat C

∙ᴹ : PSh C
∙ᴹ = record
  { _$P_   = λ I → ⊤
  ; _$P_$_ = λ f → λ { tt → tt }
  ; idP    = refl
  ; compP  = refl }

_,Cᴹ_ : (Γ : PSh C) → FamPSh Γ → PSh C
_,Cᴹ_ = _,P_

infixl 5 _,Cᴹ_
