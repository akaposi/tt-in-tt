\documentclass{beamer}
\usetheme{default}
\useoutertheme{infolines}
\usepackage[greek,english,russian]{babel}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{tikz}

\usepackage{scrextend}
\usepackage[overlay,absolute]{textpos}
\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}
\textblockcolor{white}
\changefontsizes{13pt}

%include lhs2TeX.fmt
%include agda.fmt
%include lib.fmt

%\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize/enumerate subsubbody}{size=\normalsize}


\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

\renewcommand{\U}{\mathsf{U}}
\newcommand{\El}{\mathsf{El}}
\newcommand{\REN}{\mathsf{REN}}
\newcommand{\op}{\mathsf{op}}
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\Set}{\mathsf{Set}}
\newcommand{\PSh}{\mathsf{PSh}}
\newcommand{\FamPSh}{\mathsf{FamPSh}}
\renewcommand{\ll}{\llbracket}
\providecommand{\rr}{\rrbracket}
\newcommand{\Con}{\mathsf{Con}}
\newcommand{\Ty}{\mathsf{Ty}}
\newcommand{\Tm}{\mathsf{Tm}}
\newcommand{\Tms}{\mathsf{Tms}}
\newcommand{\R}{\mathsf{R}}
\newcommand{\TM}{\mathsf{TM}}
\newcommand{\NE}{\mathsf{NE}}
\newcommand{\NF}{\mathsf{NF}}
\newcommand{\q}{\mathsf{q}}
\renewcommand{\u}{\mathsf{u}}
\renewcommand{\ne}{\mathsf{ne}}
\newcommand{\nf}{\mathsf{nf}}
\newcommand{\lQ}{\mathsf{lQ}}
\newcommand{\lU}{\mathsf{lU}}
\renewcommand{\lq}{\mathsf{lq}}
\newcommand{\lu}{\mathsf{lu}}
\newcommand{\cul}{\ulcorner}
\newcommand{\cur}{\urcorner}
\newcommand{\norm}{\mathsf{norm}}
\newcommand{\Nf}{\mathsf{Nf}}
\newcommand{\Ne}{\mathsf{Ne}}
\newcommand{\Nfs}{\mathsf{Nfs}}
\newcommand{\Nes}{\mathsf{Nes}}
\newcommand{\ID}{\mathsf{ID}}
\newcommand{\id}{\mathsf{id}}
\newcommand{\nat}{\,\dot{\rightarrow}\,}
%\newcommand{\nat}{\overset{\mathsf{n}}{\ra}} % this is how we denote it in the formalisation
\newcommand{\Nat}{\overset{\mathsf{N}}{\ra}}
\renewcommand{\S}{\overset{\mathsf{s}}{\ra}} % we have it with uppercase S in the formalisation
\newcommand{\ap}{\mathsf{ap}}
\newcommand{\blank}{\mathord{\hspace{1pt}\text{--}\hspace{1pt}}} %from the book
%\newcommand{\blank}{\!{-}\!}
\newcommand{\lam}{\mathsf{lam}}
\newcommand{\app}{\mathsf{app}}
\newcommand{\circid}{\circ\hspace{-0.2em}\id}
\newcommand{\circcirc}{\circ\hspace{-0.2em}\circ}
\newcommand{\tr}[2]{\ensuremath{{}_{#1 *}\mathopen{}{#2}\mathclose{}}} % from the book
\newcommand{\M}{\mathsf{M}}
\renewcommand{\C}{\mathcal{C}}
\newcommand{\data}{\mathsf{data}}
\newcommand{\ind}{\hspace{1em}}
\newcommand{\idP}{\mathsf{idP}}
\newcommand{\compP}{\mathsf{compP}}
\newcommand{\idF}{\mathsf{idF}}
\newcommand{\compF}{\mathsf{compF}}
\newcommand{\proj}{\mathsf{proj}}
\newcommand{\ExpPSh}{\mathsf{ExpPSh}}
\newcommand{\map}{\mathsf{map}}
\newcommand{\Var}{\mathsf{Var}}
\newcommand{\Vars}{\mathsf{Vars}}
\newcommand{\vze}{\mathsf{vze}}
\newcommand{\vsu}{\mathsf{vsu}}
\newcommand{\wk}{\mathsf{wk}}
\newcommand{\neuU}{\mathsf{neuU}}
\newcommand{\neuEl}{\mathsf{neuEl}}
\newcommand{\var}{\mathsf{var}}
\newcommand{\natn}{\mathsf{natn}}
\newcommand{\natS}{\mathsf{natS}}
\newcommand{\LET}{\mathsf{let}}
\newcommand{\IN}{\mathsf{in}}
\newcommand{\refl}{\mathsf{refl}}
\newcommand{\zero}{\mathsf{zero}}
\newcommand{\suc}{\mathsf{suc}}
\newcommand{\trans}{\mathbin{\raisebox{0.5ex}{$\displaystyle\centerdot$}}}
\renewcommand{\quote}{\mathsf{quote}}
\newcommand{\unquote}{\mathsf{unquote}}
\newcommand{\Y}{\mathsf{Y}}
\renewcommand{\P}{\mathsf{P}}

\begin{document}

%---------------------------------------------------------------------

\begin{frame}[plain]
  \vspace{0.5cm}
  \begin{center}
    \textcolor{blue}{
      {\Large Normalisation by Evaluation for Dependent Types \\\vspace{0.2em}
        }
    }
    
    \vspace{0.7cm}

    Ambrus Kaposi \\

    Eötvös Loránd University, Budapest, Hungary \\

    \vspace{0.2cm}
    
    (j.w.w. Thorsten Altenkirch, University of Nottingham) \\

    \vspace{0.7cm}

    FSCD, Porto \\
    24 June 2016
  \end{center}
\end{frame}

%=====================================================================

\begin{frame}{Introduction}
\begin{itemize}
\item Goal: 
  \begin{itemize}
  \item Prove normalisation for a type theory with dependent types
  \item Using the metalanguage of type theory itself
  \end{itemize}
\item Structure of the talk:
  \begin{itemize}
  \item Representing type theory in type theory
  \item Specifying normalisation
  \item NBE for simple types
  \item NBE for dependent types
  \end{itemize}
\end{itemize}
\end{frame}

%=====================================================================

\section{Representing type theory in type theory}

%---------------------------------------------------------------------

\begin{frame}{Simple type theory the traditional way}
  Set of variables, alphabet including $\Rightarrow$, $\lambda$ etc.

  Well-formed expressions:
  \begin{alignat*}{5}
    & A && ::= \iota \,\arrowvert\, A \Rightarrow A' \\
    & \Gamma && ::= \cdot \,\arrowvert\, \Gamma,x:A \\
    & t && ::= x \,\arrowvert\, \lambda x.t \,\arrowvert\, t\,t'
  \end{alignat*}
  An inductively defined relation:
  \begin{equation*}
    \begin{gathered}
      \infer{\Gamma \vdash x : A}{(x:A) \in \Gamma}
    \end{gathered}
    \hspace{2em}
    \begin{gathered}
      \infer{\Gamma.x:B \vdash t : A}{\Gamma \vdash t : A}
    \end{gathered}
  \end{equation*}
  \begin{equation*}
    \begin{gathered}
      \infer{\Gamma \vdash \lambda x.t : A \ra B}{\Gamma,x:A \vdash t : B}
    \end{gathered}
    \hspace{2em}
    \begin{gathered}
      \infer{\Gamma \vdash t \,u : B}{\Gamma\vdash t : A \ra B & \Gamma\vdash u : A}
    \end{gathered}
  \end{equation*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Simple type theory in idealised Agda}
\begin{spec}
data Ty     : Set where
  ι         : Ty
  _⇒_       : Ty → Ty → Ty
data Con    : Set where
  •         : Con
  _,_       : Con → Ty → Con
data Var    : Con → Ty → Set where
  zero      : Var (Γ , A) A
  suc       : Var Γ A → Var (Γ , B) A
data Tm     : Con → Ty → Set where
  var       : Var Γ A → Tm Γ A
  lam       : Tm (Γ , A) B → Tm Γ (A ⇒ B)
  app       : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
\end{spec}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Rules for dependent function space and a base type}
  \begin{equation*}
    \infer{\Gamma \vdash \Pi(x:A).B}{\Gamma \vdash A && \Gamma.x:A \vdash B}
  \end{equation*}
  \begin{equation*}
    \begin{gathered}
      \infer{\Gamma \vdash \lambda x . t : \Pi(x:A).B}{\Gamma.x:A \vdash t : B}
    \end{gathered}
    \hspace{2em}
    \begin{gathered}
      \infer{\Gamma \vdash f \, a : B[x \mapsto a]}{\Gamma \vdash f : \Pi(x:A).B && \Gamma \vdash a : A}
    \end{gathered}
  \end{equation*}
  \begin{equation*}
    \begin{gathered}
      \infer{\Gamma \vdash \U}{\Gamma \vdash}
    \end{gathered}
    \hspace{2em}
    \begin{gathered}
      \infer{\Gamma \vdash \El\,\hat{A}}{\Gamma \vdash \hat{A} : \U}
    \end{gathered}
\end{equation*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{A typed syntax of dependent types (i)}
  \begin{itemize}
  \item Types depend on contexts \\
    $\Rightarrow$ We need induction induction.
  \begin{spec}
    data Con  : Set
    data Ty   : Con → Set
  \end{spec}
  \vspace{20em}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}[noframenumbering]{A typed syntax of dependent types (ii)}
  \begin{itemize}
  \item Types depend on contexts \\
    $\Rightarrow$ We need induction induction.
  \item Substitutions are mentioned in the application rule:
    \begin{equation*}
      \mathsf{app} : \Tm\,\Gamma\,(\Pi\,A\,B)\ra(a:\Tm\,\Gamma\,A)\ra\Tm\,\Gamma\,(B[a])
    \end{equation*}
    $\Rightarrow$ We define an explicit substitution calculus.
    \begin{spec}
    data Con  : Set
    data Ty   : Con → Set
    data Tms  : Con → Con → Set
    data Tm   : (Γ : Con) → Ty Γ → Set
      _[_]  : Ty Γ → Tms Δ Γ → Ty Δ
      ...
    \end{spec}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}[noframenumbering]{A typed syntax of dependent types (iii)}
  \begin{itemize}
  \item Types depend on contexts. \\
    $\Rightarrow$ We need induction induction.
  \item Substitutions are mentioned in the application rule: \\
    $\Rightarrow$ We define an explicit substitution calculus.
  \item The following conversion rule for terms:
    \begin{equation*}
      \infer{\Gamma \vdash t : B}{\Gamma \vdash A \sim B && \Gamma \vdash t : A}
    \end{equation*}
    $\Rightarrow$ Conversion (the relation including $\beta$, $\eta$)
    needs to be defined mutually with the syntax.
    \begin{itemize}
    \item We need to add 4 new members to the inductive inductive
      definition: $\sim$ for contexts, types, substitutions and terms.
    \end{itemize}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Representing conversion}
  \begin{itemize}
    \item Lots of boilerplate:
    \begin{itemize}
    \item The $\sim$ relations are equivalence relations
    \item Coercion rules
    \item Congruence rules
    \item We need to work with setoids
    \end{itemize}
  \item What we really want is to redefine equality |_≡_| for the
    types representing the syntax.
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Higher inductive types (HITs)}
  \begin{itemize}
  \item An idea from homotopy type theory: \\ constructors for
    equalities.\vspace{-0.5em}
  \item Example:
  \begin{spec}
    data I     : Set where
      left     : I
      right    : I
      segment  : left ≡ right
  \end{spec}\pause\vspace{-1.5em}
  \begin{spec}
    RecI  :  (Iᴹ : Set)
             (leftᴹ     : Iᴹ)
             (rightᴹ    : Iᴹ)
             (segmentᴹ  : leftᴹ ≡ rightᴹ)
          →  I → Iᴹ
  \end{spec}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Using the syntax}
  \begin{itemize}
    \item We define the syntax as a HIIT, the conversion rules are
      constructors: e.g. $\beta : \app\,(\lam\,t)\,u \equiv t[u].$
    \item The arguments of the non-dependent eliminator form a model
      of type theory, equivalent to Categories with Families.
      \begin{spec}
        record Model   : Set where
          field  Conᴹ  : Set
                 Tyᴹ   : Conᴹ → Set
                 Tmᴹ   : (Γ : Conᴹ) → Tyᴹ Γ → Set
                 lamᴹ  : Tmᴹ (Γ ,ᴹ A) Bᴹ → Tmᴹ Γ (Πᴹ A B)
                 bᴹ    : appᴹ (lamᴹ t) u ≡ t [ u ]ᴹ
                 ...
      \end{spec}
    \item The eliminator says that the syntax is the initial model.
  \end{itemize}
\end{frame}

%=====================================================================

\section{Specifying normalisation}

%---------------------------------------------------------------------

\newcommand\arcfrombottom{
  \begin{tikzpicture}[scale=0.03em]
    \draw (0,0) arc (0:180:0.5);
    \draw (0,0) edge[->] (0,-0.3);
    \draw (-1,0) edge (-1,-0.3);
  \end{tikzpicture}
}
\newcommand\arcfromtop{
  \begin{tikzpicture}[scale=0.03em]
    \draw (0,0) arc (180:360:0.5);
    \draw (0,0) edge[->] (0,0.3);
    \draw (1,0) edge (1,0.3);
  \end{tikzpicture}
}

\begin{frame}{Specifying normalisation}
  Neutral terms and normal forms (typed!):
  \begin{spec}
    n ::= x | n v              {-" \hspace{3em} "-} Ne Γ A
    v ::= n | λ x . v          {-" \hspace{3em} "-} Nf Γ A
  \end{spec}
  Normalisation is an isomorphism:
\begin{equation*}
   \text{completeness }\arcfromtop \hspace{1em} \norm\downarrow\begin{array}{l}\infer={\hspace{1em} \Nf \, \Gamma \, A \hspace{1em} }{\Tm \, \Gamma \, A}\end{array}\uparrow\cul\blank\cur \hspace{1em} \arcfrombottom\text{ stability}
\end{equation*}
Soundness is given by congruence of equality:
\begin{equation*}
t \equiv t' \ra \norm\,t \equiv \norm\,t'
\end{equation*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Normalisation by Evaluation (NBE)}
\begin{center}
\begin{tikzpicture}
\node (Syntax) at (2,4.3) {Syntax};
\node (Model) at (8,4.2) {Model};
\node (NF) at (2,3) {Normal forms};
\draw (2,2) circle (2cm);
\draw (2,2) circle (1.2cm);
\draw (8,2) circle (1.8cm);
\draw[thick,->] (4,2.6) -- node[above] {eliminator} (6.2,2.6);
\draw[thick,->] (6.2,1.4) -- node[above] {quote} (3.1,1.4);
\end{tikzpicture}
\end{center}\vspace{-1em}
\begin{itemize}
\item First formulation (Berger and Schwichtenberg, 1991)
\item Simply typed case (Altenkirch, Hofmann, Streicher 1995)
\item Dependent types using untyped realizers (Abel, Coquand, Dybjer,
  2007)
\end{itemize}
\end{frame}

%=====================================================================

\section{NBE for simple types}

%---------------------------------------------------------------------

\begin{frame}{The presheaf model}
  \begin{itemize}
  \item Presheaf models are proof-relevant versions of Kripke models.
  \item They are parameterised over a category, we choose $\REN$:
    objects are contexts, morphisms are lists of variables.  \pause
  \item A type $A$ is interpreted as a presheaf $\ll A\rr : \REN^\op
    \ra \Set$.
    \begin{itemize}
    \item Given a context $\Gamma$ we have $\ll A\rr_\Gamma : \Set$.
    \item Given a renaming $\beta:\REN(\Delta,\Gamma)$, there is a
      $\ll A\rr_\Gamma \ra \ll A\rr_\Delta$.
    \end{itemize}
    \pause
    \item The function type is interpreted as the ``possible world''
      function space: $\ll A\Ra B\rr_\Gamma =
      \forall\Delta.\REN(\Delta,\Gamma)\ra\ll A\rr_\Delta\ra\ll
      B\rr_\Delta$.
    \pause
    \item The interpretation of the base type is another parameter. We
      choose $\ll\iota\rr_\Gamma = \Nf\,\Gamma\,\iota$.
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Quotation}
  The quote function is a natural transformation
  \begin{equation*}
    \quote_A : \ll A \rr \,\,\dot{\ra}\,\, \Nf\,\blank\,A
  \end{equation*}
  i.e.
  \begin{equation*}
    \quote_{A\,\Gamma} : \ll A \rr_\Gamma \,\,\ra\,\, \Nf\,\Gamma\,A
  \end{equation*}
  Defined mutually with unquote:
  \begin{equation*}
    \unquote_A : \Ne\,\blank\,A \,\,\dot{\ra}\,\, \ll A \rr
  \end{equation*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Quote and unquote}
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\Ne\,\blank\,A$};
\node (XX) at (5,0) {${\color{white}\Sigma\,(\Tm\,\blank\,A \times \,\,\,}\ll A\rr{\color{white} )\,\mathsf{R}_A}$};
\node (NF) at (10,0) {$\Nf\,\blank\,A$};
\node[color=white] (TM) at (5,-3) {$\Tm\,\blank\,A$};
\draw[->] (NE) edge node[above] {$\unquote\phantom{'}_A$} (XX);
\draw[->] (XX) edge node[above] {$\quote\phantom{'}_A$} (NF);
\end{tikzpicture}
\end{center}
\vspace{10em}
\end{frame}

\begin{frame}{With completeness}
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\Ne\,\blank\,A$};
\node (XX) at (5,0) {$\Sigma\,(\Tm\,\blank\,A \times \ll A\rr)\,\mathsf{R}_A$};
\node (NF) at (10,0) {$\Nf\,\blank\,A$};
\node (TM) at (5,-3) {$\Tm\,\blank\,A$};
\draw[->] (NE) edge node[above] {$\unquote'_A$} (XX);
\draw[->] (XX) edge node[above] {$\quote'_A$} (NF);
\draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
\draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
\draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
\end{tikzpicture}
\end{center}
$\R_A$ is a presheaf logical relation between the syntax and the
presheaf model. It says equality at the base type.
\vspace{10em}
\end{frame}

%=====================================================================

\section{NBE for dependent types}

%---------------------------------------------------------------------

\begin{frame}{The presheaf model and quote}
Types are interpreted as families of presheaves.
\begin{alignat*}{5}
  & \ll\Gamma\rr && : \REN^\op \ra \Set \\
  & \ll \Gamma \vdash A \rr && : (\Delta : \REN) \ra \ll\Gamma\rr_\Delta \ra \Set
\end{alignat*}
\pause
We define quote for contexts and types mutually.
\begin{alignat*}{5}
  & \quote_\Gamma && : \ll\Gamma\rr \,\,\dot{\ra}\,\, \Nfs\,\blank\,\Gamma \\
  & \quote_{\Gamma\vdash A} && : (\alpha : \ll\Gamma\rr_\Delta) \ra \ll A\rr_\Delta\,\alpha \ra \Nf\,\Delta\,\big(A[\quote_{\Gamma, \Delta}\,\alpha]\big)
\end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Defining quote, first try}
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\Nes\,\blank\,\Gamma$};
\node (XX) at (5,0) {${\color{white} \Sigma\,(\Tms\,\blank\,\Gamma \times \,\,\,}\ll\Gamma\rr{\color{white})\,\mathsf{R}_\Gamma}$};
\node (NF) at (10,0) {$\Nfs\,\blank\,\Gamma$};
\node[color=white] (TM) at (5,-3) {$\Tms\,\blank\,\Gamma$};
\draw[->] (NE) edge node[above] {$\unquote_\Gamma$} (XX);
\draw[->] (XX) edge node[above] {$\quote_\Gamma$} (NF);
\end{tikzpicture}
\end{center}
\pause Quote for function space needs $\quote_A\,\circ\unquote_A
\equiv \id$.

\pause
This follows from the logical relation $\R_A$.

\pause
Let's define quote and completeness mutually!
\vspace{10em}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Defining quote, second try}
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\Nes\,\blank\,\Gamma$};
\node (XX) at (5,0) {$\Sigma\,(\Tms\,\blank\,\Gamma \times \ll\Gamma\rr)\,\mathsf{R}_\Gamma$};
\node (NF) at (10,0) {$\Nfs\,\blank\,\Gamma$};
\node (TM) at (5,-3) {$\Tms\,\blank\,\Gamma$};
\draw[->] (NE) edge node[above] {$\unquote_\Gamma$} (XX);
\draw[->] (XX) edge node[above] {$\quote_\Gamma$} (NF);
\draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
\draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
\draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
\end{tikzpicture}
\end{center}
\pause

For unquote at the function space we need to define a semantic
function which works for every input, not necessarily related by the
relation. But quote needs ones which are related!
\vspace{10em}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Defining quote, last try}
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\Nes\,\blank\,\Gamma$};
\node (XX) at (5,0) {$\Sigma\,(\Tms\,\blank\,\Gamma)\,\P_\Gamma$};
\node (NF) at (10,0) {$\Nfs\,\blank\,\Gamma$};
\node (TM) at (5,-3) {$\Tms\,\blank\,\Gamma$};
\draw[->] (NE) edge node[above] {$\unquote_\Gamma$} (XX);
\draw[->] (XX) edge node[above] {$\quote_\Gamma$} (NF);
\draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
\draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
\draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
\end{tikzpicture}
\end{center}
Use a presheaf logical predicate.

\vspace{10em}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Presheaf logical predicate}
  \begin{itemize}
  \item The Yoneda embedding of the syntax:
    \begin{alignat*}{5}
      & \Y_\Gamma && : \REN^\op \ra \Set && := \Tms\,\blank\,\Gamma \\
      & \Y_{A} && : \Sigma_\REN\,\Y_\Gamma\ra\Set && := \Tm\,\blank\,A[\blank] \\
      & \Y_\sigma && : \Y_\Gamma \nat \Y_\Delta && := \sigma\circ\blank \\
      & \Y_t && : \Y_\Gamma \S \Y_A && := t[\blank]
    \end{alignat*}
    \vspace{10em}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Presheaf logical predicate}
  \begin{itemize}
  \item The Yoneda embedding of the syntax.
  \item $\P$ is a dependent version of the presheaf model:
    \begin{alignat*}{5}
      & \hspace{-1.3em} \Y_\Gamma && : \REN^\op \ra \Set && := \Tms\,\blank\,\Gamma && \P_\Gamma && : \Sigma_\REN\,\Y_\Gamma \ra \Set \\
      & \hspace{-1.3em} \Y_{A} && : \Sigma_\REN\,\Y_\Gamma\ra\Set && := \Tm\,\blank\,A[\blank] \hspace{3em} && \P_A && : \Sigma_{\REN, \Y_\Gamma, \Y_A}\,\P_\Gamma \ra \Set \\
      & \hspace{-1.3em} \Y_\sigma && : \Y_\Gamma \nat \Y_\Delta && := \sigma\circ\blank && \P_\sigma && : \Sigma_{\Y_\Gamma}\,\P_\Gamma \S \P_\Delta[\Y_\sigma] \\
      & \hspace{-1.3em} \Y_t && : \Y_\Gamma \S \Y_A && := t[\blank] && \P_t && : \Sigma_{\Y_\Gamma}\,\P_\Gamma \S \P_A[\Y_t]
    \end{alignat*}
  \item \pause We need the dependent eliminator to define it.
  \item At the base type:
    \begin{itemize}
    \item We had: $\ll\iota\rr_\Gamma = \Nf\,\Gamma\,\iota$ and $\R_\iota\,t\,n = (t \equiv \cul n\cur)$
    \item Now we have: $\P_\iota\,t = \Sigma(n:\Nf\,\Gamma\,\iota).(t\equiv \cul n\cur)$
    \end{itemize}
  \end{itemize}
\end{frame}

%=====================================================================

\section*{Summary}

\begin{frame}{Summary}
  \begin{itemize}
  \item We defined the typed syntax of type theory as an explicit
    substitution calculus using a quotient inductive inductive type
  \item Normalisation is specified as an isomorphism between terms and
    normal forms
  \item We proved normalisation and completeness using a
    proof-relevant presheaf logical predicate
  \item Most of this has been formalised in Agda
  \item Stability, injectivity of type constructors can be proven
  \item Question: how to prove decidability of conversion? N.b. normal
    forms are indexed by non-normal types
  \end{itemize}
\end{frame}

%=====================================================================

\end{document}
