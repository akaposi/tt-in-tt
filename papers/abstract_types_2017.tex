\documentclass[a4paper]{easychair}

\usepackage{doc}
\usepackage{makeidx}

\usepackage{cite}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{stmaryrd}
\usepackage{tikz}

\DeclareMathSizes{10}{9}{8}{7}

%% Document
%%
\begin{document}
%% Front Matter
%%
\title{Normalisation by Evaluation for a Type Theory with Large Elimination}

\titlerunning{Normalisation by Evaluation for a Type Theory with Large
  Elimination}

\author{
  Thorsten Altenkirch\inst{1}
  \thanks{Supported by EPSRC grant EP/M016951/1 and USAF grant FA9550-16-1-0029.}
  \and
  Ambrus Kaposi\inst{2}
\and
  Andr{\'a}s Kov{\'a}cs\inst{2}
}

\institute{
  University of Nottingham, Nottingham, United Kingdom \\
  \email{txa@cs.nott.ac.uk}
  \and
  E{\"o}tv{\"o}s Lor{\'a}nd University, Budapest, Hungary \\
  \email{\{akaposi|andraskovacs\}@caesar.elte.hu}
}

\authorrunning{Kaposi and Kov{\'a}cs}

\clearpage

\maketitle

\input{abbrevs.tex}

\pagestyle{empty}

%---------------------------------------------------------------------
\vspace{-0.2em} We use normalisation by evaluation (NbE) to prove
normalisation for a dependent type theory with a universe closed under
dependent function space and booleans, where booleans are equipped
with large elimination. This is the continuation of our previous work
\cite{nbe-conf}. There, NbE is given for an intrinsically typed syntax
with dependent function space and a base type. The main technical
addition in the current work is the inclusion of quote and unquote in
the model (similarly to the NbE proof for System F
\cite{alti:f97}). An Agda formalisation is work in progress.

%---------------------------------------------------------------------
\vspace{-0.7em}
\subsubsection*{The syntax}
\thispagestyle{empty}

We define an intrinsically typed syntax for type theory, that is, it
only contains well-typed terms. Conversion rules are added as equality
constructors, yielding a higher inductive inductive type. Our
metatheory has uniqueness of identity proofs, hence we call this
definition quotient inductive inductive. Our sorts are contexts,
types, substitutions and terms.
\vspace{-0.3em}
\[
\Con : \Set \hspace{3em} \Ty  : \Con \ra \Set \hspace{3em} \Tms  : \Con \ra \Con \ra \Set \hspace{3em} \Tm  : (\Gamma : \Con) \ra \Ty \, \Gamma \ra \Set
\]
The core calculus is given by the following constructors, omitting the
equalities expressing that $\Con$ and $\Tms$ form a category with
terminal object $\cdot$. In the rule ${,\circ}$, $_*$ denotes
transport.
\vspace{-0.4em}
\begin{alignat*}{6}
  & \cdot && : \Con && 												  \blank[\blank] && : \Tm\,\Delta\,A \ra (\sigma : \Tms\,\Gamma\,\Delta) \ra \Tm\,\Gamma\,A[\sigma] \\
  & \blank,\blank && : (\Gamma : \Con) \ra \Ty \, \Gamma \ra \Con && 						  \pi_2 && : (\sigma : \Tms\,\Gamma\,(\Delta,A)) \ra \Tm\,\Gamma\,A[\pi_1 \, \sigma] \\	  
  & \blank[\blank] && : \Ty \, \Delta \ra \Tms \, \Gamma \, \Delta \ra \Ty \, \Gamma && 		          [\id] && : A[\id] \equiv A \\							       
  & \id && : \Tms \, \Gamma \, \Gamma && 							                  [][] && : A[\sigma][\nu] \equiv A[\sigma \circ \nu] \\			       
  & \blank\circ\blank && : \Tms\,\Theta\,\Delta \ra \Tms\,\Gamma\,\Theta \ra \Tms\,\Gamma\,\Delta && 		  \pi_1\beta && : \pi_1\,(\sigma, t) \equiv \sigma \\				               
  & \epsilon && : \Tms \, \Gamma \, \cdot && 									  \pi\eta && : (\pi_1\,\sigma, \pi_2\,\sigma) \equiv \sigma \\			               
  & \blank,\blank && : (\sigma : \Tms\,\Gamma\,\Delta) \ra \Tm\,\Gamma\,A[\sigma] \ra \Tms\,\Gamma\,(\Delta,A) \hspace{2em} && ,\circ && : (\sigma, t) \circ \nu \equiv (\sigma \circ \nu) , (\tr{[][]}{t[\nu]}) \\
  & \pi_1 && : \Tms\,\Gamma\,(\Delta,A) \ra \Tms\,\Gamma\,\Delta && 						  \pi_2\beta && : \pi_2\,(\sigma, t) \equiv^{\pi_1\beta} t
\end{alignat*}
We have a universe closed under $\Pi$ and $\Bool$, with small and
large elimination for $\Bool$. We omit listing the substitution laws
and three out of four $\beta$ laws for $\Bool$.
\vspace{-0.3em}
\begin{alignat*}{6}
  & \U && : \Ty \, \Gamma                                                        && \Bool : \Tm\,\Gamma\,\U \\
  & \El && : \Tm \, \Gamma \, \U \ra \Ty \, \Gamma                               && \true, \false : \Tm\,\Gamma\,(\El\,\Bool) \\
  & \Pi && : (\hat{A}:\Tm\,\Gamma\,\U)\ra\Tm\,(\Gamma,\El\,\hat{A})\,\U\ra\Tm\,\Gamma\,\U \hspace{1em}    && \ifthenelse : (\hat{C} : \Tm\,(\Gamma, \El\,\Bool)\,U)(b : \Tm\,\Gamma\,(\El\,\Bool)) \\
  & \lam && : \Tm\,(\Gamma,\El\,\hat{A})\,(\El\,\hat{B}) \ra \Tm\,\Gamma\,(\El\,(\Pi\,\hat{A}\,\hat{B})) && \hspace{2em}\ra \Tm\,\Gamma\,(\El\,\hat{C}[\id,\true]) \ra \Tm\,\Gamma\,(\El\,\hat{C}[\id,\false]) \\
  & \app && : \Tm\,\Gamma\,(\El\,(\Pi\,\hat{A}\,\hat{B})) \ra \Tm\,(\Gamma,\El\,\hat{A})\,(\El\,\hat{B}) && \hspace{2em}\ra \Tm\,\Gamma\,(\El\,\hat{C}[\id,b])\\
  & \Pi\beta && : \app\,(\lam\,t) \equiv t                                       && \true\beta : \ifthenelse\,\hat{C}\,\true\,t\,f \equiv t \\
  & \Pi\eta && : \lam\,(\app\,t) \equiv t                                        && \mathsf{Ifthenelse} : \Tm\,\Gamma\,(\El\,\Bool)\ra\Tm\,\Gamma\,\U\ra\Tm\,\Gamma\,\U\ra\Tm\,\Gamma\,\U
\end{alignat*}
To define a function from the syntax, one needs to use the eliminator
which requires a method for each constructor, including the equality
constructors. This ensures that the function respects conversion (which
is equality).

%---------------------------------------------------------------------

\vspace{-0.7em}
\subsubsection*{Normal forms}

We define variables, neutral terms and normal forms as inductive
predicates on terms.
\vspace{-0.3em}
\[
\isVar, \isNe, \isNf : (\Gamma:\Con)(A : \Ty\,\Gamma)\ra\Tm\,\Gamma\,A \ra \Set
\]
Variables are de Bruijn indices. Neutral terms are either variables,
applications or $\mathsf{\{i|I\}fthenelse}$ applied to a neutral
boolean. Normal forms are either constructors ($\Pi$, $\lam$, $\Bool$,
$\true$, $\false$) or neutral terms. We do not allow neutral terms of
any type: the type has to be $\U$, $\Bool$ or $\El\,A$ where $A$ is
neutral. This is to preserve uniqueness of normal forms for
functions. Similarly we have $\isNes : (\Gamma, \Delta : \Con) \ra
\Tms\,\Gamma\,\Delta\ra\Set$ for neutral substitutions.

%---------------------------------------------------------------------

\vspace{-0.7em}
\subsubsection*{Presheaf logical predicate}

NbE works by evaluating the syntax in a model, then quoting semantic
values back to normal forms. In our case, the model is a
proof-relevant presheaf logical predicate. Contexts are interpreted as
predicates on substitutions into that context, together with an
unquote function. Types are mapped to predicates on their terms,
bundled with quote and unquote functions. The interpretation of
substitutions and terms provide the fundamental lemmas. All of this is
stable under variable renamings (we call the category of renamings
$\REN$).
\vspace{-0.3em}
\begin{alignat*}{6}
  & \P_\Delta && : \forall\Psi.(\rho:\Tms\,\Psi\,\Delta)\ra (\r:\Set)\times(\u : \isNes\,\Psi\,\Delta\,\rho \ra \r) \\
  & \P_A && : \forall\Psi.(\rho:\Tms\,\Psi\,\Delta)\ra\P_\Gamma\,\Psi\,\rho.\r\ra(t : \Tm\,\Psi\,A[\rho])\ra (\r:\Set)\times(\q : \r \ra \isNf\,\Psi\,A[\rho]\,t) \\
  & && \hspace{20.1em} \times(\u:\isNe\,\Psi\,A[\rho]\,t \ra r) \\
  & \P_t && : \forall\Psi.(\rho:\Tms\,\Psi\,\Gamma)(q : \P_\Gamma\,\Psi\,\rho.\r) \ra \P_A\,\Psi\,\rho\,q\,(t[\rho]).\r
\end{alignat*}
The interpretation of the universe contains a predicate over
codes. This predicate for a code $A$ expresses that it is a normal
form and that there is also a predicate on terms of type $\El\,A$ together with
quote and unquote, again.
\vspace{-0.3em}
\begin{alignat*}{6}
  & \P_\U\,\Psi\,(\rho:\Tms\,\Psi\,\Gamma)(p:\P_\Gamma\,\Psi\,\rho)(\hat{A}:\Tm\,\Psi\,\U).\r \\
  & \hspace{3em} := \isNf\,\Psi\,\U\,\hat{A} \times\, \forall\Omega.(\beta:\REN(\Omega, \Psi))(t : \Tm\,\Omega\,(\El\,\hat{A}[\beta])) \\
  & \hspace{9.5em} \ra (\r : \Set)\times(\q : \r \ra \isNf\,\Omega\,(\El\,\hat{A}[\beta])\,t)\times(\u : \isNe\,\Omega\,(\El\,\hat{A}[\beta])\,t \ra \r)
\end{alignat*}
The predicate for $\Bool$ says for a term $b :
\Tm\,\Gamma\,(\El\,\Bool)$ that $(b \equiv \true)+(b \equiv
\false)+\isNe\,\Gamma\,(\El\,\Bool)\,b$ (we don't have $\eta$ for
$\Bool$).

%---------------------------------------------------------------------

\vspace{-0.7em}
\subsubsection*{Normalisation}

Normalisation unquotes the interpretation of a term, also using the
unquote of the identity substitution: $\norm\,(t : \Tm\,\Gamma\,A) :=
\P_{A}\,\id\,p\,t.\q\,(\P_t\,\Gamma\,\id_\Gamma\,p) :
\mathsf{isNf}\,\Gamma\,A\,t$ where $p :=
\P_\Gamma\,\id_\Gamma.\mathsf{u}\,(\mathsf{idneu} :
\mathsf{isNes}\,\Gamma\,\Gamma\,\id_\Gamma) :
\P_\Gamma\,\Gamma\,\id_\Gamma.\r$.

%---------------------------------------------------------------------

\vspace{-0.7em}
\subsubsection*{References}

\renewcommand{\section}[2]{}
\bibliography{local,alti}{}
\bibliographystyle{plain}

\end{document}

% \begin{alignat*}{6}
%   & \rlap{$\isVar, \isNe, \isNf : (\Gamma:\Con)(A : \Ty\,\Gamma)\ra\Tm\,\Gamma\,A \ra \Set$} \\
%   & \iszero && :  \isVar\,(\Gamma,A)\,(A[\pi_1\,\id])\,(\pi_2\,\id) \\
%   & \issuc  && :  \isVar\,\Gamma\,A\,t \ra\isVar\,(\Gamma,B) (A[\pi_1\,\id])\,(t[\pi_1\,\id]) \\
%   & \isvar  && :  \isVar\,\Gamma\,A\,t \ra \isNe\,\Gamma\,A\,t \\
%   & \isapp  && :  \isNe\,\Gamma\,(\El\,(\Pi\,A\,B))\,t\ra \isNf\,\Gamma\,(\El\,A)\,u \ra \isNe\,\Gamma\,(B[\id,u])\,((\app\,t)[\id,u]) \\
%   & \isifthenelse && :  \isNe\,\Gamma\,(\El\,\Bool)\,b\ra \isNf\,\Gamma\,(\El\,C[\id,\true])\,t\ra \isNf\,\Gamma\,(\El\,C[\id,\false])\,f\ra\isNe\,\Gamma\,(\El\,C[\id,b])\,(\ifthenelse\,C\,b\,t\,f) \\
%   & \isIfthenelse && :  \isNe\,\Gamma\,(\El\,\Bool)\,b\ra \isNf\,\Gamma\,\U\,t\ra \isNf\,\Gamma\,\U\,f\ra\isNe\,\Gamma\,\U\,(\Ifthenelse\,C\,b\,t\,f) \\
%   & \isneU  && :  \isNe\,\Gamma\,\U\,t \ra \isNf\,\Gamma\,\U\,t \\
%   & \isneEl && :  \isNe\,\Gamma\,\U\,A \ra \isNe\,\Gamma\,(\El\,A)\,t \ra \isNf\,\Gamma\,(\El\,A)\,t \\
%   & \isneBool && :  \isNe\,\Gamma\,(\El\,\Bool)\,b \ra \isNf\,\Gamma\,(\El\,\Bool)\,b \\
%   & \isPi && :  \isNf\,\Gamma\,\U\,A \ra \isNf\,(\Gamma,A)\,\U\,B \ra \isNf\,\Gamma\,\U\,(\Pi\,A\,B) \\
%   & \isBool && :  \isNf\,\Gamma\,\U\,\Bool \\
%   & \islam && :  \isNf\,(\Gamma,\El\,A)\,(\El\,B)\,t \ra \isNf\,\Gamma\,(\El\,(\Pi\,A\,B))\,(\lam\,t) \\
%   & \istrue && :  \isNf\,\Gamma\,(\El\,\Bool)\,\true \\
%   & \isfalse && :  \isNf\,\Gamma\,(\El\,\Bool)\,\false
% \end{alignat*}
% 
% We use normalisation by evaluation (NbE) to prove normalisation for
% a dependent type theory with a universe closed under dependent
% function space and booleans, where booleans are equipped with large
% elimination. We have an intrinsically typed syntax (that is, we only
% define well-typed terms) and it is presented as a quotient inductive
% inductive type (QIIT) which means that the conversion relation is
% modelled by equality and terms are quotiented up to $\beta$-$\eta$
% equality. Normal forms are given as a predicate on terms and
% normalisation expresses that the type of normal forms for each term
% is contractible. The construction is carried out in the metalanguage
% of Martin-L{\"o}f type theory extended with an eliminator for the
% QIIT of the syntax, functional extensionality and uniqueness of
% identity proofs. NbE works by evaluating the syntax in a model and
% then defining a quote function from the model to normal forms. In
% our case, the model is a presheaf logical predicate which also
% contains the definition of quote. Most details have been checked on
% paper and a formalisation in Agda is on the way.
