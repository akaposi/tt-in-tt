\documentclass{beamer}
\usetheme{default}
\useoutertheme{infolines}
\usepackage[greek,english,russian]{babel}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{tikz}

\usepackage{scrextend}
\usepackage[overlay,absolute]{textpos}
\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}
\textblockcolor{white}
\changefontsizes{13pt}

%include lhs2TeX.fmt
%include agda.fmt
%include lib.fmt

%\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize/enumerate subsubbody}{size=\normalsize}


\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

\renewcommand{\U}{\mathsf{U}}
\newcommand{\El}{\mathsf{El}}
\newcommand{\REN}{\mathsf{REN}}
\newcommand{\op}{\mathsf{op}}
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\Set}{\mathsf{Set}}
\newcommand{\PSh}{\mathsf{PSh}}
\newcommand{\FamPSh}{\mathsf{FamPSh}}
\renewcommand{\ll}{\llbracket}
\providecommand{\rr}{\rrbracket}
\newcommand{\Con}{\mathsf{Con}}
\newcommand{\Ty}{\mathsf{Ty}}
\newcommand{\Tm}{\mathsf{Tm}}
\newcommand{\Tms}{\mathsf{Tms}}
\newcommand{\R}{\mathsf{R}}
\newcommand{\TM}{\mathsf{TM}}
\newcommand{\NE}{\mathsf{NE}}
\newcommand{\NF}{\mathsf{NF}}
\newcommand{\q}{\mathsf{q}}
\renewcommand{\u}{\mathsf{u}}
\renewcommand{\ne}{\mathsf{ne}}
\newcommand{\nf}{\mathsf{nf}}
\newcommand{\lQ}{\mathsf{lQ}}
\newcommand{\lU}{\mathsf{lU}}
\renewcommand{\lq}{\mathsf{lq}}
\newcommand{\lu}{\mathsf{lu}}
\newcommand{\cul}{\ulcorner}
\newcommand{\cur}{\urcorner}
\newcommand{\norm}{\mathsf{norm}}
\newcommand{\Nf}{\mathsf{Nf}}
\newcommand{\Ne}{\mathsf{Ne}}
\newcommand{\Nfs}{\mathsf{Nfs}}
\newcommand{\Nes}{\mathsf{Nes}}
\newcommand{\ID}{\mathsf{ID}}
\newcommand{\id}{\mathsf{id}}
\newcommand{\nat}{\,\dot{\rightarrow}\,}
%\newcommand{\nat}{\overset{\mathsf{n}}{\ra}} % this is how we denote it in the formalisation
\newcommand{\Nat}{\overset{\mathsf{N}}{\ra}}
\renewcommand{\S}{\overset{\mathsf{s}}{\ra}} % we have it with uppercase S in the formalisation
\newcommand{\ap}{\mathsf{ap}}
\newcommand{\blank}{\mathord{\hspace{1pt}\text{--}\hspace{1pt}}} %from the book
%\newcommand{\blank}{\!{-}\!}
\newcommand{\lam}{\mathsf{lam}}
\newcommand{\app}{\mathsf{app}}
\newcommand{\circid}{\circ\hspace{-0.2em}\id}
\newcommand{\circcirc}{\circ\hspace{-0.2em}\circ}
\newcommand{\tr}[2]{\ensuremath{{}_{#1 *}\mathopen{}{#2}\mathclose{}}} % from the book
\newcommand{\M}{\mathsf{M}}
\renewcommand{\C}{\mathcal{C}}
\newcommand{\data}{\mathsf{data}}
\newcommand{\ind}{\hspace{1em}}
\newcommand{\idP}{\mathsf{idP}}
\newcommand{\compP}{\mathsf{compP}}
\newcommand{\idF}{\mathsf{idF}}
\newcommand{\compF}{\mathsf{compF}}
\newcommand{\proj}{\mathsf{proj}}
\newcommand{\ExpPSh}{\mathsf{ExpPSh}}
\newcommand{\map}{\mathsf{map}}
\newcommand{\Var}{\mathsf{Var}}
\newcommand{\Vars}{\mathsf{Vars}}
\newcommand{\vze}{\mathsf{vze}}
\newcommand{\vsu}{\mathsf{vsu}}
\newcommand{\wk}{\mathsf{wk}}
\newcommand{\neuU}{\mathsf{neuU}}
\newcommand{\neuEl}{\mathsf{neuEl}}
\newcommand{\var}{\mathsf{var}}
\newcommand{\natn}{\mathsf{natn}}
\newcommand{\natS}{\mathsf{natS}}
\newcommand{\LET}{\mathsf{let}}
\newcommand{\IN}{\mathsf{in}}
\newcommand{\refl}{\mathsf{refl}}
\newcommand{\zero}{\mathsf{zero}}
\newcommand{\suc}{\mathsf{suc}}
\newcommand{\trans}{\mathbin{\raisebox{0.5ex}{$\displaystyle\centerdot$}}}
\renewcommand{\quote}{\mathsf{quote}}
\newcommand{\unquote}{\mathsf{unquote}}

\begin{document}

%---------------------------------------------------------------------

\begin{frame}[plain]
  \vspace{0.5cm}
  \begin{center}
    \textcolor{blue}{
      {\Large Normalisation by Evaluation for Dependent Types \\\vspace{0.2em}
        }
    }
    
    \vspace{0.7cm}

    Ambrus Kaposi \\

    Eötvös Loránd University, Budapest, Hungary \\

    \vspace{0.2cm}
    
    (joint work with Thorsten Altenkirch of Nottingham) \\

    \vspace{0.7cm}

    TYPES, Нови Сад \\
    25 May 2016
    
  \end{center}
\end{frame}

%=====================================================================

\begin{frame}{Introduction}
\begin{itemize}
\item Goal: 
  \begin{itemize}
  \item Prove normalisation for a type theory with dependent types
  \item Using the metalanguage of type theory itself
  \end{itemize}
\item Structure of the talk:
  \begin{itemize}
  \item Representing type theory in type theory
  \item Specifying normalisation
  \item NBE for simple types
  \item NBE for dependent types
  \end{itemize}
\end{itemize}
\end{frame}

%=====================================================================

\section{Representing type theory in type theory}

%---------------------------------------------------------------------

\begin{frame}{Simple type theory in idealised Agda}
\begin{spec}
data Ty     : Set where
  ι         : Ty
  _⇒_       : Ty → Ty → Ty
data Con    : Set where
  •         : Con
  _,_       : Con → Ty → Con
data Var    : Con → Ty → Set where
  zero      : Var (Γ , A) A
  suc       : Var Γ A → Var (Γ , B) A
data Tm     : Con → Ty → Set where
  var       : Var Γ A → Tm Γ A
  lam       : Tm (Γ , A) B → Tm Γ (A ⇒ B)
  app       : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
\end{spec}
\end{frame}

\begin{frame}{Simple type theory in idealised Agda}
\begin{spec}
data Ty     : Set where
  ι         : Ty
  _⇒_       : Ty → Ty → Ty
data Con    : Set where
  •         : Con
  _,_       : Con → Ty → Con
data Var    : Con → Ty → Set where
  zero      : Var (Γ , A) A
  suc       : Var Γ A → Var (Γ , B) A
data Tm     : Con → Ty → Set where
  var       : Var Γ A → Tm Γ A
  lam       : Tm (Γ , A) B → Tm Γ (A ⇒ B)
  app       : Tm Γ (A ⇒ B) → Tm Γ A → Tm Γ B
\end{spec}
\begin{textblock}{10}(4,4)
  \Huge{No preterms!}
\end{textblock}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{A typed syntax of dependent types (i)}
  \begin{itemize}
  \item Types depend on contexts. \\
    $\Rightarrow$ We need induction induction.
  \begin{spec}
    data Con  : Set
    data Ty   : Con → Set
  \end{spec}
  \vspace{20em}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{A typed syntax of dependent types (ii)}
  \begin{itemize}
  \item Types depend on contexts. \\
    $\Rightarrow$ We need induction induction.
  \item Substitutions are mentioned in the application rule:
    \begin{equation*}
      \mathsf{app} : \Tm\,\Gamma\,(\Pi\,A\,B)\ra(a:\Tm\,\Gamma\,A)\ra\Tm\,\Gamma\,(B[a])
    \end{equation*}
    $\Rightarrow$ We define an explicit substitution calculus.
    \begin{spec}
    data Con  : Set
    data Ty   : Con → Set
    data Tms  : Con → Con → Set
    data Tm   : (Γ : Con) → Ty Γ → Set
      _[_]  : Ty Γ → Tms Δ Γ → Ty Δ
      ...
    \end{spec}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{A typed syntax of dependent types (iii)}
  \begin{itemize}
  \item Types depend on contexts. \\
    $\Rightarrow$ We need induction induction.
  \item Substitutions are mentioned in the application rule: \\
    $\Rightarrow$ We define an explicit substitution calculus.
  \item The following conversion rule for terms:
    \begin{equation*}
      \infer{\Gamma \vdash t : B}{\Gamma \vdash A \sim B && \Gamma \vdash t : A}
    \end{equation*}
    $\Rightarrow$ Conversion (the relation including $\beta$, $\eta$)
    needs to be defined mutually with the syntax.
    \begin{itemize}
    \item We need to add 4 new members to the inductive inductive
      definition: $\sim$ for contexts, types, substitutions and terms.
    \end{itemize}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Representing conversion}
  \begin{itemize}
    \item Lots of boilerplate:
    \begin{itemize}
    \item The $\sim$ relations are equivalence relations
    \item Coercion rules
    \item Congruence rules
    \item We need to work with setoids
    \end{itemize}
  \item The identity type |_≡_| is an equivalence relation with
    coercion and congruence laws.
  \item Higher inductive types are an idea from homotopy type theory:
    constructors for equalities.
  \item We add the conversion rules as constructors: e.g. $\beta :
    \app\,(\lam\,t)\,u \equiv t[u]$.
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{QIITs}
  We formalise the syntax of type theory as a quotient inductive
  inductive type (QIIT).
  \begin{itemize}
  \item A QIT is a HIT which is a set
  \item QITs are not the same as quotient types
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Using the syntax}
  \begin{itemize}
    \item One defines functions from a QIIT using its eliminator.
    \item The arguments of the non-dependent eliminator form a model
      of type theory, equivalent to Categories with Families.
      \begin{spec}
        record Model   : Set where
          field  Conᴹ  : Set
                 Tyᴹ   : Conᴹ → Set
                 Tmᴹ   : (Γ : Conᴹ) → Tyᴹ Γ → Set
                 lamᴹ  : Tmᴹ (Γ ,ᴹ A) Bᴹ → Tmᴹ Γ (Πᴹ A B)
                 bᴹ    : appᴹ (lamᴹ t) a ≡ t [ a ]ᴹ
                 ...
      \end{spec}
    \item The eliminator says that the syntax is the initial model.
  \end{itemize}
\end{frame}

%=====================================================================

\section{Specifying normalisation}

%---------------------------------------------------------------------

\newcommand\arcfrombottom{
  \begin{tikzpicture}[scale=0.03em]
    \draw (0,0) arc (0:180:0.5);
    \draw (0,0) edge[->] (0,-0.3);
    \draw (-1,0) edge (-1,-0.3);
  \end{tikzpicture}
}
\newcommand\arcfromtop{
  \begin{tikzpicture}[scale=0.03em]
    \draw (0,0) arc (180:360:0.5);
    \draw (0,0) edge[->] (0,0.3);
    \draw (1,0) edge (1,0.3);
  \end{tikzpicture}
}

\begin{frame}{Specifying normalisation}
  Neutral terms and normal forms (typed!):
  \begin{spec}
    n ::= x | n v              {-" \hspace{3em} "-} Ne Γ A
    v ::= n | λ x . v          {-" \hspace{3em} "-} Nf Γ A
  \end{spec}
  Normalisation is an isomorphism:
\begin{equation*}
   \text{completeness }\arcfromtop \hspace{1em} \norm\downarrow\begin{array}{l}\infer={\hspace{1em} \Nf \, \Gamma \, A \hspace{1em} }{\Tm \, \Gamma \, A}\end{array}\uparrow\cul\blank\cur \hspace{1em} \arcfrombottom\text{ stability}
\end{equation*}
Soundness is given by congruence of equality:
\begin{equation*}
t \equiv t' \ra \norm\,t \equiv \norm\,t'
\end{equation*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Normalisation by Evaluation (NBE)}
\begin{center}
\begin{tikzpicture}
\node (Syntax) at (2,4.3) {Syntax};
\node (Model) at (8,4.2) {Model};
\node (NF) at (2,3) {Normal forms};
\draw (2,2) circle (2cm);
\draw (2,2) circle (1.2cm);
\draw (8,2) circle (1.8cm);
\draw[thick,->] (4,2.6) -- node[above] {eliminator} (6.2,2.6);
\draw[thick,->] (6.2,1.4) -- node[above] {quote} (3.1,1.4);
\end{tikzpicture}
\end{center}\vspace{-1em}
\begin{itemize}
\item First formulation (Berger and Schwichtenberg, 1991)
\item Simply typed case (Altenkirch, Hofmann, Streicher 1995)
\item Dependent types using untyped realizers (Abel, Coquand, Dybjer,
  2007)
\end{itemize}
\end{frame}

%=====================================================================

\section{NBE for simple types}

%---------------------------------------------------------------------

\begin{frame}{The presheaf model}
  \begin{itemize}
  \item Presheaf models are proof-relevant versions of Kripke models.
  \item They are parameterised over a category, here we choose $\REN$:
    objects are contexts, morphisms are lists of variables.
    \pause
  \item A context $\Gamma$ is interpreted as a presheaf $\ll\Gamma\rr :
    \REN^\op \ra \Set$.
    \begin{itemize}
    \item Given another context $\Delta$ we have $\ll\Gamma\rr_\Delta :
      \Set$.
    \item Given a renaming $\Delta \overset{\beta}{\longrightarrow}
      \Theta$, there is a $\ll\Gamma\rr_\Theta
      \overset{\ll\Gamma\rr\,\beta}{\longrightarrow}
      \ll\Gamma\rr_\Delta$.
    \end{itemize}
    \pause    
  \item Types are presheaves too: $\ll A\rr : \REN^\op \ra \Set$
    \begin{itemize}
    \item $\ll \iota\rr_\Delta := \Nf\,\Delta\,\iota$
%    \item $\ll A \ra B\rr_\Delta := \forall \Theta.(\beta : \Theta \ra \Delta) \ra \ll A\rr_\Theta \ra \ll B\rr_\Theta $
    \end{itemize}
    \pause    
%  \item A term $t : \Tm\,\Gamma\,A$ is a natural transformation $\ll
%    t\rr : \ll \Gamma\rr \dot{\ra} \ll A\rr$
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Quotation}
  The quote function is a natural transformation.
  \begin{equation*}
    \quote_A : \ll A \rr \,\,\dot{\ra}\,\, \Nf\,\blank\,A
  \end{equation*}
  At a given context we have:
  \begin{equation*}
    \quote_{A\,\Gamma} : \ll A \rr_\Gamma \,\,\ra\,\, \Nf\,\Gamma\,A
  \end{equation*}
  It is defined mutually with unquote:
  \begin{equation*}
    \unquote_A : \Ne\,\blank\,A \,\,\dot{\ra}\,\, \ll A \rr
  \end{equation*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Quote and unquote}
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\Ne\,\blank\,A$};
\node (XX) at (5,0) {${\color{white}\Sigma\,(\Tm\,\blank\,A \times \,\,\,}\ll A\rr{\color{white} )\,\mathsf{R}_A}$};
\node (NF) at (10,0) {$\Nf\,\blank\,A$};
\node[color=white] (TM) at (5,-3) {$\Tm\,\blank\,A$};
\draw[->] (NE) edge node[above] {$\unquote\phantom{'}_A$} (XX);
\draw[->] (XX) edge node[above] {$\quote\phantom{'}_A$} (NF);
\end{tikzpicture}
\end{center}
\vspace{10em}
\end{frame}

\begin{frame}{With completeness}
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\Ne\,\blank\,A$};
\node (XX) at (5,0) {$\Sigma\,(\Tm\,\blank\,A \times \ll A\rr)\,\mathsf{R}_A$};
\node (NF) at (10,0) {$\Nf\,\blank\,A$};
\node (TM) at (5,-3) {$\Tm\,\blank\,A$};
\draw[->] (NE) edge node[above] {$\unquote'_A$} (XX);
\draw[->] (XX) edge node[above] {$\quote'_A$} (NF);
\draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
\draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
\draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
\end{tikzpicture}
\end{center}
$\R_A$ is a presheaf logical relation between the syntax and the
presheaf model. It is equality at the base type.
\vspace{10em}
\end{frame}

%=====================================================================

\section{NBE for dependent types}

%---------------------------------------------------------------------

\begin{frame}{Defining quote, first try}
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\Nes\,\blank\,\Gamma$};
\node (XX) at (5,0) {${\color{white} \Sigma\,(\Tms\,\blank\,\Gamma \times \,\,\,}\ll\Gamma\rr{\color{white})\,\mathsf{R}_\Gamma}$};
\node (NF) at (10,0) {$\Nfs\,\blank\,\Gamma$};
\node[color=white] (TM) at (5,-3) {$\Tms\,\blank\,\Gamma$};
\draw[->] (NE) edge node[above] {$\unquote_\Gamma$} (XX);
\draw[->] (XX) edge node[above] {$\quote_\Gamma$} (NF);
\end{tikzpicture}
\end{center}
\pause
When we try to define this quote for function space, we need the
equation $\quote_A\,\circ\unquote_A \equiv \id$.

\pause
Let's define quote and its completeness mutually!
\vspace{10em}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Defining quote, second try}
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\Nes\,\blank\,\Gamma$};
\node (XX) at (5,0) {$\Sigma\,(\Tms\,\blank\,\Gamma \times \ll\Gamma\rr)\,\mathsf{R}_\Gamma$};
\node (NF) at (10,0) {$\Nfs\,\blank\,\Gamma$};
\node (TM) at (5,-3) {$\Tms\,\blank\,\Gamma$};
\draw[->] (NE) edge node[above] {$\unquote_\Gamma$} (XX);
\draw[->] (XX) edge node[above] {$\quote_\Gamma$} (NF);
\draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
\draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
\draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
\end{tikzpicture}
\end{center}
\pause

For unquote at the function space we need to define a semantic
function which works for every input, not necessarily related by the
relation. But quote needs ones which are related!
\vspace{10em}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Defining quote, third try}
\begin{center}
\begin{tikzpicture}
\node (NE) at (0,0) {$\Nes\,\blank\,\Gamma$};
\node (XX) at (5,0) {$\Sigma\,(\Tms\,\blank\,\Gamma)\,\mathsf{P}_\Gamma$};
\node (NF) at (10,0) {$\Nfs\,\blank\,\Gamma$};
\node (TM) at (5,-3) {$\Tms\,\blank\,\Gamma$};
\draw[->] (NE) edge node[above] {$\unquote_\Gamma$} (XX);
\draw[->] (XX) edge node[above] {$\quote_\Gamma$} (NF);
\draw[->] (NE) edge node[below] {$\cul\blank\cur\hspace{1em}$} (TM);
\draw[->] (NF) edge node[below] {$\hspace{1em}\cul\blank\cur$} (TM);
\draw[->] (XX) edge node[right] {$\mathsf{proj}$} (TM);
\end{tikzpicture}
\end{center}
Use a proof-relevant logical predicate. At the base type it says that
there exists a normal form which is equal to the term.

Instance of categorical glueing.
\vspace{10em}
\end{frame}

%=====================================================================

\section{Extra slides}

%---------------------------------------------------------------------

\begin{frame}{The presheaf model and quote}
For dependent types, types are interpreted as families of presheaves.
\begin{alignat*}{5}
  & \ll\Gamma\rr && : \REN^\op \ra \Set \\
  & \ll \Gamma \vdash A \rr && : (\Delta : \REN) \ra \ll\Gamma\rr_\Delta \ra \Set
\end{alignat*}
\pause
Quote for contexts is the same, but for types it is more subtle:
\begin{alignat*}{5}
  & \quote_\Gamma && : \ll\Gamma\rr \,\,\dot{\ra}\,\, \Tms\,\blank\,\Gamma \\
  & \quote_{\Gamma\vdash A} && : (\alpha : \ll\Gamma\rr_\Delta) \ra \ll A\rr_\Delta\,\alpha \ra \Nf\,\Delta\,\big(A[\quote_{\Gamma, \Delta}\,\alpha]\big)
\end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Quotation}
  The quote function is a natural transformation.
  \begin{equation*}
    \quote_A : \ll A \rr \,\,\dot{\ra}\,\, \Nf\,\blank\,A
  \end{equation*}
  For the base type it is the identity.
  \begin{alignat*}{3}
    & \quote_\iota \,v := v \hspace{30em}
  \end{alignat*}
  For function types:
  \begin{alignat*}{4}
    & \quote_{A \ra B\,\Delta} \,\big(f : \forall \Theta.(\beta : \Theta \ra \Delta) \ra \ll A\rr_\Theta \ra \ll B\rr_\Theta\big) : \Nf\,\Delta\,(A\ra B)\hspace{20em}
  \end{alignat*}\vspace{-2em}
  \begin{alignat*}{8}
    & := \lam\, && \Big( \hspace{30em} \\
    & && \uparrow \Nf\,(\Delta,A)\,B && \hspace{30em}
  \end{alignat*}
  \vphantom{10em}
\end{frame}

\begin{frame}{Quotation}
  The quote function is a natural transformation.
  \begin{equation*}
    \quote_A : \ll A \rr \,\,\dot{\ra}\,\, \Nf\,\blank\,A
  \end{equation*}
  For the base type it is the identity.
  \begin{alignat*}{3}
    & \quote_\iota \,v := v \hspace{30em}
  \end{alignat*}
  For function types:
  \begin{alignat*}{4}
    & \quote_{A \ra B\,\Delta} \,\big(f : \forall \Theta.(\beta : \Theta \ra \Delta) \ra \ll A\rr_\Theta \ra \ll B\rr_\Theta\big) : \Nf\,\Delta\,(A\ra B)\hspace{20em}
  \end{alignat*}\vspace{-2em}
  \begin{alignat*}{8}
    & := \lam\, && \Big( \quote_{B, (\Delta,A)}\,&& \big( \hspace{30em} \\
    & && \hphantom{\uparrow \Nf\,(\Delta,A)\,B} && \uparrow \ll B\rr_{\Delta,A}
  \end{alignat*}
  \vphantom{10em}
\end{frame}

\begin{frame}{Quotation}
  The quote function is a natural transformation.
  \begin{equation*}
    \quote_A : \ll A \rr \,\,\dot{\ra}\,\, \Nf\,\blank\,A
  \end{equation*}
  For the base type it is the identity.
  \begin{alignat*}{3}
    & \quote_\iota \,v := v \hspace{30em}
  \end{alignat*}
  For function types:
  \begin{alignat*}{4}
    & \quote_{A \ra B\,\Delta} \,\big(f : \forall \Theta.(\beta : \Theta \ra \Delta) \ra \ll A\rr_\Theta \ra \ll B\rr_\Theta\big) : \Nf\,\Delta\,(A\ra B)\hspace{20em}
  \end{alignat*}\vspace{-2em}
  \begin{alignat*}{8}
    & := \lam\, && \Big( \quote_{B, (\Delta,A)}\,&& \big(f_{\Delta,A}\, && \hspace{30em} \\
    & && \hphantom{\uparrow \Nf\,(\Delta,A)\,B} && \hphantom{\uparrow \ll B\rr_{\Delta,A}} && \uparrow \Delta,A\ra\Delta
  \end{alignat*}
  \vphantom{10em}
\end{frame}

\begin{frame}{Quotation}
  The quote function is a natural transformation.
  \begin{equation*}
    \quote_A : \ll A \rr \,\,\dot{\ra}\,\, \Nf\,\blank\,A
  \end{equation*}
  For the base type it is the identity.
  \begin{alignat*}{3}
    & \quote_\iota \,v := v \hspace{30em}
  \end{alignat*}
  For function types:
  \begin{alignat*}{4}
    & \quote_{A \ra B\,\Delta} \,\big(f : \forall \Theta.(\beta : \Theta \ra \Delta) \ra \ll A\rr_\Theta \ra \ll B\rr_\Theta\big) : \Nf\,\Delta\,(A\ra B)\hspace{20em}
  \end{alignat*}\vspace{-2em}
  \begin{alignat*}{8}
    & := \lam\, && \Big( \quote_{B, (\Delta,A)}\,&& \big(f_{\Delta,A}\, && \wk \, && (\hspace{30em} \\
    & && \hphantom{\uparrow \Nf\,(\Delta,A)\,B} && \hphantom{\uparrow \ll B\rr_{\Delta,A}} && \hphantom{\uparrow \Delta,A\ra\Delta} && \uparrow \ll A\rr_{\Delta,A}
  \end{alignat*}
  \vphantom{10em}
\end{frame}

\begin{frame}{Quotation}
  The quote function is a natural transformation.
  \begin{equation*}
    \quote_A : \ll A \rr \,\,\dot{\ra}\,\, \Nf\,\blank\,A
  \end{equation*}
  For the base type it is the identity.
  \begin{alignat*}{3}
    & \quote_\iota \,v := v \hspace{30em}
  \end{alignat*}
  For function types:
  \begin{alignat*}{4}
    & \quote_{A \ra B\,\Delta} \,\big(f : \forall \Theta.(\beta : \Theta \ra \Delta) \ra \ll A\rr_\Theta \ra \ll B\rr_\Theta\big) : \Nf\,\Delta\,(A\ra B)\hspace{20em}
  \end{alignat*}\vspace{-2em}
  \begin{alignat*}{8}
    & := \lam\, && \Big( \quote_{B, (\Delta,A)}\,&& \big(f_{\Delta,A}\, && \wk \, && ( \hspace{30em} \\
    & && \hphantom{\uparrow \Nf\,(\Delta,A)\,B} && \hphantom{\uparrow \ll B\rr_{\Delta,A}} && \hphantom{\uparrow \Delta,A\ra\Delta} && \uparrow \ll A\rr_{\Delta,A}
  \end{alignat*}
  We need to unquote neutral terms: $\unquote_A : \Ne\,\blank\,A \,\,\dot{\ra}\,\, \ll A \rr$.
\end{frame}

\begin{frame}{Quotation}
  The quote function is a natural transformation.
  \begin{equation*}
    \quote_A : \ll A \rr \,\,\dot{\ra}\,\, \Nf\,\blank\,A
  \end{equation*}
  For the base type it is the identity.
  \begin{alignat*}{3}
    & \quote_\iota \,v := v \hspace{30em}
  \end{alignat*}
  For function types:
  \begin{alignat*}{4}
    & \quote_{A \ra B\,\Delta} \,\big(f : \forall \Theta.(\beta : \Theta \ra \Delta) \ra \ll A\rr_\Theta \ra \ll B\rr_\Theta\big) : \Nf\,\Delta\,(A\ra B)\hspace{20em}
  \end{alignat*}\vspace{-2em}
  \begin{alignat*}{8}
    & := \lam\, && \Big( \quote_{B, (\Delta,A)}\,&& \big(f_{\Delta,A}\, && \wk \, && (\unquote_{A\,(\Delta,A)}\,\mathsf{zero})  \big) \Big) \hspace{30em} \\
    & && \hphantom{\uparrow \Nf\,(\Delta,A)\,B} && \hphantom{\uparrow \ll B\rr_{\Delta,A}} && \hphantom{\uparrow \Delta,A\ra\Delta} && \hphantom{\uparrow \ll A\rr_{\Delta,A}}
  \end{alignat*}
  We need to unquote neutral terms: $\unquote_A : \Ne\,\blank\,A \,\,\dot{\ra}\,\, \ll A \rr$.
\end{frame}

%=====================================================================

\end{document}
