\documentclass{beamer}
\usetheme{default}
\useoutertheme{infolines}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{proof}
\usepackage{tikz}
\usepackage[utf8]{inputenc}
\usepackage{pmboxdraw}
\usepackage{csquotes}
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{arrows}

\usepackage{scrextend}
%\changefontsizes{14pt}

\usetheme{Boadilla}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}[frame number]
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}
\setbeamerfont{itemize/enumerate subsubbody}{size=\normalsize}

\DeclareUnicodeCharacter{393}{$\Gamma$}
\DeclareUnicodeCharacter{2192}{$\ra$}
\DeclareUnicodeCharacter{1D39}{${^\M}$}
\DeclareUnicodeCharacter{3B2}{$\beta$}
\DeclareUnicodeCharacter{2261}{$\equiv$}
\DeclareUnicodeCharacter{2081}{$_1$}

\input{abbrevs.tex}

\AtBeginSection[]{
  \begin{frame}
  \vfill
  \centering
  \begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
    \usebeamerfont{title}\insertsectionhead\par%
  \end{beamercolorbox}
  \vfill
  \end{frame}
}

\begin{document}

%---------------------------------------------------------------------

\begin{frame}[plain]
  \vspace{0.5cm}
  \begin{center}
    \textcolor{blue}{
      {\Large Formalising the metatheory of type theory using quotient
        inductive types \\\vspace{0.2em}}
    }
    
    \vspace{0.7cm}

    Ambrus Kaposi \\
    E{\"o}tv{\"o}s Lor{\'a}nd University, Budapest \\

    \vspace{0.7cm}
    
    j.w.w. Thorsten Altenkirch and Andr{\'a}s Kov{\'a}cs \\

    \vspace{0.7cm}

    Workshop on Foundations for the \\
    Practical Formalization of Mathematics, \\
    Nantes, \\
    27 April 2017
    
  \end{center}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Motivation}
  Why type theory in type theory?
  \begin{itemize}
  \item Study the metatheory of type theory in a nice language
  \item Type-safe template type theory (metaprogamming)
    \begin{itemize}
    \item generic programming
    \item extensions of type theory justified by models
    \end{itemize}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Plan}
  \tableofcontents
\end{frame}

%=====================================================================

\section{Extrinsic vs. intrinsic syntax for simple type theory}

\begin{frame}{Extrinsic syntax for simple type theory}
  4 inductive sets + 2 inductive relations.
  
  \begin{alignat*}{10}
    & x ::= \zero \,|\, \suc\,x \\
    & t ::= x \,|\, \lam\,t \,|\, \app\,t\,t' \\
    & A ::= \iota \,|\, A \Ra A' \\
    & \Gamma ::= \cdot \,|\, \Gamma,A
  \end{alignat*}
  \begin{equation*}
    \begin{gathered}
      \infer{\Gamma,A \vdash_v \zero : A}{}
    \end{gathered}
    \hspace{2em}
    \begin{gathered}
      \infer{\Gamma,B \vdash_v \suc\,x : A}{\Gamma \vdash_v x : A}
    \end{gathered}
  \end{equation*}
  \begin{equation*}
    \begin{gathered}
      \infer{\Gamma \vdash x : A}{\Gamma \vdash_v x : A}
    \end{gathered}
    \hspace{0.7em}
    \begin{gathered}
      \infer{\Gamma \vdash \lam\,t : A \ra B}{\Gamma,A \vdash t : B}
    \end{gathered}
    \hspace{0.7em}
    \begin{gathered}
      \infer{\Gamma \vdash \app\, t \,u : B}{\Gamma\vdash t : A \ra B & \Gamma\vdash u : A}
    \end{gathered}
  \end{equation*}  
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Intrinsic syntax for simple type theory}
  4 inductively defined families of sets. 
  \begin{alignat*}{10}
    & \Ty && : \Set \\
    & \ind \iota && : \Ty \\
    & \ind \blank\Ra\blank && : \Ty \ra \Ty \ra \Ty \\
    & \Con && : \Set \\
    & \ind \cdot && : \Con \\
    & \ind \blank,\blank && : \Con \ra \Ty \ra \Con \\
    & \Var && : \Con \ra \Ty \ra \Set \\
    & \ind \zero && : \Var\,(\Gamma,A)\,A \\
    & \ind \suc && : \Var\,\Gamma\,A \ra \Var\,(\Gamma,B)\,A \\
    & \Tm && : \Con \ra \Ty \ra \Set \\
    & \ind \var && : \Var\,\Gamma\,A \ra \Tm\,\Gamma\,A \\
    & \ind \lam && : \Tm\,(\Gamma,A)\,B \ra \Tm\,\Gamma\,(A\Ra B) \\
    & \ind \app && : \Tm\,\Gamma\,(A \Ra B) \ra \Tm\,\Gamma\,A \ra \Tm\,\Gamma\,B
  \end{alignat*}
\end{frame}

%=====================================================================

\section{Extrinsic vs. intrinsic syntax for type theory}

\begin{frame}{Extrinsic syntax}
  4 inductive sets + 8 inductive relations (we can't avoid talking
  about conversion).

  \begin{alignat*}{10}
    & \rlap{$\PCon, \PTy, \PTm, \PTms : \Set$} \\
    & \vdash_\Con && :                 &&     && \PCon   &&           && \ra\, && \Prop \\
    & \vdash_\Ty  && : \PCon           && \ra && \PTy    &&           && \ra && \Prop \\
    & \vdash_\Tm  && : \PCon \ra \PTy  && \ra  && \PTm   &&           && \ra && \Prop \\
    & \vdash_\Tms && : \PCon \ra \PCon && \ra  && \PTms  &&           && \ra && \Prop \\
    & \sim_\Con   && :                 &&      && \PCon  && \ra \PCon && \ra && \Prop \\
    & \sim_\Ty    && : \PCon           && \ra  && \PTy   && \ra \PTy && \ra && \Prop \\
    & \sim_\Tm    && : \PCon \ra \PTy  && \ra  && \PTm   && \ra \PTm  && \ra && \Prop \\
    & \sim_\Tms   && : \PCon \ra \PCon && \ra\, && \PTms && \ra \PTms && \ra && \Prop
  \end{alignat*}
  Relations are given by rules for ER, coercion, congruence,
  conversion.
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Extrinsic syntax, PER variant (Dybjer: Undec... LCCC)}
  4 inductive sets + 4 inductive relations.
  \begin{alignat*}{10}
    & \rlap{$\PCon, \PTy, \PTm, \PTms : \Set$} \\
    & \sim_\Con && : && && && && \PCon && \ra \PCon && \ra \Prop \\
    & \sim_\Ty  && : \PCon && && && \ra && \PTy && \ra \PTy && \ra \Prop \\
    & \sim_\Tm  && : \PCon && \ra && \PTy && \ra && \PTm && \ra \PTm && \ra \Prop \\
    & \sim_\Tms && : \PCon && \ra\, && \PCon && \ra\, && \PTms && \ra \PTms && \ra \Prop
  \end{alignat*}
  Recovering typing relations as reflexive cases:
  \begin{alignat*}{5}
    & \vdash_\Con\,\Gamma && := \Gamma \sim_\Con \Gamma \\
    & \Gamma \vdash_\Ty \,A && := \Gamma \vdash A \sim_\Ty A \\
    & \Gamma \vdash_\Tm \,t : A && := \Gamma \vdash t \sim_\Tm t : A \\
    & \Gamma \vdash_\Tms \,\sigma : \Delta && := \Gamma \vdash \sigma \sim_\Tms \sigma : \Delta
  \end{alignat*}
  Congruence rules and typing rules are
  identified. E.g. \\ $\infer{\vdash_\Con\Gamma,A}{\vdash_\Con\Gamma
    && \Gamma \vdash_\Ty A}$ is expressed by $\infer{\Gamma,A
    \sim_\Con\Gamma',A'}{\Gamma\sim_\Con\Gamma && \Gamma \vdash A
    \sim_\Ty A'}$.
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Intrinsic syntax (James Chapman: TT should eat itself)}
  An inductive inductive definition of 4 families of sets + 4 families
  of relations.
  
  \begin{alignat*}{5}
    & \Con && : \Set \\
    & \Ty && : \Con \ra \Set \\
    & \Tm && : (\Gamma : \Con) \ra \Ty\,\Gamma \ra \Set \\
    & \Tms && : \Con \ra \Con \ra \Set \\
    & \sim_\Con && : \Con \ra \Con \ra \Prop \\
    & \sim_\Ty && : (\Gamma : \Con) \ra \Ty\,\Gamma \ra \Ty\,\Gamma \ra \Prop \\
    & \sim_\Tm && : (\Gamma : \Con)(A : \Ty\,\Gamma) \ra \Tm\,\Gamma\,A \ra \Tm\,\Gamma\,A \ra \Prop \\
    & \sim_\Tms && : (\Gamma\,\Delta:\Con) \ra \Tms\,\Gamma\,\Delta \ra \Tms\,\Gamma\,\Delta \ra \Prop
  \end{alignat*}
  No more separation of pre-things and things. One can only talk about
  well-typed terms.
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Quotient intrinsic syntax}
  A quotient inductive inductive definition of 4 families of sets.

  \begin{alignat*}{5}
    & \Con && : \Set \\
    & \Ty && : \Con \ra \Set \\
    & \Tm && : (\Gamma : \Con) \ra \Ty\,\Gamma \ra \Set \\
    & \Tms && : \Con \ra \Con \ra \Set \\
  \end{alignat*}
  \begin{itemize}
  \item Conversion relation is the identity type for each
    set. Conversion rules (e.g. $\beta$, $\eta$) are given as equality
    constructors.
  \item Rules for ER, coercion and congruence are properties of the
    identity type.
  \item No more separation of convertible things. One can only do
    constructions on the syntax up to equality.
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{The syntax for a type theory with $\Pi$ and an empty universe}
\fontsize{9}{9.2}\selectfont

\begin{alignat*}{6}
  & \cdot && : \Con && 												  [\id] && : A[\id] \equiv A \\
  & \blank,\blank && : (\Gamma : \Con) \ra \Ty \, \Gamma \ra \Con && 						  [][] && : A[\sigma][\nu] \equiv A[\sigma \circ \nu] \\
  & \blank[\blank] && : \Ty \, \Delta \ra \Tms \, \Gamma \, \Delta \ra \Ty \, \Gamma && 		          {\id\circ} && : \id \circ \sigma \equiv \sigma \\
  & \id && : \Tms \, \Gamma \, \Gamma && 							                  {\circ\id} && : \sigma \circ \id \equiv \sigma \\
  & \blank\circ\blank && : \Tms\,\Theta\,\Delta \ra \Tms\,\Gamma\,\Theta \ra \Tms\,\Gamma\,\Delta && 		  {\circ\circ} && : (\sigma \circ \nu) \circ \delta \equiv \sigma \circ (\nu \circ \delta) \\  
  & \epsilon && : \Tms \, \Gamma \, \cdot && 									  \epsilon\eta && : \{\sigma : \Tms\,\Gamma\,\cdot\} \ra \sigma \equiv \epsilon \\
  & \blank,\blank && : (\sigma : \Tms\,\Gamma\,\Delta) \ra \Tm\,\Gamma\,A[\sigma] \ra \Tms\,\Gamma\,(\Delta,A) \hspace{1em} && \pi_1\beta && : \pi_1\,(\sigma, t) \equiv \sigma \\
  & \pi_1 && : \Tms\,\Gamma\,(\Delta,A) \ra \Tms\,\Gamma\,\Delta &&                                               \pi\eta && : (\pi_1\,\sigma, \pi_2\,\sigma) \equiv \sigma \\
  & \blank[\blank] && : \Tm\,\Delta\,A \ra (\sigma : \Tms\,\Gamma\,\Delta) \ra \Tm\,\Gamma\,A[\sigma] &&          {,\circ} && : (\sigma, t) \circ \nu \equiv (\sigma \circ \nu) , (\tr{[][]}{t[\nu]}) \\
  & \pi_2 && : (\sigma : \Tms\,\Gamma\,(\Delta,A)) \ra \Tm\,\Gamma\,A[\pi_1 \, \sigma] &&                         \color{red}{\pi_2\beta} && \color{red}{: \pi_2\,(\sigma, t) \equiv^{\pi_1\beta} t} \\\hline
  & \Pi && : (A : \Ty \, \Gamma) \ra \Ty \, (\Gamma , A) \ra \Ty \, \Gamma &&                                     \Pi[] && : (\Pi\,A\,B)[\sigma] \equiv \Pi\,A[\sigma]\,B[\sigma\uparrow] \\
  & \lam && : \Tm\,(\Gamma,A)\,B \ra \Tm\,\Gamma\,(\Pi\,A\,B) &&                                                  \Pi\beta && : \app\,(\lam\,t) \equiv t \\
  & \app && : \Tm\,\Gamma\,(\Pi\,A\,B) \ra \Tm\,(\Gamma,A)\,B &&                                                  \Pi\eta && : \lam\,(\app\,t) \equiv t \\
  & && &&                                                                                                         \lam[] && : (\lam\,t)[\sigma] \equiv^{\Pi[]} \lam\,(t[\sigma\uparrow]) \\\hline
  & \U && : \Ty \, \Gamma &&                                                                                      \U[] && : \U[\sigma] \equiv \U \\
  & \El && : \Tm \, \Gamma \, \U \ra \Ty \, \Gamma &&                                                             \El[] && : (\El\,\hat{A})[\sigma] \equiv \El\,(\tr{\U[]}{\hat{A}[\sigma]})
\end{alignat*}
\end{frame}

%=====================================================================

\section{Defining functions from the intrinsic syntax}

\begin{frame}{Nondependent eliminator (recursor)}
  The inductive type of natural numbers:
  \begin{alignat*}{5}
    & \N : \Set \\
    & \zero : \N \\
    & \suc : \N \ra \N
  \end{alignat*}
  Arguments of the recursor (a natural number algebra):
  \begin{alignat*}{5}
    & \N_1 : \Set \\
    & \zero_1 : \N_1 \\
    & \suc_1 : \N_1 \ra \N_1
  \end{alignat*}
  The recursor is a function which respects the operations (an algebra
  morphism).
  \begin{alignat*}{5}
    & \Rec\N : \N \ra \N_1 \\
    & \Rec\N\,\zero = \zero_1 \\
    & \Rec\N\,(\suc\,n) = \suc_1\,(\Rec\N\,n)
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Recursor for a higher inductive type}
  Example:
  \begin{alignat*}{10}
    & \rlap{$\text{Constructors:} \hspace{8.4em} \text{Arguments of the recursor:}$} \\
    & \mathsf{I} && : \Set                                       && \mathsf{I}_1 && : \Set \\
    & \mathsf{left} && : \mathsf{I}                              && \mathsf{left}_1 && : \mathsf{I}_1 \\
    & \mathsf{right} && : \mathsf{I}                             && \mathsf{right}_1 && : \mathsf{I}_1 \\
    & \mathsf{segment} && : \mathsf{left} \equiv \mathsf{right} \hspace{5em} && \mathsf{segment}_1 && : \mathsf{left}_1 \equiv \mathsf{right}_1
  \end{alignat*}
  The recursor:
  \begin{alignat*}{5}
    & \Rec\mathsf{I} : \mathsf{I} \ra \mathsf{I}_1 \\
    & \Rec\mathsf{I}\,\mathsf{left} = \mathsf{left}_1 \\
    & \Rec\mathsf{I}\,\mathsf{right} = \mathsf{right}_1 \\
    & \ap\, \Rec\mathsf{I}\,\mathsf{segment} = \mathsf{segment}_1 \\
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Recursor for intrinsic type theory}
  \begin{itemize}
  \item An algebra for the quotient intrinsic syntax is a categories
    with families (CwF, a notion of model of type theory).
    \begin{itemize}
    \item An algebra for the intrinsic syntax is a more relaxed CwF,
      where conversion can be interpreted by relations other than
      equality.
    \end{itemize}
  \item The recursor is a strict morphism of models from the initial
    model (the syntax) to the model given by the arguments of the
    recursor.
  \item The recursor for an inductive inductive type is recursive
    recursive (Forsberg).
    \begin{alignat*}{5}
      & \hspace{-2em} \Con : \Set   && \Ty : \Con \ra \Set \\
      & \hspace{-2em} \Con_1 : \Set && \Ty_1 : \Con_1\ra\Set \\
      & \hspace{-2em} \Rec\Con : \Con \ra \Con_1 \hspace{1em} &&  \Rec\Ty  : \Ty\,\Gamma\ra \Ty_1\,(\Rec\Con\,\Gamma)
    \end{alignat*}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}[fragile]{Recursor for quotient inductive types in Agda}
\begin{verbatim}
{-# OPTIONS --rewriting #-}

postulate
  Con : Set
  Ty  : Con → Set
  _,_ : (Γ : Con) → Ty Γ → Con

  RecCon : Con → Con₁
  RecTy  : Ty Γ → Ty₁ (RecCon Γ)

  β, : RecCon (Γ , A) ≡ RecCon Γ ,₁ RecTy A

{-# REWRITE β, #-}
...
\end{verbatim}
\end{frame}

%=====================================================================

\section{Relationship of extrinsic and intrinsic syntax}

\begin{frame}{From intrinsic to extrinsic syntax (work in progress)}
  \begin{itemize}
  \item Inductive inductive types can be represented by normal
    inductive types and ``typing relations'' (noticed by Altenkirch
    and Capriotti). Similar to representing indexed W-types by plain
    ones.
    \begin{itemize}
    \item If we start with intrinsic syntax, we get back an extrinsic
      syntax which is
      \begin{itemize}
      \item fully annotated (e.g. $\blank\circ\blank$ has 5 arguments)
      \item paranoid (e.g. typing for $\lam$ needs well-formedness of
        $\Gamma$)
      \end{itemize}
    \item The usual syntax comes after some ad-hoc constructions
      (removing assumptions that are admissible).
    \end{itemize}
  \item Going from quotient intrinsic syntax to intrinsic is doing an
    internal setoid-interpretation. For example, the inductively
    defined setoid equality relation for $\N$:
    \begin{alignat*}{5}
      & \sim_\N && : \N \ra \N \ra \Prop \\
      & \sim_\zero && : \zero \sim_\N \zero \\
      & \sim_\suc && : (n_0 \sim_\N n_1) \ra \suc\,n_0 \sim_\N \suc\,n_1
    \end{alignat*}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{From extrinsic to intrinsic syntax (Streicher: Semantics of TT)}
  A model is given by a category with families $\C$.

  A preterm carries enough information to reconstruct its precontext
  ($\ctx$) and pretype ($\ty$). Similarly for the other pre-things.
  
  Partial functions by recursion on the presyntax:
  \begin{alignat*}{5}
    & \ll\blank\rr_\Con && : \PCon \rightharpoonup |\C| \\
    & \ll\blank\rr_\Ty && : (A : \PTy) \rightharpoonup \Ty_\C\big(\ll\ctx\,A\rr_\Con\big) \\
    & \ll\blank\rr_\Tm && : (t : \PTm) \rightharpoonup \Tm_\C\big(\ll\ctx\,t\rr_\Con, \ll\ty\,t\rr_\Ty\big) \\
    & \ll\blank\rr_\Tms && : (\sigma : \PTms) \rightharpoonup \C\big(\ll\dom\,\sigma\rr_\Con, \ll\cod\,\sigma\rr_\Con\big)
  \end{alignat*}

  By induction on the typing and conversion relations we have:
  \begin{alignat*}{5}
    & \Gamma \vdash_\Tm t : A \ra \ll t \rr_\Tm \text{ is defined} \\
    & \Gamma \vdash t \sim_\Tm t' : A \ra \ll t \rr_\Tm \text{ and } \ll t' \rr_\Tm \text{ are defined and are equal}
  \end{alignat*}
  Similarly for contexts, types and subsitutions.

  This can be used to map extrinsic syntax to quotient intrinsic
  syntax.
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Conjecture}
  \begin{equation*}
    \Con \,\,\cong\,\, \Big(\big(\Gamma:\PCon\big)\,\times\,\vdash\Gamma\Big)/{\sim_\Con}
  \end{equation*}
  \begin{alignat*}{5}
    & && \,(\Gamma:\Con)\,\times\,\Ty\,\Gamma \\
    & \cong\,\, && \Big(\big(\Gamma:\PCon\big)\,\times\,{\vdash\Gamma}\,\times\,(A:\PTy)\,\times\,{\Gamma\vdash A}\Big)/{\sim_\Con}/{\sim_\Ty}
  \end{alignat*}
  \vspace{1em}
  \begin{equation*}
    ...
  \end{equation*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Typechecking (not yet formalised)}
  \begin{alignat*}{5}
    & \mathsf{inferTm} : (\Gamma : \Con)(t : \PTm) \ra (A' : \Ty\,\Gamma)\times\Tm\,\Gamma\,A' \\
    & \mathsf{checkTm} : (\Gamma : \Con)(t : \PTm) \ra (A' : \Ty\,\Gamma) \ra \Tm\,\Gamma\,A' \\
    & \mathsf{checkTy} : (\Gamma : \Con) \ra \PTy \ra \Ty\,\Gamma \\
    & \mathsf{inferTm}\,\Gamma\,x := \mathsf{lookup}\,\Gamma\,x \\
    & \mathsf{inferTm}\,\Gamma\,(t\,u) := \mathsf{case}\,\mathsf{inferTm}\,\Gamma\,t\,\mathsf{of} \\
    & \hspace{3em} \big((x:A')\ra B', t'\big) \mapsto \mathsf{case}\,\mathsf{checkTm}\,\Gamma\,u\,A'\mathsf{of} \\
    & \hspace{6em} u' \mapsto \big(B'[x \mapsto u'], t'\,u'\big) \\
    & \mathsf{inferTm}\,\Gamma\,(t : A) := \mathsf{case}\,\mathsf{checkTy}\,\Gamma\,A\,\mathsf{of} \\
    & \hspace{3em} A' \mapsto \mathsf{case}\,\mathsf{checkTm}\,\Gamma\,t\,A'\mathsf{of} \\
    & \hspace{6em} t' \mapsto (A', t') \\
    & \mathsf{checkTm}\,\Gamma\,(\lambda x.t)\,\big((x:A')\ra B'\big) := \lam\,(\mathsf{checkTm}\,(\Gamma,x:A')\,t\,B') \\
    & \mathsf{checkTm}\,\Gamma\,t\,A := \mathsf{case}\,\mathsf{inferTm}\,\Gamma\,t\,\mathsf{of} \\
    & \hspace{3em} (A', t') \mapsto \mathsf{if}\,\color{red}A \overset{?}{=} A'\color{black}\,\mathsf{then}\,t'
  \end{alignat*}
\end{frame}

%=====================================================================

\section{Models}

\begin{frame}{Models formalised in Agda (with $\mathsf{K}$ and funext)}
  For a theory with $\Pi$, a base type and a family over the base
  type.
  \begin{itemize}
  \item Non-dependent eliminator:
    \begin{itemize}
    \item standard model
    \item presheaf model
    \item setoid model
    \end{itemize}
  \item Dependent eliminator:
    \begin{itemize}
    \item logical predicate translation of Bernardy
    \item presheaf logical predicate interpretation
    \end{itemize}
  \end{itemize}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{Standard model}
  \begin{itemize}
  \item A sanity check
  \item Every syntactic construct is interpreted as the corresponding
    metatheoretic construction.
    \begin{alignat*}{5}
      & \Con_1 && := \Set \\
      & \Ty_1\,\ll\Gamma\rr && := \ll\Gamma\rr \ra \Set \\
      & \ll\Gamma\rr,_1\ll A\rr && := (\gamma : \ll\Gamma\rr)\times \ll A\rr\,\gamma \\
      & ... \\
      & \Pi_1\,\ll A\rr\,\ll B\rr\,\gamma && := (x : \ll A\rr\,\gamma) \ra \ll B\rr\,(\gamma, x) \\
      & \lam_1\,\ll t\rr\,\gamma && := \lambda x \ra \ll t\rr\,(\gamma, x) \\
      & ... \\
      & {\Pi\beta}_1 && := \refl
    \end{alignat*}
    \item We defined this for a syntax with $\Sigma$, $\bot$, $\top$,
      $\Bool$, $\N$, $\Id$ as well.
  \end{itemize}
\end{frame}
%---------------------------------------------------------------------

\begin{frame}{Logical predicate interpretation}
  Parametricity expressed as a syntactic translation.
  \begin{equation*}
    \begin{gathered}
      \infer{\Gamma^\P \vdash}{\Gamma \vdash}
    \end{gathered}
    \hspace{2em}
    \begin{gathered}
      \infer{\Gamma^\P \vdash A^\P : A \ra \U}{\Gamma \vdash A : \U}
    \end{gathered}
    \hspace{2em}
    \begin{gathered}
      \infer{\Gamma^\P \vdash t^\P : A^\P\,t}{\Gamma \vdash t : A}
    \end{gathered}
  \end{equation*}
  All of the following equations need to be well-typed (and preserve
  conversion).
  \begin{alignat*}{5}
    & (\Gamma,x:A)^\P && := \Gamma^\P,x:A, x^M : A^\P\,x \\
    & x^\P && := x^M \\
    & \U^\P\,A && := A \ra \U \\
    & \big((x:A)\ra B\big)^\P\,f && := (x:A)(x^M:A^\P\,x)\ra B^\P\,(f\,x) \\
    & (\lambda x .t)^\P && := \lambda x \,x^M.t^\P \\
    & (f\,a)^\P && := f^\P\,a\,a^\P
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{NBE for dependent types}
  Presheaf logical predicate
  \begin{alignat*}{5}
    & \P_\Delta && : \forall\Psi.\Tms\,\Psi\,\Delta \ra \Set \\
    & \P_A && : \forall\Psi.(\rho:\Tms\,\Psi\,\Gamma)\ra\P_\Gamma\,\rho \ra \Tm\,\Psi\,A[\rho]\ra\Set \\
    & \P_\sigma && : \forall\Psi.(\rho:\Tms\,\Psi\,\Gamma)\ra\P_\Gamma\,\rho \ra \P_\Delta\,(\sigma\circ\rho) \\
    & \P_t && : \forall\Psi.(\rho:\Tms\,\Psi\,\Gamma)(p : \P_\Gamma\,\rho) \ra \P_A\,\rho\,p\,(t[\rho])
  \end{alignat*}
  At the base type:
  \[
  \P_\iota\,\rho\,t = \isNf\,\Psi\,\iota\,t
  \]
  Quote and unquote:
  \begin{alignat*}{5}
    & \q_A : (p:\P_\Gamma\,\rho)(t : \Tm\,\Psi\,A[\rho]) \ra \P_A\,\rho\,p\,t \ra \isNf\,\Psi\,A[\rho]\,t \\
    & \u_A : (p:\P_\Gamma\,\rho)(t : \Tm\,\Psi\,A[\rho]) \ra \isNe\,\Psi\,A[\rho]\,t \ra \P_A\,\rho\,p\,t
  \end{alignat*}
\end{frame}

%---------------------------------------------------------------------

\begin{frame}{NBE for a universe and Bool with large elimination (not yet formalised)}
\begin{alignat*}{6}
  & \P_\Delta && : \forall\Psi.(\rho:\Tms\,\Psi\,\Delta)\ra (\r:\Set)\times(\u : \isNes\,\Psi\,\Delta\,\rho \ra \r) \\
  & \P_A && : \forall\Psi.(\rho:\Tms\,\Psi\,\Delta)\ra\P_\Gamma\,\rho.\r\ra(t : \Tm\,\Psi\,A[\rho]) \\
  & && \hspace{10em} \ra (\r:\Set)\times(\q : \r \ra \isNf\,\Psi\,A[\rho]\,t) \\
  & && \hspace{14.87em} \times(\u:\isNe\,\Psi\,A[\rho]\,t \ra r) \\
  & \P_t && : \forall\Psi.(\rho:\Tms\,\Psi\,\Delta)\ra \P_\Gamma\,\rho.\r \ra \P_A\,\rho\,q\,(t[\rho]).\r
\end{alignat*}

\begin{alignat*}{6}
  & \P_\U\,\Psi\,(\rho:\Tms\,\Psi\,\Gamma)(p:\P_\Gamma\,\rho)(\hat{A}:\Tm\,\Psi\,\U).\r \\
  & := \isNf\,\Psi\,\U\,\hat{A} \times\, \forall\Omega.(\beta:\REN(\Omega, \Psi))(t : \Tm\,\Omega\,(\El\,\hat{A}[\beta])) \\
  & \hspace{1em} \ra (\r : \Set)\times(\q : \r \ra \isNf\,\Omega\,(\El\,\hat{A}[\beta])\,t)\times(\u : \isNe\,\Omega\,(\El\,\hat{A}[\beta])\,t \ra \r)
\end{alignat*}
\end{frame}

%=====================================================================

\section*{Summary}

\begin{frame}{Summary}
  Quotient intrinsic syntax has the following properties:
  \begin{itemize}
  \item more abstract: close to categorical models; analogy with HITs
    and setoids (c.f. Peter Dybjer's talk)
  \item get back old-style syntax using general methods (WIP)
  \item typechecking and normalisation fit well
  \item definition of operations on the syntax in a type-safe way
  \end{itemize}
  Future work: 
  \begin{itemize}
  \item finish unfinished things
  \item extend the syntax with QIITs to do full internalisation
  \item formalisation is hard
  \item we need cubical type theory or similar to compute with
    quotient types
  \end{itemize}
  Formalisation: \url{http://bitbucket.org/akaposi/tt-in-tt}
\end{frame}

%---------------------------------------------------------------------

\end{document}
