{-# OPTIONS #-}

module ReflGraph.Core where

open import lib

open import ReflGraph.Decl

∙ : Con
∙ = record
  { ⌜_⌝C = ⊤
  ; _⁼C  = λ _ _ → ⊤
  ; RC   = λ _ → tt
  }

_,_ : (Γ : Con) → Ty Γ → Con
Γ , A = record
  { ⌜_⌝C = Σ ⌜ Γ ⌝C ⌜ A ⌝T
  ; _⁼C  = λ { (γ₀ ,Σ α₀)(γ₁ ,Σ α₁) → Σ ((Γ ⁼C) γ₀ γ₁) λ γ₌ → (A ⁼T) γ₌ α₀ α₁ }
  ; RC   = λ { (γ ,Σ α) → RC Γ γ ,Σ RT A α }
  }

_[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ
_[_]T {Γ}{Δ} A σ = record
  { ⌜_⌝T = λ γ → ⌜ A ⌝T (⌜ σ ⌝s γ)
  ; _⁼T = λ γ₌ α₀ α₁ → (A ⁼T) ((σ ⁼s) γ₌) α₀ α₁
  ; RT = λ {γ} α → transport (λ z → (A ⁼T) z α α) (Rs σ γ) (RT A α)
  }

id : ∀{Γ} → Tms Γ Γ
id {Γ} = record
  { ⌜_⌝s = λ γ → γ
  ; _⁼s  = λ γ₌ → γ₌
  ; Rs   = λ γ → refl
  }

_∘_ : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
_∘_ {Γ}{Δ}{Σ} σ ν = record
  { ⌜_⌝s = λ γ → ⌜ σ ⌝s (⌜ ν ⌝s γ)
  ; _⁼s  = λ γ₌ → (σ ⁼s) ((ν ⁼s) γ₌)
  ; Rs   = λ γ → Rs σ (⌜ ν ⌝s γ) ◾ ap (σ ⁼s) (Rs ν γ)
  }

ε : ∀{Γ} → Tms Γ ∙
ε {Γ} =  record
  { ⌜_⌝s = λ γ → tt
  ; _⁼s  = λ γ₌ → tt
  ; Rs   = λ γ → refl
  }

_,s_ : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ , A)
σ ,s t =  record
  { ⌜_⌝s = λ γ → ⌜ σ ⌝s γ ,Σ ⌜ t ⌝t γ
  ; _⁼s  = λ γ₌ → (σ ⁼s) γ₌ ,Σ (t ⁼t) γ₌
  ; Rs   = λ γ → ,Σ= (Rs σ γ) (Rt t γ)
  }

π₁ : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ , A) → Tms Γ Δ
π₁ σ =  record
  { ⌜_⌝s = λ γ → proj₁ (⌜ σ ⌝s γ)
  ; _⁼s  = λ γ₌ → proj₁ ((σ ⁼s) γ₌)
  ; Rs   = λ γ → ,Σ=0 (Rs σ γ)
  }

_[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
_[_]t {Γ}{Δ}{A} t σ = record
  { ⌜_⌝t = λ γ → ⌜ t ⌝t (⌜ σ ⌝s γ)
  ; _⁼t  = λ γ₌ → (t ⁼t) ((σ ⁼s) γ₌)
  ; Rt   = λ γ → ap (coe (ap (λ z → (A ⁼T) z _ _) (Rs σ γ))) (Rt t (⌜ σ ⌝s γ))
               ◾ apd (t ⁼t) (Rs σ γ)
  }

π₂ : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ , A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ {Γ}{Δ}{A} σ = record
  { ⌜_⌝t = λ γ → proj₂ (⌜ σ ⌝s γ)
  ; _⁼t  = λ γ₌ → proj₂ ((σ ⁼s) γ₌)
  ; Rt   = λ γ → ,Σ=1 (Rs σ γ)
  }

[id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
[id]T = refl

[][]T : {Γ Θ Δ : Con}{A : Ty Δ}{δ : Tms Γ Θ}{σ : Tms Θ Δ}
        → A [ σ ]T [ δ ]T ≡ A [ _∘_ {Γ}{Θ}{Δ} σ δ ]T
[][]T {Γ}{Θ}{Δ}{A}{δ}{σ} = Ty= (funexti λ γ → funext λ α →
  J (λ z → transport (λ z → (A ⁼T) ((σ ⁼s) z) α α) z (transport (λ z → (A ⁼T) z α α) (Rs σ (⌜ δ ⌝s γ)) (RT A α)) ≡ transport (λ z → (A ⁼T) z α α) (Rs σ (⌜ δ ⌝s γ) ◾ ap (σ ⁼s) z) (RT A α))
  (J (λ z → transport (λ z → (A ⁼T) z α α) z (RT A α) ≡ transport (λ z → (A ⁼T) z α α) (z ◾ refl) (RT A α)) refl
  (Rs σ (⌜ δ ⌝s γ)))
  (Rs δ γ))

idl : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Δ}{Δ} id δ) ≡ δ
idl {δ = δ} = Tms=' {!!} -- (funext λ γ → ◾lid _ ◾ apid (Rs δ γ))

idr : ∀{Γ Δ}{δ : Tms Γ Δ} → (_∘_ {Γ}{Γ}{Δ} δ id) ≡ δ
idr {δ = δ} = Tms=' (funext λ γ → ◾rid (Rs δ γ))

ass : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
      → _∘_ {Γ}{Δ}{Ω}(_∘_ {Δ}{Σ}{Ω} σ δ) ν ≡ _∘_ {Γ}{Σ}{Ω} σ (_∘_ {Γ}{Δ}{Σ} δ ν)
ass {σ = σ}{δ}{ν}
  = Tms=' (funext λ γ → ◾ass (Rs σ (⌜ δ ⌝s (⌜ ν ⌝s γ)))(ap (σ ⁼s) (Rs δ (⌜ ν ⌝s γ)))(ap (λ γ₌ → (σ ⁼s) ((δ ⁼s) γ₌)) (Rs ν γ)) ⁻¹
                      ◾ ap (λ z → Rs σ (⌜ δ ⌝s (⌜ ν ⌝s γ)) ◾ z)
                           ( ap (λ z → ap (σ ⁼s) (Rs δ (⌜ ν ⌝s γ)) ◾ z) (apap {f = δ ⁼s}{g = σ ⁼s}(Rs ν γ))
                           ◾ ap◾ (σ ⁼s)(Rs δ (⌜ ν ⌝s γ))(ap (δ ⁼s) (Rs ν γ)) ⁻¹))

π₁β : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
      → (π₁ {Γ}{Δ}{A}(_,s_ {Γ}{Δ} δ {A} a)) ≡ δ
π₁β {δ = δ}{a} = Tms=' (funext λ γ → ,Σ=β0 (Rs δ γ) (Rt a γ))

πη : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ , A)}
      → _,s_ {Γ}{Δ}(π₁ {Γ}{Δ}{A} δ){A}(π₂ {Γ}{Δ}{A} δ) ≡ δ
πη {δ = δ} = Tms=' (funext λ γ → ,Σ=η (Rs δ γ))

εη : ∀{Γ}{σ : Tms Γ ∙}
      → σ ≡ ε
εη {Γ}{σ} = Tms= refl refl (funext λ γ → ⊤== _ _)
  where
    ⊤== : {a b : ⊤}(p q : a ≡ b) → p ≡ q
    ⊤== refl refl = refl

,∘ : {Γ Δ Σ : Con}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)} →
  ((_,s_  δ {A} a) ∘ σ) ≡ (_,s_ (δ ∘ σ) {A} (transport (Tm Σ) ([][]T {A = A}{δ = σ}{σ = δ}) (a [ σ ]t)))
,∘ {Γ}{Δ}{Σ}{δ}{σ}{A}{a} = Tms=
  {!!}
  {!!}
  {!!}

{-

(,Σ= (Rs δ (⌜ σ ⌝s γ)) (Rt a (⌜ σ ⌝s γ)) ◾ ap (λ γ₌ → (δ ⁼s) γ₌ ,Σ (a ⁼t) γ₌) (Rs σ γ)) ≡ ,Σ= (Rs δ (⌜ σ ⌝s γ) ◾ ap (δ ⁼s) (Rs σ γ)) (ap (coe (ap (λ z → (A ⁼T) ((δ ⁼s) z) (⌜ a ⌝t (⌜ σ ⌝s γ)) (⌜ a ⌝t (⌜ σ ⌝s γ))) (Rs σ γ))) (Rt a (⌜ σ ⌝s γ)) ◾ apd (a ⁼t) (Rs σ γ))

(λ e → (,Σ= (Rs δ (⌜ σ ⌝s γ)) (Rt a (⌜ σ ⌝s γ)) ◾ ap (λ γ₌ → (δ ⁼s) γ₌ ,Σ (a ⁼t) γ₌) (Rs σ γ)) ≡ ,Σ= (Rs δ (⌜ σ ⌝s γ) ◾ ap (δ ⁼s) (Rs σ γ)) (ap (coe (ap (λ z → (A ⁼T) ((δ ⁼s) z) (⌜ a ⌝t (⌜ σ ⌝s γ)) (⌜ a ⌝t (⌜ σ ⌝s γ))) (Rs σ γ))) (Rt a (⌜ σ ⌝s γ)) ◾ apd (a ⁼t) (Rs σ γ))


(Rs δ (⌜ σ ⌝s γ)) : RC Δ (⌜ δ ⌝s (⌜ σ ⌝s γ)) ≡ (δ ⁼s) (RC Γ (⌜ σ ⌝s γ))
(Rt a (⌜ σ ⌝s γ)) :
   transport (λ z → (A ⁼T) z (⌜ a ⌝t (⌜ σ ⌝s γ)) (⌜ a ⌝t (⌜ σ ⌝s γ))) (Rs δ (⌜ σ ⌝s γ)) (RT A (⌜ a ⌝t (⌜ σ ⌝s γ)))
   ≡ (a ⁼t) (RC Γ (⌜ σ ⌝s γ))
(Rs σ γ) : RC Γ (⌜ σ ⌝s γ) ≡ (σ ⁼s) (RC Σ γ)
-}
