{-# OPTIONS --without-K #-}

module ReflGraph.ExtIden where

open import lib

open import ReflGraph.Decl
open import ReflGraph.Core

Id : {Γ : Con}{A : Ty Γ} → Tm Γ A → Tm Γ A → Ty Γ
Id {Γ}{A} t u = record
  { ⌜_⌝T = λ γ → ⌜ t ⌝t γ ≡ ⌜ u ⌝t γ
  ; _⁼T = λ {γ}{γ'} γ₌ e e' → (t ⁼t) γ₌ ≡[ ap2 (λ z z' → (A ⁼T) γ₌ z z') e e' ]≡ (u ⁼t) γ₌
  ; RT = λ {γ} e →
      ap (coe (ap2 (λ z z' → (A ⁼T) (RC Γ γ) z z') e e)) (Rt t γ ⁻¹) ◾
      J (λ e → coe (ap2 (λ z z' → (A ⁼T) (RC Γ γ) z z') e e) (RT A (⌜ t ⌝t γ)) ≡ coe (ap (λ z → (A ⁼T) (RC Γ γ) z z) e) (RT A (⌜ t ⌝t γ))) refl e ◾
      apd (RT A) e ◾
      Rt u γ
  }

ref : {Γ : Con} {A : Ty Γ} (u : Tm Γ A) → Tm Γ (Id u u)
ref {Γ}{A} u = record
  { ⌜_⌝t = λ γ → refl
  ; _⁼t = λ γ₌ → refl
  ; Rt = λ γ → J (λ e → (ap (λ a → a) (e ⁻¹) ◾ refl ◾ refl ◾ e) ≡ refl) refl (Rt u γ)
  }

reflect : {Γ : Con} {A : Ty Γ} {u v : Tm Γ A} → Tm Γ (Id u v) → u ≡ v
reflect {Γ}{A}{u}{v} e = Tm=
  (funext ⌜ e ⌝t)
  (funexti λ γ → funexti λ γ' → funext λ γ~ → {!!} ◾ (e ⁼t) γ~)
  (funext λ γ → {!Rt e γ!})

open import TT.ExtIden

i : ExtIden c
i = record
  { Id = Id
  ; Id[] = Ty= (funexti λ γ → funext λ α → {!!})
  ; ref = ref
  ; ref[] = {!!}
  ; reflect = reflect
  }
