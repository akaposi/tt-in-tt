{-# OPTIONS #-}

module ReflGraph.Decl where

open import lib
open import JM

record Con : Set₁ where
  field
    ⌜_⌝C : Set
    _⁼C  : ⌜_⌝C → ⌜_⌝C → Set
    RC   : (γ : ⌜_⌝C) → _⁼C γ γ
  infix 7 ⌜_⌝C
open Con public

record Ty (Γ : Con) : Set₁ where
  field
    ⌜_⌝T : ⌜ Γ ⌝C → Set
    _⁼T  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀)(α₁ : ⌜_⌝T γ₁) → Set
    RT   : {γ : ⌜ Γ ⌝C}(α : ⌜_⌝T γ) → _⁼T (RC Γ γ) α α
  infix 7 ⌜_⌝T
open Ty public

record Tms (Γ Δ : Con) : Set where
  field
    ⌜_⌝s : ⌜ Γ ⌝C → ⌜ Δ ⌝C
    _⁼s  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s γ₀) (⌜_⌝s γ₁)
    Rs   : (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s γ) ≡ _⁼s (RC Γ γ)
  infix 7 ⌜_⌝s
open Tms public

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    ⌜_⌝t : (γ : ⌜ Γ ⌝C) → ⌜ A ⌝T γ
    _⁼t  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (A ⁼T) γ₌ (⌜_⌝t γ₀) (⌜_⌝t γ₁)
    Rt   : (γ : ⌜ Γ ⌝C) → RT A (⌜_⌝t γ) ≡ _⁼t (RC Γ γ)
  infix 7 ⌜_⌝t
open Tm public

-- congruences

Ty=
  : {Γ : Con}
    {⌜_⌝T  : ⌜ Γ ⌝C → Set}
    {_⁼T : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜_⌝T γ₀)(α₁ : ⌜_⌝T γ₁) → Set}
    {RT₀ RT₁ : {γ : ⌜ Γ ⌝C}(α : ⌜_⌝T γ) → _⁼T (RC Γ γ) α α}
    (RT₌ : _≡_ {A = {γ : ⌜ Γ ⌝C}(α : ⌜_⌝T γ) → _⁼T (RC Γ γ) α α} RT₀ RT₁)
  → _≡_ {A = Ty Γ}
        (record { ⌜_⌝T = ⌜_⌝T ; _⁼T = _⁼T ; RT = RT₀ })
        (record { ⌜_⌝T = ⌜_⌝T ; _⁼T = _⁼T ; RT = RT₁ })
Ty= refl = refl


RsT=
  : {Γ Δ : Con}
    {⌜_⌝s₀ ⌜_⌝s₁ : ⌜ Γ ⌝C → ⌜ Δ ⌝C}(⌜_⌝s₌ : ⌜_⌝s₀ ≡ ⌜_⌝s₁)
    {_⁼s₀ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₀ γ₀) (⌜_⌝s₀ γ₁)}
    {_⁼s₁ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₁ γ₀) (⌜_⌝s₁ γ₁)}
    (_⁼s₌ : _≡_ {A = {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₁ γ₀) (⌜_⌝s₁ γ₁)}
                (coe (ap (λ z → {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (z γ₀) (z γ₁)) ⌜_⌝s₌) _⁼s₀)
                _⁼s₁)
  → _≡_ ((γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s₀ γ) ≡ _⁼s₀ (RC Γ γ))
        ((γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s₁ γ) ≡ _⁼s₁ (RC Γ γ))
RsT= refl refl = refl

Tms=
  : {Γ Δ : Con}
    {⌜_⌝s₀ ⌜_⌝s₁ : ⌜ Γ ⌝C → ⌜ Δ ⌝C}(⌜_⌝s₌ : ⌜_⌝s₀ ≡ ⌜_⌝s₁)
    {_⁼s₀ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₀ γ₀) (⌜_⌝s₀ γ₁)}
    {_⁼s₁ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₁ γ₀) (⌜_⌝s₁ γ₁)}
    (_⁼s₌ : _≡_ {A = {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s₁ γ₀) (⌜_⌝s₁ γ₁)}
                (coe (ap (λ z → {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (z γ₀) (z γ₁)) ⌜_⌝s₌) _⁼s₀)
                _⁼s₁)
    {Rs₀ : (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s₀ γ) ≡ _⁼s₀ (RC Γ γ)}
    {Rs₁ : (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s₁ γ) ≡ _⁼s₁ (RC Γ γ)}
    (Rs₌ : _≡_ {A = (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s₁ γ) ≡ _⁼s₁ (RC Γ γ)}
               (coe (RsT= {Γ}{Δ} ⌜_⌝s₌ _⁼s₌) Rs₀)
               Rs₁)
  → _≡_ {A = Tms Γ Δ}(record { ⌜_⌝s = ⌜_⌝s₀ ; _⁼s = _⁼s₀ ; Rs = Rs₀ })(record { ⌜_⌝s = ⌜_⌝s₁ ; _⁼s = _⁼s₁ ; Rs = Rs₁ })
Tms= refl refl refl = refl

Tms='
  : {Γ Δ : Con}
    {⌜_⌝s : ⌜ Γ ⌝C → ⌜ Δ ⌝C}
    {_⁼s : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (Δ ⁼C) (⌜_⌝s γ₀) (⌜_⌝s γ₁)}
    {Rs₀ Rs₁ : (γ : ⌜ Γ ⌝C) → RC Δ (⌜_⌝s γ) ≡ _⁼s (RC Γ γ)}(Rs₌ : Rs₀ ≡ Rs₁)
  → _≡_ {A = Tms Γ Δ}(record { ⌜_⌝s = ⌜_⌝s ; _⁼s = _⁼s ; Rs = Rs₀ })(record { ⌜_⌝s = ⌜_⌝s ; _⁼s = _⁼s ; Rs = Rs₁ })
Tms=' {Γ}{Δ} Rs₌ = Tms= {Γ}{Δ} refl refl Rs₌

⌜Ty=⌝
  : {Γ : Con}
    {⌜⌝T  : ⌜ Γ ⌝C → Set}
    {_⁼T : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁)(α₀ : ⌜⌝T γ₀)(α₁ : ⌜⌝T γ₁) → Set}
    {RT₀ RT₁ : {γ : ⌜ Γ ⌝C}(α : ⌜⌝T γ) → _⁼T (RC Γ γ) α α}(RT₌ : _≡_ {A = {γ : ⌜ Γ ⌝C}(α : ⌜⌝T γ) → _⁼T (RC Γ γ) α α} RT₀ RT₁)
    {γ : ⌜ Γ ⌝C}
  → ap (λ z → ⌜ z ⌝T γ) (Ty= {Γ}{⌜⌝T}{_⁼T} RT₌) ≡ refl
⌜Ty=⌝ refl = refl

Tm= : {Γ : Con}{A : Ty Γ}
  {⌜⌝₀ ⌜⌝₁  : (γ : ⌜ Γ ⌝C) → ⌜ A ⌝T γ}(⌜⌝₌ : ⌜⌝₀ ≡ ⌜⌝₁)
  {⁼₀ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (A ⁼T) γ₌ (⌜⌝₀ γ₀) (⌜⌝₀ γ₁)}
  {⁼₁ : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (A ⁼T) γ₌ (⌜⌝₁ γ₀) (⌜⌝₁ γ₁)}
  (⁼₌ : (λ {_}{_} → ⁼₀) ≡[ ap (λ ⌜⌝ → {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (A ⁼T) γ₌ (⌜⌝ γ₀) (⌜⌝ γ₁)) ⌜⌝₌ ]≡ (λ {_}{_} → ⁼₁))
  {R₀ : (γ : ⌜ Γ ⌝C) → RT A (⌜⌝₀ γ) ≡ ⁼₀ (RC Γ γ)}
  {R₁ : (γ : ⌜ Γ ⌝C) → RT A (⌜⌝₁ γ) ≡ ⁼₁ (RC Γ γ)}
  (R₌ : R₀ ≡[ ap2d (λ ⌜⌝ ⁼ → (γ : ⌜ Γ ⌝C) → RT A (⌜⌝ γ) ≡ ⁼ (RC Γ γ)) ⌜⌝₌ ⁼₌ ]≡ R₁) →
  _≡_ {A = Tm Γ A}(record { ⌜_⌝t = ⌜⌝₀ ; _⁼t = ⁼₀ ; Rt = R₀ })(record { ⌜_⌝t = ⌜⌝₁ ; _⁼t = ⁼₁ ; Rt = R₁ })
Tm= refl refl refl = refl

-- model

open import TT.Decl

d : Decl
d = record
  { Con = Con
  ; Ty  = Ty
  ; Tms = Tms
  ; Tm  = Tm
  }

-- congruences using d

open import TT.Decl.Congr.NoJM d

⌜coe⌝t
  : {Γ : Con}{A₀ A₁ : Ty Γ}(A₌ : A₀ ≡ A₁){t : Tm Γ A₀}{γ : ⌜ Γ ⌝C}
  → ⌜ coe (TmΓ= A₌) t ⌝t γ ≡ coe (ap (λ z → ⌜ z ⌝T γ) A₌) (⌜ t ⌝t γ)
⌜coe⌝t refl = refl

coeTmΓ
  : {Γ : Con}{A : Ty Γ}{A' : Ty Γ}(p : A ≡ A')
    {⌜_⌝t : (γ : ⌜ Γ ⌝C) → ⌜ A ⌝T γ}
    {_⁼t  : {γ₀ γ₁ : ⌜ Γ ⌝C}(γ₌ : (Γ ⁼C) γ₀ γ₁) → (A ⁼T) γ₌ (⌜_⌝t γ₀) (⌜_⌝t γ₁)}
    {Rt   : (γ : ⌜ Γ ⌝C) → RT A (⌜_⌝t γ) ≡ _⁼t (RC Γ γ)}
  → coe (TmΓ= p) (record { ⌜_⌝t = ⌜_⌝t ; _⁼t = _⁼t ; Rt = Rt })
  ≡ record { ⌜_⌝t = λ γ → coe (ap (λ z → ⌜ z ⌝T γ) p)
                              (⌜ γ ⌝t)
           ; _⁼t = λ {γ₀}{γ₁} γ₌ → coe (J (λ {A'} p  → (A ⁼T) γ₌ ⌜ γ₀ ⌝t ⌜ γ₁ ⌝t ≡ (A' ⁼T) γ₌ (coe (ap (λ z → ⌜ z ⌝T γ₀) p) ⌜ γ₀ ⌝t) (coe (ap (λ z → ⌜ z ⌝T γ₁) p) ⌜ γ₁ ⌝t)) refl p )
                                       (γ₌ ⁼t)
           ; Rt = λ γ → coe (J (λ {A'} p → (RT A ⌜ γ ⌝t ≡ (RC Γ γ ⁼t)) ≡ (RT A' (coe (ap (λ z → ⌜ z ⌝T γ) p) ⌜ γ ⌝t) ≡ coe (J (λ {A''} p₁ → (A ⁼T) (RC Γ γ) ⌜ γ ⌝t ⌜ γ ⌝t ≡ (A'' ⁼T) (RC Γ γ) (coe (ap (λ z → ⌜ z ⌝T γ) p₁) ⌜ γ ⌝t) (coe (ap (λ z → ⌜ z ⌝T γ) p₁) ⌜ γ ⌝t)) refl p) (RC Γ γ ⁼t))) refl p)
                            (Rt γ)
           }
coeTmΓ refl = refl
