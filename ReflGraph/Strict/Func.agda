{-# OPTIONS --without-K --allow-unsolved-metas #-}

module ReflGraph.Strict.Func where

open import lib

open import ReflGraph.Strict.Decl
open import ReflGraph.Strict.Core

Π : {Γ : Con}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
⌜ Π {Γ} A B ⌝ γ = Σ
  ((α : ⌜ A ⌝ γ) → ⌜ B ⌝ (γ ,Σ α)) λ f → Σ
  ({α₀ α₁ : ⌜ A ⌝ γ}(α₀₁ : (A ⁼) (R Γ γ) α₀ α₁) → (B ⁼) (R Γ γ ,Σ α₀₁) (f α₀) (f α₁)) λ f⁼ →
  (_≡_ {A = (α : ⌜ A ⌝ γ) → (B ⁼) (R Γ γ ,Σ R A α) (f α) (f α)} (λ α → R B (f α)) (λ α → f⁼ (R A α)))
(Π A B ⁼) {γ₀}{γ₁} γ₀₁ (f₀ ,Σ _) (f₁ ,Σ _) = {α₀ : ⌜ A ⌝ γ₀}{α₁ : ⌜ A ⌝ γ₁}(α₀₁ : (A ⁼) γ₀₁ α₀ α₁) → (B ⁼) (γ₀₁ ,Σ α₀₁) (f₀ α₀) (f₁ α₁)
R (Π A B) (f ,Σ (f⁼ ,Σ _)) = f⁼

lam : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm (Γ , A) B) → Tm Γ (Π A B)
⌜ lam {Γ} t ⌝ γ =
  (λ α → ⌜ t ⌝ (γ ,Σ α)) ,Σ
  ((λ {α₀}{α₁} α₀₁ → (t ⁼) (R Γ γ ,Σ α₀₁)) ,Σ
  ap (λ z α → z (γ ,Σ α)) (R t))
(lam t ⁼) γ⁼ α⁼ = (t ⁼) (γ⁼ ,Σ α⁼)
R (lam t) = refl

app : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm Γ (Π A B)) → Tm (Γ , A) B
⌜ app t ⌝ (γ ,Σ α) = proj₁ (⌜ t ⌝ γ) α
(app t ⁼) (γ⁼ ,Σ α⁼) = (t ⁼) γ⁼ α⁼
R (app t) = funext λ { (γ ,Σ α) → {!ap (λ z → z α) (proj₂ (proj₂ (⌜ t ⌝ γ)))!} }

Πβ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B} → app (lam t) ≡ t
Πβ = {!!}

Πη : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)} → _≡_ {A = Tm Γ (Π A B)} (lam (app {A = A}{B = B} t)) t
Πη = {!!}
{-
Π[] : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{Θ : Con}{σ : Tms Θ Γ} →
  Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ (σ ∘ π₁ id) ,s π₂ id ]T)
Π[] = ?

lam[] :
  {Γ : Con}{Δ : Con}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B} →
  lam t [ σ ]t ≡ lam (t [ (σ ∘ π₁ id) ,s π₂ id ]t)
lam[] = ?
-}
