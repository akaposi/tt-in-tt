{-# OPTIONS --without-K #-}

module ReflGraph.Strict.Decl where

open import lib

record Con : Set₁ where
  field
    ⌜_⌝ : Set
    _⁼  : ⌜_⌝ → ⌜_⌝ → Set
    R   : (γ : ⌜_⌝) → _⁼ γ γ
  infix 7 ⌜_⌝
open Con public

record Ty (Γ : Con) : Set₁ where
  field
    ⌜_⌝ : ⌜ Γ ⌝ → Set
    _⁼  : {γ₀ γ₁ : Con.⌜ Γ ⌝}(γ₌ : (Γ ⁼) γ₀ γ₁)(α₀ : ⌜_⌝ γ₀)(α₁ : ⌜_⌝ γ₁) → Set
    R   : {γ : Con.⌜ Γ ⌝}(α : ⌜_⌝ γ) → _⁼ (R Γ γ) α α
  infix 7 ⌜_⌝
open Ty public

record Tms (Γ Δ : Con) : Set where
  field
    ⌜_⌝ : ⌜ Γ ⌝ → ⌜ Δ ⌝
    _⁼  : {γ₀ γ₁ : Con.⌜ Γ ⌝}(γ₌ : (Γ ⁼) γ₀ γ₁) → (Δ ⁼) (⌜_⌝ γ₀) (⌜_⌝ γ₁)
    R   : _≡_ {A = (γ : Con.⌜ Γ ⌝) → (Δ Con.⁼) (⌜_⌝ γ) (⌜_⌝ γ)} (λ γ → R Δ (⌜_⌝ γ)) (λ γ → _⁼ (R Γ γ))
  infix 7 ⌜_⌝
open Tms public

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    ⌜_⌝ : (γ : ⌜ Γ ⌝) → ⌜ A ⌝ γ
    _⁼  : {γ₀ γ₁ : Con.⌜ Γ ⌝}(γ₌ : (Γ ⁼) γ₀ γ₁) → (A ⁼) γ₌ (⌜_⌝ γ₀) (⌜_⌝ γ₁)
    R   : _≡_ {A = (γ : Con.⌜ Γ ⌝) → (A Ty.⁼) (R Γ γ) (⌜_⌝ γ) (⌜_⌝ γ)} (λ γ → R A (⌜_⌝ γ)) (λ γ → _⁼ (R Γ γ))
  infix 7 ⌜_⌝
open Tm public
