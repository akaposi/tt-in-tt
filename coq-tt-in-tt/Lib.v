
Require Export Coq.Unicode.Utf8.

Set Maximal Implicit Insertion.
Set Contextual Implicit.
Set Reversible Pattern Implicit.
Set Primitive Projections.

Polymorphic Inductive eq {A : Type}(a : A) : A → Type :=
  refl : eq a a.
Arguments refl {A}{a}.
Infix "≡" := eq (at level 51, no associativity).

Polymorphic Definition coe {A B : Type}(p : A ≡ B) : A → B :=
  match p with refl => λ a, a end.

Polymorphic Definition ap {A B : Type}(f : A → B){x y : A}(p : x ≡ y): f x ≡ f y :=
  match p with refl => refl end.
Infix "&" := ap (at level 61, left associativity).

Polymorphic Definition trans {A : Type}{x y z : A}(p : x ≡ y) : y ≡ z → x ≡ z :=
  match p with refl => λ q, q end.
Infix "◾" := trans (at level 52, right associativity).