{-# OPTIONS --without-K #-}

module Graph.Func where

open import lib

open import Graph.Decl
open import Graph.Core

open import TT.Func

Π : {Γ : Con} (A : Ty Γ) → Ty (Γ , A) → Ty Γ
Π {Γ} A B = record
  { ⌜_⌝T = λ γ → (α : ⌜ A ⌝T γ) → ⌜ B ⌝T (γ ,Σ α)
  ; _⁼T = λ {γ₀}{γ₁} γ⁼ ⌜f₀⌝ ⌜f₁⌝ →
      {α₀ : ⌜ A ⌝T γ₀}{α₁ : ⌜ A ⌝T γ₁}(α⁼ : (A ⁼T) γ⁼ α₀ α₁) → (B ⁼T) (γ⁼ ,Σ α⁼) (⌜f₀⌝ α₀) (⌜f₁⌝ α₁)
  }

lam : {Γ : Con} {A : Ty Γ} {B : Ty (Γ , A)} → Tm (Γ , A) B → Tm Γ (Π A B)
lam {Γ}{A}{B} t = record
  { ⌜_⌝t = λ γ α → ⌜ t ⌝t (γ ,Σ α)
  ; _⁼t = λ γ⁼ α⁼ → (t ⁼t) (γ⁼ ,Σ α⁼)
  }

app : {Γ : Con} {A : Ty Γ} {B : Ty (Γ , A)} → Tm Γ (Π A B) → Tm (Γ , A) B
app {Γ}{A}{B} t = record
  { ⌜_⌝t = λ { (γ ,Σ α) → ⌜ t ⌝t γ α }
  ; _⁼t = λ { (γ⁼ ,Σ α⁼) → (t ⁼t) γ⁼ α⁼ }
  } 

f : Func c
f = record
  { Π = Π
  ; Π[] = refl
  ; lam = lam
  ; app = app
  ; lam[] = refl
  ; Πβ = refl
  ; Πη = refl
  }
