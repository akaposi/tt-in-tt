{-# OPTIONS --without-K --no-eta --rewriting #-}

module Elim.LogPred where

open import lib

open import StandardModel.FuncSP

open import TT.Decl.Syntax
open import TT.Decl.DepModel

-- this is glueing over the interpretation into the standard
-- model. context:
-- 
--  * LogPred is glueing over the identity model morphism
-- 
--  * NBE.LogPred is glueing over the Yoneda embedding into the
--    presheaf model
-- 
-- this is the indexed family version

open import TT.Syntax
open import TT.FuncSP.Rec fsp

⟦_⟧C : Con → Set₁
⟦_⟧T : ∀{Γ} → Ty Γ → ⟦ Γ ⟧C → Set₁
⟦_⟧s : ∀{Γ Δ} → Tms Γ Δ → ⟦ Γ ⟧C → ⟦ Δ ⟧C
⟦_⟧t : ∀{Γ}{A : Ty Γ} → (t : Tm Γ A) → (γ : ⟦ Γ ⟧C) → ⟦ A ⟧T γ

⟦_⟧C = RecCon
⟦_⟧T = RecTy
⟦_⟧s = RecTms
⟦_⟧t = RecTm

d : Declᴹ
d = record
  { Conᴹ = λ           Γ → ⟦ Γ ⟧C → Set₁
  ; Tyᴹ  = λ {Γ} Γᴾ    A → (γ : ⟦ Γ ⟧C)(_  : Γᴾ γ) → ⟦ A ⟧T γ → Set₁
  ; Tmsᴹ = λ {Γ} Γᴾ Δᴾ σ → (γ : ⟦ Γ ⟧C)(_  : Γᴾ γ) → Δᴾ (⟦ σ ⟧s γ)
  ; Tmᴹ  = λ {Γ} Γᴾ Aᴾ t → (γ : ⟦ Γ ⟧C)(γᴾ : Γᴾ γ) → Aᴾ γ γᴾ (⟦ t ⟧t γ)
  }

open import TT.Core.Syntax
open import TT.Core.DepModel

c : Coreᴹ d
c = record { c1ᴹ = record
  { •ᴹ     = λ _ → Lift ⊤
  ; _,Cᴹ_  = λ { Γᴾ Aᴾ (γ ,Σ a) → Σ (Γᴾ γ) λ γᴾ → Aᴾ γ γᴾ a }
  ; _[_]Tᴹ = λ Aᴹ {σ} σᴾ γ γᴾ a → Aᴹ (⟦ σ ⟧s γ) (σᴾ γ γᴾ) a
  ; εᴹ     = λ _ _ → lift tt
  ; _,sᴹ_  = λ σᴹ tᴹ γ γᴾ → (σᴹ γ γᴾ) ,Σ tᴹ γ γᴾ
  ; idᴹ    = λ γ γᴾ → γᴾ
  ; _∘ᴹ_   = λ σᴾ νᴾ γ γᴾ → σᴾ _ (νᴾ γ γᴾ)
  ; π₁ᴹ    = λ σᴾ γ γᴾ → proj₁ (σᴾ γ γᴾ)
  ; _[_]tᴹ = λ tᴾ σᴾ γ γᴾ → tᴾ _ (σᴾ γ γᴾ)
  ; π₂ᴹ    = λ σᴾ γ γᴾ → proj₂ (σᴾ γ γᴾ)
  } ; c2ᴹ = record
  { [id]Tᴹ = {!!} -- this is essentially: coe (ap something [id]T) (λ x → x) ≡ (λ x → x)
  ; [][]Tᴹ = {!!}
  ; idlᴹ   = {!!}
  ; idrᴹ   = {!!}
  ; assᴹ   = {!!}
  ; π₁βᴹ   = {!!}
  ; πηᴹ    = {!!}
  ; εηᴹ    = {!!}
  ; ,∘ᴹ    = {!!}
  ; π₂βᴹ   = {!!}
  } }

-- TODO: these holes need UIP or that the syntax is a set

-- if the syntax is a set, we might be able to define the
-- interpretation into an inductive-recursive universe as in the case
-- of the standard model

open import TT.Base.Syntax
open import TT.Base.DepModel

b : Baseᴹ c
b = record
  { Uᴹ    = λ γ γᴾ a → a → Set
  ; Elᴹ   = λ { tᴾ γ γᴾ (lift a) → Lift (tᴾ γ γᴾ a) }
  ; U[]ᴹ  = {!!}
  ; El[]ᴹ = {!!}
  }

open import TT.FuncSP.Syntax
open import TT.FuncSP.DepModel

f : FuncSPᴹ b
f = record
  { Πᴹ     = λ {_}{_}{a} aᴾ {B} Bᴾ γ γᴾ f → (x : ⟦ a ⟧t γ)(xᴾ : aᴾ γ γᴾ x) → Bᴾ (γ ,Σ lift x) (γᴾ ,Σ lift xᴾ) (f x)
  ; appᴹ   = λ { tᴾ (γ ,Σ lift a) (γᴾ ,Σ lift aᴾ) → tᴾ γ γᴾ a aᴾ }
  ; lamᴹ   = λ { tᴾ γ γᴾ a aᴾ → tᴾ (γ ,Σ lift a) (γᴾ ,Σ lift aᴾ) }
  ; Π[]ᴹ   = {!!}
  ; lam[]ᴹ = {!!}
  ; Πβᴹ    = {!!}
  ; Πηᴹ    = {!!}
  }
