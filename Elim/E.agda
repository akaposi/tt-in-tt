{-# OPTIONS --rewriting #-}

module Elim.E where

open import lib
open import JM

open import TT.Decl.Syntax
open import TT.Core.Syntax

open import TT.Decl.DepModel
open import TT.Core.DepModel

open import TT.Decl.Congr.NoJM syntaxDecl
open import TT.Core.Congr.NoJM syntaxCore

open import StandardModel.FuncSP
open import StandardModel.FuncPar

open import Elim.P

private
  dᴹ : Declᴹ
  dᴹ = record
    { Conᴹ = λ Δ               → (γ : ⌜ Δ ⌝C) → (Δ ᴾC) γ → Set
    ; Tyᴹ  = λ {Δ} Δᴱ A        → (γ : ⌜ Δ ⌝C)(γᴾ : (Δ ᴾC) γ) → Δᴱ γ γᴾ → (α : ⌜ A ⌝T γ) → (A ᴾT) γ γᴾ α → Set
    ; Tmsᴹ = λ {Δ}{Θ} Δᴱ Θᴱ σ  → (γ : ⌜ Δ ⌝C)(γᴾ : (Δ ᴾC) γ) → Δᴱ γ γᴾ → Θᴱ (⌜ σ ⌝s γ) ((σ ᴾs) γ γᴾ)
    ; Tmᴹ  = λ {Δ} Δᴱ {A} Aᴱ t → (γ : ⌜ Δ ⌝C)(γᴾ : (Δ ᴾC) γ)(γᴱ : Δᴱ γ γᴾ) → Aᴱ γ γᴾ γᴱ (⌜ t ⌝t γ)((t ᴾt) γ γᴾ)
    }

  open Declᴹ dᴹ

  c1ᴹ : Core1ᴹ dᴹ
  c1ᴹ = record
    { •ᴹ     = λ _ _ → ⊤
    ; _,Cᴹ_  = λ { Γᴱ Aᴱ (γ ,Σ α) (γᴾ ,Σ αᴾ) → Σ (Γᴱ γ γᴾ) λ γᴱ → Aᴱ γ γᴾ γᴱ α αᴾ }
    ; _[_]Tᴹ = λ Aᴱ σᴱ γ γᴾ γᴱ α αᴾ → Aᴱ _ _ (σᴱ γ γᴾ γᴱ) α αᴾ
    ; εᴹ     = λ _ _ _ → tt
    ; _,sᴹ_  = λ σᴱ tᴱ γ γᴾ γᴱ → σᴱ γ γᴾ γᴱ ,Σ tᴱ γ γᴾ γᴱ
    ; idᴹ    = λ γ γᴾ γᴱ → γᴱ
    ; _∘ᴹ_   = λ σᴱ νᴱ γ γᴾ γᴱ → σᴱ _ _ (νᴱ γ γᴾ γᴱ)
    ; π₁ᴹ    = λ σ γ γᴾ γᴱ → proj₁ (σ γ γᴾ γᴱ)
    ; _[_]tᴹ = λ tᴱ σᴱ γ γᴾ γᴱ → tᴱ _ _ (σᴱ γ γᴾ γᴱ)
    ; π₂ᴹ    = λ σ γ γᴾ γᴱ → proj₂ (σ γ γᴾ γᴱ)
    }

  open Core1ᴹ c1ᴹ

  [][]Tᴾ
    : {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}
      {Σ : Con}{Σᴹ : Conᴹ Σ}{A : Ty Σ}{Aᴹ : Tyᴹ Σᴹ A}
      {σ : Tms Γ Δ}{σᴹ : Tmsᴹ Γᴹ Δᴹ σ}{δ : Tms Δ Σ}{δᴹ : Tmsᴹ Δᴹ Σᴹ δ}
    →  _[_]Tᴹ {A = A [ δ ]T} (_[_]Tᴹ {A = A} Aᴹ {δ} δᴹ) {σ} σᴹ
    ≡[ TyΓᴹ= {Γ}{Γᴹ}{A [ δ ]T [ σ ]T}{A [ δ ∘ σ ]T} [][]T ]≡
       _[_]Tᴹ {A = A} Aᴹ {δ ∘ σ}(_∘ᴹ_ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ} {σ = δ} δᴹ  {σ} σᴹ)
  [][]Tᴾ = coe= {p₀ = TyΓᴹ= [][]T}{refl} (UIP _ _)

  c2ᴹ : Core2ᴹ dᴹ c1ᴹ
  c2ᴹ = record
    { [id]Tᴹ = coe= {p₀ = TyΓᴹ= [id]T}{refl}(UIP _ _)
    ; [][]Tᴹ = λ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ}{A}{Aᴹ}{σ}{σᴹ}{δ}{δᴹ} → [][]Tᴾ {Γ}{Γᴹ}{Δ}{Δᴹ}{Σ}{Σᴹ}{A}{Aᴹ}{σ}{σᴹ}{δ}{δᴹ}
    ; idlᴹ   = coe= {p₀ = TmsΓΔᴹ= idl}{refl}(UIP _ _)
    ; idrᴹ   = coe= {p₀ = TmsΓΔᴹ= idr}{refl}(UIP _ _)
    ; assᴹ   = coe= {p₀ = TmsΓΔᴹ= ass}{refl}(UIP _ _)
    ; π₁βᴹ   = coe= {p₀ = TmsΓΔᴹ= π₁β}{refl}(UIP _ _)
    ; πηᴹ    = coe= {p₀ = TmsΓΔᴹ= πη}{refl}(UIP _ _)
    ; εηᴹ    = coe= {p₀ = TmsΓΔᴹ= εη}{refl}(UIP _ _)
    ; ,∘ᴹ    = {!!}
    ; π₂βᴹ   = coe= {p₀ = TmΓᴹ= (A[]T= π₁β) ([]Tᴹ=′ dᴹ c1ᴹ refl refl refl refl refl refl π₁β (coe= (UIP (TmsΓΔᴹ= π₁β) refl))) π₂β}
                    {refl}(UIP _ _)
    }


open import TT.Decl.Elim dᴹ

_ᴱC : (Δ : Con)            → (γ : ⌜ Δ ⌝C) → (Δ ᴾC) γ → Set
_ᴱT : ∀{Δ}(A : Ty Δ)       → (γ : ⌜ Δ ⌝C)(γᴾ : (Δ ᴾC) γ) → (Δ ᴱC) γ γᴾ → (α : ⌜ A ⌝T γ) → (A ᴾT) γ γᴾ α → Set
_ᴱs : ∀{Δ}{Θ}(σ : Tms Δ Θ) → (γ : ⌜ Δ ⌝C)(γᴾ : (Δ ᴾC) γ) → (Δ ᴱC) γ γᴾ → (Θ ᴱC) (⌜ σ ⌝s γ) ((σ ᴾs) γ γᴾ)
_ᴱt : ∀{Δ}{A}(t : Tm Δ A)  → (γ : ⌜ Δ ⌝C)(γᴾ : (Δ ᴾC) γ)(γᴱ : (Δ ᴱC) γ γᴾ) → (A ᴱT) γ γᴾ γᴱ (⌜ t ⌝t γ)((t ᴾt) γ γᴾ)

_ᴱC = ElimCon
_ᴱT = ElimTy
_ᴱs = ElimTms
_ᴱt = ElimTm
