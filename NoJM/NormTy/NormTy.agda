{-# OPTIONS --without-K --no-eta --rewriting #-}

-- normalisation of types

module NoJM.NormTy.NormTy where

open import lib using (⊤; refl)

open import NoJM.TT as M using ()
open import NoJM.Syntax
open import NoJM.NormTy.NTy

----------------------------------------------------------------------
-- Substitution of normal types
----------------------------------------------------------------------

-- we define it mutually with a naturality law

_[_]NT : ∀{Δ}(A : NTy Δ){Γ : Con}(σ : Tms Γ Δ) → NTy Γ

⌜[]⌝T : ∀{Δ}{A : NTy Δ}
        {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
        {σ₀ : Tms Γ₀ Δ}{σ₁ : Tms Γ₁ Δ}(σ₂ : Tms= Γ₂ refl σ₀ σ₁)
      → Ty= Γ₂ (⌜ A ⌝T [ σ₀ ]T) ⌜ A [ σ₁ ]NT ⌝T

NΠ A B [ σ ]NT
  = NΠ (A [ σ ]NT)
       (B [ coeTms (,= refl (⌜[]⌝T refl refl)) refl (σ ^ ⌜ A ⌝T) ]NT)
NU [ σ ]NT = NU
NEl Â [ σ ]NT = NEl (coeTm refl U[] (Â [ σ ]t))

⌜[]⌝T {A = NΠ A B} refl refl
  = coeTy= refl refl refl refl
           (Π= (⌜[]⌝T {A = A} refl refl)
               (⌜[]⌝T {A = B}
                      (,= refl (⌜[]⌝T refl refl))
                      (cohTms (,= refl (⌜[]⌝T refl refl)) refl)))
           Π[]
⌜[]⌝T {A = NU} refl refl = U[] -- TODO: are we allowed to use J here?
⌜[]⌝T {A = NEl Â} refl refl = El[] -- and here?

-- functoriality laws

[]NT= : {Δ₀ Δ₁ : Con}(Δ₂ : Con= Δ₀ Δ₁)
        {A₀ : NTy Δ₀}{A₁ : NTy Δ₁}(A₂ : NTy= Δ₂ A₀ A₁)
        {Γ₀ Γ₁ : Con}(Γ₂ : Con= Γ₀ Γ₁)
        {σ₀ : Tms Γ₀ Δ₀}{σ₁ : Tms Γ₁ Δ₁}(σ₂ : Tms= Γ₂ Δ₂ σ₀ σ₁)
      → NTy= Γ₂ (A₀ [ σ₀ ]NT) (A₁ [ σ₁ ]NT)
[]NT= refl refl refl refl = refl

[id]NT : {Γ : Con}{A : NTy Γ} → NTy= refl (A [ id ]NT) A
[id]NT {A = NΠ A B}
  = NΠ= refl
        ([id]NT {A = A})
        {![]NT= !}
[id]NT {A = NU} = refl
[id]NT {A = NEl Â} = NEl= refl {!!} -- TODO: we need [id]t

-- a model with normal types

d : M.Decl
d = record
  { Con = Con
  ; Ty  = NTy
  ; Tms = Tms
  ; Tm  = λ Γ A → Tm Γ ⌜ A ⌝T
  }

c : M.Core d
c = record
  { • = •
  ; _,_ = λ Γ A → Γ , ⌜ A ⌝T
  ; _[_]T = λ {Γ}{Δ} A σ → _[_]NT {Δ} A {Γ} σ
  ; id = id
  ; _∘_ = _∘_
  ; ε = ε
  ; _,s_ = λ {Γ}{Δ} σ {A} t → σ ,s coeTm refl {!!} t
  ; π₁ = π₁
  ; _[_]t = {!!}
  ; π₂ = {!!}
  ; [id]T = [id]NT
  ; [][]T = {!!}
  ; idl = idl
  ; idr = idr
  ; ass = ass
  ; ,∘ = {!!}
  ; π₁β = {!!}
  ; πη = {!!}
  ; εη = {!!}
  ; π₂β = {!!}
  }
