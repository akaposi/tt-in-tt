{-# OPTIONS --without-K --no-eta --rewriting #-}

module NoJM.Syntax where

open import Agda.Primitive
open import lib using (_≡_; refl; idfun)

open import NoJM.TT as M using ()

----------------------------------------------------------------------
-- The syntax
----------------------------------------------------------------------

postulate
  syntaxDecl : M.Decl {lzero}{lzero}
  syntaxCore : M.Core syntaxDecl
  syntaxBase : M.Base syntaxDecl syntaxCore
  syntaxFunc : M.Func syntaxDecl syntaxCore
  
open M.Decl syntaxDecl public
open M.Core syntaxCore public
open M.Base syntaxBase public
open M.Func syntaxFunc public

----------------------------------------------------------------------
-- The recursor
----------------------------------------------------------------------

module _ {i}{j}
  (d : M.Decl {i}{j})
  (c : M.Core d)
  (b : M.Base d c)
  (f : M.Func d c)
  where

----------------------------------------------------------------------
-- Declaration
----------------------------------------------------------------------

  postulate
    recCon : Con → M.Decl.Con d
    recTy  : ∀{Γ} → Ty Γ → M.Decl.Ty d (recCon Γ)
    recTms : ∀{Γ Δ} → Tms Γ Δ → M.Decl.Tms d (recCon Γ) (recCon Δ)
    recTm  : ∀{Γ A} → Tm Γ A → M.Decl.Tm d (recCon Γ) (recTy A)

----------------------------------------------------------------------
-- Core substitution calculus
----------------------------------------------------------------------

  postulate
    β• : M.Decl.Con= d (recCon •) (M.Core.• c)
    β, : ∀{Γ A} → M.Decl.Con= d (recCon (Γ , A)) (M.Core._,_ c (recCon Γ) (recTy A))

  {-# REWRITE β• #-}
  {-# REWRITE β, #-}
  
  postulate
    β[]T   : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ Δ}
           → M.Decl.Ty= d refl (recTy (A [ σ ]T)) (M.Core._[_]T c (recTy A) (recTms σ))

  {-# REWRITE β[]T #-}

  postulate
    βid : ∀{Γ} → M.Decl.Tms= d refl refl (recTms (id {Γ})) (M.Core.id c)
    β∘  : ∀{Γ Δ Σ}{σ : Tms Δ Σ}{ν : Tms Γ Δ}
        → M.Decl.Tms= d refl refl (recTms (σ ∘ ν)) (M.Core._∘_ c (recTms σ) (recTms ν))
    βε  : ∀{Γ} → M.Decl.Tms= d refl refl (recTms (ε {Γ})) (M.Core.ε c)
    β,s : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{t : Tm Γ (A [ σ ]T)}
        → M.Decl.Tms= d refl refl (recTms (σ ,s t)) (M.Core._,s_ c (recTms σ) (recTm t))
    βπ₁ : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}
        → M.Decl.Tms= d refl refl (recTms (π₁ σ)) (M.Core.π₁ c (recTms σ))

  {-# REWRITE βid #-}
  {-# REWRITE β∘  #-}
  {-# REWRITE βε  #-}
  {-# REWRITE β,s #-}
  {-# REWRITE βπ₁ #-}

  postulate
    β[]t : ∀{Γ Δ}{A : Ty Δ}{t : Tm Δ A}{σ : Tms Γ Δ}
         → M.Decl.Tm= d refl refl (recTm (t [ σ ]t)) (M.Core._[_]t c (recTm t) (recTms σ))
    βπ₂  : ∀{Γ Δ}{A : Ty Δ}{σ : Tms Γ (Δ , A)}
         → M.Decl.Tm= d refl refl (recTm (π₂ σ)) (M.Core.π₂ c (recTms σ))

  {-# REWRITE β[]t #-}
  {-# REWRITE βπ₂  #-}

  -- TODO: computation rules for equalities

----------------------------------------------------------------------
-- Base type and family
----------------------------------------------------------------------

  postulate
    βU : ∀{Γ} → M.Decl.Ty= d refl (recTy (U {Γ})) (M.Base.U b)
    
  {-# REWRITE βU #-}

  postulate
    βEl : ∀{Γ}{Â : Tm Γ U}
        → M.Decl.Ty= d refl (recTy (El Â)) (M.Base.El b (recTm Â))
        
  {-# REWRITE βEl #-}
  
  -- TODO: computation rules for equalities

----------------------------------------------------------------------
-- Function space
----------------------------------------------------------------------

  postulate
    βΠ : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}
       → M.Decl.Ty= d refl (recTy (Π A B)) (M.Func.Π f (recTy A) (recTy B))

  {-# REWRITE βΠ #-}

  postulate
    βlam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B}
         → M.Decl.Tm= d refl refl (recTm (lam t)) (M.Func.lam f (recTm t))
    βapp : ∀{Γ}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)}
         → M.Decl.Tm= d refl refl (recTm (app t)) (M.Func.app f (recTm t))

  {-# REWRITE βlam #-}
  {-# REWRITE βapp #-}

  -- TODO: computation rules for equalities
