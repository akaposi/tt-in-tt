{-# OPTIONS --without-K --no-eta --rewriting #-}

-- consistency

module NoJM.Cons where

open import lib using (refl; ⊥)
open import NoJM.Syntax
open import NoJM.Nf
open import NoJM.Disj

mVar : MVar
mVar = record
  { Varᴹ = λ Γ _ _ → Con= Γ • → ⊥
  ; vzeᴹ = disj,•
  ; vsuᴹ = λ _ → disj,•
  }

mNeNf : MNeNf
mNeNf = record
  { Neᴹ    = λ Γ _ _ → Con= Γ • → ⊥
  ; Nfᴹ    = λ Γ A _ → Con= Γ • → Ty= refl A U → ⊥
  ; varᴹ   = elimVar mVar
  ; appNeᴹ = λ nᴹ _ → nᴹ
  ; neuUᴹ  = λ nᴹ p _ → nᴹ p
  ; neuElᴹ = λ nᴹ p _ → nᴹ p
  ; lamNfᴹ = λ _ _ → disjΠU
  }

consNf : Nf • U → ⊥
consNf v = elimNf mNeNf v refl refl
