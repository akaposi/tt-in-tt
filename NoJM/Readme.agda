{-# OPTIONS --without-K --no-eta --rewriting #-}

module NoJM.Readme where

import NoJM.TT
-- definition of type theory as records for the core substitution
-- calculus, the base type and family and function space

import NoJM.Syntax
-- postulating the syntax (initial model) and the recursor using
-- rewrite rules

import NoJM.Disj
-- proving that (Γ , A) ≠ • and Π A B ≠ U simply using the recursor

import NoJM.Nf
-- definition of the inductive types of variables, normal forms and
-- neutral terms (indexed by contexts and types) together with their
-- (dependent) elimination principles

import NoJM.Cons
-- the proof that the theory given in Syntax is consistent (Nf • U → ⊥): by
-- mutual induction on variables, neutral terms and normal forms.

import NoJM.NormTy.NTy
-- definition of normal types (indexed by contexts and referring to
-- terms) together with the (dependent) elimination principle

-- import NoJM.NormTy.NormTy -- a model of normal types (not yet
-- finished)
