{-# OPTIONS --without-K #-}

module Yoneda where

open import lib

open import TT.Syntax

open import TT.Decl.Morphism
open import TT.Core.Morphism

open import StandardModel.Decl
open import StandardModel.Core

open import TT.Core.Laws.JM syntaxCore

dᴱ : Declᴱ syntaxDecl d
dᴱ = record
  { Conᴱ = λ Γ → Tms • Γ
  ; Tyᴱ  = λ {Γ} A σ → Tm • (A [ σ ]T)
  ; Tmsᴱ = λ {Γ}{Δ} δ σ → δ ∘ σ
  ; Tmᴱ  = λ {Γ}{A} t σ → t [ σ ]t
  }

cᴱ : Coreᴱ syntaxCore c dᴱ
cᴱ = record
  { •ᴱ   = {!!}
  ; ,ᴱ   = {!!}
  ; []Tᴱ = λ {Γ}{Δ}{A}{σ} → funext λ ν → ap (Tm •) [][]T
  ; idᴱ  = λ {Γ} → funext λ ν → idl
  ; ∘ᴱ   = λ {Γ}{Δ}{Σ}{σ}{δ} → funext λ ν → ass
  ; εᴱ   = {!!}
  ; ,sᴱ  = {!!}
  ; π₁ᴱ  = {!!}
  ; []tᴱ = λ {Γ}{Σ}{A}{t}{σ} → funext λ ν → {!!} ◾ [][]t
  ; π₂ᴱ  = {!!}
  }
