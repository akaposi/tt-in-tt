{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.LogPred
  (U̅  : Ty (• , U))
  (El̅ : Ty (• , U , El (coe (TmΓ= U[]) vz) , U̅ [ wk ]T))
  where

-- the logical predicate model

open import LogPred.Decl       -- declaration
open import LogPred.Core       -- subsitution calculus
open import LogPred.Base U̅ El̅  -- a base type and base family
open import LogPred.Func       -- dependent function space

open import TT.Elim

open elim d c b f

-- the logical predicate (unary logical relation) interpretation of
-- Bernardy et al

_ᴾᶜ : Con → Con
_ᴾˢ : ∀{Γ Δ} → Tms Γ Δ → Tms (Γ ᴾᶜ) (Δ ᴾᶜ)
pr  : (Γ : Con) → Tms (Γ ᴾᶜ) Γ
_ᴾᵀ : ∀{Γ}(A : Ty Γ) → Ty ((Γ ᴾᶜ) , A [ pr Γ ]T)
_ᴾᵗ : ∀{Γ}{A : Ty Γ}(t : Tm Γ A) → Tm (Γ ᴾᶜ) ((A ᴾᵀ) [ < t [ pr Γ ]t > ]T)

Γ ᴾᶜ = ∣ ElimCon Γ ∣C
ρ ᴾˢ = ∣ ElimTms ρ ∣s
pr Γ = Pr (ElimCon Γ)
A ᴾᵀ = ElimTy A
t ᴾᵗ = ElimTm t
