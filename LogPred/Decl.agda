{-# OPTIONS --no-eta #-}

module LogPred.Decl where

open import lib
open import TT.Syntax
open import TT.DepModel

record Conᴹ (Γ : Con) : Set where
  constructor cConᴹ
  field
    ∣_∣C : Con
    Pr : Tms ∣_∣C Γ

  infix 7 ∣_∣C

open Conᴹ public

Tyᴹ : {Γ : Con} → Conᴹ Γ → Ty Γ → Set
Tyᴹ {Γ} Γᴹ A = Ty (∣ Γᴹ ∣C , A [ Pr Γᴹ ]T)

record Tmsᴹ {Γ Δ : Con}(Γᴹ : Conᴹ Γ)(Δᴹ : Conᴹ Δ)(ρ : Tms Γ Δ) : Set where
  constructor cTmsᴹ
  field
    ∣_∣s    : Tms ∣ Γᴹ ∣C ∣ Δᴹ ∣C
    PrNat : (Pr Δᴹ) ∘ ∣_∣s ≡ ρ ∘ (Pr Γᴹ)

  infix 7 ∣_∣s

open Tmsᴹ public

Tmᴹ : {Γ : Con}(Γᴹ : Conᴹ Γ){A : Ty Γ}(Aᴾ : Tyᴹ Γᴹ A) → Tm Γ A → Set
Tmᴹ {Γ} Γᴹ {A} Aᴾ a = Tm ∣ Γᴹ ∣C (Aᴾ [ < a [ Pr Γᴹ ]t > ]T)

d : Declᴹ
d = record
  { Conᴹ = Conᴹ
  ; Tyᴹ  = Tyᴹ
  ; Tmsᴹ = Tmsᴹ
  ; Tmᴹ  = Tmᴹ
  }
