{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

open import LogPred.Func.Ty
open import LogPred.Func.Tm

open import LogPred.Func.Ty.Proofs6
open import LogPred.Func.Tm.Proofs1
open import LogPred.Func.Tm.Proofs3

f : Funcᴹ d c
f = record
  { Πᴹ = λ {Γ}{Γᴹ}{A} Aᴹ {B} Bᴹ → Πᴹ {Γ}{Γᴹ}{A} Aᴹ {B} Bᴹ
  ; appᴹ = λ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t} tᴹ → appᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t} tᴹ
  ; lamᴹ = λ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t} tᴹ → lamᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t} tᴹ
  ; Π[]ᴹ = λ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ} → Π[]ᴹ.ret {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ}
  ; lam[]ᴹ = λ { {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ}
             → let
                 open LogPred.Func.Tm.Proofs1.lam[]ᴹ' {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ}
               in
                 from≃ ( uncoe (Declᴹ.TmΓᴹ= d
                                  {Γ}{Γᴹ}
                                  {Π A B [ δ ]T}{Π (A [ δ ]T) (B [ δ ^ A ]T)} Π[]
                                  {Πᴹ {Δ}{Δᴹ}{A} Aᴹ {B} Bᴹ [ δᴹ ]Tᴹ}
                                  {Πᴹ {Γ}{Γᴹ}{A [ δ ]T}(Aᴹ [ δᴹ ]Tᴹ){B [ δ ^ A ]T} (Bᴹ [ Coreᴹ._^ᴹ_ c δᴹ Aᴹ ]Tᴹ)}
                                  (Π[]ᴹ.ret {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ})
                                  {lam t [ δ ]t}{lam (t [ δ ^ A ]t)} lam[]) ⁻¹̃
                       ◾̃ ret)
             }
  ; Πβᴹ = λ { {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ}
          → from≃ ( uncoe (Declᴹ.TmΓAᴹ= d Πβ) ⁻¹̃
                  ◾̃ applam {t = tᴹ}
                           (plamᴹ {Γᴹ = Γᴹ})
                           Π[]
                           Π[]
                           (ap (λ z → Bᴹ [ < z [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t > ]T) (Πβ ⁻¹))
                           (pappᴹ {Γᴹ = Γᴹ})) }
  
  ; Πηᴹ = λ { {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ}
          → let
              open eqΓABᴹ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}
            in
              from≃ ( uncoe (Declᴹ.TmΓAᴹ= d Πη) ⁻¹̃
                    ◾̃ lamapp {t = tᴹ}
                             Π[]
                             Π[]
                             eqΓAPrAᴹ
                             eqBᴹ
                             (plamᴹ {Γᴹ = Γᴹ})) }
  }
