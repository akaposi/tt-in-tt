{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import TT.Congr

module LogPred.Func.Ty.Proofs2 where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.DepModel

open import LogPred.Decl
open import LogPred.Core

module Π[]ᴹ
  {Γ : Con}{Γᴹ : Conᴹ Γ}{Δ : Con}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}
  {δᴹ : Tmsᴹ Γᴹ Δᴹ δ}{A : Ty Δ}{Aᴹ : Tyᴹ Δᴹ A}
  where

  _^ᴹ_ : ∀{Γ}{Γᴹ : Conᴹ Γ}{Δ}{Δᴹ : Conᴹ Δ}{δ : Tms Γ Δ}(δᴹ : Tmsᴹ Γᴹ Δᴹ δ)
         {A : Ty Δ}(Aᴹ : Tyᴹ Δᴹ A) → Tmsᴹ (Γᴹ ,Cᴹ (Aᴹ [ δᴹ ]Tᴹ)) (Δᴹ ,Cᴹ Aᴹ) (δ ^ A)
  _^ᴹ_ = λ δᴹ Aᴹ → (δᴹ ∘ᴹ π₁ᴹ idᴹ) ,sᴹ coe (Declᴹ.TmΓᴹ= d [][]T [][]Tᴹ refl) (π₂ᴹ idᴹ)

  abstract
    π₂Pr : ∀{Γ}{Γᴹ : Conᴹ Γ}{A : Ty Γ}{Aᴹ : Tyᴹ Γᴹ A}
         → π₂ id [ Pr (Γᴹ ,Cᴹ Aᴹ) ]t ≃ π₂ {A = A [ Pr Γᴹ ]T} id [ π₁ {A = Aᴹ} id ]t
    π₂Pr {Γ}{Γᴹ}{A}{Aᴹ} = π₂[]'
                        ◾̃ π₂≃ (idl ◾ ,∘)
                        ◾̃ π₂β'
                        ◾̃ coecoe[]t [][]T [][]T

  abstract
    eqA : A [ δ ]T [ Pr Γᴹ ]T ≃ A [ Pr Δᴹ ]T [ ∣ δᴹ ∣s ]T
    eqA = to≃ (TyNat δᴹ ⁻¹)
    
  abstract
    eqΓA : (∣ Γᴹ ∣C , (A [ δ ]T) [ Pr Γᴹ ]T) ≡ (∣ Γᴹ ∣C , (A [ Pr Δᴹ ]T) [ ∣ δᴹ ∣s ]T)
    eqΓA = ,C≃ refl eqA
    
  abstract
    eqAᴹ : Aᴹ [ δᴹ ]Tᴹ ≃ Aᴹ [ ∣ δᴹ ∣s ^ A [ Pr Δᴹ ]T ]T
    eqAᴹ = []T≃ eqΓA (uncoe (Tms-Γ= (,C≃ refl (to≃ (TyNat δᴹ)))) ⁻¹̃)
    
  abstract
    eqΓAAᴹ : ∣ Γᴹ ∣C , A [ δ ]T [ Pr Γᴹ ]T     , Aᴹ [ δᴹ ]Tᴹ
           ≡ ∣ Γᴹ ∣C , A [ Pr Δᴹ ]T [ ∣ δᴹ ∣s ]T , Aᴹ [ ∣ δᴹ ∣s ^ A [ Pr Δᴹ ]T ]T
    eqΓAAᴹ = ,C≃ eqΓA eqAᴹ

  abstract
    =s^ : ∣ δᴹ ^ᴹ Aᴹ ∣s ≃ ∣ δᴹ ∣s ^ A [ Pr Δᴹ ]T ^ Aᴹ
    =s^ = ,s≃ eqΓAAᴹ
             refl
             ( ,s≃ eqΓAAᴹ
                   refl
                   ( ∘≃ eqΓAAᴹ
                        ( π₁π₁id≃ refl eqA eqAᴹ
                        ◾̃ to≃ (ap π₁ idl ⁻¹ ◾ π₁∘ ⁻¹))
                   ◾̃ to≃ (ass ⁻¹))
                   r̃
                   ( coecoe[]t [][]T (TyNat (δᴹ ∘ᴹ π₁ᴹ idᴹ) ⁻¹)
                   ◾̃ π₂Pr {Γᴹ = Γᴹ}
                   ◾̃ π₂[]'
                   ◾̃ π₂≃' eqΓAAᴹ
                          refl
                          eqA
                          ( to≃ idl
                          ◾̃ π₁id≃ eqΓA eqAᴹ
                          ◾̃ to≃ (idl ⁻¹))
                   ◾̃ π₂[]' ⁻¹̃
                   ◾̃ coecoe[]t [][]T [][]T ⁻¹̃)
             ◾̃ to≃ (,∘ ⁻¹))
             r̃
             (uncoe (TmΓ= (p,sᴹ _)) ⁻¹̃
             ◾̃ uncoe (Declᴹ.TmΓᴹ= d [][]T [][]Tᴹ refl) ⁻¹̃
             ◾̃ uncoe (TmΓ= (pπ₂ᴹ _ _)) ⁻¹̃
             ◾̃ π₂id≃ eqΓA eqAᴹ
             ◾̃ uncoe (TmΓ= [][]T))
