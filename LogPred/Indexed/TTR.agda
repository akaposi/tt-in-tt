{-# OPTIONS --rewriting #-}

module LogPred.Indexed.TTR where

open import lib using (_≡_; refl; _⁻¹; _◾_; ap; apd)
open import TTR

-- specification of the ᴾ operation

postulate
  _ᴾC   : (Γ : Con){Ω : Con}(ρ : Tms Ω Γ) → Ty Ω
  _ᴾ[]C : (Γ : Con){Ω : Con}{ρ : Tms Ω Γ}{Ψ : Con}{τ : Tms Ψ Ω} → (Γ ᴾC) ρ [ τ ]T ≡ (Γ ᴾC) (ρ ∘ τ)

{-# REWRITE _ᴾ[]C #-}

postulate
  _ᴾT   : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ : Tms Ω Γ}(ρ̂ : Tm Ω ((Γ ᴾC) ρ))(t : Tm Ω (A [ ρ ]T)) → Ty Ω
  _ᴾ[]T : {Γ : Con}(A : Ty Γ){Ω : Con}{ρ : Tms Ω Γ}{ρ̂ : Tm Ω ((Γ ᴾC) ρ)}{t : Tm Ω (A [ ρ ]T)}
      {Ψ : Con}{τ : Tms Ψ Ω} → (A ᴾT) ρ̂ t [ τ ]T ≡ (A ᴾT) (ρ̂ [ τ ]t) (t [ τ ]t)

{-# REWRITE _ᴾ[]T #-}

postulate
  _ᴾs   : {Γ Δ : Con}(σ : Tms Γ Δ){Ω : Con}{ρ : Tms Ω Γ}(ρ̂ : Tm Ω ((Γ ᴾC) ρ)) → Tm Ω ((Δ ᴾC) (σ ∘ ρ))
  _ᴾ[]s : {Γ Δ : Con}{σ : Tms Γ Δ}{Ω : Con}{ρ : Tms Ω Γ}{ρ̂ : Tm Ω ((Γ ᴾC) ρ)}{Ψ : Con}{τ : Tms Ψ Ω} →
    (σ ᴾs) ρ̂ [ τ ]t ≡ (σ ᴾs) (ρ̂ [ τ ]t)

{-# REWRITE _ᴾ[]s #-}

postulate
  _ᴾt   : {Γ : Con}{A : Ty Γ}(t : Tm Γ A){Ω : Con}{ρ : Tms Ω Γ}(ρ̂ : Tm Ω ((Γ ᴾC) ρ)) → Tm Ω ((A ᴾT) ρ̂ (t [ ρ ]t))
  _ᴾ[]t : {Γ : Con}{A : Ty Γ}(t : Tm Γ A){Ω : Con}{ρ : Tms Ω Γ}{ρ̂ : Tm Ω ((Γ ᴾC) ρ)}{Ψ : Con}{τ : Tms Ψ Ω} →
    (t ᴾt) ρ̂ [ τ ]t ≡ (t ᴾt) (ρ̂ [ τ ]t)

{-# REWRITE _ᴾ[]t #-}

-- implementation for Con

postulate
  •ᴾ : {Ω : Con}{ρ : Tms Ω •} → (• ᴾC) ρ ≡ ⊤

{-# REWRITE •ᴾ #-}

•ᴾ[] : {Ω : Con}{ρ : Tms Ω •}{Ψ : Con}{τ : Tms Ψ Ω} → (• ᴾC) ρ [ τ ]T ≡ (• ᴾC) (ρ ∘ τ)
•ᴾ[] = refl

postulate
  _▷_ᴾ : (Γ : Con)(A : Ty Γ){Ω : Con}{ρ : Tms Ω (Γ ▷ A)} →
    ((Γ ▷ A) ᴾC) ρ ≡ Σ ((Γ ᴾC) (π₁ ρ)) ((A ᴾT) vz (π₂ ρ [ wk ]t))

{-# REWRITE _▷_ᴾ #-}

_▷_ᴾ[] : (Γ : Con)(A : Ty Γ){Ω : Con}{ρ : Tms Ω (Γ ▷ A)}{Ψ : Con}{τ : Tms Ψ Ω} →
  ((Γ ▷ A) ᴾC) ρ [ τ ]T ≡ ((Γ ▷ A) ᴾC) (ρ ∘ τ)
_▷_ᴾ[] Γ A {Ω}{ρ}{Ψ}{τ} = ap (λ z → Σ ((Γ ᴾC) (π₁ (ρ ∘ τ))) ((A ᴾT) z (π₂ (ρ ∘ (τ ∘ π₁ id)))))
                             (vz[_∘wk,vz] {B = (Γ ᴾC) {Ω} (π₁ {Ω} {Γ} {A} ρ)})

-- implementation for Ty

-- _[_]T

postulate
  _[_]Tᴾ : {Γ Δ : Con}(A : Ty Δ)(σ : Tms Γ Δ){Ω : Con}
    {ρ : Tms Ω Γ}(ρ̂ : Tm Ω ((Γ ᴾC) ρ))(t : Tm Ω (A [ σ ]T [ ρ ]T)) →
    ((A [ σ ]T) ᴾT) ρ̂ t ≡ (A ᴾT) ((σ ᴾs) ρ̂) t

{-# REWRITE _[_]Tᴾ #-}

-- implementation for Tms

postulate
  idᴾ : {Γ : Con}{Ω : Con}{ρ : Tms Ω Γ}(ρ̂ : Tm Ω ((Γ ᴾC) ρ)) →
    (id {Γ} ᴾs) ρ̂ ≡ ρ̂

  _∘_ᴾ : ∀{Γ Δ Σ}(σ : Tms Δ Σ)(δ : Tms Γ Δ){Ω : Con}{ρ : Tms Ω Γ}(ρ̂ : Tm Ω ((Γ ᴾC) ρ)) →
    ((σ ∘ δ) ᴾs) ρ̂ ≡ (σ ᴾs) ((δ ᴾs) ρ̂)

-- ε, _,_, π₁


{-# REWRITE idᴾ #-}
{-# REWRITE _∘_ᴾ #-}

-- implementation for Tm

-- π₂, _[_]t
