{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.Weaken where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars
open import NBE.Renamings.Proj
open import NBE.Renamings.Comp

-- weakening

wkV   : ∀{Γ Δ A} → Vars Γ Δ → Vars (Γ , A) Δ
⌜wkV⌝ : ∀{Γ Δ A}(β : Vars Γ Δ) → ⌜ β ⌝V ∘ wk {A = A} ≡ ⌜ wkV β ⌝V

wkV εV = εV
wkV (β ,V x) = wkV β ,V coe (VarΓ= ([][]T ◾ ap (_[_]T _) (⌜wkV⌝ β))) (vsu x)

abstract
  ⌜wkV⌝ εV = εη
  ⌜wkV⌝ (β ,V x) = ,∘
                 ◾ ,s≃' (⌜wkV⌝ β)
                        ( uncoe (TmΓ= [][]T) ⁻¹̃
                        ◾̃ ⌜coe⌝v ([][]T ◾ ap (_[_]T _) (⌜wkV⌝ β)) ⁻¹̃)

-- congruence

wkV≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≃ β₁)
       {A₀ : Ty Γ₀}{A₁ : Ty Γ₁}(A₂ : A₀ ≃ A₁)
     → wkV {A = A₀} β₀ ≃ wkV {A = A₁} β₁
wkV≃ refl refl (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

-- a law

abstract
  wkVβ : ∀{Θ Γ Δ A}{β : Vars Γ Δ}{γ : Vars Θ Γ}{x : Var Θ (A [ ⌜ γ ⌝V ]T)}
       → wkV β ∘V (γ ,V x) ≡ β ∘V γ
  wkVβ {β = εV} = refl
  wkVβ {β = β ,V x}{γ}{y}
  
    = ,V≃' (wkVβ {β = β})
           ( uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) (⌜∘V⌝ (wkV β)))) ⁻¹̃
           ◾̃ coe[]v' ([][]T ◾ ap (_[_]T _) (⌜wkV⌝ β))
           ◾̃ uncoe (VarΓ= (ap (_[_]T (_ [ ⌜ β ⌝V ]T)) (⌜π₁v⌝ (γ ,V y) ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹)) ⁻¹̃
           ◾̃ uncoe (VarΓ= ([][]T ◾ ap (_[_]T _) (⌜∘V⌝ β))))
