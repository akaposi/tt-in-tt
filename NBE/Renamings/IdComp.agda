{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.IdComp where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars
open import NBE.Renamings.Proj
open import NBE.Renamings.Comp
open import NBE.Renamings.Weaken
open import NBE.Renamings.Id
open import NBE.Renamings.Idl
open import NBE.Renamings.VsuComp

abstract
  [id]v : ∀{Γ A}{x : Var Γ A} → x ≃ x [ idV ]v
  [id]v {x = vze}
    = uncoe (VarΓ= (ap (_[_]T _) (pidV, _ _ ⁻¹)))
    ◾̃ uncoe (Var= refl (πv.,Vᴹ.pA[β] idV _ _ refl refl))
    ◾̃ uncoe (VarΓ= (ap (_[_]T _) (⌜π₁v⌝ idV ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹))
  [id]v {x = vsu x}
    = vsu≃' ([id]T ⁻¹ ◾ ap (_[_]T _) (⌜idV⌝ ⁻¹)) ([id]v {x = x})
    ◾̃ vsu[] {x = x}{β = idV}
    ◾̃ uncoe (VarΓ= (ap (_[_]T _) (⌜π₁v⌝ idV ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹))

