{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.VsuComp where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import TT.ConElim

open import NBE.Renamings.Var
open import NBE.Renamings.Vars
open import NBE.Renamings.Proj
open import NBE.Renamings.Comp
open import NBE.Renamings.Weaken
open import NBE.Renamings.Id
open import NBE.Renamings.Idl

abstract
  pvsu[] : ∀{Γ Δ A B}{β : Vars Γ (Δ , A)}
           {Γ' : Con}{Δ' : Con}{β' : Vars Γ' Δ'}
           (βᴹ : (pΓ : Γ' ≡ Γ)(pΔA : Δ' ≡ Δ , A) → β' ≡[ Vars= pΓ pΔA ]≡ β → vsu {B = B}(π₂v β) ≃ π₂v (wkV {A = B} β))
           {A' : Ty Δ'}(x : Var Γ' (A' [ ⌜ β' ⌝V ]T))(pΓ : Γ' ≡ Γ)
           (pΔA : Δ' , A' ≡ Δ , A)
         →  β' ,V x ≡[ Vars= pΓ pΔA ]≡ β
         → vsu {B = B}(π₂v β) ≃ π₂v (wkV {A = B} β)
  pvsu[] {Γ}{Δ}{A}{B}{β}{Γ'}{Δ'}{β'} βᴹ {A'} x pΓ pΔA pβx
    = vsu≃ (pΓ ⁻¹)
           ([]T≃' (pΓ ⁻¹) (inj,₀ pΔA ⁻¹) (from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA) ⁻¹̃)
                  (⌜⌝V≃ (pΓ ⁻¹) (inj,₀ pΔA ⁻¹) {π₁v β}{β'}
                        ( π₁v≃ (pΓ ⁻¹) (inj,₀ pΔA ⁻¹) (from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA) ⁻¹̃) {β}{β' ,V x}



                               (from≡ (Vars= pΓ pΔA) pβx ⁻¹̃)
                        ◾̃ to≃ (π₁vβ {β = β'}{x = x}))))
           ( π₂v≃ (pΓ ⁻¹) (inj,₀ pΔA ⁻¹) (from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA) ⁻¹̃) {β}{β' ,V x}
                  (from≡ (Vars= pΓ pΔA) pβx ⁻¹̃)
           ◾̃ π₂vβ')
           (uncoe (Ty= pΓ ⁻¹))
    ◾̃ uncoe (VarΓ= ([][]T ◾ ap (_[_]T A') (⌜wkV⌝ β')))
    ◾̃ π₂vβ' ⁻¹̃
    ◾̃ π₂v≃ (,C= pΓ (from≃ (uncoe (Ty= pΓ) ⁻¹̃ ◾̃ uncoe (Ty= pΓ ⁻¹) ⁻¹̃)))
           (inj,₀ pΔA) (from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA))
           (wkV≃ pΓ pΔA {β' ,V x}{β} (from≡ (Vars= pΓ pΔA) pβx) (uncoe (Ty= pΓ ⁻¹) ⁻¹̃))

abstract
  pvsu[]' : ∀{Γ Δ A B C}{β : Vars Γ (Δ , C)}{x : Var Δ A}
           (rec : {Γ'' : Con}{B' : Ty Γ''}{β'' : Vars Γ'' Δ} → vsu {B = B'} (x [ β'' ]v) ≃ x [ wkV {A = B'} β'' ]v)
           {Γ' : Con}{Δ' : Con}{β' : Vars Γ' Δ'}
           (βᴹ : (pΓ : Γ' ≡ Γ)(pΔC : Δ' ≡ Δ , C) → β' ≡[ Vars= pΓ pΔC ]≡ β → vsu {B = B}(x [ π₁v β ]v) ≃ x [ π₁v (wkV {A = B} β) ]v)
           {A' : Ty Δ'}(x' : Var Γ' (A' [ ⌜ β' ⌝V ]T))(pΓ : Γ' ≡ Γ)
           (pΔC : Δ' , A' ≡ Δ , C)
         →  β' ,V x' ≡[ Vars= pΓ pΔC ]≡ β
         → vsu {B = B}(x [ π₁v β ]v) ≃ x [ π₁v (wkV {A = B} β) ]v
  pvsu[]' {Γ}{Δ}{A}{B}{C}{β}{x} rec {Γ'}{Δ'}{β'} βᴹ {A'} x' pΓ pΔA pβx
    = rec {β'' = π₁v β}
    ◾̃ []v≃ refl refl r̃ {x} r̃
           {wkV {A = B} (π₁v β)}{π₁v (wkV {A = B} β)}
           ( wkV≃ (pΓ ⁻¹) (inj,₀ pΔA ⁻¹)
                  (π₁v≃ (pΓ ⁻¹) (inj,₀ pΔA ⁻¹)
                        (from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA) ⁻¹̃)
                        {β}{β' ,V x'}
                        (from≡ (Vars= pΓ pΔA) pβx ⁻¹̃))
                  (uncoe (Ty= (pΓ ⁻¹)))
           ◾̃ wkV≃ refl refl {π₁v (β' ,V x')}{β'} (to≃ (π₁vβ {β = β'}{x'})) r̃
           ◾̃ to≃ (π₁vβ {β = wkV β'}{x = coe (VarΓ= ([][]T ◾ ap (_[_]T A') (⌜wkV⌝ β'))) (vsu x')} ⁻¹)
           ◾̃ π₁v≃ (,C= pΓ (from≃ (uncoe (Ty= pΓ) ⁻¹̃ ◾̃ uncoe (Ty= (pΓ ⁻¹)) ⁻¹̃))) (inj,₀ pΔA)
                  (from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA))
                  (wkV≃ pΓ pΔA {β' ,V x'}{β} (from≡ (Vars= pΓ pΔA) pβx) (uncoe (Ty= (pΓ ⁻¹)) ⁻¹̃)))

abstract
  vsu[] : ∀{Γ Δ A B}{x : Var Δ A}{β : Vars Γ Δ}
        → vsu {B = B} (x [ β ]v) ≃ x [ wkV {A = B} β ]v
        
  vsu[] {Γ}{B = B}{x = vze {Δ}{A}}{β}
  
    = vsu≃' ((ap (_[_]T _) (⌜π₁v⌝ β ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹) ⁻¹)
           (uncoe (VarΓ= (ap (_[_]T _) (⌜π₁v⌝ β ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹)) ⁻¹̃)
    ◾̃ Vars-elim
        (record
         { Varsᴹ = λ Γ' Δ' γ' → (pΓ : Γ' ≡ Γ)(pΔA : Δ' ≡ (Δ , A)) → γ' ≡[ Vars= pΓ pΔA ]≡ β
                   → vsu {B = B}(π₂v β) ≃ π₂v (wkV {A = B} β)
         ; εVᴹ   = λ _ p _ → ⊥-elim (disj•, p)
         ; _,Vᴹ_ = pvsu[] {Γ}{Δ}{A}{B}{β}
         }) {Γ}{Δ , A} β refl refl refl
    ◾̃ uncoe (VarΓ= (ap (_[_]T _) (⌜π₁v⌝ (wkV β) ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹))
    
  vsu[] {Γ}{B = B}{x = vsu {Δ}{A}{C} x}{β}
    = vsu≃' ((ap (_[_]T _) (⌜π₁v⌝ β ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹) ⁻¹)
           (uncoe (VarΓ= (ap (_[_]T _) (⌜π₁v⌝ β ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹)) ⁻¹̃)
    ◾̃ Vars-elim
        (record
         { Varsᴹ = λ Γ' Δ' γ' → (pΓ : Γ' ≡ Γ)(pΔA : Δ' ≡ (Δ , C)) → γ' ≡[ Vars= pΓ pΔA ]≡ β
                   → vsu {B = B}(x [ π₁v β ]v) ≃ x [ π₁v (wkV {A = B} β) ]v
         ; εVᴹ   = λ _ p _ → ⊥-elim (disj•, p)
         ; _,Vᴹ_ = pvsu[]' {Γ}{Δ}{A}{B}{C}{β}{x}(vsu[] {x = x})
         }) {Γ}{Δ , C} β refl refl refl
    ◾̃ uncoe (VarΓ= (ap (_[_]T _) (⌜π₁v⌝ (wkV β) ◾ π₁id∘ ⁻¹) ◾ [][]T ⁻¹))
