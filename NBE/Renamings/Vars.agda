{-# OPTIONS --no-eta --rewriting #-}

module NBE.Renamings.Vars where

open import Agda.Primitive
open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.ConElim

open import NBE.Renamings.Var

-- renaming

data Vars : (Γ : Con)(Δ : Con)  → Set
⌜_⌝V : ∀{Γ Δ} → Vars Γ Δ → Tms Γ Δ

data Vars where
  εV   : ∀{Γ} → Vars Γ •
  _,V_ : ∀{Γ Δ}(β : Vars Γ Δ){A : Ty Δ} → Var Γ (A [ ⌜ β ⌝V ]T) → Vars Γ (Δ , A)

⌜ εV ⌝V = ε
⌜ β ,V x ⌝V = ⌜ β ⌝V ,s ⌜ x ⌝v

infix 4 ⌜_⌝V
infixl 5 _,V_


-- congruence rules

Vars= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
        {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
      → Vars Γ₀ Δ₀ ≡ Vars Γ₁ Δ₁
Vars= refl refl = refl

,V≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≃ β₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≃ A₁)
      {x₀ : Var Γ₀ (A₀ [ ⌜ β₀ ⌝V ]T)}{x₁ : Var Γ₁ (A₁ [ ⌜ β₁ ⌝V ]T)}(x₂ : x₀ ≃ x₁)
    → _≃_ {A = Vars Γ₀ (Δ₀ , A₀)}{Vars Γ₁ (Δ₁ , A₁)} (β₀ ,V x₀) (β₁ ,V x₁)
,V≃ refl refl (refl ,≃ refl) (refl ,≃ refl) (refl ,≃ refl) = refl ,≃ refl

,V≃' : ∀{Γ Δ}{β₀ β₁ : Vars Γ Δ}(β₂ : β₀ ≡ β₁)
       {A : Ty Δ}
       {x₀ : Var Γ (A [ ⌜ β₀ ⌝V ]T)}
       {x₁ : Var Γ (A [ ⌜ β₁ ⌝V ]T)}
       (x₂ : x₀ ≃ x₁)
     → _≡_ {A = Vars Γ (Δ , A)} (β₀ ,V x₀) (β₁ ,V x₁)
,V≃' refl (refl ,≃ refl) = refl


⌜⌝V≃ : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≃ β₁)
     → ⌜ β₀ ⌝V ≃ ⌜ β₁ ⌝V
⌜⌝V≃ refl refl (refl ,≃ refl) = refl ,≃ refl

⌜⌝V= : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁){Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
       {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≡[ Vars= Γ₂ Δ₂ ]≡ β₁)
     → ⌜ β₀ ⌝V ≡[ Tms= Γ₂ Δ₂ ]≡ ⌜ β₁ ⌝V
⌜⌝V= refl refl refl = refl

,V= : ∀{Γ₀ Γ₁}(Γ₂ : Γ₀ ≡ Γ₁)
      {Δ₀ Δ₁}(Δ₂ : Δ₀ ≡ Δ₁)
      {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≡[ Vars= Γ₂ Δ₂ ]≡ β₁)
      {A₀ : Ty Δ₀}{A₁ : Ty Δ₁}(A₂ : A₀ ≡[ Ty= Δ₂ ]≡ A₁)
      {x₀ : Var Γ₀ (A₀ [ ⌜ β₀ ⌝V ]T)}{x₁ : Var Γ₁ (A₁ [ ⌜ β₁ ⌝V ]T)}(x₂ : x₀ ≡[ Var= Γ₂ ([]T= Γ₂ Δ₂ A₂ (⌜⌝V= Γ₂ Δ₂ β₂)) ]≡ x₁)
    → (β₀ ,V x₀) ≡[ Vars= Γ₂ (,C= Δ₂ A₂) ]≡ (β₁ ,V x₁)
,V= refl refl refl refl refl = refl

-- eliminators

record MVars {i} : Set (lsuc i) where
  field
    Varsᴹ  : ∀ Γ Δ → Vars Γ Δ → Set i
    εVᴹ    : ∀{Γ} → Varsᴹ Γ • εV
    _,Vᴹ_  : ∀{Γ Δ}{β : Vars Γ Δ}(βᴹ : Varsᴹ Γ Δ β)
             {A : Ty Δ}(x : Var Γ (A [ ⌜ β ⌝V ]T))
           → Varsᴹ Γ (Δ , A) (β ,V x)

module _ {i}(M : MVars {i}) where
  open MVars M

  Vars-elim : ∀{Γ Δ}(β : Vars Γ Δ) → Varsᴹ Γ Δ β
  Vars-elim εV = εVᴹ
  Vars-elim (β ,V x) = Vars-elim β ,Vᴹ x

Vars-case,
  : ∀{i}(Varsᴹ : (Γ Δ : Con) → Vars Γ Δ → Set i)
    (_,Vᴹ_  : ∀{Γ Δ}(β : Vars Γ Δ)
              {A : Ty Δ}(x : Var Γ (A [ ⌜ β ⌝V ]T))
            → Varsᴹ Γ (Δ , A) (β ,V x))
  → ∀{Γ Δ A}(β : Vars Γ (Δ , A))
  → Varsᴹ Γ (Δ , A) β
Vars-case, Varsᴹ _,Vᴹ_ {Γ}{Δ}{A} β

  = Vars-elim (record
      { Varsᴹ = λ Γ' Δ' β' → Δ' ≡ Δ , A → Varsᴹ Γ' Δ' β'
      ; εVᴹ   = λ p → ⊥-elim (disj•, p)
      ; _,Vᴹ_ = λ {Γ'}{Δ'}{β'} _ {A'} x _ → _,Vᴹ_ {Γ'}{Δ'} β' {A'} x
      }) β refl

module Vars-case,'
  {i}(Varsᴹ : (Γ Δ : Con) → Vars Γ Δ → Set i)
  {Γ Δ : Con}{A : Ty Δ}
  (γ : Vars Γ (Δ , A))
  (,Vᴹ : (β : Vars Γ Δ)(x : Var Γ (A [ ⌜ β ⌝V ]T)) → Varsᴹ Γ (Δ , A) (β ,V x))
  where

  Varsᴹ= : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁)
           {Δ₀ Δ₁ : Con}(Δ₂ : Δ₀ ≡ Δ₁)
           {β₀ : Vars Γ₀ Δ₀}{β₁ : Vars Γ₁ Δ₁}(β₂ : β₀ ≡[ Vars= Γ₂ Δ₂ ]≡ β₁)
         → Varsᴹ Γ₀ Δ₀ β₀ ≡ Varsᴹ Γ₁ Δ₁ β₁
  Varsᴹ= refl refl refl = refl

  module ,Vᴹ
    {Γ' : Con}{Δ' : Con}{β : Vars Γ' Δ'}
    (βᴹ : Γ' ≡ Γ → Δ' ≡ Δ , A → Varsᴹ Γ' Δ' β)
    {A' : Ty Δ'}(x : Var Γ' (A' [ ⌜ β ⌝V ]T))
    (pΓ : Γ' ≡ Γ)(pΔA : Δ' , A' ≡ Δ , A)
    where

    abstract
      pA[β] : A' [ ⌜ β ⌝V ]T ≡[ Ty= pΓ ]≡ A [ ⌜ coe (Vars= pΓ (inj,₀ pΔA)) β ⌝V ]T
      pA[β] = []T= pΓ (inj,₀ pΔA) (inj,₁ pΔA) (⌜⌝V= pΓ (inj,₀ pΔA) refl)

    abstract
      pβx : coe (Vars= pΓ (inj,₀ pΔA)) β ,V coe (Var= pΓ pA[β]) x
          ≡[ Vars= (pΓ ⁻¹) (pΔA ⁻¹) ]≡
            β ,V x
      pβx = UIP' (Vars= (pΓ ⁻¹) (,C= (inj,₀ pΔA ⁻¹) _))
                 (Vars= (pΓ ⁻¹) (pΔA ⁻¹))
                 (,V= (pΓ ⁻¹)
                      (inj,₀ pΔA ⁻¹)
                      (from≃ (uncoe (Vars= (pΓ ⁻¹) (inj,₀ pΔA ⁻¹)) ⁻¹̃ ◾̃ uncoe (Vars= pΓ (inj,₀ pΔA)) ⁻¹̃))
                      (from≃ (uncoe (Ty= (inj,₀ pΔA ⁻¹)) ⁻¹̃ ◾̃ from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA) ⁻¹̃))
                      (from≃ ( uncoe (Var= (pΓ ⁻¹) ([]T= (pΓ ⁻¹) (inj,₀ pΔA ⁻¹) (from≃ (uncoe (Ty= (inj,₀ pΔA ⁻¹)) ⁻¹̃ ◾̃ from≡ (Ty= (inj,₀ pΔA)) (inj,₁ pΔA) ⁻¹̃)) (⌜⌝V= (pΓ ⁻¹) (inj,₀ pΔA ⁻¹) (from≃ (uncoe (Vars= (pΓ ⁻¹) (inj,₀ pΔA ⁻¹)) ⁻¹̃ ◾̃ uncoe (Vars= pΓ (inj,₀ pΔA)) ⁻¹̃))))) ⁻¹̃
                             ◾̃ uncoe (Var= pΓ pA[β]) ⁻¹̃)))  

    res : Varsᴹ Γ' (Δ' , A') (β ,V x)
    res = coe (Varsᴹ= (pΓ ⁻¹) (pΔA ⁻¹) pβx)
              (,Vᴹ (coe (Vars= pΓ (inj,₀ pΔA)) β)
                   (coe (Var= pΓ pA[β]) x) )

  res : Varsᴹ Γ (Δ , A) γ
  res = Vars-elim (record
    { Varsᴹ = λ Γ' Δ' β' → Γ' ≡ Γ → Δ' ≡ Δ , A → Varsᴹ Γ' Δ' β'
    ; εVᴹ   = λ _ p → ⊥-elim (disj•, p)
    ; _,Vᴹ_ = ,Vᴹ.res
    }) γ refl refl
