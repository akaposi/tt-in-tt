{-# OPTIONS --no-eta --rewriting #-}

module NBE.Stab where

open import lib
open import JM
open import Cats
open import TT.Syntax
open import TT.Congr
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Norm
open import NBE.Quote.Motives
open import NBE.Quote.Quote
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l

open import NBE.Cheat

-- stability for normal forms

stabNf : ∀{Γ A}(n : Nf Γ A)
       → normt ⌜ n ⌝nf ≡ n

stabNe : ∀{Γ A}(n : Ne Γ A)
       → ⟦ ⌜ n ⌝ne ⟧t $S (id ,Σ (ID Γ))
       ≃ uT (QT A) $S (id ,Σ coe (NeΓ= ([id]T ⁻¹)) n ,Σ ID Γ)

stabNf (neuU n) = cheat

stabNf (neuEl n) = cheat

stabNf (lamNf n) = cheat

stabNe (var vze) = cheat

stabNe (var (vsu x)) = cheat

stabNe (appNe n v) = cheat

-- stability for normal substitutions

stabNfs : ∀{Γ Δ}(τ : Nfs Γ Δ) → norms ⌜ τ ⌝nfs ≡ τ
stabNfs εnfs = refl
stabNfs (τ ,nfs v)

  = ,nfs= (cheat ◾ stabNfs τ)
          (cheat ◾̃ to≃ (stabNf v))
