{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

module NBE.LogPred.Ty.Subst where

open import lib
open import JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt

open Motives M
open MethodsCon mCon

module m[]Tᴹ
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {Δ : Con}{⟦Δ⟧ : Conᴹ Δ}
  {A : Ty Δ}(⟦A⟧ : Tyᴹ ⟦Δ⟧ A)
  {σ : Tms Γ Δ}(⟦σ⟧ : Tmsᴹ ⟦Γ⟧ ⟦Δ⟧ σ)
  where

    p$F : ∀{Ψ} → (TMC Γ ,P TMT (A [ σ ]T) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ → Set
    p$F (ρ ,Σ s ,Σ α) = ⟦A⟧ $F (σ ∘ ρ ,Σ coe (TmΓ= [][]T) s ,Σ ⟦σ⟧ $S (ρ ,Σ α))

    abstract
      $F$coe : {Ψ Ω : Con}{f : Vars Ω Ψ}
               {α : (TMC Γ ,P TMT (A [ σ ]T) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
             → _≡_ {A = (TMC Δ ,P TMT A ,P ⟦Δ⟧ [ wkn ]F) $P Ω}
                   ( (σ ∘ proj₁ (proj₁ α)) ∘ ⌜ f ⌝V
                   ,Σ coe (TmΓ= [][]T) (coe (TmΓ= [][]T) (proj₂ (proj₁ α)) [ ⌜ f ⌝V ]t)
                   ,Σ ⟦Δ⟧ $F f $ (⟦σ⟧ $S (proj₁ (proj₁ α) ,Σ proj₂ α)))
                   ( σ ∘ (proj₁ (proj₁ α) ∘ ⌜ f ⌝V)
                   ,Σ coe (TmΓ= [][]T) (coe (TmΓ= [][]T) (proj₂ (proj₁ α) [ ⌜ f ⌝V ]t))
                   ,Σ ⟦σ⟧ $S (proj₁ (proj₁ α) ∘ ⌜ f ⌝V ,Σ ⟦Γ⟧ $F f $ proj₂ α))
      $F$coe {Ψ}{Ω}{f}{α}
      
        = Tbase= ⟦Δ⟧
                 ass
                 ( uncoe (TmΓ= [][]T) ⁻¹̃
                 ◾̃ coe[]t' [][]T
                 ◾̃ uncoe (TmΓ= [][]T)
                 ◾̃ uncoe (TmΓ= [][]T))
                 (uncoe (ap (_$F_ ⟦Δ⟧) ass) ◾̃ to≃ (natS ⟦σ⟧))

    p$F$ : ∀{Ψ Ω : Con}(f : Vars Ω Ψ)
           {α : (TMC Γ ,P TMT (A [ σ ]T) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
         → p$F α
         → p$F ((TMC Γ ,P TMT (A [ σ ]T) ,P ⟦Γ⟧ [ wkn ]F) $P f $ α)
    p$F$ f a = coe ($F= ⟦A⟧ $F$coe) (⟦A⟧ $F f $ a)

    abstract
      pidF : ∀{Ψ}{α : (TMC Γ ,P TMT (A [ σ ]T) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}{a : p$F α}
           → p$F$ idV a ≡[ ap p$F (idP (TMC Γ ,P TMT (A [ σ ]T) ,P ⟦Γ⟧ [ wkn ]F)) ]≡ a
      pidF {Ψ}{α}{a}
      
        = from≃ ( uncoe (ap p$F (idP (TMC Γ ,P TMT (A [ σ ]T) ,P ⟦Γ⟧ [ wkn ]F))) ⁻¹̃
                ◾̃ uncoe ($F= ⟦A⟧ $F$coe) ⁻¹̃
                ◾̃ idF' ⟦A⟧)
                
    abstract
      pcompF : ∀{Ψ Ω Ξ}{f : Vars Ω Ψ}{g : Vars Ξ Ω}
               {α : (TMC Γ ,P TMT (A [ σ ]T) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}
               {a : p$F α}
             → p$F$ (f ∘V g) a
               ≡[ ap p$F (compP (TMC Γ ,P TMT (A [ σ ]T) ,P ⟦Γ⟧ [ wkn ]F)) ]≡
               p$F$ g (p$F$ f a)
      pcompF {Ψ}{Ω}{Ξ}{f}{g}{α}{a}
      
        = from≃ ( uncoe (ap p$F (compP (TMC Γ ,P TMT (A [ σ ]T) ,P ⟦Γ⟧ [ wkn ]F))) ⁻¹̃
                ◾̃ uncoe ($F= ⟦A⟧ $F$coe) ⁻¹̃
                ◾̃ compF' ⟦A⟧
                ◾̃ $Ff$≃ ⟦A⟧ $F$coe
                            (uncoe ($F= ⟦A⟧ $F$coe))
                ◾̃ uncoe ($F= ⟦A⟧ $F$coe))

    _[_]Tᴹ : Tyᴹ ⟦Γ⟧ (A [ σ ]T)
    _[_]Tᴹ = record
      { _$F_   = p$F
      ; _$F_$_ = p$F$
      ; idF    = pidF
      ; compF  = pcompF
      }
