{-# OPTIONS --no-eta #-}

open import Cats
open import NBE.TM

open import lib hiding (lift)
open import JM
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.LogPred.Motives
open import NBE.LogPred.Cxt
open import NBE.LogPred.Ty.Subst
open import NBE.LogPred.Ty.Pi.Exp

open Motives M
open MethodsCon mCon

module NBE.LogPred.Ty.Pi.LiftExp
  {Γ : Con}{⟦Γ⟧ : Conᴹ Γ}
  {A : Ty Γ}{⟦A⟧ : Tyᴹ ⟦Γ⟧ A}
  {B : Ty (Γ , A)}{⟦B⟧ : Tyᴹ (⟦Γ⟧ ,Cᴹ ⟦A⟧) B}
  {Ψ Ψ' : Con}(β' : Vars Ψ' Ψ)
  {α' : (TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P Ψ}  
  (e : E ⟦B⟧ α')
  where

abstract
  pcoe6 : ∀{Ω}{β : Vars Ω Ψ'}
        → (ρ ⟦B⟧ α' ∘ ⌜ β' ⌝V) ∘ ⌜ β ⌝V ≡ ρ ⟦B⟧ α' ∘ ⌜ β' ∘V β ⌝V
  pcoe6 = ass ◾ ap (_∘_ _) (⌜∘V⌝ β')

abstract
  pcoe7 : ∀{Ω}{β : Vars Ω Ψ'}
          {u : TMT A $F ((TMC Γ $P β' $ ρ ⟦B⟧ α') ∘ ⌜ β ⌝V)}
        → _≡_ {A = (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F) $P Ω}
              ( (TMC Γ $P β' $ ρ ⟦B⟧ α') ∘ ⌜ β ⌝V
              ,Σ u
              ,Σ ⟦Γ⟧ $F β $ (⟦Γ⟧ $F β' $ α ⟦B⟧ α'))
              ( ρ ⟦B⟧ α' ∘ ⌜ β' ∘V β ⌝V
              ,Σ coe (TmΓ= (ap (_[_]T A) pcoe6)) u
              ,Σ ⟦Γ⟧ $F β' ∘V β $ α ⟦B⟧ α')

  pcoe7 = Tbase= ⟦Γ⟧
                 pcoe6
                 (uncoe (TmΓ= (ap (_[_]T A) pcoe6)))
                 (compF' ⟦Γ⟧ ⁻¹̃)

abstract
  pcoe8
    : ∀{Ω}{β : Vars Ω Ψ'}
      {u : TMT A $F ((TMC Γ $P β' $ ρ ⟦B⟧ α') ∘ ⌜ β ⌝V)}
      {v : ⟦A⟧ $F ((TMC Γ $P β' $ ρ ⟦B⟧ α') ∘ ⌜ β ⌝V ,Σ u ,Σ ⟦Γ⟧ $F β $ (⟦Γ⟧ $F β' $ α ⟦B⟧ α'))}
    → _≡_
        {A = (TMC (Γ , A) ,P TMT B ,P (⟦Γ⟧ ,Cᴹ ⟦A⟧) [ wkn ]F) $P Ω}
        ( (ρ ⟦B⟧ α' ∘ ⌜ β' ∘V β ⌝V ,s coe (TmΓ= (ap (_[_]T A) pcoe6)) u)
        ,Σ coe (TmΓ= (pcoe1 ⟦B⟧ α')) (coe (TmΓ= (pcoe2 ⟦B⟧ α')) (s ⟦B⟧ α' [ ⌜ β' ∘V β ⌝V ]t) $$ coe (TmΓ= (ap (_[_]T A) pcoe6)) u)
        ,Σ (coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β' ∘V β $ α ⟦B⟧ α') ,Σ coe ($F= ⟦A⟧ (projTbase ⟦B⟧ α')) (coe ($F= ⟦A⟧ pcoe7) v)))
        ( ((TMC Γ $P β' $ ρ ⟦B⟧ α') ∘ ⌜ β ⌝V ,s u)
        ,Σ coe (TmΓ= (pcoe1 ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α'))) (coe (TmΓ= (pcoe2 ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α'))) ((TMT (Π A B) $F β' $ s ⟦B⟧ α') [ ⌜ β ⌝V ]t) $$ u)
        ,Σ (coe ($F= ⟦Γ⟧ (π₁β ⁻¹)) (⟦Γ⟧ $F β $ (⟦Γ⟧ $F β' $ α ⟦B⟧ α')) ,Σ coe ($F= ⟦A⟧ (projTbase ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α'))) v))
  pcoe8 {Ω}{β}{u}{v}

    = Tbase= (⟦Γ⟧ ,Cᴹ ⟦A⟧)
              (,s≃' (pcoe6 ⁻¹) (uncoe (TmΓ= (ap (_[_]T A) pcoe6)) ⁻¹̃))
              ( uncoe (TmΓ= (pcoe1 ⟦B⟧ α')) ⁻¹̃
              ◾̃ $≃' (ap (_[_]T A) (pcoe6 ⁻¹))
                    ([]T≃ (,C≃ refl (to≃ (ap (_[_]T A) (pcoe6 ⁻¹))))
                          (^≃ (pcoe6 ⁻¹)))
                    (uncoe (TmΓ= (pcoe2 ⟦B⟧ α')) ⁻¹̃
                    ◾̃ []t≃ refl (to≃ (⌜∘V⌝ β' ⁻¹))
                    ◾̃ [][]t' ⁻¹̃
                    ◾̃ coe[]t' [][]T ⁻¹̃
                    ◾̃ uncoe (TmΓ= (pcoe2 ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α'))))
                    (uncoe (TmΓ= (ap (_[_]T A) pcoe6)) ⁻¹̃)
              ◾̃ uncoe (TmΓ= (pcoe1 ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α'))))
              (,Σ≃≃ (funext≃ ($F= ⟦Γ⟧ (π₁β ◾ pcoe6 ⁻¹ ◾ π₁β ⁻¹))
                           (λ x₂ → to≃ ($F= ⟦A⟧ (Tbase= ⟦Γ⟧
                                                        (π₁β ◾ pcoe6 ⁻¹ ◾ π₁β ⁻¹)
                                                        (π₂β' ◾̃ uncoe (TmΓ= (ap (_[_]T A) pcoe6)) ⁻¹̃ ◾̃ π₂β' ⁻¹̃)
                                                        x₂))))
                  ( uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)) ⁻¹̃
                  ◾̃ compF' ⟦Γ⟧
                  ◾̃ uncoe ($F= ⟦Γ⟧ (π₁β ⁻¹)))
                  ( uncoe ($F= ⟦A⟧ (projTbase ⟦B⟧ α')) ⁻¹̃
                  ◾̃ uncoe ($F= ⟦A⟧ pcoe7) ⁻¹̃
                  ◾̃ uncoe ($F= ⟦A⟧ (projTbase ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α')))))

open E {⟦B⟧ = ⟦B⟧}{α' = α'} e

abstract
  pnat
    : {Ω : Con}{β : Vars Ω Ψ'}
      {u : TMT A $F ((TMC Γ $P β' $ ρ ⟦B⟧ α') ∘ ⌜ β ⌝V)}
      {v : ⟦A⟧ $F ((TMC Γ $P β' $ ρ ⟦B⟧ α') ∘ ⌜ β ⌝V ,Σ u ,Σ ⟦Γ⟧ $F β $ (⟦Γ⟧ $F β' $ α ⟦B⟧ α'))}
      {Ξ : Con}{γ : Vars Ξ Ω}
    → ⟦B⟧ $F γ $ coe ($F= ⟦B⟧ pcoe8) (map (β' ∘V β) (coe ($F= ⟦A⟧ pcoe7) v))
      ≡[ $F= ⟦B⟧ (pcoe5 ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α')) ]≡
      coe ($F= ⟦B⟧ pcoe8) (map (β' ∘V (β ∘V γ)) (coe ($F= ⟦A⟧ pcoe7) (coe ($F= ⟦A⟧ (pcoe4 ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α') β)) (⟦A⟧ $F γ $ v))))
  pnat {Ω}{β}{u}{v}{Ξ}{γ}
    = from≃ ( uncoe ($F= ⟦B⟧ (pcoe5 ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α'))) ⁻¹̃
            ◾̃ $Ff$≃ ⟦B⟧ (pcoe8 ⁻¹) (uncoe ($F= ⟦B⟧ pcoe8) ⁻¹̃)
            ◾̃ from≡ ($F= ⟦B⟧ (pcoe5 ⟦B⟧ α')) nat
            ◾̃ map≃ ⟦B⟧ α' e
                   assV
                   ( uncoe (TmΓ= (pcoe3 ⟦B⟧ α' (β' ∘V β))) ⁻¹̃
                   ◾̃ coe[]t' (ap (_[_]T A) pcoe6)
                   ◾̃ uncoe (TmΓ= (pcoe3 ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α') β))
                   ◾̃ uncoe (TmΓ= (ap (_[_]T A) pcoe6)))
                   ( uncoe ($F= ⟦A⟧ (pcoe4 ⟦B⟧ α' (β' ∘V β))) ⁻¹̃
                   ◾̃ $Ff$≃ ⟦A⟧ (pcoe7 ⁻¹) (uncoe ($F= ⟦A⟧ pcoe7) ⁻¹̃)
                   ◾̃ uncoe ($F= ⟦A⟧ (pcoe4 ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α') β))
                   ◾̃ uncoe ($F= ⟦A⟧ pcoe7))
            ◾̃ uncoe ($F= ⟦B⟧ pcoe8))

lift : E ⟦B⟧ ((TMC Γ ,P TMT (Π A B) ,P ⟦Γ⟧ [ wkn ]F) $P β' $ α')
lift = record
  { map = λ β v → coe ($F= ⟦B⟧ pcoe8)
                      (map (β' ∘V β) (coe ($F= ⟦A⟧ pcoe7) v))
  ; nat = pnat
  }
