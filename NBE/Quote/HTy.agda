module NBE.Quote.HTy where

open import lib
open import JM
open import Cats
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Quote.Motives
open import NBE.Quote.Cxt
open import NBE.Quote.Ty
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l
import NBE.LogPred.Ty

open Motives M
open MethodsCon mCon
open MethodsTy mTy

open import NBE.Cheat

mTms : MethodsTms M mCon mTy
mTms = record { εᴹ = tt ; _,sᴹ_ = λ _ _ → tt ; idᴹ = tt ; _∘ᴹ_ = λ _ _ → tt ; π₁ᴹ = λ _ → tt }

mTm : MethodsTm M mCon mTy mTms
mTm = record { _[_]tᴹ = λ _ _ → tt ; π₂ᴹ = λ _ → tt ; appᴹ = λ _ → tt ; lamᴹ = λ _ → tt }

mHTy : MethodsHTy M mCon mTy mTms mTm
mHTy = record
  { [id]Tᴹ = cheat
  ; [][]Tᴹ = cheat
  ; U[]ᴹ   = cheat
  ; El[]ᴹ  = cheat
  ; Π[]ᴹ   = cheat }
