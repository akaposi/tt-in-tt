{-# OPTIONS --no-eta --rewriting #-}

module NBE.Quote.Ty where

open import lib
open import JM
open import Cats
open import TT.Syntax renaming (_$_ to _$$_)
open import TT.ElimOld
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.TM
open import NBE.Nf
open import NBE.Quote.Motives
open import NBE.Quote.Cxt
open import NBE.Quote.Ty.Subst
open import NBE.Quote.Ty.Base
open import NBE.Quote.Ty.Pi
open import NBE.LogPred.Motives using (Tbase=)
open import NBE.LogPred.LogPred U̅ E̅l
import NBE.LogPred.Ty.Pi

open Motives M
open MethodsCon mCon

open import NBE.Cheat

open import NBE.Glue

mTy : MethodsTy M mCon
mTy = record
  { _[_]Tᴹ = m[]Tᴹ._[_]Tᴹ
  ; Uᴹ     = mUᴹ.Uᴹ
  ; Elᴹ    = mElᴹ.Elᴹ
  ; Πᴹ     = Πᴹ
  }
