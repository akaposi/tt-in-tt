module NBE.experiments.Nes where

open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.Nf.Nfs
open import NBE.Nf

-- neutral substitutions form a category

infix 3 ⌜_⌝v' ⌜_⌝V'

⌜_⌝v' : ∀{Γ A} → Var Γ A → Ne Γ A
⌜_⌝V' : ∀{Γ Δ} → Vars Δ Γ → Nes Δ Γ

⌜ x ⌝v' = var x

⌜ ε ⌝V' = ε
⌜ β ,V x ⌝V' = ⌜ β ⌝V' ,nes coe (NeΓ= (ap (_[_]T _) {!!})) ⌜ x ⌝v'

_[_]v'  : ∀{Γ A} → Var Γ A → {Δ : Con}(ρ : Nes Δ Γ) → Ne Δ (A [ ⌜ ρ ⌝nes ]T)
_[_]ne' : ∀{Γ A} → Ne Γ A → {Δ : Con}(ρ : Nes Δ Γ) → Ne Δ (A [ ⌜ ρ ⌝nes ]T)
_[_]nf' : ∀{Γ A} → Nf Γ A → {Δ : Con}(ρ : Nes Δ Γ) → Nf Δ (A [ ⌜ ρ ⌝nes ]T)

⌜[]nf'⌝ : ∀{Γ A}{v : Nf Γ A}{Δ}{ρ : Nes Δ Γ} → ⌜ v [ ρ ]nf' ⌝nf ≃ ⌜ v ⌝nf [ ⌜ ρ ⌝nes ]t

vze [ ρ ,nes n ]v' = coe (NeΓ= (ap (_[_]T _) (π₁idβ ⁻¹) ◾ [][]T ⁻¹)) n
vsu x [ ρ ,nes n ]v' = coe (NeΓ= (ap (_[_]T _) (π₁idβ ⁻¹) ◾ [][]T ⁻¹))
                           (x [ ρ ]v')

var x [ ρ ]ne' = x [ ρ ]v'
_[_]ne' (appNe {Γ}{A}{B} n v) {Δ} ρ = coe l (appNe (coe (NeΓ= Π[]) (n [ ρ ]ne')) (v [ ρ ]nf'))
  where
    abstract
      l : Ne Δ (B [ ⌜ ρ ⌝nes ^ A ]T [ < ⌜ v [ ρ ]nf' ⌝nf > ]T) ≡ Ne Δ (B [ < ⌜ v ⌝nf > ]T [ ⌜ ρ ⌝nes ]T)
      l = NeΓ= ( [][]T
               ◾ ap (_[_]T B)
                    ( ^∘<> refl
                    ◾ ,s≃' (idl ⁻¹)
                           ( ⌜[]nf'⌝
                           ◾̃ coe[]t' ([id]T ⁻¹) ⁻¹̃
                           ◾̃ uncoe (TmΓ= [][]T))
                    ◾ ,∘ ⁻¹)
               ◾ [][]T ⁻¹)

neuU n [ ρ ]nf' = coe (NfΓ= (U[] ⁻¹)) (neuU (coe (NeΓ= U[]) (n [ ρ ]ne')))
neuEl n [ ρ ]nf' = coe (NfΓ= (El[] ⁻¹)) (neuEl (coe (NeΓ= El[]) (n [ ρ ]ne')))
lamNf v [ ρ ]nf' = coe (NfΓ= (Π[] ⁻¹)) (lamNf {!!})

⌜[]nf'⌝ = {!!}

_∘nes_ : ∀{Γ Δ Θ} → Nes Δ Γ → Nes Θ Δ → Nes Θ Γ

ε ∘nes σ = ε
(ρ ,nes n) ∘nes σ = (ρ ∘nes σ) ,nes coe (NeΓ= ([][]T ◾ ap (_[_]T _) {!!})) (n [ σ ]ne')
