{-# OPTIONS --no-eta #-}

open import lib
open import TT.Syntax
open import NBE.Nf
open import NBE.Nfs

module NBE.experiments.NormalModel.HTm
  (norms  : ∀{Γ Δ} → Tms Γ Δ → Nfs Γ Δ)
  (compls : ∀{Γ Δ}(σ : Tms Γ Δ) → σ ≡ ⌜ norms σ ⌝nfs)
  (normt  : ∀{Γ A} → Tm Γ A → Nf Γ A)
  (complt : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ normt t ⌝nf)
  (stabs  : ∀{Γ Δ}(τ : Nfs Γ Δ) → norms ⌜ τ ⌝nfs ≡ τ)
  (stabt  : ∀{Γ A}(n : Nf Γ A) → normt ⌜ n ⌝nf ≡ n)
  where

open import JM
open import TT.Congr
open import TT.Laws
open import TT.Elim
open import NBE.experiments.NConTy
open import NBE.experiments.NormalModel.Motives
open import NBE.experiments.NormalModel.Cxt
open import NBE.experiments.NormalModel.Ty  norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.Tms norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.Tm norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.HTy norms compls normt complt stabs stabt
open import NBE.experiments.NormalModel.HTms norms compls normt complt stabs stabt

abstract
  lam[]ᴹ : {Γ : Con}{Γᴹ : Motives.Conᴹ M Γ}
           {Δ : Con}{Δᴹ : Motives.Conᴹ M Δ}
           {δ : Tms Γ Δ}{δᴹ : Motives.Tmsᴹ M Γᴹ Δᴹ δ}
           {A : Ty Δ}{Aᴹ : Motives.Tyᴹ M Δᴹ A}
           {B : Ty (Δ , A)}{Bᴹ : Motives.Tyᴹ M (MethodsCon._,Cᴹ_ mCon Δᴹ Aᴹ) B}
           {t : Tm (Δ , A) B}{tᴹ : Motives.Tmᴹ M (MethodsCon._,Cᴹ_ mCon Δᴹ Aᴹ) Bᴹ t}
         → MethodsTm._[_]tᴹ mTm {Γ}{Γᴹ}{Δ}{Δᴹ} (MethodsTm.lamᴹ mTm {Δ}{Δᴹ} tᴹ) δᴹ
         ≡[ Motives.TmΓᴹ= M {Γ}{Γᴹ} Π[] (MethodsHTy.Π[]ᴹ mHTy {Γ}{Γᴹ}{Δ}{Δᴹ}) lam[] ]≡
           MethodsTm.lamᴹ mTm {Γ}{Γᴹ} (MethodsTm._[_]tᴹ mTm {Γ , A [ δ ]T}{MethodsCon._,Cᴹ_ mCon Γᴹ (MethodsTy._[_]Tᴹ mTy {Γ}{Γᴹ}{Δ}{Δᴹ} Aᴹ δᴹ)}{Δ , A}{MethodsCon._,Cᴹ_ mCon Δᴹ Aᴹ} tᴹ (MethodsHTy._^ᴹ_ mHTy {Γ}{Γᴹ}{Δ}{Δᴹ} δᴹ Aᴹ))
  lam[]ᴹ {Γ}{Γ' , pΓ}{Δ}{Δ' , pΔ}{δ}{δ' , pδ}{A}{A' , pA}{B}{B' , pB}{t}{t' , pt}

    = =Tmᴹ' pΓ
            Π[]
            p
            _
            (Π= pΓ
                ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝)
                ( []T= (,C= pΓ ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝))
                       (,C= pΔ pA)
                       pB
                       (compls (coe (Tms= (,C= pΓ ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝)) (,C= pΔ pA)) (δ ∘ π₁ id , coe (ap (Tm (Γ , A [ δ ]T)) [][]T) (π₂ id))))
                ◾ ⌜[]nT⌝))
            lam[]
            (from≃ ( uncoe (NfΓ= (ap ⌜_⌝T p)) ⁻¹̃
                   ◾̃ normt≃
                       refl
                       (to≃ (Π= refl
                                refl
                                (ap (λ z → ⌜ B' [ norms z ]nT ⌝T) (from≃ q))))
                       ( uncoe (Tm= pΓ ([]T= pΓ pΔ (Π= pΔ pA pB) pδ ◾ ⌜[]nT⌝)) ⁻¹̃
                       ◾̃ from≡ (TmΓ= Π[]) lam[]
                       ◾̃ uncoe (Tm= pΓ (Π= pΓ ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝) ([]T= (,C= pΓ ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝)) (,C= pΔ pA) pB (compls (coe (Tms= (,C= pΓ ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝)) (,C= pΔ pA)) (δ ∘ π₁ id , coe (ap (Tm (Γ , A [ δ ]T)) [][]T) (π₂ id)))) ◾ ⌜[]nT⌝))))))
            _

    where
      abstract
        q : coe (Tms= (,C= refl (⌜[]nT⌝ {Γ'}{Δ'}{A'}{δ'})) refl)
                (⌜ δ' ⌝nfs ∘ π₁ id , coe (TmΓ= [][]T) (π₂ id))
          ≃ coe (Tms= (,C= pΓ ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝)) (,C= pΔ pA))
                (δ ∘ π₁ id , coe (TmΓ= [][]T) (π₂ id))
        q = uncoe (Tms= (,C= refl ⌜[]nT⌝) refl) ⁻¹̃
          ◾̃ ,s≃ (,C≃ (pΓ ⁻¹)
                     ([]T≃' (pΓ ⁻¹) (pΔ ⁻¹) (from≡ (Ty= pΔ) pA ⁻¹̃) (from≡ (Tms= pΓ pΔ) pδ ⁻¹̃)))
                (pΔ ⁻¹)
                (∘≃' (pΓ ⁻¹)
                     (pΔ ⁻¹)
                     (,C≃ (pΓ ⁻¹) ([]T≃' (pΓ ⁻¹) (pΔ ⁻¹) (from≡ (Ty= pΔ) pA ⁻¹̃) (from≡ (Tms= pΓ pΔ) pδ ⁻¹̃)))
                     (from≡ (Tms= pΓ pΔ) pδ ⁻¹̃)
                     (π₁id≃ (pΓ ⁻¹) ([]T≃' (pΓ ⁻¹) (pΔ ⁻¹) (from≡ (Ty= pΔ) pA ⁻¹̃) (from≡ (Tms= pΓ pΔ) pδ ⁻¹̃))))
                (from≡ (Ty= pΔ) pA ⁻¹̃)
                ( uncoe (TmΓ= [][]T) ⁻¹̃
                ◾̃ π₂id≃ (pΓ ⁻¹) ([]T≃' (pΓ ⁻¹) (pΔ ⁻¹) (from≡ (Ty= pΔ) pA ⁻¹̃) (from≡ (Tms= pΓ pΔ) pδ ⁻¹̃))
                ◾̃ uncoe (TmΓ= [][]T))
          ◾̃ uncoe (Tms= (,C= pΓ ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝)) (,C= pΔ pA))

      abstract
        p : ΠN (A' [ δ' ]nT) (B' [ norms (coe (Tms= (,C= refl ⌜[]nT⌝) refl)                           (⌜ δ' ⌝nfs ∘ π₁ id , coe (TmΓ= [][]T) (π₂ id))) ]nT)
          ≡ ΠN (A' [ δ' ]nT) (B' [ norms (coe (Tms= (,C= pΓ ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝)) (,C= pΔ pA)) (δ ∘ π₁ id         , coe (TmΓ= [][]T) (π₂ id))) ]nT)
        p = ΠN=' refl ([]nT≃ B' refl (norms≃ refl refl q))
          
mHTm : MethodsHTm M mCon mTy mTms mTm mHTy mHTms
mHTm = record
  { π₂βᴹ   = λ { {Γ}{Γ' , pΓ}{Δ}{Δ' , pΔ}{A}{A' , pA}{δ}{δ' , pδ}{a}{a' , pa}
               → =Tmᴹ'
                   pΓ
                   (ap (_[_]T A) π₁β)
                   (from≃ ([]nT≃ A' refl (to≃ (inj⌜⌝nfs (compls (coe (Tms= pΓ pΔ) (π₁ (δ , a))) ⁻¹ ◾ from≃ (uncoe (Tms= pΓ pΔ) ⁻¹̃ ◾̃ to≃ π₁β ◾̃ uncoe (Tms= pΓ pΔ)) ◾ pδ)))))
                   ([]T= pΓ pΔ pA (compls (coe (Tms= pΓ pΔ) (π₁ (δ , a)))) ◾ ⌜[]nT⌝)
                   ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝)
                   π₂β
                   (from≃ ( uncoe (NfΓ= (ap ⌜_⌝T (from≃ ([]nT≃ A' refl (to≃ (inj⌜⌝nfs (compls (coe (Tms= pΓ pΔ) (π₁ (δ , a))) ⁻¹ ◾ from≃ (uncoe (Tms= pΓ pΔ) ⁻¹̃ ◾̃ to≃ π₁β ◾̃ uncoe (Tms= pΓ pΔ)) ◾ pδ))))))) ⁻¹̃
                          ◾̃ inj⌜⌝nf'
                              (from≃ (⌜⌝T≃ refl ([]nT≃ A' refl (to≃ (inj⌜⌝nfs (compls (coe (Tms= pΓ pΔ) (π₁ (δ , a))) ⁻¹ ◾ ap (coe (Tms= pΓ pΔ)) π₁β ◾ pδ))))))
                              ( to≃ (complt (coe (Tm= pΓ ([]T= pΓ pΔ pA (compls (coe (Tms= pΓ pΔ) (π₁ (δ , a)))) ◾ ⌜[]nT⌝)) (π₂ (δ , a))) ⁻¹)
                              ◾̃ uncoe (Tm= pΓ ([]T= pΓ pΔ pA (compls (coe (Tms= pΓ pΔ) (π₁ (δ , a)))) ◾ ⌜[]nT⌝)) ⁻¹̃
                              ◾̃ π₂β'
                              ◾̃ from≡ (Tm= pΓ ([]T= pΓ pΔ pA pδ ◾ ⌜[]nT⌝)) pa)))
                   (MethodsTm.[]Tᴹ= mTm (A' , pA) π₁β (MethodsHTms.π₁βᴹ mHTms))
               }
  ; lam[]ᴹ = λ {Γ}{Γᴹ}{Δ}{Δᴹ}{δ}{δᴹ}{A}{Aᴹ}{B}{Bᴹ}{t}{tᴹ} → lam[]ᴹ {δᴹ = δᴹ}{tᴹ = tᴹ}
  ; Πβᴹ    = λ { {Γ}{Γ' , pΓ}{A}{A' , pA}{B}{B' , pB}{t}{t' , pt}
                → =TmΓAᴹ (,C= pΓ pA) pB Πβ
                         (inj⌜⌝nf ( complt (coe (Tm= (,C= pΓ pA) pB) (app (lam t))) ⁻¹
                                  ◾ from≃ ( uncoe (Tm= (,C= pΓ pA) pB) ⁻¹̃
                                          ◾̃ to≃ Πβ
                                          ◾̃ from≡ (Tm= (proj₂ ((mCon MethodsCon.,Cᴹ Γ' , pΓ) (A' , pA))) pB) pt)))
                }
  ; Πηᴹ    = λ { {Γ}{Γ' , pΓ}{A}{A' , pA}{B}{B' , pB}{t}{t' , pt}
                →  =TmΓAᴹ pΓ (Π= pΓ pA pB) Πη
                          ( inj⌜⌝nf (complt (coe (Tm= pΓ (Π= pΓ pA pB)) (lam (app t))) ⁻¹
                          ◾ from≃ ( uncoe (Tm= pΓ (Π= pΓ pA pB)) ⁻¹̃
                                  ◾̃ to≃ Πη
                                  ◾̃ from≡ (Tm= pΓ (proj₂ (MethodsTy.Πᴹ mTy {Γ}{Γ' , pΓ}(A' , pA)(B' , pB)))) pt)))
                }
  }
