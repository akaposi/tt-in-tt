{-# OPTIONS --no-eta #-}

open import lib hiding (_,_)
open import TT.Syntax
open import NBE.Nf

module NBE.experiments.NormalModel.Tms
  (norms  : ∀{Γ Δ} → Tms Γ Δ → Nfs Γ Δ)
  (compls : ∀{Γ Δ}(σ : Tms Γ Δ) → σ ≡ ⌜ norms σ ⌝nfs)
  (normt  : ∀{Γ A} → Tm Γ A → Nf Γ A)
  (complt : ∀{Γ A}(t : Tm Γ A) → t ≡ ⌜ normt t ⌝nf)
  (stabs  : ∀{Γ Δ}(τ : Nfs Γ Δ) → norms ⌜ τ ⌝nfs ≡ τ)
  (stabt  : ∀{Γ A}(n : Nf Γ A) → normt ⌜ n ⌝nf ≡ n)
  where

open import TT.Congr
open import TT.Laws
open import TT.Elim
open import NBE.experiments.NConTy
open import NBE.experiments.NormalModel.Motives
open import NBE.experiments.NormalModel.Cxt
open import NBE.experiments.NormalModel.Ty norms compls normt complt stabs stabt

mTms : MethodsTms M mCon mTy
mTms = record
  { εᴹ     = λ { {Γ}{Γ' Σ., pΓ}
               →   norms  (coe (Tms= pΓ refl) ε)
               Σ., compls (coe (Tms= pΓ refl) ε)
               }
  ; _,sᴹ_  = λ { {Γ}{Γ' Σ., pΓ}{Δ}{Δ' Σ., pΔ}{σ}(σ' Σ., pσ){A}{A' Σ., pA}{t}(t' Σ., pt)
               →   norms  (coe (Tms= pΓ (,C= pΔ pA)) (σ ,s t))
               Σ., compls (coe (Tms= pΓ (,C= pΔ pA)) (σ ,s t))
               }
  ; idᴹ    = λ { {Γ}{Γ' Σ., pΓ}
               →   norms  (coe (Tms= pΓ pΓ) id)
               Σ., compls (coe (Tms= pΓ pΓ) id)
               }
  ; _∘ᴹ_   = λ { {Γ}{Γ' Σ., pΓ}{Δ}{Δ' Σ., pΔ}{Θ}{Θ' Σ., pΘ}{σ}(σ' Σ., pσ){ν}(ν' Σ., pν)
               →   norms  (coe (Tms= pΓ pΘ) (σ ∘ ν))
               Σ., compls (coe (Tms= pΓ pΘ) (σ ∘ ν))
               }
  ; π₁ᴹ    = λ { {Γ}{Γ' Σ., pΓ}{Δ}{Δ' Σ., pΔ}{A}{A' Σ., pA}{σ}(σ' Σ., pσ)
               →   norms  (coe (Tms= pΓ pΔ) (π₁ σ))
               Σ., compls (coe (Tms= pΓ pΔ) (π₁ σ))
               }
  }
