{-# OPTIONS --no-eta #-}

open import lib hiding (_,_)
open import TT.Syntax
open import NBE.Nf

module NBE.experiments.NormalModel.Motives where

open import TT.Elim
open import TT.Congr
open import NBE.experiments.NConTy

M : Motives
M = record

  { Conᴹ = λ Γ → Σ NCon λ Γ' → Γ ≡ ⌜ Γ' ⌝C
  
  ; Tyᴹ  = λ { {Γ}(Γ' Σ., pΓ) A
  
             → Σ (NTy Γ') λ A' → A ≡[ Ty= pΓ ]≡ ⌜ A' ⌝T

             }
  
  ; Tmsᴹ = λ { {Γ}{Δ}(Γ' Σ., pΓ)(Δ' Σ., pΔ) σ

             → Σ (Nfs ⌜ Γ' ⌝C ⌜ Δ' ⌝C) λ σ' → σ ≡[ Tms= pΓ pΔ ]≡ ⌜ σ' ⌝nfs
             
             }
  
  ; Tmᴹ  = λ { {Γ}(Γ' Σ., pΓ){A}(A' Σ., pA) t

             → Σ (Nf ⌜ Γ' ⌝C ⌜ A' ⌝T) λ t' → t ≡[ Tm= pΓ pA ]≡ ⌜ t' ⌝nf
             
             }
  
  }

open Motives M public

=Tyᴹ : {Γ : Con}{Γ' : NCon}(pΓ : Γ ≡ ⌜ Γ' ⌝C)
       {A₀ : Ty Γ}{A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
       {A'₀ : NTy Γ'}{A'₁ : NTy Γ'}(A'₂ : A'₀ ≡ A'₁)
       {pA₀ : A₀ ≡[ Ty= pΓ ]≡ ⌜ A'₀ ⌝T}{pA₁ : A₁ ≡[ Ty= pΓ ]≡ ⌜ A'₁ ⌝T}
     → (A'₀ Σ., pA₀) ≡[ TyΓᴹ= {Γ}{Γ' Σ., pΓ} A₂ ]≡ (A'₁ Σ., pA₁)
=Tyᴹ refl refl refl {refl} {refl} = refl

=Tmsᴹ : {Γ : Con}{Γ' : NCon}(pΓ : Γ ≡ ⌜ Γ' ⌝C)
        {Δ : Con}{Δ' : NCon}(pΔ : Δ ≡ ⌜ Δ' ⌝C)
        {σ₀ σ₁ : Tms Γ Δ}(σ₂ : σ₀ ≡ σ₁)
        {σ'₀ σ'₁ : Nfs ⌜ Γ' ⌝C ⌜ Δ' ⌝C}(σ'₂ : σ'₀ ≡ σ'₁)
        {pσ₀ : σ₀ ≡[ Tms= pΓ pΔ ]≡ ⌜ σ'₀ ⌝nfs}{pσ₁ : σ₁ ≡[ Tms= pΓ pΔ ]≡ ⌜ σ'₁ ⌝nfs}
      → (σ'₀ Σ., pσ₀) ≡[ TmsΓΔᴹ= {Γ}{Γ' Σ., pΓ}{Δ}{Δ' Σ., pΔ} σ₂ ]≡ (σ'₁ Σ., pσ₁)
=Tmsᴹ refl refl refl refl {refl}{refl} = refl

=Tmᴹ : {Γ : Con}{Γ' : NCon}(pΓ : Γ ≡ ⌜ Γ' ⌝C)
       {A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
       {A'₀ A'₁ : NTy Γ'}(A'₂ : A'₀ ≡ A'₁)
       (pA₀ : A₀ ≡[ Ty= pΓ ]≡ ⌜ A'₀ ⌝T)
       (pA₁ : A₁ ≡[ Ty= pΓ ]≡ ⌜ A'₁ ⌝T)
       {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≡[ TmΓ= A₂ ]≡ t₁)
       {t'₀ : Nf ⌜ Γ' ⌝C ⌜ A'₀ ⌝T}{t'₁  : Nf ⌜ Γ' ⌝C ⌜ A'₁ ⌝T}
       (t'₂ : t'₀ ≡[ NfΓ= (ap ⌜_⌝T A'₂) ]≡ t'₁)
       {pt₀ : t₀ ≡[ Tm= pΓ pA₀ ]≡ ⌜ t'₀ ⌝nf}{pt₁ : t₁ ≡[ Tm= pΓ pA₁ ]≡ ⌜ t'₁ ⌝nf}
     → (t'₀ Σ., pt₀) ≡[ TmΓᴹ= A₂ (=Tyᴹ pΓ A₂ A'₂) t₂ ]≡ (t'₁ Σ., pt₁)
=Tmᴹ refl refl refl refl refl refl refl {refl}{refl} = refl

=Tmᴹ' : {Γ : Con}{Γ' : NCon}(pΓ : Γ ≡ ⌜ Γ' ⌝C)
        {A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁)
        {A'₀ A'₁ : NTy Γ'}(A'₂ : A'₀ ≡ A'₁)
        (pA₀ : A₀ ≡[ Ty= pΓ ]≡ ⌜ A'₀ ⌝T)
        (pA₁ : A₁ ≡[ Ty= pΓ ]≡ ⌜ A'₁ ⌝T)
        {t₀ : Tm Γ A₀}{t₁ : Tm Γ A₁}(t₂ : t₀ ≡[ TmΓ= A₂ ]≡ t₁)
        {t'₀ : Nf ⌜ Γ' ⌝C ⌜ A'₀ ⌝T}{t'₁  : Nf ⌜ Γ' ⌝C ⌜ A'₁ ⌝T}
        (t'₂ : t'₀ ≡[ NfΓ= (ap ⌜_⌝T A'₂) ]≡ t'₁)
        {pt₀ : t₀ ≡[ Tm= pΓ pA₀ ]≡ ⌜ t'₀ ⌝nf}{pt₁ : t₁ ≡[ Tm= pΓ pA₁ ]≡ ⌜ t'₁ ⌝nf}
        (ApA₂ : A'₀ Σ., pA₀ ≡[ Motives.TyΓᴹ= M {Γ}{Γ' Σ., pΓ} A₂ ]≡ A'₁ Σ., pA₁)
      → (t'₀ Σ., pt₀) ≡[ TmΓᴹ= {Γ}{Γ' Σ., pΓ} A₂ ApA₂ t₂ ]≡ (t'₁ Σ., pt₁)
=Tmᴹ' refl refl refl refl refl refl refl {refl}{refl} refl = refl

=TmΓAᴹ : {Γ : Con}{Γ' : NCon}(pΓ : Γ ≡ ⌜ Γ' ⌝C)
         {A : Ty Γ}{A' : NTy Γ'}
         (pA : A ≡[ Ty= pΓ ]≡ ⌜ A' ⌝T)
         {t₀ t₁ : Tm Γ A}(t₂ : t₀ ≡ t₁)
         {t'₀ t'₁ : Nf ⌜ Γ' ⌝C ⌜ A' ⌝T}(t'₂ : t'₀ ≡ t'₁)
         {pt₀ : t₀ ≡[ Tm= pΓ pA ]≡ ⌜ t'₀ ⌝nf}{pt₁ : t₁ ≡[ Tm= pΓ pA ]≡ ⌜ t'₁ ⌝nf}
       → (t'₀ Σ., pt₀) ≡[ TmΓAᴹ= {Γ}{Γ' Σ., pΓ}{A}{A' Σ., pA} t₂ ]≡ (t'₁ Σ., pt₁)
=TmΓAᴹ refl refl refl refl {refl}{refl} = refl
