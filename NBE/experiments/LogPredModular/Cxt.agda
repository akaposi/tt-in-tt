{-# OPTIONS --no-eta #-}

module NBE.LogPred.Cxt where

open import lib
open import JM.JM
open import TT.Syntax hiding (_$_)
open import TT.Elim
open import TT.Congr
open import TT.Laws
open import Cats
open import NBE.TM
open import NBE.Renamings
open import NBE.LogPred.Motives

open Motives M

module m,Cᴹ
  {Γ : Con}(⟦Γ⟧ : Conᴹ Γ)
  {A : Ty Γ}(⟦A⟧ : Tyᴹ ⟦Γ⟧ A)
  where

    abstract
      pcoe : ∀{Ψ Ω}{β : Vars Ω Ψ}{ρ : TMC (Γ , A) $P Ψ}
             (α : Σ (⟦Γ⟧ $F π₁ ρ) (λ α₁ → ⟦A⟧ $F (π₁ ρ , π₂ ρ , α₁)))
           → (TMC Γ ,P TMT A ,P ⟦Γ⟧ [ wkn ]F) $P β $ (π₁ ρ , π₂ ρ , proj₁ α)
           ≡ π₁ (TMC (Γ , A) $P β $ ρ) , π₂ (TMC (Γ , A) $P β $ ρ) , coe ($F= ⟦Γ⟧ π₁∘) (⟦Γ⟧ $F β $ proj₁ α)
      pcoe α = Tbase= ⟦Γ⟧ π₁∘
                          (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ π₂[]')
                          (uncoe ($F= ⟦Γ⟧ π₁∘))

    abstract
      idF,Cᴹ : ∀{Ψ}{α : TMC (Γ , A) $P Ψ}
               {x : Σ (⟦Γ⟧ $F π₁ α) (λ α₁ → ⟦A⟧ $F (π₁ α , π₂ α , α₁))}
             →   coe ($F= ⟦Γ⟧ π₁∘) (⟦Γ⟧ $F idV $ proj₁ x)
               , coe ($F= ⟦A⟧ (pcoe x)) (⟦A⟧ $F idV $ proj₂ x)
               ≡[ ap (λ ρ → Σ (⟦Γ⟧ $F π₁ ρ) (λ α₁ → ⟦A⟧ $F (π₁ ρ , π₂ ρ , α₁))) (idP (TMC (Γ , A))) ]≡
               x
      idF,Cᴹ {x = x}
      
        = from≃ ( uncoe (ap (λ ρ → Σ (⟦Γ⟧ $F π₁ ρ) (λ α₁ → ⟦A⟧ $F (π₁ ρ , π₂ ρ , α₁))) (idP (TMC (Γ , A)))) ⁻¹̃
                ◾̃ ,≃ (funext≃ ($F= ⟦Γ⟧ (ap π₁ (ap (_∘_ _) ⌜idV⌝ ◾ idr)))
                              (λ x₂ → to≃ ($F= ⟦A⟧ (Tbase= ⟦Γ⟧
                                                           (ap π₁ (ap (_∘_ _) ⌜idV⌝ ◾ idr))
                                                           (π₂≃ (ap (_∘_ _) ⌜idV⌝ ◾ idr))
                                                           x₂))))
                     (uncoe ($F= ⟦Γ⟧ π₁∘) ⁻¹̃ ◾̃ idF' ⟦Γ⟧)
                     (uncoe ($F= ⟦A⟧ (pcoe x)) ⁻¹̃ ◾̃ idF' ⟦A⟧))

    abstract
      compF,Cᴹ : {I : Con} {J : Con} {K : Con} {f : Vars J I}
                 {g : Vars K J} {α : TMC (Γ , A) $P I}
                 {x : Σ (⟦Γ⟧ $F π₁ α) (λ α₁ → ⟦A⟧ $F (π₁ α , π₂ α , α₁))} →
                 coe ($F= ⟦Γ⟧ π₁∘) (⟦Γ⟧ $F f ∘V g $ proj₁ x) ,
                 coe ($F= ⟦A⟧ (pcoe x)) (⟦A⟧ $F f ∘V g $ proj₂ x)
                 ≡[
                 ap (λ ρ → Σ (⟦Γ⟧ $F π₁ ρ) (λ α₁ → ⟦A⟧ $F (π₁ ρ , π₂ ρ , α₁)))
                 (compP (TMC (Γ , A)))
                 ]≡
                 coe ($F= ⟦Γ⟧ π₁∘)
                 (⟦Γ⟧ $F g $ coe ($F= ⟦Γ⟧ π₁∘) (⟦Γ⟧ $F f $ proj₁ x))
                 ,
                 coe
                 ($F= ⟦A⟧
                  (pcoe
                   (coe ($F= ⟦Γ⟧ π₁∘) (⟦Γ⟧ $F f $ proj₁ x) ,
                    coe ($F= ⟦A⟧ (pcoe x)) (⟦A⟧ $F f $ proj₂ x))))
                 (⟦A⟧ $F g $ coe ($F= ⟦A⟧ (pcoe x)) (⟦A⟧ $F f $ proj₂ x))
      compF,Cᴹ {f = f}{x = x}
      
        = from≃ ( uncoe (ap (λ ρ → Σ (⟦Γ⟧ $F π₁ ρ) (λ α₁ → ⟦A⟧ $F (π₁ ρ , π₂ ρ , α₁))) (compP (TMC (Γ , A)))) ⁻¹̃
                ◾̃ ,≃ (funext≃ ($F= ⟦Γ⟧ (ap π₁ (ap (_∘_ _) (⌜∘V⌝ ⁻¹) ◾ ass ⁻¹)))
                              (λ x₂ → to≃ ($F= ⟦A⟧ (Tbase= ⟦Γ⟧ (ap π₁ (compP (TMC (Γ , A)))) (π₂≃ (compP (TMC (Γ , A)))) x₂))))
                     ( uncoe ($F= ⟦Γ⟧ π₁∘) ⁻¹̃
                     ◾̃ compF' ⟦Γ⟧
                     ◾̃ $Ff$≃ ⟦Γ⟧ π₁∘ (uncoe ($F= ⟦Γ⟧ π₁∘))
                     ◾̃ uncoe ($F= ⟦Γ⟧ π₁∘))
                     ( uncoe ($F= ⟦A⟧ (pcoe x)) ⁻¹̃
                     ◾̃ compF' ⟦A⟧
                     ◾̃ $Ff$≃ ⟦A⟧
                            (Tbase= ⟦Γ⟧ π₁∘ (uncoe (TmΓ= [][]T) ⁻¹̃ ◾̃ π₂[]') (uncoe ($F= ⟦Γ⟧ π₁∘)))
                            (uncoe ($F= ⟦A⟧ (pcoe x)))
                     ◾̃ uncoe ($F= ⟦A⟧ (pcoe (coe ($F= ⟦Γ⟧ π₁∘) (⟦Γ⟧ $F f $ proj₁ x) , coe ($F= ⟦A⟧ (pcoe x)) (⟦A⟧ $F f $ proj₂ x))))))

    _,Cᴹ_ : Conᴹ (Γ , A)
    _,Cᴹ_ = record
      { _$F_   = λ {Δ} ρ → Σ (⟦Γ⟧ $F π₁ ρ) λ α → ⟦A⟧ $F (π₁ ρ , π₂ ρ , α)
      ; _$F_$_ = λ {Ψ Ω} β {ρ} α
                 → coe ($F= ⟦Γ⟧ π₁∘) (⟦Γ⟧ $F β $ proj₁ α)
                 , coe ($F= ⟦A⟧ (pcoe α)) (⟦A⟧ $F β $ proj₂ α)
      ; idF    = idF,Cᴹ
      ; compF  = compF,Cᴹ
      }

mCon : MethodsCon M
mCon = record

  { •ᴹ = record
      { _$F_   = λ _ → ⊤
      ; _$F_$_ = λ _ _ → tt
      ; idF    = refl
      ; compF  = refl
      }

  ; _,Cᴹ_ = m,Cᴹ._,Cᴹ_

  }
