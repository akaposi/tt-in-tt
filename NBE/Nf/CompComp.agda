{-# OPTIONS --no-eta #-}

module NBE.Nf.CompComp where

open import lib
open import JM
open import TT.Syntax
open import TT.Congr
open import TT.Laws
open import NBE.Renamings
open import NBE.Nf.Nf
open import NBE.Nf.Rename
open import NBE.Nf.IdComp

[][]ne : ∀{Γ Δ Θ A}{n : Ne Θ A}{β : Vars Δ Θ}{γ : Vars Γ Δ} → n [ β ]ne [ γ ]ne ≃ n [ β ∘V γ ]ne
[][]nf : ∀{Γ Δ Θ A}{v : Nf Θ A}{β : Vars Δ Θ}{γ : Vars Γ Δ} → v [ β ]nf [ γ ]nf ≃ v [ β ∘V γ ]nf

abstract
  [][]ne {n = var x}{β}{γ} = var≃ ([⌜∘V⌝]T {β = β}{γ = γ}) ([][]v {x = x}{β = γ}{γ = β})
  [][]ne {n = appNe n v}{β}{γ}

    = coe[]ne' ([][]T ◾ ap (_[_]T _) _ ◾ [][]T ⁻¹)
    ◾̃ uncoe (NeΓ= ([][]T ◾ ap (_[_]T _) _ ◾ [][]T ⁻¹)) ⁻¹̃
    ◾̃ appNe≃ ([⌜∘V⌝]T {β = β}{γ = γ})
             (to≃ [][]T
             ◾̃ []T≃ (,C≃ refl (to≃ ([⌜∘V⌝]T {β = β}{γ = γ})))
                    ( to≃ (ap (_∘_ (⌜ β ⌝V ^ _)) (⌜^⌝ {β = γ}) ⁻¹)
                    ◾̃ to≃ (ap (λ z → z ∘ ⌜ γ ^V (_ [ ⌜ β ⌝V ]T) ⌝V) (⌜^⌝ {β = β}) ⁻¹)
                    ◾̃ to≃ (⌜∘V⌝ (β ^V _))
                    ◾̃ ⌜⌝V≃ (,C≃ refl (to≃ ([⌜∘V⌝]T {β = β}{γ = γ}))) refl (∘V^ {β = β}{γ = γ})
                    ◾̃ to≃ (⌜^⌝ {β = β ∘V γ})))
             ( uncoe (NeΓ= Π[]) ⁻¹̃
             ◾̃ coe[]ne' Π[]
             ◾̃ [][]ne {n = n}
             ◾̃ uncoe (NeΓ= Π[]))
             ([][]nf {v = v})
    ◾̃ uncoe (NeΓ= ([][]T ◾ ap (_[_]T _) _ ◾ [][]T ⁻¹))

abstract
  [][]nf {v = neuU n}
    = coe[]nf' (U[] ⁻¹)
    ◾̃ uncoe (NfΓ= (U[] ⁻¹)) ⁻¹̃
    ◾̃ to≃ (neuU=' (from≃ ( uncoe (NeΓ= U[]) ⁻¹̃
                         ◾̃ coe[]ne' U[]
                         ◾̃ [][]ne {n = n}
                         ◾̃ uncoe (NeΓ= U[]))))
    ◾̃ uncoe (NfΓ= (U[] ⁻¹))

  [][]nf {v = neuEl n}{β}
    = coe[]nf' (El[] ⁻¹)
    ◾̃ uncoe (NfΓ= (El[] ⁻¹)) ⁻¹̃
    ◾̃ neuEl≃ (from≃ ( uncoe (TmΓ= U[]) ⁻¹̃
                    ◾̃ coe[]t' U[]
                    ◾̃ [][]t'
                    ◾̃ []t≃'' (⌜∘V⌝ β)
                    ◾̃ uncoe (TmΓ= U[])))
             (uncoe (NeΓ= El[]) ⁻¹̃
             ◾̃ coe[]ne' El[]
             ◾̃ [][]ne {n = n}
             ◾̃ uncoe (NeΓ= El[]))
    ◾̃ uncoe (NfΓ= (El[] ⁻¹))
    
  [][]nf {v = lamNf v}{β}{γ}

    = coe[]nf' (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹)
    ◾̃ uncoe (NfΓ= (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹)) ⁻¹̃
    ◾̃ lamNf≃
         ([⌜∘V⌝]T {β = β}{γ = γ})
         ( to≃ [][]T
         ◾̃ []T≃ (,C≃ refl (to≃ ([⌜∘V⌝]T {β = β}{γ = γ})))
                ( to≃ (⌜∘V⌝ (β ^V _){γ ^V _ [ ⌜ β ⌝V ]T})
                ◾̃ ⌜⌝V≃ (,C≃ refl (to≃ ([⌜∘V⌝]T {β = β}{γ = γ})))
                       refl
                       {(β ^V _) ∘V (γ ^V _ [ ⌜ β ⌝V ]T)}
                       {β ∘V γ ^V _}
                       (∘V^ {β = β}{γ = γ})))
         {v [ β ^V _ ]nf [ γ ^V _ [ ⌜ β ⌝V ]T ]nf}{v [ β ∘V γ ^V _ ]nf}
         ( [][]nf {v = v}{β = β ^V _}{γ = γ ^V _ [ ⌜ β ⌝V ]T}
         ◾̃ []nf≃ (,C≃ refl (to≃ ([⌜∘V⌝]T {β = β}{γ = γ}))) {v = v} (∘V^ {β = β}{γ = γ}))
    ◾̃ uncoe (NfΓ= (Π≃' refl (to≃ (ap (_[_]T _) ⌜^⌝)) ◾ Π[] ⁻¹))
