{-# OPTIONS --rewriting #-}

module TTR where

open import lib using (_≡_; refl; _⁻¹; _◾_; ap; apd; _≡⟨_⟩_; _∎; apd''; transport)
open import JM  using (loopcoe)

-- substitution calculus

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixl 5 _$_
infixl 7 _‚_

postulate
  Con   : Set
  Ty    : Con → Set
  Tms   : Con → Con → Set
  Tm    : (Γ : Con) → Ty Γ → Set

  •     : Con  -- \bub
  _▷_   : (Γ : Con) → Ty Γ → Con
  
  _[_]T : ∀{Γ Δ} → Ty Δ → Tms Γ Δ → Ty Γ

  id    : ∀{Γ} → Tms Γ Γ
  _∘_   : ∀{Γ Δ Σ} → Tms Δ Σ → Tms Γ Δ → Tms Γ Σ
  ε     : ∀{Γ} → Tms Γ •
  _,_   : ∀{Γ Δ}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
  π₁    : ∀{Γ Δ}{A : Ty Δ} → Tms Γ (Δ ▷ A) →  Tms Γ Δ

  _[_]t : ∀{Γ Δ}{A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T) 
  π₂    : ∀{Γ Δ}{A : Ty Δ}(σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ σ ]T)

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}
        → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)

{-# REWRITE [id]T #-}
{-# REWRITE [][]T #-}

postulate
  idl   : ∀{Γ Δ}{δ : Tms Γ Δ} → (id ∘ δ) ≡ δ 
  idr   : ∀{Γ Δ}{δ : Tms Γ Δ} → (δ ∘ id) ≡ δ 
  ass   : ∀{Γ Δ Σ Ω}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ}
        → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
  ▷β₁   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
        → (π₁ {A = A} (δ , a)) ≡ δ

{-# REWRITE idl #-}
{-# REWRITE idr #-}
{-# REWRITE ass #-}
{-# REWRITE ▷β₁ #-}

postulate
  ▷β₂   : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ Δ}{a : Tm Γ (A [ δ ]T)}
        → π₂ {A = A} (δ , a) ≡ a
  ▷η    : ∀{Γ Δ}{A : Ty Δ}{δ : Tms Γ (Δ ▷ A)}
        → (π₁ δ , π₂ δ) ≡ δ
  ,∘    : ∀{Γ Δ Σ}{δ : Tms Γ Δ}{σ : Tms Σ Γ}{A : Ty Δ}{a : Tm Γ (A [ δ ]T)}
        → ((_,_ δ {A} a) ∘ σ) ≡ (δ ∘ σ) , (a [ σ ]t)
  •ηid  : id {•} ≡ ε {•}
  •η∘   : {Γ Δ : Con}{σ : Tms Γ Δ} → ε ∘ σ ≡ ε
  •ηπ₁  : {Γ : Con}{A : Ty •}{σ : Tms Γ (• ▷ A)} → π₁ σ ≡ ε

{-# REWRITE ▷η #-}
{-# REWRITE ▷β₂ #-}
{-# REWRITE ,∘ #-}
{-# REWRITE •ηid #-}
{-# REWRITE •η∘ #-}
{-# REWRITE •ηπ₁ #-}

-- TODO: prove these

postulate
  [id]t : {Γ : Con}{A : Ty Γ}{t : Tm Γ A} → t [ id ]t ≡ t
  [][]t : {Γ Δ Σ : Con}{A : Ty Σ}{σ : Tms Γ Δ}{δ : Tms Δ Σ}{A : Ty Σ}{t : Tm Σ A} → t [ δ ]t [ σ ]t ≡ t [ δ ∘ σ ]t

{-# REWRITE [id]t #-}
{-# REWRITE [][]t #-}

-- abbreviations

wk : ∀{Γ}{A : Ty Γ} → Tms (Γ ▷ A) Γ
wk = π₁ id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▷ A) (A [ wk ]T)
vz = π₂ id

vs : ∀{Γ}{A B : Ty Γ} → Tm Γ A → Tm (Γ ▷ B) (A [ wk ]T) 
vs x = x [ wk ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Tms Γ (Γ ▷ A)
< t > = id , t

_^_ : {Γ Δ : Con}(σ : Tms Γ Δ)(A : Ty Δ) → Tms (Γ ▷ A [ σ ]T) (Δ ▷ A)
σ ^ A = (σ ∘ wk) , vz

π₁∘ : {Γ Θ Δ : Con}{A : Ty Δ}{σ : Tms Θ (Δ ▷ A)}{ν : Tms Γ Θ} →
  π₁ σ ∘ ν ≡ π₁ (σ ∘ ν)
π₁∘ {Γ}{Θ}{Δ}{A}{σ}{ν} = ap π₁ (,∘ {δ = π₁ σ}{ν}{A}{π₂ σ}) ⁻¹

{-# REWRITE π₁∘ #-}

π₂[] : {Γ Θ Δ : Con}{A : Ty Δ}{σ : Tms Θ (Δ ▷ A)}{ν : Tms Γ Θ} →
  π₂ σ [ ν ]t ≡ π₂ (σ ∘ ν)
π₂[] {Γ}{Θ}{Δ}{A}{σ}{ν} =
  apd π₂ (,∘ {δ = π₁ σ}{ν}{A}{π₂ σ}) ⁻¹ ◾
  loopcoe (ap (λ z → Tm Γ (A [ π₁ z ]T)) (,∘ {δ = π₁ σ}{ν}{A}{π₂ σ}))

{-# REWRITE π₂[] #-}

ε,vz : {Γ : Con}{A : Ty •} → _,_ {• ▷ A}{•} ε {A} vz ≡ id
ε,vz {Γ}{A} = ▷η

{-# REWRITE ε,vz #-}

vz[<>] : {Γ Δ : Con}{A : Ty Δ}{σ : Tms Γ Δ}{u : Tm Γ (A [ σ ]T)} → vz [ < u > ]t ≡ u
vz[<>] {Γ}{Δ}{A}{σ}{u} = π₂[] {σ = id}{id , u}

{-# REWRITE vz[<>] #-}

vz[_∘wk,vz] : {Ψ Ω : Con}{B : Ty Ω}{τ : Tms Ψ Ω} →
  π₂ {Ω ▷ B} id [ τ ∘ wk , π₂ id ]t ≡ π₂ (id {Ψ ▷ B [ τ ]T})
vz[_∘wk,vz] {B}{τ} = refl

-- universe

postulate
  U : {Γ : Con} → Ty Γ
  El : {Γ : Con} → Tm Γ U → Ty Γ
  U[] : {Γ Δ : Con}{σ : Tms Γ Δ} → U [ σ ]T ≡ U

{-# REWRITE U[] #-}

postulate
  El[] : {Γ Δ : Con}{σ : Tms Γ Δ}{a : Tm Δ U} → (El a) [ σ ]T ≡ El (a [ σ ]t)

{-# REWRITE El[] #-}

-- unit type

postulate
  ⊤ : {Γ : Con} → Ty Γ
  ⊤[] : {Γ Δ : Con}{σ : Tms Γ Δ} → ⊤ [ σ ]T ≡ ⊤

{-# REWRITE ⊤[] #-}

postulate
  tt : {Γ : Con} → Tm Γ ⊤
  tt[] : {Γ Δ : Con}{σ : Tms Γ Δ} → tt [ σ ]t ≡ tt

{-# REWRITE tt[] #-}

-- Pi

postulate
  Π : ∀{Γ}(A : Ty Γ)(B : Ty (Γ ▷ A)) → Ty Γ

  Π[] : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▷ A)}
      → ((Π A B) [ σ ]T) ≡ (Π (A [ σ ]T) (B [ σ ^ A ]T))

{-# REWRITE Π[] #-}

postulate
  lam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
  app : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm Γ (Π A B) → Tm (Γ ▷ A) B
  Πβ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{t : Tm (Γ ▷ A) B}
        → (app (lam t)) ≡ t
  Πη    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{t : Tm Γ (Π A B)}
        → (lam (app t)) ≡ t
  lam[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▷ A)}{t : Tm (Δ ▷ A) B}
        → ((lam t) [ δ ]t) ≡ (lam (t [ δ ^ A ]t))

{-# REWRITE Πβ #-}
{-# REWRITE Πη #-}
{-# REWRITE lam[] #-}

_$_ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)}(t : Tm Γ (Π A B))(u : Tm Γ A) → Tm Γ (B [ < u > ]T)
t $ u = (app t) [ < u > ]t

_⇒_ : {Γ : Con}(A B : Ty Γ) → Ty Γ
A ⇒ B = Π A (B [ wk ]T)

app[] : ∀{Γ Δ}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▷ A)}{t : Tm Δ (Π A B)} →
  ((app t) [ δ ^ A ]t) ≡ (app (t [ δ ]t))
app[] {Γ}{Δ}{δ}{A}{B}{t} = ap app (lam[] {Γ}{Δ}{δ}{A}{B}{app t} ⁻¹)

{-# REWRITE app[] #-}

-- Sigma

postulate
  Σ : ∀{Γ}(A : Ty Γ)(B : Ty (Γ ▷ A)) → Ty Γ
  Σ[]    : ∀{Γ Δ}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ ▷ A)}
         → ((Σ A B) [ σ ]T) ≡ (Σ (A [ σ ]T) (B [ σ ^ A ]T))

{-# REWRITE Σ[] #-}

postulate
  _‚_   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}(u : Tm Γ A)(v : Tm Γ (B [ < u > ]T)) → Tm Γ (Σ A B) -- \glq
  proj₁ : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}(w : Tm Γ (Σ A B)) → Tm Γ A
  proj₂ : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}(w : Tm Γ (Σ A B)) → Tm Γ (B [ < proj₁ w > ]T)
  Σβ₁   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{u : Tm Γ A}{v : Tm Γ (B [ < u > ]T)}
        → proj₁ {B = B}(u ‚ v) ≡ u

{-# REWRITE Σβ₁ #-}

postulate
  Σβ₂   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{u : Tm Γ A}{v : Tm Γ (B [ < u > ]T)}
        → proj₂ {B = B}(u ‚ v) ≡ v
  Ση    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{w : Tm Γ (Σ A B)}
        → (proj₁ w ‚ proj₂ w) ≡ w
  proj₁[] : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{w : Tm Γ (Σ A B)}{Θ}{σ : Tms Θ Γ} →
    (proj₁ w) [ σ ]t ≡ proj₁ (w [ σ ]t)

{-# REWRITE Σβ₂ #-}
{-# REWRITE Ση #-}
{-# REWRITE proj₁[] #-}

postulate
  proj₂[] : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{w : Tm Γ (Σ A B)}{Θ}{σ : Tms Θ Γ} →
    (proj₂ w) [ σ ]t ≡ proj₂ (w [ σ ]t)


  ‚[] : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▷ A)}{u : Tm Γ A}{v : Tm Γ (B [ < u > ]T)}{Θ}{σ : Tms Θ Γ} →
    (_‚_ {B = B} u v) [ σ ]t ≡ (u [ σ ]t ‚ v [ σ ]t)

