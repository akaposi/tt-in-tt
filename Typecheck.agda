{-# OPTIONS --rewriting #-}

open import lib
open import TT.Syntax
open import Nf.Dec using (Dec; yes; no)

module Typecheck
  (decTy : {Γ : Con}(A B : Ty Γ) → Dec (A ≡ B))
  (decΠ  : {Γ : Con}(C : Ty Γ) → Dec (Σ (Ty Γ) λ A → Σ (Ty (Γ , A)) λ B → C ≡ Π A B))
  where

-- well-scoped presyntax with annotated terms

data PTy (n : ℕ) : Set
data PTm (n : ℕ) : Set

data PTm n where
  pvar : Fin n → PTm n
  plam : PTm (suc n) → PTm n
  papp : PTm n → PTm n → PTm n
  pann : PTm n → PTy n → PTm n

data PTy n where
  pU   : PTy n
  pEl  : PTm n → PTy n
  pΠ   : PTy n → PTy (suc n) → PTy n

data Maybe {i}(A : Set i) : Set i where
  Nothing : Maybe A
  Just : A → Maybe A

open import TT.ConElim

-- length of a context

len : Con → ℕ
len = RecCon' ℕ zero suc

-- lookup a variable from a context

lookup : (Γ : Con) → Fin (len Γ) → (Σ (Ty Γ) λ A → Tm Γ A)
lookup
  = ElimCon'
      (λ Γ → Fin (len Γ) → (Σ (Ty Γ) λ A → Tm Γ A))
      (λ ())
      (λ { {Γ} {A} _ zero    →                       A [ wk ]T ,Σ vz
         ; {Γ} {A} f (suc n) → let (A ,Σ t) = f n in A [ wk ]T ,Σ vs t })

-- bidirectional type checking

inferTm : (Γ : Con)(t : PTm (len Γ)) → Maybe (Σ (Ty Γ) λ A → Tm Γ A)
checkTm : (Γ : Con)(t : PTm (len Γ))(A : Ty Γ) → Maybe (Tm Γ A)
checkTy : (Γ : Con)(A : PTy (len Γ)) → Maybe (Ty Γ)

inferTm Γ (pvar x) = Just (lookup Γ x)

inferTm Γ (papp t u) with inferTm Γ t
inferTm Γ (papp t u) | Nothing        = Nothing
inferTm Γ (papp t u) | Just (C ,Σ t') with decΠ C
inferTm Γ (papp t u) | Just (C ,Σ t') | (yes (A ,Σ (B ,Σ refl))) with checkTm Γ u A
inferTm Γ (papp t u) | Just (C ,Σ t') | (yes (A ,Σ (B ,Σ refl))) | Nothing  = Nothing
inferTm Γ (papp t u) | Just (C ,Σ t') | (yes (A ,Σ (B ,Σ refl))) | (Just u') = Just (B [ < u' > ]T ,Σ (t' $ u'))
inferTm Γ (papp t u) | Just (C ,Σ t') | (no  _)                  = Nothing

inferTm Γ (pann t A) with checkTy Γ A
... | Nothing = Nothing
... | Just A' with checkTm Γ t A'
... | Nothing = Nothing
... | Just t' = Just (A' ,Σ t')

inferTm Γ (plam t) = Nothing

checkTm Γ (plam t) C with decΠ C
checkTm Γ (plam t) C | yes (A ,Σ (B ,Σ refl)) with checkTm (Γ , A) t B
checkTm Γ (plam t) C | yes (A ,Σ (B ,Σ refl)) | Nothing = Nothing
checkTm Γ (plam t) C | yes (A ,Σ (B ,Σ refl)) | (Just t') = Just (lam t')
checkTm Γ (plam t) C | no  _                  = Nothing

checkTm Γ (pvar x) A with inferTm Γ (pvar x)
... | Nothing = Nothing
... | Just (A' ,Σ t') with decTy A A'
... | (yes refl) = Just t'
... | (no  _)    = Nothing

checkTm Γ (papp t u) A with inferTm Γ (papp t u)
... | Nothing = Nothing
... | Just (A' ,Σ t') with decTy A A'
... | (yes refl) = Just t'
... | (no  _)    = Nothing

checkTm Γ (pann t B) A with inferTm Γ (pann t B)
... | Nothing = Nothing
... | Just (A' ,Σ t') with decTy A A'
... | (yes refl) = Just t'
... | (no  _)    = Nothing

checkTy Γ pU = Just U

checkTy Γ (pEl t) with checkTm Γ t U
... | Nothing = Nothing
... | Just t' = Just (El t')

checkTy Γ (pΠ A B) with checkTy Γ A
... | Nothing = Nothing
... | Just A' with checkTy (Γ , A') B
... | Nothing = Nothing
... | Just B' = Just (Π A' B')

-- typing relations

infixl 5 _p▷_
infixl 5 _w▷_

data PCon : ℕ → Set where 
  p• : PCon zero
  _p▷_ : {n : ℕ} → PCon n → PTy n → PCon (suc n)

data CheckCon : {n : ℕ}(pΓ : PCon n) → Set
data InferTm : {n : ℕ}(pΓ : PCon n)(pt : PTm n)(pA : PTy n) → Set
data CheckTm : {n : ℕ}(pΓ : PCon n)(pt : PTm n)(pA : PTy n) → Set
data CheckTy : {n : ℕ}(pΓ : PCon n)(pA : PTy n) → Set

data CheckCon where
  w• : CheckCon p•
  _w▷_ : {n : ℕ}{pΓ : PCon n} → CheckCon pΓ → {pA : PTy n} → CheckTy pΓ pA → CheckCon (pΓ p▷ pA)

data InferTm where
  wapp : {n : ℕ}{pΓ : PCon n}{pt : PTm n}{pu : PTm n}{pA : PTy n}{pB : PTy (suc n)} →
    InferTm pΓ pt (pΠ pA pB) → CheckTm pΓ pu pA → InferTm pΓ (papp pt pu) {!pB!}

data CheckTm where
  wlam : {n : ℕ}{pΓ : PCon n} → CheckCon pΓ → {pA : PTy n} → CheckTy pΓ pA → {pB : PTy (suc n)} →
    CheckTy (pΓ p▷ pA) pB → {pt : PTm (suc n)} → CheckTm (pΓ p▷ pA) pt pB → CheckTm pΓ (plam pt) (pΠ pA pB)

data CheckTy where
  wU  : {n : ℕ}{pΓ : PCon n} → CheckCon pΓ → CheckTy pΓ pU
  wEl : {n : ℕ}{pΓ : PCon n} → CheckCon pΓ → {pt : PTm n} → CheckTm pΓ pt pU → CheckTy pΓ (pEl pt)
  wΠ  : {n : ℕ}{pΓ : PCon n} → CheckCon pΓ → {pA : PTy n} → CheckTy pΓ pA → {pB : PTy (suc n)} →
    CheckTy (pΓ p▷ pA) pB → CheckTy pΓ (pΠ pA pB)

-- inferTm : (Γ : Con)(t : PTm (len Γ)) → Maybe (Σ (Ty Γ) λ A → Tm Γ A)
-- checkTm : (Γ : Con)(t : PTm (len Γ))(A : Ty Γ) → Maybe (Tm Γ A)
-- checkTy : (Γ : Con)(A : PTy (len Γ)) → Maybe (Ty Γ)

cCon : {n : ℕ}{pΓ : PCon n} → CheckCon pΓ → Σ Con λ Γ → n ≡ len Γ
cTy  : {n : ℕ}{pΓ : PCon n}{pA : PTy n} → CheckTy pΓ pA → Σ Con λ Γ →
  Σ (n ≡ len Γ) λ plen → Σ (Ty Γ) λ A → checkTy Γ (transport PTy plen pA) ≡ Just A

cCon w• = • ,Σ refl
cCon (wΓ w▷ wA) with cTy wA
cCon (wΓ w▷ wA) | Γ ,Σ (refl ,Σ (A ,Σ pjust)) = (Γ , A) ,Σ refl

cTy (wU wΓ) with cCon wΓ
... | (Γ ,Σ refl) = Γ ,Σ (refl ,Σ (U ,Σ refl))
cTy (wEl wΓ wt) = {!!}
cTy (wΠ wΓ wA wB) with cTy wA | cTy wB
... | (Γ ,Σ (refl ,Σ (A ,Σ pjustA))) | (ΓA ,Σ (aa ,Σ (B ,Σ pjustB))) = Γ ,Σ (refl ,Σ ({!Π A !} ,Σ {!!}))


{-
infix 3 _⊢T_
infix 3 _⊢t_∶_
infix 3 _⊢v_∶_

data PCon : ℕ → Set where
  p• : PCon zero -- \.
  _p▷_ : {n : ℕ} → PCon n → PTy n → PCon (suc n)

data ⊢C : {n : ℕ} → PCon n → Set
data _⊢T_ : {n : ℕ} → PCon n → PTy n → Set
data _⊢v_∶_ : {n : ℕ} → PCon n → Fin n → PTy n → Set
data _⊢t_∶_ : {n : ℕ} → PCon n → PTm n → PTy n → Set

data ⊢C where
  w• : ⊢C p•
  _w▷_ : {n : ℕ}{pΓ : PCon n} → ⊢C pΓ → {pA : PTy n} → pΓ ⊢T pA → ⊢C (pΓ p▷ pA)

data _⊢T_ where
  wU  : {n : ℕ}{pΓ : PCon n} → ⊢C pΓ → pΓ ⊢T pU
  wEl : {n : ℕ}{pΓ : PCon n} → ⊢C pΓ → {pa : PTm n} → pΓ ⊢t pa ∶ pU → pΓ ⊢T pEl pa
  wΠ  : {n : ℕ}{pΓ : PCon n} → ⊢C pΓ → {pA : PTy n} → pΓ ⊢T pA → {pB : PTy (suc n)} → pΓ p▷ pA ⊢T pB → pΓ ⊢T pΠ pA pB

data _⊢v_∶_ where
  wzero : {n : ℕ}{pΓ : PCon n} → ⊢C pΓ → {pA : PTy n} → pΓ ⊢T pA → (pΓ p▷ pA) ⊢v zero ∶ {!pA!}
  wsuc  : {n : ℕ}{pΓ : PCon n} → ⊢C pΓ → {pA : PTy n} → pΓ ⊢T pA → {x : Fin n} → pΓ ⊢v x ∶ pA →
    {pB : PTy n} → pΓ ⊢T pB → pΓ p▷ pB ⊢v suc x ∶ {!pA!}

data _⊢t_∶_ where
  wvar : {n : ℕ}{pΓ : PCon n} → ⊢C pΓ → {pA : PTy n} → pΓ ⊢T pA → {x : Fin n} → pΓ ⊢v x ∶ pA → pΓ ⊢t pvar x ∶ pA
  wlam : {n : ℕ}{pΓ : PCon n} → ⊢C pΓ → {pA : PTy n} → pΓ ⊢T pA → {pB : PTy (suc n)} → pΓ p▷ pA ⊢T pB →
    {pt : PTm (suc n)} → pΓ p▷ pA ⊢t pt ∶ pB → pΓ ⊢t plam pt ∶ pΠ pA pB
  wapp : {n : ℕ}{pΓ : PCon n} → ⊢C pΓ → {pA : PTy n} → pΓ ⊢T pA → {pB : PTy (suc n)} → pΓ p▷ pA ⊢T pB →
    {pt : PTm n} → pΓ ⊢t pt ∶ pΠ pA pB → {pu : PTm n} → pΓ ⊢t pu ∶ pA → pΓ ⊢t papp pt pu ∶ {!pB!}
  pann : {n : ℕ}{pΓ : PCon n} → ⊢C pΓ → {pA : PTy n} → pΓ ⊢T pA → {pt : PTm n} → pΓ ⊢t pt ∶ pA →
    pΓ ⊢t pann pt pA ∶ pA
-}
