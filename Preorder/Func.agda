{-# OPTIONS --prop #-}

module Preorder.Func where

open import lib
open import JM
open import Setoid.SProp.lib
open import Agda.Primitive

open import Preorder.Decl
open import Preorder.Core

Π : {Γ : Con}(A : Ty Γ)(B : Ty (Γ ▷ A)) → Ty Γ
Π {Γ} A B = record {
  ∣_∣T_ = λ γ → Σsp ((α : ∣ A ∣T γ) → ∣ B ∣T γ ,Σ α) λ f →
                    {α₀ α₁ : ∣ A ∣T γ}(α₀₁ : (A ~T) (refC Γ γ) α₀ α₁) → (B ~T) (refC Γ γ ,p α₀₁) (f α₀) (f α₁) ;
  _~T = λ { {γ₀}{γ₁} γ₀₁ (f₀ ,sp _) (f₁ ,sp _) → {α₀ : ∣ A ∣T γ₀}{α₁ : ∣ A ∣T γ₁}(α₀₁ : (A ~T) γ₀₁ α₀ α₁) → (B ~T) (γ₀₁ ,p α₀₁) (f₀ α₀) (f₁ α₁) } ;
  refT = proj₂sp ;
  transT = λ {γ₀}{γ₁}{γ₂}{γ₀₁}{γ₁₂}{f₀}{f₁}{f₂} f₀₁ f₁₂ {α₀}{α₂} α₀₂ → {!!} }
