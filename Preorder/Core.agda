{-# OPTIONS --prop #-}

module Preorder.Core where

open import lib
open import JM
open import Setoid.SProp.lib
open import Agda.Primitive

open import Preorder.Decl

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t

-- definitions

• : Con
• = record
  { ∣_∣C = ⊤
  ; _~C  = λ _ _ → ⊤p
  }

_▷_ : (Γ : Con) → Ty Γ → Con
Γ ▷ A = record
  { ∣_∣C   = Σ ∣ Γ ∣C ∣ A ∣T_
  ; _~C    = λ { (γ₀ ,Σ α₀)(γ₁ ,Σ α₁) → Σp ((Γ ~C) γ₀ γ₁) λ γ₀₁ → (A ~T) γ₀₁ α₀ α₁ }
  ; refC   = λ { (γ ,Σ α) → refC Γ γ ,p refT A α }
  ; transC = λ { (γ₀₁ ,p α₀₁)(γ₁₂ ,p α₁₂) → transC Γ γ₀₁ γ₁₂ ,p transT A α₀₁ α₁₂ }
  }

_[_]T : {Γ Δ : Con} → Ty Δ → Sub Γ Δ → Ty Γ
A [ σ ]T = record
  { ∣_∣T_  = λ γ → ∣ A ∣T (∣ σ ∣s γ)
  ; _~T    = λ γ₀₁ α₀ α₁ → (A ~T) ((σ ~s) γ₀₁) α₀ α₁
  ; refT   = refT A
  ; transT = transT A
--  ; coeT    = λ p → coeT A (~s σ p)
--  ; cohT    = λ p → cohT A (~s σ p)
  }

id : {Γ : Con} → Sub Γ Γ
id = record
  { ∣_∣s = λ γ → γ
  ; _~s  = λ γ₀₁ → γ₀₁
  }

_∘_ : ∀{Γ Θ Δ} → Sub Θ Δ → Sub Γ Θ → Sub Γ Δ
σ ∘ ν = record
  { ∣_∣s = λ γ → ∣ σ ∣s (∣ ν ∣s γ)
  ; _~s  = λ γ₀₁ → (σ ~s) ((ν ~s) γ₀₁)
  }

ε : {Γ : Con} → Sub Γ •
ε = record
  { ∣_∣s = λ _ → tt
  }

_,_ : {Γ Δ : Con}(σ : Sub Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▷ A)
σ , t = record { ∣_∣s = λ γ → ∣ σ ∣s γ ,Σ ∣ t ∣t γ ; _~s = λ γ₀₁ → (σ ~s) γ₀₁ ,p (t ~t) γ₀₁ }

π₁ : {Γ Δ : Con}{A : Ty Δ} → Sub Γ (Δ ▷ A) → Sub Γ Δ
π₁ σ = record { ∣_∣s = λ γ → proj₁ (∣ σ ∣s γ) ; _~s = λ γ₀₁ → proj₁p ((σ ~s) γ₀₁) }

_[_]t : {Γ Δ : Con}{A : Ty Δ} → Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
t [ σ ]t = record { ∣_∣t = λ γ → ∣ t ∣t (∣ σ ∣s γ) ; _~t = λ γ₀₁ → (t ~t) ((σ ~s) γ₀₁) }

π₂ : {Γ Δ : Con}{A : Ty Δ}(σ : Sub Γ (Δ ▷ A)) → Tm Γ (A [ π₁ {A = A} σ ]T)
π₂ σ = record { ∣_∣t = λ γ → proj₂ (∣ σ ∣s γ) ; _~t = λ γ₀₁ → proj₂p ((σ ~s) γ₀₁) }

[id]T : ∀{Γ}{A : Ty Γ}  → A [ id ]T ≡ A
[][]T : ∀{Γ Δ Σ}{A : Ty Σ}{σ : Sub Γ Δ}{δ : Sub Δ Σ} → (A [ δ ]T [ σ ]T) ≡ (A [ δ ∘ σ ]T)
idl   : ∀{Γ Δ}{δ : Sub Γ Δ} → (id ∘ δ) ≡ δ
idr   : ∀{Γ Δ}{δ : Sub Γ Δ} → (δ ∘ id) ≡ δ
ass   : ∀{Γ Δ Σ Ω}{σ : Sub Σ Ω}{δ : Sub Δ Σ}{ν : Sub Γ Δ} → ((σ ∘ δ) ∘ ν) ≡ (σ ∘ (δ ∘ ν))
,∘    : ∀{Γ Δ Σ}{δ : Sub Γ Δ}{σ : Sub Σ Γ}{A : Ty Δ} {a : Tm Γ (A [ δ ]T)} → (_,_ δ {A = A} a) ∘ σ ≡ _,_ (δ ∘ σ) {A = A} (a [ σ ]t)
π₁β   : ∀{Γ Δ}{A : Ty Δ} {δ : Sub Γ Δ}{a : Tm Γ (A [ δ ]T)} → (π₁ {A = A}(_,_ δ {A = A} a)) ≡ δ
πη    : ∀{Γ Δ}{A : Ty Δ} {δ : Sub Γ (Δ ▷ A)} → (_,_ (π₁ {A = A} δ) {A = A} (π₂ {A = A} δ)) ≡ δ
εη    : ∀{Γ}{σ : Sub Γ •} → σ ≡ ε
π₂β   : ∀{Γ Δ}{A : Ty Δ} {δ : Sub Γ Δ}{a : Tm Γ (A [ δ ]T)} → π₂ {A = A}(_,_ δ {A = A} a) ≡ a

[id]T {A = A} = refl
[][]T {A = A} = refl
idl   = refl
idr   = refl
ass   = refl
,∘ = refl
π₁β  = refl
πη   = refl
εη    = refl
π₂β  = refl

wk : ∀{Γ}{A : Ty Γ} → Sub (Γ ▷ A) Γ
wk {A = A} = π₁ {A = A} id

vz : ∀{Γ}{A : Ty Γ} → Tm (Γ ▷ A) (A [ wk {A = A} ]T)
vz {A = A} = π₂ {A = A} id

vs : ∀{Γ}{A : Ty Γ}{B : Ty Γ} → Tm Γ A → Tm (Γ ▷ B) (A [ wk {A = B} ]T) 
vs {B = B} x = x [ wk {A = B} ]t

<_> : ∀{Γ}{A : Ty Γ} → Tm Γ A → Sub Γ (Γ ▷ A)
<_> {Γ}{A} t = _,_ id {A = A} t

infix 4 <_>

_^_ : {Γ Δ : Con}(σ : Sub Γ Δ)(A : Ty Δ) → Sub (Γ ▷ A [ σ ]T) (Δ ▷ A)
_^_ {Γ}{Δ} σ A = _,_ (σ ∘ wk {A = A [ σ ]T}) {A = A} (vz {A = A [ σ ]T})

infixl 5 _^_
