{-# OPTIONS --prop #-}

module Preorder.Decl where

open import lib
open import Agda.Primitive
open import JM
open import Setoid.SProp.lib

record Con : Set₁ where
  field
    ∣_∣C : Set
    _~C : ∣_∣C → ∣_∣C → Prop
    refC : ∀ γ → _~C γ γ
    transC : {γ₀ γ₁ γ₂ : ∣_∣C} → _~C γ₀ γ₁ → _~C γ₁ γ₂ → _~C γ₀ γ₂
  infix 4 ∣_∣C
open Con public

record Sub (Γ Δ : Con) : Set where
  field
    ∣_∣s : ∣ Γ ∣C → ∣ Δ ∣C
    _~s  : {γ₀ γ₁ : ∣ Γ ∣C} → (Γ ~C) γ₀ γ₁ → (Δ ~C) (∣_∣s γ₀) (∣_∣s γ₁)
  infix 4 ∣_∣s
open Sub public

record Ty (Γ : Con) : Set₁ where
  field
    ∣_∣T_  : ∣ Γ ∣C → Set
    _~T    : {γ₀ γ₁ : ∣ Γ ∣C}(γ₀₁ : (Γ ~C) γ₀ γ₁) → ∣_∣T_ γ₀ → ∣_∣T_ γ₁ → Prop
    refT   : {γ : ∣ Γ ∣C}(α : ∣_∣T_ γ) → _~T (refC Γ γ) α α
    transT : {γ₀ γ₁ γ₂ : ∣ Γ ∣C}{γ₀₁ : (Γ ~C) γ₀ γ₁}{γ₁₂ : (Γ ~C) γ₁ γ₂}
             {α₀ : ∣_∣T_ γ₀}{α₁ : ∣_∣T_ γ₁}{α₂ : ∣_∣T_ γ₂}
            → _~T γ₀₁ α₀ α₁ → _~T γ₁₂ α₁ α₂ → _~T (transC Γ γ₀₁ γ₁₂) α₀ α₂
--    coeT    : {γ γ' : ∣ Γ ∣C} → Γ C γ ~ γ' → ∣_∣T_ γ → ∣_∣T_ γ'
--    cohT    : {γ γ' : ∣ Γ ∣C}(p : Γ C γ ~ γ')(α : ∣_∣T_ γ) → _T_⊢_~_ p α (coeT p α)
  infix 4 ∣_∣T_
open Ty public

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    ∣_∣t : (γ : ∣ Γ ∣C) → ∣ A ∣T γ
    _~t  : {γ₀ γ₁ : ∣ Γ ∣C}(γ₀₁ : (Γ ~C) γ₀ γ₁) → (A ~T) γ₀₁ (∣_∣t γ₀) (∣_∣t γ₁)
  infix 4 ∣_∣t
open Tm public
