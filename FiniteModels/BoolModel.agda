module FiniteModels.BoolModel where

open import lib

open import TT.Decl
open import TT.Core
open import TT.Func
open import TT.Sigma

infixl 5 _,_
infixl 5 _,s_
infixl 8 _[_]t

i : Bool → Set
i true = ⊤
i false = ⊥

d : Decl
d = decl
  Bool
  (λ Γ → i Γ → Bool)
  (λ Γ Δ → i Γ → i Δ)
  λ Γ A → (γ : i Γ) → i (A γ)

open Decl d
open import TT.Decl.Congr.NoJM d

module Comprehension where

  -- 'logical ∧"
  _,_ : (Γ : Bool) → (i Γ → Bool) → Bool
  true , A = A tt
  false , A = false

  _,s_ : {Γ Δ : Bool}(σ : i Γ → i Δ){A : i Δ → Bool} → ((γ : i Γ) → i (A (σ γ))) → i Γ → i (Δ , A)
  _,s_ {Γ}{true} σ t γ = t γ
  _,s_ {Γ}{false} σ t γ = σ γ

  π₁ : {Γ Δ : Bool}{A : i Δ → Bool} → (i Γ → i (Δ , A)) → i Γ → i Δ
  π₁ {Γ}{true} σ γ = tt
  π₁ {Γ}{false} σ γ = σ γ

  π₂ : {Γ Δ : Bool}{A : i Δ → Bool}(σ : i Γ → i (Δ , A))(γ : i Γ) → i (A (π₁ σ γ))
  π₂ {Γ}{true} σ γ = σ γ
  π₂ {Γ}{false} σ γ = ⊥-elim (σ γ)

  π₁β : {Γ Δ : Bool}{A : i Δ → Bool}{δ : i Γ → i Δ}{a : (γ : i Γ) → i (A (δ γ))} →
        π₁ (_,s_ δ {A} a) ≡ δ
  π₁β {Γ}{true} = refl
  π₁β {Γ}{false} = refl

  πη : {Γ Δ : Bool}{A : i Δ → Bool}{δ : i Γ → i (Δ , A)} → π₁ {A = A} δ ,s π₂ {A = A} δ ≡ δ
  πη {Γ}{true} = refl
  πη {Γ}{false} = refl

c1 : Core1 d
c1 = record
  { • = true
  ; _,_ = Comprehension._,_
  ; _[_]T = λ A σ γ → A (σ γ)
  ; id = λ γ → γ
  ; _∘_ = λ σ δ γ → σ (δ γ)
  ; ε = λ _ → tt
  ; _,s_ = Comprehension._,s_
  ; π₁ = Comprehension.π₁
  ; _[_]t = λ t σ γ → t (σ γ)
  ; π₂ = λ {Γ}{Δ}{A} σ → Comprehension.π₂ {Γ}{Δ}{A} σ
  }

open Core1 c1

π₂β : {Γ Δ : Bool}{A : i Δ → Bool}{δ : i Γ → i Δ}{a : (γ : i Γ) → i (A (δ γ))} →
  π₂ {A = A}(_,s_ δ {A} a) ≡[ TmΓ= ([]T=′ d c1 {Γ} refl {Δ} refl {A} refl {Comprehension.π₁ (_,s_ δ {A} a)}{δ} Comprehension.π₁β) ]≡ a
π₂β {Γ} {true} = refl
π₂β {Γ} {false} = {!!}

c : Core d
c = record {c1 = c1 ; c2 = record
  { [id]T = refl
  ; [][]T = refl
  ; idl = refl
  ; idr = refl
  ; ass = refl
  ; ,∘ = {!!}
  ; π₁β = Comprehension.π₁β
  ; πη = λ {Γ}{Δ} → Comprehension.πη {Γ}{Δ}
  ; εη = refl
  ; π₂β = {!!}
  } }

{-
f : Func c
f = record
  { Π = λ A B γ → (α : A γ) → B (γ ,p α)
  ; Π[] = refl
  ; lam = λ t → liftp λ γ α → unliftp t (γ ,p α)
  ; app = λ t → liftp λ γ → unliftp t (proj₁p γ) (proj₂p γ)
  ; lam[] = refl
  ; Πβ = refl
  ; Πη = refl
  }

s : Sigma c
s = record
  { Σ' = λ A B γ → Σp (A γ) λ α → B (γ ,p α)
  ; Σ[] = refl
  ; _,Σ'_ = λ u v → liftp λ γ → unliftp u γ ,p unliftp v γ
  ; proj₁' = λ t → liftp λ γ → proj₁p (unliftp t γ)
  ; proj₂' = λ t → liftp λ γ → proj₂p (unliftp t γ)
  ; Σβ₁ = refl
  ; Σβ₂ = refl
  ; Ση = refl
  ; ,Σ[] = refl
  }
-}
