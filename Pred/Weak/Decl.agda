{-# OPTIONS --without-K #-}

module Pred.Weak.Decl where

open import lib

record Con : Set₁ where
  field
    ⌜_⌝ : Set
    _ᴾ  : Set
    π   : _ᴾ → ⌜_⌝
  infix 7 ⌜_⌝
open Con public

record Ty (Γ : Con) : Set₁ where
  field
    ⌜_⌝ : ⌜ Γ ⌝ → Set
    _ᴾ  : (γᴾ : Γ ᴾ)(α : ⌜_⌝ (π Γ γᴾ)) → Set
  infix 7 ⌜_⌝
open Ty public

record Tms (Γ Δ : Con) : Set where
  field
    ⌜_⌝ : ⌜ Γ ⌝ → ⌜ Δ ⌝
    _ᴾ  : Γ ᴾ → Δ ᴾ
    π   : (γᴾ : Γ Con.ᴾ) → ⌜_⌝ (π Γ γᴾ) ≡ π Δ (_ᴾ γᴾ)
  infix 7 ⌜_⌝
open Tms public

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    ⌜_⌝ : (γ : ⌜ Γ ⌝) → ⌜ A ⌝ γ
    _ᴾ  : (γᴾ : Γ ᴾ) → (A Ty.ᴾ) γᴾ (⌜_⌝ (π Γ γᴾ))
  infix 7 ⌜_⌝
open Tm public

Ty= : {Γ : Con}{⌜_⌝ : ⌜ Γ ⌝ → Set}
  {_ᴾ₀ _ᴾ₁ : (γᴾ : Γ ᴾ)(α : ⌜_⌝ (π Γ γᴾ)) → Set}
  (_ᴾ₀₁ : _≡_ _ᴾ₀ _ᴾ₁) →
  _≡_ {A = Ty Γ} (record { ⌜_⌝ = ⌜_⌝ ; _ᴾ = _ᴾ₀ }) (record { ⌜_⌝ = ⌜_⌝ ; _ᴾ = _ᴾ₁ })
Ty= refl = refl

Tms= : {Γ Δ : Con}{⌜_⌝ : ⌜ Γ ⌝ → ⌜ Δ ⌝}{_ᴾ : Γ ᴾ → Δ ᴾ}
  {π₀ π₁ : (γᴾ : Γ Con.ᴾ) → ⌜_⌝ (π Γ γᴾ) ≡ π Δ (_ᴾ γᴾ)}
  (π₀₁ : _≡_ π₀ π₁) →
  _≡_ {A = Tms Γ Δ} (record { ⌜_⌝ = ⌜_⌝ ; _ᴾ = _ᴾ ; π = π₀ }) (record { ⌜_⌝ = ⌜_⌝ ; _ᴾ = _ᴾ ; π = π₁ })
Tms= refl = refl

Tms=' : {Γ Δ : Con}{⌜_⌝ : ⌜ Γ ⌝ → ⌜ Δ ⌝}
  {_ᴾ₀ _ᴾ₁ : Γ ᴾ → Δ ᴾ}(_ᴾ₀₁ : _≡_ _ᴾ₀ _ᴾ₁)
  {π₀ : (γᴾ : Γ Con.ᴾ) → ⌜_⌝ (π Γ γᴾ) ≡ π Δ (_ᴾ₀ γᴾ)}
  {π₁ : (γᴾ : Γ Con.ᴾ) → ⌜_⌝ (π Γ γᴾ) ≡ π Δ (_ᴾ₁ γᴾ)}
  (π₀₁ : _≡_ (transport (λ _ᴾ → (γᴾ : Γ Con.ᴾ) → ⌜ π Γ γᴾ ⌝ ≡ π Δ (γᴾ ᴾ)) _ᴾ₀₁ π₀) π₁) →
  _≡_ {A = Tms Γ Δ} (record { ⌜_⌝ = ⌜_⌝ ; _ᴾ = _ᴾ₀ ; π = π₀ }) (record { ⌜_⌝ = ⌜_⌝ ; _ᴾ = _ᴾ₁ ; π = π₁ })
Tms=' refl refl = refl

Tms='' : {Γ Δ : Con}
  {⌜_⌝₀ ⌜_⌝₁ : ⌜ Γ ⌝ → ⌜ Δ ⌝}(⌜_⌝₀₁ : _≡_ ⌜_⌝₀ ⌜_⌝₁)
  {_ᴾ₀ _ᴾ₁ : Γ ᴾ → Δ ᴾ}(_ᴾ₀₁ : _≡_ _ᴾ₀ _ᴾ₁)
  {π₀ : (γᴾ : Γ Con.ᴾ) → ⌜_⌝₀ (π Γ γᴾ) ≡ π Δ (_ᴾ₀ γᴾ)}
  {π₁ : (γᴾ : Γ Con.ᴾ) → ⌜_⌝₁ (π Γ γᴾ) ≡ π Δ (_ᴾ₁ γᴾ)}
  (π₀₁ : _≡_ (coe (ap2 (λ ⌜_⌝ _ᴾ → (γᴾ : Γ Con.ᴾ) → ⌜ π Γ γᴾ ⌝ ≡ π Δ (γᴾ ᴾ)) ⌜_⌝₀₁ _ᴾ₀₁) π₀) π₁) →
  _≡_ {A = Tms Γ Δ} (record { ⌜_⌝ = ⌜_⌝₀ ; _ᴾ = _ᴾ₀ ; π = π₀ }) (record { ⌜_⌝ = ⌜_⌝₁ ; _ᴾ = _ᴾ₁ ; π = π₁ })
Tms='' refl refl refl = refl
