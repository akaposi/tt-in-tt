{-# OPTIONS --without-K #-}

module Pred.Indexed.Decl where

open import lib

record Con : Set₁ where
  field
    ⌜_⌝ : Set
    _ᴾ  : ⌜_⌝ → Set
  infix 7 ⌜_⌝
open Con public

record Ty (Γ : Con) : Set₁ where
  field
    ⌜_⌝ : ⌜ Γ ⌝ → Set
    _ᴾ  : {γ : Con.⌜ Γ ⌝}(γᴾ : (Γ ᴾ) γ)(α : ⌜_⌝ γ) → Set
  infix 7 ⌜_⌝
open Ty public

record Tms (Γ Δ : Con) : Set where
  field
    ⌜_⌝ : ⌜ Γ ⌝ → ⌜ Δ ⌝
    _ᴾ  : {γ : Con.⌜ Γ ⌝} →  (Γ ᴾ) γ → (Δ ᴾ) (⌜_⌝ γ)
  infix 7 ⌜_⌝
open Tms public

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    ⌜_⌝ : (γ : ⌜ Γ ⌝) → ⌜ A ⌝ γ
    _ᴾ  : {γ : Con.⌜ Γ ⌝}(γᴾ : (Γ ᴾ) γ) → (A Ty.ᴾ) γᴾ (⌜_⌝ γ)
  infix 7 ⌜_⌝
open Tm public
