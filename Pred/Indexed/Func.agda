{-# OPTIONS --without-K #-}

module Pred.Indexed.Func where

open import lib

open import Pred.Indexed.Decl
open import Pred.Indexed.Core

Π : {Γ : Con}(A : Ty Γ)(B : Ty (Γ , A)) → Ty Γ
⌜ Π A B ⌝ γ = (α : ⌜ A ⌝ γ) → ⌜ B ⌝ (γ ,Σ α)
(Π A B ᴾ) {γ} γᴾ f = {α : ⌜ A ⌝ γ}(αᴾ : (A ᴾ) γᴾ α) → (B ᴾ) (γᴾ ,Σ αᴾ) (f α)

lam : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm (Γ , A) B) → Tm Γ (Π A B)
⌜ lam t ⌝ γ α = ⌜ t ⌝ (γ ,Σ α)
(lam t ᴾ) γᴾ αᴾ = (t ᴾ) (γᴾ ,Σ αᴾ)

app : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}(t : Tm Γ (Π A B)) → Tm (Γ , A) B
⌜ app t ⌝ (γ ,Σ α)= ⌜ t ⌝ γ α
(app t ᴾ) (γᴾ ,Σ αᴾ) = (t ᴾ) γᴾ αᴾ

Πβ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm (Γ , A) B} → app (lam t) ≡ t
Πβ = refl

Πη : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{t : Tm Γ (Π A B)} → _≡_ {A = Tm Γ (Π A B)} (lam (app {A = A}{B = B} t)) t
Πη = refl

Π[] : {Γ : Con}{A : Ty Γ}{B : Ty (Γ , A)}{Θ : Con}{σ : Tms Θ Γ} →
  Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ (σ ∘ π₁ id) ,s π₂ id ]T)
Π[] = refl

lam[] :
  {Γ : Con}{Δ : Con}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B} →
  lam t [ σ ]t ≡ lam (t [ (σ ∘ π₁ id) ,s π₂ id ]t)
lam[] = refl
