{-# OPTIONS --rewriting #-}

module NegFunext.Syntactic where

open import lib
open import JM
open import TT.Decl
open import TT.Decl.Syntax
open import TT.Decl.Congr.NoJM syntaxDecl
open import TT.Core
open import TT.Core.Syntax
open import TT.Core.Congr.NoJM syntaxCore
open import TT.Core.Congr.JM syntaxCore
open import TT.Core.Laws.JM syntaxCore
open import TT.FuncNoEta
open import TT.Func.Syntax
open import TT.Sigma
open import TT.Sigma.Syntax
open import TT.Sigma.Congr.NoJM syntaxSigma
open import TT.Sigma.Congr.JM syntaxSigma
open import TT.Boolean
open import TT.Boolean.Syntax
open import TT.Empty
open import TT.Empty.Syntax

d : Decl
d = syntaxDecl

c : Core d
c = syntaxCore

coeBool' : {Γ₀ Γ₁ : Con}(Γ₂ : Γ₀ ≡ Γ₁) → coe (Ty= Γ₂) Bool' ≡ Bool'
coeBool' refl = refl

Π[]model
  : {Γ Δ : Con}{σ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}
  → Σ' (Π A B) Bool' [ σ ]T
  ≡ Σ' (Π (A [ σ ]T) (B [ σ ^ A ]T)) Bool'
Π[]model = Σ[] ◾ Σ'= refl Π[] (ap (coe (Ty= (,C= refl Π[]))) Bool[] ◾ coeBool' (Γ,C=  Π[]))

lam[]model
  : {Γ Δ : Con}{δ : Tms Γ Δ}{A : Ty Δ}{B : Ty (Δ , A)}{t : Tm (Δ , A) B}
  → (lam t ,Σ' coe (TmΓ= (Bool[] ⁻¹)) true') [ δ ]t
  ≡[ TmΓ= Π[]model ]≡
    (lam (t [ δ ^ A ]t) ,Σ' coe (TmΓ= (Bool[] ⁻¹)) true')
lam[]model {Γ}{Δ}{δ}{A}{B}{t}
  = from≃ ( uncoe (TmΓ= Π[]model) ⁻¹̃
          ◾̃ from≡ (Tm= refl Σ[]) (,Σ[] {u = lam t}{coe (TmΓ= (Bool[] ⁻¹)) true'}{_}{δ})
          ◾̃ ,Σ'= refl
                 (from≡ refl Π[])
                 (from≡ (Ty= (Γ,C= Π[])) (ap (coe (Ty= (Γ,C= Π[]))) Bool[] ◾ coeBool' (Γ,C= Π[])))
                 (from≡ (TmΓ= Π[]) lam[])
                 ( uncoe (TmΓ= ([][]T ◾ A[]T= <>∘ ◾ [][]T ⁻¹)) ⁻¹̃
                 ◾̃ (coe[]t' (Bool[] ⁻¹) ◾̃ (from≡ (TmΓ= Bool[]) true[] ◾̃ uncoe (TmΓ= (Bool[] ⁻¹))))))

f : FuncNoEta c
f = record
  { Π = λ A B → Σ' (Π A B) Bool'
  ; Π[] = Π[]model
  ; lam = λ t → lam t ,Σ' coe (TmΓ= (Bool[] ⁻¹)) true'
  ; app = λ t → app (proj₁' t)
  ; lam[] = lam[]model
  ; Πβ = ap app Σβ₁ ◾ Πβ
  }

b : Boolean c
b = syntaxBoolean

s : Sigma c
s = syntaxSigma

e : Empty c
e = syntaxEmpty

open import TT.RecFuncNoEtaBoolSigmaEmpty f b s e

postulate
  Funext : Ty •

p : Tm • (RecTy (Π Funext Void)) → Tm • Funext → Tm • Void
p t u = {!!} -- coe ? (proj₁' t $ u)
