{-# OPTIONS --without-K #-}

module Glob.Setoid.Decl where

open import lib

open import TT.Decl

-- declaration of the model

record Con : Set₁ where
  coinductive
  field
    ∣_∣C : Set
    homC : ∣_∣C → ∣_∣C → Con
  infix 3 ∣_∣C
open Con public

record RCon (Γ : Con) : Set where
  coinductive
  field
    ∣_∣RC : (γ : ∣ Γ ∣C) → ∣ homC Γ γ γ ∣C
    homRC : (γ γ' : ∣ Γ ∣C) → RCon (homC Γ γ γ')
  infix 3 ∣_∣RC
open RCon public

record Ty (Γ : Con) : Set₁ where
  coinductive
  field
    ∣_∣T : ∣ Γ ∣C → Set
    homT : {γ γ' : ∣ Γ ∣C} → ∣_∣T γ → ∣_∣T γ' → Ty (homC Γ γ γ')
  infix 3 ∣_∣T
open Ty public

record RTy {Γ : Con}(RΓ : RCon Γ)(A : Ty Γ) : Set where
  coinductive
  field
    ∣_∣RT : {γ : ∣ Γ ∣C}(α : ∣ A ∣T γ) → ∣ homT A α α ∣T (∣ RΓ ∣RC γ)
    homRT : {γ γ' : ∣ Γ ∣C}(α : ∣ A ∣T γ)(α' : ∣ A ∣T γ') → RTy (homRC RΓ γ γ') (homT A α α')

record Tms (Γ Δ : Con) : Set where
  coinductive
  field
    ∣_∣s : ∣ Γ ∣C → ∣ Δ ∣C
    homs : (γ γ' : ∣ Γ ∣C) → Tms (homC Γ γ γ') (homC Δ (∣_∣s γ) (∣_∣s γ'))
  infix 3 ∣_∣s
open Tms public

record Tm (Γ : Con)(A : Ty Γ) : Set where
  coinductive
  field
    ∣_∣t : (γ : ∣ Γ ∣C) → ∣ A ∣T γ
    homt : (γ γ' : ∣ Γ ∣C) → Tm (homC Γ γ γ') (homT A (∣_∣t γ) (∣_∣t γ'))
  infix 3 ∣_∣t
open Tm public

d : Decl
d = decl Con Ty Tms Tm
{-
-- equality constructors

record Con= (Γ Γ' : Con) : Set₁ where
  coinductive
  field
    ∣_∣C= : ∣ Γ ∣C ≡ ∣ Γ' ∣C
    homC= : (γ γ' : ∣ Γ ∣C) → Con= (homC Γ γ γ') (homC Γ' (coe ∣_∣C= γ) (coe ∣_∣C= γ'))
  infix 3 ∣_∣C=
open Con= public
postulate
  Con=ext : {Γ Γ' : Con} → Con= Γ Γ' → Γ ≡ Γ'

record Ty= {Γ : Con}(A A' : Ty Γ) : Set₁ where
  coinductive
  field
    ∣_∣T= : ∣ A ∣T ≡ ∣ A' ∣T
    homT= : {γ γ' : ∣ Γ ∣C}(α : ∣ A ∣T γ)(α' : ∣ A ∣T γ') →
      Ty= (homT A α α') (homT A' (transport (λ z → z γ) ∣_∣T= α) (transport (λ z → z γ') ∣_∣T= α'))
  infix 3 ∣_∣T=
open Ty= public
postulate
  Ty=ext : {Γ : Con}{A A' : Ty Γ} → Ty= A A' → A ≡ A'

record Tms= {Γ Δ : Con}(σ σ' : Tms Γ Δ) : Set where
  coinductive
  field
    ∣_∣s= : ∣ σ ∣s ≡ ∣ σ' ∣s
    homs= : {γ γ' : ∣ Γ ∣C} → Tms= (homs σ γ γ') (transport (λ z → Tms (homC Γ γ γ') (homC Δ (z γ) (z γ'))) (∣_∣s= ⁻¹) (homs σ' γ γ'))
  infix 3 ∣_∣s=
open Tms= public
postulate
  Tms=ext : {Γ Δ : Con}{σ σ' : Tms Γ Δ} → Tms= σ σ' → σ ≡ σ'

record Tm= {Γ : Con}{A : Ty Γ}(t t' : Tm Γ A) : Set where
  coinductive
  field
    ∣_∣t= : ∣ t ∣t ≡ ∣ t' ∣t
    homt= : {γ γ' : ∣ Γ ∣C} → Tm= (homt t γ γ') (transport (λ z → Tm (homC Γ γ γ') (homT A (z γ) (z γ'))) (∣_∣t= ⁻¹) (homt t' γ γ'))
  infix 3 ∣_∣t=
open Tm= public
postulate
  Tm=ext : {Γ : Con}{A : Ty Γ}{t t' : Tm Γ A} → Tm= t t' → t ≡ t'

-- congruence rules

import TT.Decl.Congr.NoJM

∣coe∣t : {Γ : Con}{A₀ A₁ : Ty Γ}(A₂ : A₀ ≡ A₁){t : Tm Γ A₀}{γ : ∣ Γ ∣C} →
  ∣ coe (TT.Decl.Congr.NoJM.TmΓ= d A₂) t ∣t γ ≡ transport (λ z → ∣ z ∣T γ) A₂ (∣ t ∣t γ)
∣coe∣t refl = refl

postulate -- this is not usable because copattern matching is not the same as records
  ∣Ty=ext∣T : {Γ : Con}{A A' : Ty Γ}
    (∣_∣T= : ∣ A ∣T ≡ ∣ A' ∣T)
    (homT= : {γ γ' : ∣ Γ ∣C}(α : ∣ A ∣T γ)(α' : ∣ A ∣T γ') → Ty= (homT A α α') (homT A' (transport (λ z → z γ) ∣_∣T= α) (transport (λ z → z γ') ∣_∣T= α'))) →
    ap ∣_∣T (Ty=ext {Γ}{A}{A'}(record { ∣_∣T= = ∣_∣T= ; homT= = homT= })) ≡ ∣_∣T=
-}
