{-# OPTIONS --without-K --rewriting #-}

module Glob.Core where

open import lib

open import Glob.Decl

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]t

-- Core

∙ : Con
∣ ∙ ∣C = ⊤
homC ∙ _ _ = ∙

_▷_ : (Γ : Con)(A : Ty Γ) → Con
∣ Γ ▷ A ∣C = Σ ∣ Γ ∣C ∣ A ∣T
homC (Γ ▷ A) (γ ,Σ α) (γ' ,Σ α') = homC Γ γ γ' ▷ homT A α α'

_[_]T : {Γ Δ : Con} → Ty Δ → Tms Γ Δ → Ty Γ
∣ A [ σ ]T ∣T γ = ∣ A ∣T (∣ σ ∣s γ )
homT (A [ σ ]T) {γ} {γ'} α α' = homT A α α' [ homs σ γ γ' ]T

id : {Γ : Con} → Tms Γ Γ
∣ id ∣s γ = γ
homs (id {Γ}) γ γ' = id {homC Γ γ γ'}

{-# TERMINATING #-}
_∘_ : {Γ Θ Δ : Con} → Tms Θ Δ → Tms Γ Θ → Tms Γ Δ
∣ σ ∘ δ ∣s γ = ∣ σ ∣s (∣ δ ∣s γ)
homs (σ ∘ δ) γ γ' = homs σ (∣ δ ∣s γ) (∣ δ ∣s γ') ∘ homs δ γ γ'

ε : {Γ : Con} → Tms Γ ∙
∣ ε ∣s _ = tt
homs (ε {Γ}) γ γ' = ε {homC Γ γ γ'}

{-# TERMINATING #-}
_,_ : {Γ Δ : Con}(σ : Tms Γ Δ){A : Ty Δ} → Tm Γ (A [ σ ]T) → Tms Γ (Δ ▷ A)
∣ σ , t ∣s γ = ∣ σ ∣s γ ,Σ ∣ t ∣t γ
homs (σ , t) γ γ' = homs σ γ γ' , homt t γ γ'

{-# TERMINATING #-}
π₁ : {Γ Δ : Con}{A : Ty Δ} → Tms Γ (Δ ▷ A) → Tms Γ Δ
∣ π₁ σ ∣s γ = proj₁ (∣ σ ∣s γ)
homs (π₁ σ) γ γ' = π₁ (homs σ γ γ')

_[_]t : {Γ Δ : Con} {A : Ty Δ} → Tm Δ A → (σ : Tms Γ Δ) → Tm Γ (A [ σ ]T)
∣ t [ σ ]t ∣t γ = ∣ t ∣t (∣ σ ∣s γ)
homt (t [ σ ]t) γ γ' = homt t (∣ σ ∣s γ) (∣ σ ∣s γ') [ homs σ γ γ' ]t

π₂ : {Γ Δ : Con} {A : Ty Δ} (σ : Tms Γ (Δ ▷ A)) → Tm Γ (A [ π₁ σ ]T)
∣ π₂ σ ∣t γ = proj₂ (∣ σ ∣s γ)
homt (π₂ σ) γ γ' = π₂ (homs σ γ γ')

-- Core equalities

[id]T : {Γ : Con}{A : Ty Γ} → Ty= (A [ id ]T) A
∣ [id]T ∣T= = refl
homT= [id]T α α' = [id]T

[id]T' : {Γ : Con}{A : Ty Γ} → A [ id ]T ≡ A
[id]T' = Ty=ext [id]T

[][]T : {Γ Θ Δ : Con}{A : Ty Δ}{δ : Tms Γ Θ}{σ : Tms Θ Δ} → Ty= (A [ σ ]T [ δ ]T) (A [ σ ∘ δ ]T)
∣ [][]T ∣T= = refl
homT= [][]T α α' = [][]T

[][]T' : {Γ Θ Δ : Con}{A : Ty Δ}{δ : Tms Γ Θ}{σ : Tms Θ Δ} → (A [ σ ]T [ δ ]T) ≡ (A [ σ ∘ δ ]T)
[][]T' = Ty=ext [][]T

{-# REWRITE [][]T' #-}

idl : {Γ Δ : Con}{δ : Tms Γ Δ} → Tms= (id ∘ δ) δ
∣ idl ∣s= = refl
homs= idl = idl

idr : {Γ Δ : Con}{δ : Tms Γ Δ} → Tms= (δ ∘ id) δ
∣ idr ∣s= = refl
homs= idr = idr

ass : {Γ Δ Σ Ω : Con}{σ : Tms Σ Ω}{δ : Tms Δ Σ}{ν : Tms Γ Δ} → Tms= ((σ ∘ δ) ∘ ν) (σ ∘ (δ ∘ ν))
∣ ass ∣s= = refl
homs= ass = ass

π₁β : {Γ Δ : Con} {A : Ty Δ} {δ : Tms Γ Δ} {a : Tm Γ (A [ δ ]T)} → Tms= (π₁ {A = A} (δ , a)) δ
∣ π₁β ∣s= = refl
homs= π₁β = π₁β

π₁β' : {Γ Δ : Con} {A : Ty Δ} {δ : Tms Γ Δ} {a : Tm Γ (A [ δ ]T)} → π₁ {A = A} (δ , a) ≡ δ
π₁β' = Tms=ext π₁β

{-# REWRITE π₁β' #-}

πη : {Γ Δ : Con}{A : Ty Δ} {δ : Tms Γ (Δ ▷ A)} → Tms= (π₁ δ , π₂ δ) δ
∣ πη ∣s= = refl
homs= πη = πη

εη : {Γ : Con} {σ : Tms Γ ∙} → Tms= σ ε
∣ εη ∣s= = refl
homs= εη = εη

import TT.Decl.Congr.NoJM

,∘ : {Γ Δ Σ : Con} {δ : Tms Γ Δ} {σ : Tms Σ Γ} {A : Ty Δ} {a : Tm Γ (A [ δ ]T)} →
  Tms= ((_,_ δ {A} a) ∘ σ) (δ ∘ σ ,  (a [ σ ]t))
∣ ,∘ ∣s= = refl
homs= ,∘ = ,∘

open import TT.Core

c1 : Core1 d
c1 = record
  { • = ∙
  ; _,_ = _▷_
  ; _[_]T = _[_]T
  ; id = id
  ; _∘_ = _∘_
  ; ε = ε
  ; _,s_ = _,_
  ; π₁ = π₁
  ; _[_]t = _[_]t
  ; π₂ = π₂
  }

π₂β : {Γ Δ : Con} {A : Ty Δ} {δ : Tms Γ Δ} {a : Tm Γ (A [ δ ]T)} →
  Tm= (π₂ {A = A} (δ , a)) a
∣ π₂β ∣t= = refl
homt= π₂β = π₂β

c2 : Core2 d c1
c2 = record
  { [id]T = [id]T'
  ; [][]T = refl -- Ty=ext [][]T
  ; idl = Tms=ext idl
  ; idr = Tms=ext idr
  ; ass = Tms=ext ass
  ; ,∘ = Tms=ext ,∘
  ; π₁β = refl -- Tms=ext π₁β
  ; πη = Tms=ext πη
  ; εη = Tms=ext εη
  ; π₂β = Tm=ext π₂β
  }

c : Core d
c = record { c1 = c1 ; c2 = c2 }

{-# REWRITE [id]T' #-}
