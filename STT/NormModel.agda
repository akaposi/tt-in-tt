{-# OPTIONS --no-eta --rewriting #-}

module STT.NormModel where

open import Level
open import lib

open import STT.Syntax
open import STT.Elim
open import STT.Nf
open import STT.NormMethods

M : DModel {Level.suc Level.zero}{Level.zero}
M = record
      { Tyᴹ    = Tyᴹ
      ; Conᴹ   = Conᴹ
      ; Tmsᴹ   = Tmsᴹ
      ; Tmᴹ    = Tmᴹ

      ; ιᴹ     = ιᴹ
      ; _⇒ᴹ_   = _⇒ᴹ_

      ; ∙ᴹ     = ∙ᴹ
      ; _,ᴹ_   = _,ᴹ_

      ; idᴹ    = idᴹ

      ; _∘ᴹ_   = λ {a}{b}{c}{d}{e}{f} → _∘ᴹ_ {a}{b}{c}{d}{e}{f}

      ; εᴹ     = εᴹ

      ; _,ₛᴹ_  = λ {a}{b}{c}{d}{e} → _,ₛᴹ_ {a}{b}{c}{d}{e}

      ; π₁ᴹ    = λ {a}{b}{c}{d}{e}{f} → π₁ᴹ {a}{b}{c}{d}{e}{f}

      ; _[_]ᴹ  = λ {a}{b}{c}{d}{e}{f} → _[_]ᴹ {a}{b}{c}{d}{e}{f}

      ; π₂ᴹ    = λ {a}{b}{c}{d}{e}{f} → π₂ᴹ {a}{b}{c}{d}{e}{f}

      ; [id]ᴹ  = [id]ᴹ

      ; [][]ᴹ  = {!!}

      ; idlᴹ   = {!!}

      ; idrᴹ   = {!!}

      ; assᴹ   = {!!}

      ; ,∘ᴹ    = {!+!}

      ; ,β₁ᴹ   = {!!}

      ; ,ηᴹ    = {!!}

      ; ∙ηᴹ    = refl

      ; ,β₂ᴹ   = {!!}

      ; lamᴹ   = λ {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t} tᴹ Δ σ σᴹ u uᴹ
                 → coe (ap Bᴹ ((
                    lam t [ σ ] $ u                                    ≡⟨ ap (_$ u) lam[] ⟩
                    lam (t [ σ ^ A ]) $ u                              ≡⟨ ap (_[ id ,ₛ u ]) ⇒β ⟩
                    t [ σ ^ A ] [ id ,ₛ u ]                            ≡⟨ [][] ⟩
                    ap (t [_]) (
                      (σ ∘ π₁ id ,ₛ π₂ id) ∘ (id ,ₛ u)             ≡⟨ ,∘ ⟩
                      (σ ∘ π₁ id) ∘ (id ,ₛ u) ,ₛ π₂ id [ id ,ₛ u ] ≡⟨ ap (_,ₛ π₂ id [ id ,ₛ u ]) ass ⟩
                      {!!}
                                   -- π₁ id ∘ (id ,ₛ u) ≡ id
                                   -- π₂ id [ id ,ₛ u ] ≡ u
                      )

                   ) ⁻¹))
                   (tᴹ _ (σ ,ₛ u) (coe (ap Γᴹ (,β₁ ⁻¹)) σᴹ ,Σ coe (ap Aᴹ (,β₂ ⁻¹)) uᴹ))

      ; appᴹ   = λ { {Γ}{Γᴹ}{A}{Aᴹ}{B}{Bᴹ}{t} tᴹ δ σ (π₁σᴹ ,Σ π₂σᴹ)
                 → coe (ap Bᴹ {!!}) (tᴹ _ (π₁ σ) π₁σᴹ (π₂ σ) π₂σᴹ) -- ?
                 }

      ; lam[]ᴹ = {!!}

      ; ⇒βᴹ    = {!!}

      ; ⇒ηᴹ    = {!!}
      }
