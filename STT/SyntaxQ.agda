{-# OPTIONS --without-K #-}

module STT.Presyntax where

open import lib

infixl 5 _▷_
infixl 4 _⇒_
infixl 5 _,_
infix  6 _∘_
infixl 8 _[_]
infix  3 _~Sub_
infix  3 _~Tm_

data Ty : Set where
  ι   : Ty
  _⇒_ : Ty → Ty → Ty

data Con : Set where
  ∙   : Con
  _▷_ : Con → Ty → Con

data Sub' : Con → Con → Set
data Tm'  : Con → Ty  → Set

data Sub' where
  id  : ∀{Γ} → Sub' Γ Γ
  _∘_ : ∀{Γ Θ Δ} → Sub' Θ Δ → Sub' Γ Θ → Sub' Γ Δ
  ε   : ∀{Γ} → Sub' Γ ∙
  _,_ : ∀{Γ Δ}(δ : Sub' Γ Δ){A : Ty} → Tm' Γ A → Sub' Γ (Δ ▷ A)
  p   : ∀{Γ}{A : Ty} → Sub' (Γ ▷ A) Γ

data Tm' where
  _[_] : ∀{Γ Δ A} → Tm' Δ A → Sub' Γ Δ → Tm' Γ A
  q    : ∀{Γ A} → Tm' (Γ ▷ A) A
  lam  : ∀{Γ A B} → Tm' (Γ ▷ A) B → Tm' Γ (A ⇒ B)
  app  : ∀{Γ A B} → Tm' Γ (A ⇒ B) → Tm' (Γ ▷ A) B

data _~Sub_ : ∀{Γ Δ} → Sub' Γ Δ → Sub' Γ Δ → Set where
  idl : ∀{Γ Δ}{δ : Sub' Γ Δ} → id ∘ δ ~Sub δ 
  idr : ∀{Γ Δ}{δ : Sub' Γ Δ} → δ ∘ id ~Sub δ 
  ass : ∀{Γ Θ Ψ Δ}{σ : Sub' Ψ Δ}{δ : Sub' Θ Ψ}{ν : Sub' Γ Θ} → (σ ∘ δ) ∘ ν ~Sub σ ∘ (δ ∘ ν)
  ∙η  : ∀{Γ}{σ : Sub' Γ ∙} → σ ~Sub ε
  ▷β₁ : ∀{Γ Δ}{A : Ty}{σ : Sub' Γ Δ}{t : Tm' Γ A} → p ∘ (σ , t) ~Sub σ
  ▷η  : ∀{Γ Δ}{A : Ty}{σ : Sub' Γ (Δ ▷ A)} → p ∘ σ , q [ σ ] ~Sub σ
  ,∘  : ∀{Γ Θ Δ}{σ : Sub' Θ Δ}{δ : Sub' Γ Θ}{A : Ty}{t : Tm' Θ A} → (σ , t) ∘ δ ~Sub (σ ∘ δ) , t [ δ ]

data _~Tm_ : ∀{Γ A} → Tm' Γ A → Tm' Γ A → Set where
   [id]  : ∀{Γ A}{t : Tm' Γ A} → t [ id ] ~Tm t
   [][]  : ∀{Γ Θ Δ A}{t : Tm' Δ A}{σ : Sub' Θ Δ}{δ : Sub' Γ Θ} → t [ σ ] [ δ ] ~Tm t [ σ ∘ δ ]
   ▷β₂   : ∀{Γ Δ A}{σ : Sub' Γ Δ}{t : Tm' Γ A} → q [ σ , t ] ~Tm t
   ⇒β    : ∀{Γ A B}{t : Tm' (Γ ▷ A) B} → app (lam t) ~Tm t
   ⇒η    : ∀{Γ A B}{t : Tm' Γ (A ⇒ B)} → lam (app t) ~Tm t
   lam[] : ∀{Γ Δ}{σ : Sub' Γ Δ}{A B : Ty}{t : Tm' (Δ ▷ A) B} → (lam t) [ σ ] ~Tm lam (t [ σ ∘ p , q ])

Sub : Con → Con → Set
Sub Γ Δ = Sub' Γ Δ // _~Sub_

Tm : Con → Ty → Set
Tm Γ A = Tm' Γ A // _~Tm_

⟦_⟧Ty : Ty → Set
⟦ ι ⟧Ty = ⊤
⟦ A ⇒ B ⟧Ty = ⟦ A ⟧Ty → ⟦ B ⟧Ty

⟦_⟧Con : Con → Set
⟦ ∙ ⟧Con = ⊤
⟦ Γ ▷ A ⟧Con = ⟦ Γ ⟧Con × ⟦ A ⟧Ty

