{-# OPTIONS --no-eta #-}

module StandardModel.ExtIden {i} where

open import StandardModel.Decl {i}
open import StandardModel.Core {i}

open import lib
open import JM

open import TT.Decl
open import TT.Core
open import TT.ExtIden

open Decl d
open Core c

iden : ExtIden c
iden = record
  { Id      = λ ⟦t⟧ ⟦u⟧ γ → ⟦t⟧ γ ≡ ⟦u⟧ γ
  ; Id[]    = refl
  ; ref     = λ ⟦u⟧ γ → refl
  ; ref[]   = refl
  ; reflect = funext
  }

module _ where
  open ExtIden iden

  K : {Γ : Con}{A : Ty Γ}{u v : Tm Γ A}(t t' : Tm Γ (Id u v)) → Tm Γ (Id t t')
  K {Γ}{A}{u}{v} t t' γ = UIP _ _

-- Rk: the standard models seems to require UIP in the metatheory to interpret J

iden' : ExtIden c
iden' = record
  { Id      = λ ⟦t⟧ ⟦u⟧ γ → (⟦t⟧ γ ≡ ⟦u⟧ γ) × Bool
  ; Id[]    = refl
  ; ref     = λ ⟦u⟧ γ → refl ,Σ true
  ; ref[]   = refl
  ; reflect = λ f → funext λ γ → proj₁ (f γ)
  }

module _ where
  open ExtIden iden'

  open import TT.One
  open import StandardModel.One {i}

  open One o

  ¬K : ({Γ : Con}{A : Ty Γ}{u v : Tm Γ A}(t t' : Tm Γ (Id u v)) → Tm Γ (Id t t')) → ⊥
  ¬K f with loopcoe (ap (λ _ → Bool) (,Σ=0 (proj₁ (f (λ _ → refl ,Σ true) (λ _ → refl ,Σ false) (lift tt))))) ⁻¹ ◾
            ,Σ=1 (proj₁ (f {•}{Unit}{*}{*}(λ _ → refl ,Σ true)(λ _ → refl ,Σ false) _))
  ... | ()
