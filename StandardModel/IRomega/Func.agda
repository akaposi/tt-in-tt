module StandardModel.IRomega.Func where

open import lib

open import StandardModel.IRomega.Decl
open import StandardModel.IRomega.Core

Π : {Γ : Con}{i : ℕ}(A : Ty Γ i) → Ty (Γ ▷ A) i → Ty Γ i
Π A B γ = π (A γ) λ x → B (γ ,Σ x)

Π[] : {Γ Δ : Con} {σ : Tms Γ Δ}{i : ℕ}{A : Ty Δ i} {B : Ty (Δ ▷ A) i} →
  Π A B [ σ ]T ≡ Π (A [ σ ]T) (B [ _,_ (σ ∘ π₁ {A = A [ σ ]T} id) {A = A} (π₂ {A = A [ σ ]T} id) ]T)
Π[] = refl

lam : {Γ : Con} {i : ℕ} {A : Ty Γ i} {B : Ty (Γ ▷ A) i} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
lam t γ = {!!}

app : {Γ : Con} {i : ℕ} {A : Ty Γ i} {B : Ty (Γ ▷ A) i} → Tm Γ (Π A B) → Tm (Γ ▷ A) B
app = {!!}
