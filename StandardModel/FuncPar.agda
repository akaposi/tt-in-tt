{-# OPTIONS --without-K --no-eta #-}

module StandardModel.FuncPar {k} where

open import Agda.Primitive

open import StandardModel.Decl {k}
open import StandardModel.Core {k}

open import lib

open import TT.FuncPar

fp : FuncPar {k = k} c
fp = record
  { Π     = λ A B γ → (x : A) → B x γ
  ; Π[]   = refl
  ; lam   = λ t γ x → t x γ
  ; app   = λ t x γ → t γ x
  ; lam[] = refl
  ; Πβ    = refl
  ; Πη    = refl
  }
