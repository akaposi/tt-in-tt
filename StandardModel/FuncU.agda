{-# OPTIONS --without-K --no-eta #-}

module StandardModel.FuncU where

open import lib

open import StandardModel.Decl 
open import StandardModel.Core
open import StandardModel.Base Set Lift

open import TT.FuncU

f : FuncU b
f = record
  { Π     = λ ⟦A⟧ ⟦B⟧ γ → (x : ⟦A⟧ γ) → ⟦B⟧ (γ ,Σ lift x)
  ; Π[]   = refl
  ; lam   = λ ⟦t⟧ γ → lift (λ x → unlift (⟦t⟧ (γ ,Σ lift x)))
  ; app   = λ ⟦t⟧ γ → lift (unlift (⟦t⟧ (proj₁ γ)) (unlift (proj₂ γ)))
  ; lam[] = refl
  ; Πβ    = refl
  ; Πη    = refl
  }
