module StandardModel.One {i} where

open import Agda.Primitive

open import StandardModel.Decl {i}
open import StandardModel.Core {i}

open import lib
open import JM

open import TT.Decl
open import TT.Core
open import TT.One

open Decl d
open Core c

o : One c
o = record { Unit = λ _ → Lift ⊤ ; Unit[] = refl ; * = λ _ → lift tt ; *[] = refl }
