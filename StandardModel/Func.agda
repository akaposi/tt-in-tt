{-# OPTIONS --without-K --no-eta #-}

module StandardModel.Func {i} where

open import StandardModel.Decl {i}
open import StandardModel.Core {i}

open import lib
open import TT.Func

f : Func c
f = record
  { Π     = λ ⟦A⟧ ⟦B⟧ γ → (x : ⟦A⟧ γ) → ⟦B⟧ (γ ,Σ x)
  ; Π[]   = refl
  ; lam   = λ ⟦t⟧     γ → λ a → ⟦t⟧ (γ ,Σ a)
  ; app   = λ ⟦t⟧     γ → ⟦t⟧ (proj₁ γ) (proj₂ γ)
  ; lam[] = refl
  ; Πβ    = refl
  ; Πη    = refl
  }
