{-# OPTIONS --type-in-type #-}

module StandardModel.Setoid.Base where

open import Agda.Primitive
open import lib

open import TT.Core
open import TT.Base

open import StandardModel.Decl {lzero}
open import StandardModel.Core {lzero}

Prop = Set

b : Base c
b = record
  { U = λ _ → Prop
  ; U[] = refl
  ; El = λ p γ → p γ
  ; El[] = refl
  }
