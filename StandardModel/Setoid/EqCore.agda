{-# OPTIONS --type-in-type #-}

module StandardModel.Setoid.EqCore where

open import Agda.Primitive

open import lib
open import TT.Core
open import TT.Base
open import TT.Setoid.EqCore

open import StandardModel.Decl {lsuc lzero}
open import StandardModel.Core {lsuc lzero}
open import StandardModel.Setoid.Base

ec : EqCore b
ec = record { ec1 = record
  { _⁼ᶜ = λ Γ → Γ
  ; Pr0 = λ Γ γ → γ
  ; Pr1 = λ Γ γ → γ
  ; _[_]_~_ = λ A δ t0 t1 γ → t0 γ ≡ t1 γ
  ; ~[] = refl
  } ; ec2 = record
  { _⁼ᵗ = λ _ _ → refl
  ; _⁼ˢ = λ σ → σ
  ; Pr0⁼ = refl
  ; Pr1⁼ = refl
  ; •⁼ = refl
  ; Pr0• = refl
  ; Pr1• = refl
  ; prb = λ σ γ → proj₁ (σ γ)
  ; pr0 = λ σ γ → proj₂ (σ γ)
  ; pr1 = λ σ γ → proj₂ (σ γ)
  ; pr= = λ _ _ → refl
  ; prb∘ = refl
  ; pr0[] = refl
  ; pr1[] = refl
  ; pr=[] = refl
  ; ,⁼ = λ σ t0 t1 t= γ → σ γ ,Σ t0 γ
  ; Pr0, = refl
  ; Pr1, = refl
  ; []T⁼ = refl
  ; id⁼ = refl
  ; ∘⁼ = refl
  ; ε⁼ = refl
  ; π₁⁼ = refl
  } ; ec3 = record
  { ,s⁼ = refl
  ; []t⁼ = refl
  ; π₂⁼ = refl
  } }
