{-# OPTIONS --without-K --no-eta #-}

module StandardModel.Decl {i} where

open import TT.Decl

d : Decl
d = record
  { Con   =             Set i
  ; Ty    = λ ⟦Γ⟧     → ⟦Γ⟧ → Set i
  ; Tms   = λ ⟦Γ⟧ ⟦Δ⟧ → ⟦Γ⟧ → ⟦Δ⟧
  ; Tm    = λ ⟦Γ⟧ ⟦A⟧ → (γ : ⟦Γ⟧) → (⟦A⟧ γ)
  }
