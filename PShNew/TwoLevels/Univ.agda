{-# OPTIONS --without-K #-}

open import Agda.Primitive
open import PShNew.Cats

module PShNew.TwoLevels.Univ (C : Cat {lzero}{lzero}) where

open Cat C

open import lib

open import PShNew.TwoLevels.Decl C
open import PShNew.TwoLevels.Core C

open import TT.TwoLevels.Decl
open import TT.TwoLevels.Core
open import TT.TwoLevels.Univ

lift∘unlift : ∀{ℓ ℓ'}{A : Set ℓ}{a : Lift {ℓ}{ℓ'} A} → lift (unlift a) ≡ a
lift∘unlift = refl

injapunlift : ∀{ℓ ℓ'}{A : Set ℓ}{a b : A}{p q : lift {_}{ℓ'} a ≡ lift b}
            → ap unlift p ≡ ap unlift q → p ≡ q
injapunlift {p = p}{q} w = apid p ⁻¹ ◾ apap p ◾ ap (ap lift) w ◾ apap q ⁻¹ ◾ apid q

y : (Ψ : Obj) → Con
y Ψ = record
  { ∣_∣C    = λ Ω → Lift (Ω ⇒ Ψ)
  ; isSetC  = λ { {Ω}{lift α₀}{lift α₁} p q → injapunlift (isSet⇒ (ap unlift p) (ap unlift q)) }
  ; _∶_⟦_⟧C = λ { {Ω}(lift β){Ξ} γ → lift (β ∘c γ) }
  ; idC     = λ { {Ω}{lift β} → ap lift idrc }
  ; ∘C      = λ { {Ω}{lift β}{Ξ}{γ}{Σ}{δ} → ap lift (assc ⁻¹) }
  }

U : {Γ : Con {lsuc lzero}} → Ty {lsuc lzero}{lsuc lzero} Γ
U {Γ} = record
  { ∣_∣T    = λ {Ψ} _ → Ty {lsuc lzero}{lzero}(y Ψ) -- small types
  ; isSetT  = {!!}
  ; _∶_⟦_⟧T = λ {Ψ}{α} A {Ω} β → record
                { ∣_∣T    = {!λ {Ξ} γ → Ty {lsuc lzero}{lzero}(y Ξ)!}
                ; isSetT  = {!!}
                ; _∶_⟦_⟧T = {!!}
                ; idT     = {!!}
                ; ∘T      = {!!}
                }
  ; idT     = {!!}
  ; ∘T      = {!!}
  }

u : Univ c
u = record
  { U    = U
  ; U[]  = {!!}
  ; El   = {!!}
  ; El[] = {!!}
  }
