{-# OPTIONS --rewriting #-}

module nat where

open import lib hiding (ℕ; zero; suc)
open import JM using (UIP)

-- definition of natural numbers in the old and new style

------------------------------------------------------------------------------
-- old style
------------------------------------------------------------------------------

module oldFashioned where

  data ℕ : Set where
    zero : ℕ
    suc : ℕ → ℕ
    -- con : ∀ n → suc (suc n) ≡ n -- equality constructors are not allowed

  add : ℕ →  (ℕ → ℕ)
  add zero    n = n
  add (suc m) n = suc ((add m) n)

  p : add (suc (suc (suc zero))) (suc (suc (suc zero))) ≡ suc (suc (suc (suc (suc (suc zero)))))
  p = refl

------------------------------------------------------------------------------
-- new style
------------------------------------------------------------------------------

-- algebra

record Nat : Set₁ where
  field
    ℕ : Set
    zero : ℕ
    suc : ℕ → ℕ

-- example Nat-algebras:

two three : Nat
two = record { ℕ = Bool ; zero = true ; suc = if_then false else true }
three = record { ℕ = ⊤ ⊎ ⊤ ⊎ ⊤ ; zero = inl tt ; suc = suc }
  where
    suc : ⊤ ⊎ ⊤ ⊎ ⊤ → ⊤ ⊎ ⊤ ⊎ ⊤
    suc (inl tt) = inr (inl tt)
    suc (inr (inl tt)) = inr (inr tt)
    suc (inr (inr tt)) = inl tt

-- algebra morphism (homomorphism)

record Natᴹ (A B : Nat) : Set where
  private module A = Nat A
  private module B = Nat B
  field
    ℕ : A.ℕ → B.ℕ
    zero : ℕ A.zero ≡ B.zero
    suc : (n : A.ℕ) → ℕ (A.suc n) ≡ B.suc (ℕ n)

exMor : Natᴹ three two
exMor = record { ℕ = f ; zero = refl ; suc = {!!} }
  where
    f : Nat.ℕ three → Nat.ℕ two
    f (inl tt) = true
    f (inr (inl tt)) = false
    f (inr (inr tt)) = true
    suc : (n : Nat.ℕ three) → f (Nat.suc three n) ≡ Nat.suc two (f n)
    suc (inl tt) = refl
    suc (inr (inl tt)) = refl
    suc (inr (inr tt)) = {!!}

-- syntax (an algebra)

postulate
  S : Nat

module S = Nat S

-- the syntax is (weakly) initial (recursor)

module _ (A : Nat) where

  postulate
    r : Natᴹ S A

  module r = Natᴹ r

  {-# REWRITE r.zero #-}
  {-# REWRITE r.suc #-}

-- simpler example

A : Nat
A = record {
  ℕ    = S.ℕ ;
  zero = S.suc (S.suc S.zero) ;
  suc  = λ gn → S.suc (S.suc (S.suc gn))
  }

g : S.ℕ → S.ℕ
g = Natᴹ.ℕ (r A)

-- example

add : S.ℕ → S.ℕ → S.ℕ
add = r.ℕ record
  { ℕ    = S.ℕ → S.ℕ
  ; zero = λ n → n
  ; suc  = λ addm n → S.suc (addm n)
  }

p : add (S.suc (S.suc (S.suc S.zero))) (S.suc (S.suc (S.suc S.zero))) ≡ S.suc (S.suc (S.suc (S.suc (S.suc (S.suc S.zero)))))
p = refl

-- dependent algebra over S

record Natᴰ : Set₁ where
  field
    ℕ : S.ℕ → Set
    zero : ℕ S.zero
    suc : {n : S.ℕ} → ℕ n → ℕ (S.suc n)

-- dependent morphism from S

record Natᴰᴹ (P : Natᴰ) : Set where
  private module P = Natᴰ P
  field
    ℕ : (n : S.ℕ) → P.ℕ n
    zero : ℕ S.zero ≡ P.zero
    suc : (n : S.ℕ) → ℕ (S.suc n) ≡ P.suc (ℕ n)

-- dependent eliminator from uniqueness

module UD (u : {A : Nat}(f : Natᴹ S A) → r A ≡ f) where

  d : (P : Natᴰ) → Natᴰᴹ P -- dependent eliminator
  d P = record
    { ℕ    = λ n → coe (ap P.ℕ (ap (λ z → Natᴹ.ℕ z n) (u f' ⁻¹ ◾ u id))) (proj₂ (rM.ℕ n))
    ; zero = ap (λ z → coe z P.zero)
                (UIP (ap P.ℕ (ap (λ z → Natᴹ.ℕ z S.zero) (u f' ⁻¹ ◾ u id))) refl)
    ; suc  = λ n → ap (λ z → coe z (proj₂ (rM.ℕ (S.suc n)))) (UIP (ap P.ℕ (ap (λ z → Natᴹ.ℕ z (S.suc n)) (u f' ⁻¹ ◾ u id)))(ap P.ℕ (ap (λ x → S.suc (Natᴹ.ℕ x n)) (u f' ⁻¹ ◾ u id))))
                 ◾ ap (λ z → coe (ap P.ℕ z) (proj₂ (rM.ℕ (S.suc n))))
                      (apap (u f' ⁻¹ ◾ u id))
                 ◾ lem (ap (λ z → Natᴹ.ℕ z n) (u f' ⁻¹ ◾ u id))
    }
    where
      private module P = Natᴰ P

      lem : {n n' : S.ℕ}{p : P.ℕ n}(q : n ≡ n')
          → coe (ap P.ℕ (ap S.suc q)) (P.suc p) ≡ P.suc (coe (ap P.ℕ q) p)
      lem refl = refl

      M : Nat
      M = record
        { ℕ    = Σ S.ℕ P.ℕ
        ; zero = S.zero ,Σ P.zero
        ; suc  = λ { (n ,Σ p) → S.suc n ,Σ P.suc p }
        }

      private module rM = Natᴹ (r M)

      f' : Natᴹ S S
      f' = record
        { ℕ    = λ n → proj₁ (rM.ℕ n)
        ; zero = refl
        ; suc  = λ _ → refl
        }

      id : Natᴹ S S
      id = record { ℕ = λ n → n ; zero = refl ; suc = λ _ → refl }

-- uniqueness from dependent eliminator

-- equality constructor for Natᴹ
=Natᴹ : {A B : Nat} → let private module A = Nat A in let private module B = Nat B in
        {ℕ₀ ℕ₁ : A.ℕ → B.ℕ}(ℕ₂ : ℕ₀ ≡ ℕ₁)
        {zero₀ : ℕ₀ A.zero ≡ B.zero}{zero₁ : ℕ₁ A.zero ≡ B.zero}
        {suc₀ : (n : A.ℕ) → ℕ₀ (A.suc n) ≡ B.suc (ℕ₀ n)}{suc₁ : (n : A.ℕ) → ℕ₁ (A.suc n) ≡ B.suc (ℕ₁ n)}
      → _≡_ {A = Natᴹ A B}
            (record { ℕ = ℕ₀ ; zero = zero₀ ; suc = suc₀ })
            (record { ℕ = ℕ₁ ; zero = zero₁ ; suc = suc₁ })
=Natᴹ {A}{B}{ℕ} refl = ap2 (λ zero suc → record { ℕ = ℕ ; zero = zero ; suc = suc })
                           (UIP _ _)
                           (funext λ _ → UIP _ _)

module DU (d : (P : Natᴰ) → Natᴰᴹ P) where

  u : {A : Nat}(f : Natᴹ S A) → r A ≡ f -- uniqueness
  u {A} f = =Natᴹ (funext λ n → dP.ℕ n)
    where
      module f = Natᴹ f
      module A = Nat A

      P : Natᴰ
      P = record
        { ℕ    = λ n → Natᴹ.ℕ (r A) n ≡ Natᴹ.ℕ f n
        ; zero = f.zero ⁻¹
        ; suc  = λ {n} p → ap A.suc p ◾ f.suc n ⁻¹
        }

      module P = Natᴰ P
      module dP = Natᴰᴹ (d P)

------------------------------------------------------------------------------
-- case split without termination check
------------------------------------------------------------------------------

record Natᴰ' : Set₁ where
  field
    ℕ : S.ℕ → Set
    zero : ℕ S.zero
    suc : (n : S.ℕ) → ℕ (S.suc n)

module _ (M : Natᴰ') where
  private module M = Natᴰ' M

  postulate
    caseNat  : (n : S.ℕ) → M.ℕ n
    casezero : caseNat S.zero ≡ M.zero
    casesuc  : {n : S.ℕ} → caseNat (S.suc n) ≡ M.suc n

  {-# REWRITE casezero #-}
  {-# REWRITE casesuc #-}


not : Bool → Bool
not true = false
not false = true

{-# TERMINATING #-}
f : S.ℕ → Bool
f = caseNat (record { ℕ = λ _ → Bool ; zero = true ; suc = λ n → not (f n) })

p0 : f S.zero ≡ true
p0 = refl

p1 : f (S.suc S.zero) ≡ false
p1 = refl

p2 : f (S.suc (S.suc S.zero)) ≡ true
p2 = refl

p3 : f (S.suc (S.suc (S.suc S.zero))) ≡ false
p3 = refl

p4 : f (S.suc (S.suc (S.suc (S.suc S.zero)))) ≡ true
p4 = refl

{-# TERMINATING #-}
add' : S.ℕ → S.ℕ → S.ℕ
add' = caseNat (record { ℕ = λ _ → S.ℕ → S.ℕ ; zero = λ n → n ; suc = λ n m → S.suc (add' n m) })

q34 : add' (S.suc (S.suc (S.suc S.zero))) (S.suc (S.suc (S.suc (S.suc S.zero)))) ≡ S.suc (S.suc (S.suc (S.suc (S.suc (S.suc (S.suc S.zero))))))
q34 = refl

q30 : add' (S.suc (S.suc (S.suc S.zero))) S.zero ≡ S.suc (S.suc (S.suc S.zero))
q30 = refl

q33 : add' (S.suc (S.suc (S.suc S.zero))) (S.suc (S.suc (S.suc S.zero))) ≡ S.suc (S.suc (S.suc (S.suc (S.suc (S.suc S.zero)))))
q33 = refl
